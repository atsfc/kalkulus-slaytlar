\section{Multiple Integrals}
%
\subsection{Change of Variables in Multiple Integrals}
%
\begin{frame}{\insertsubsectionhead}
A change of variables can also be useful in double integrals. We have already seen one example of this: conversion to polar coordinates. The new variables $r$ and $\theta$ are related to the old variables $x$ and $y$ by the equations
\[ x=r\cos \theta \qquad y=r\sin \theta \]
and the change of variables formula can be written as
\[ \iint\limits_R f(x,y)dA=\iint\limits_S f(r\cos \theta,r\sin \theta)r dr d\theta \]
where $S$ is the region in the $r\theta$-plane that corresponds to the region $R$  $xy$-plane.
\end{frame}
%
\begin{frame}
\begin{block}{}
	More generally, we consider a change of variables that is given by a \textbf{transformation} $\bm{T}$ from the $uv$-plane to the $xy$-plane:
	\[ T(u,v)=(x,y) \]
	where $x$ and $y$ are related to $u$ and $v$ by the equations
	\begin{equation}\label{p902eq3}
	x=g(u,v)\qquad y=h(u,v)
	\end{equation}
	or, as we sometimes write,
	\[ x=x(u,v)\qquad y=y(u,v). \]
	We usually assume that $T$ is a \textbf{$\bm{C^1}$ transformation}, which means that $g$ and $h$ have continuous first-order partial derivatives.
\end{block}
\end{frame}
%
\begin{frame}
\begin{block}{}
	A transformation $T$ is really just a function whose domain and range are both subsets of $\mathds{R}^2$.
\end{block}
\begin{block}{}
	If $T(u_1,v_1)=(x_1,y_1)$, then the point $(x_1,y_1)$ is called the \textbf{image} of the point $(u_1,v_1)$.\medskip
	
	If no two points have the same image, $T$ is called \textbf{one-to-one}.
\end{block}
\end{frame}
%
\begin{frame}
\begin{block}{}
	$T$ transforms $S$ into a region $R$ in the $xy$-plane called the \textbf{image of $\bm{S}$}, image of $S$.
\end{block}
\begin{figure}[h]
	\includegraphics[page=902,clip,trim=81mm 80mm 28mm 160.5mm,scale=.95]{stewart-ccac.pdf}
\end{figure}
\begin{block}{}
	If $T$ is a one-to-one transformation, then it has an \textbf{inverse transformation $\bm{T^{-1}}$} from the $xy$-plane to the $uv$-plane and it may be possible to solve Equations \eqref{p902eq3} for $u$ and $v$ in terms of $x$ and $y$:
	\[ u=G(x,y)\qquad v=H(x,y). \]
\end{block}
\end{frame}
%
\begin{frame}
Now let's see how a change of variables affects a double integral. We start with a small rectangle $S$ in the $uv$-plane whose lower left corner is the point $(u_0,v_0)$ and whose dimensions are $\Delta u$ and $\Delta v$.
\begin{figure}[h]
	\includegraphics[page=903,clip,trim=76mm 81mm 24.5mm 158.5mm]{stewart-ccac.pdf}
\end{figure}
The image of $S$ is a region $R$ in the $xy$-plane, one of whose boundary points is $(x_0,y_0)=T(u_0,v_0)$.
\end{frame}
%
%\begin{frame}
%The vector
%\[\vec{\textbf{r}}(u,v)=g(u,v)\vec{\textbf{i}}+h(u,v)\vec{\textbf{j}}\]
%is the position vector of the image of the point $(u,v)$. The equation of the lower side of $S$ is $v=v_0$, whose image curve is given by the vector function $\vec{\textbf{r}}(u,v_0)$. The tangent vector at $(x_0,y_0)$ to this image curve is
%\[ \vec{\textbf{r}}_u=g_u(u_0,v_0)\vec{\textbf{i}}+h_u(u_0,v_0)\vec{\textbf{j}}=\frac{\partial x}{\partial u}\vec{\textbf{i}}+\frac{\partial y}{\partial u}\vec{\textbf{j}}. \]
%Similarly, the tangent vector at $(x_0,y_0)$ to the image curve of the left side of $S$ (namely, $u=u_0$) is
%\[ \vec{\textbf{r}}_v=g_v(u_0,v_0)\vec{\textbf{i}}+h_v(u_0,v_0)\vec{\textbf{j}}=\frac{\partial x}{\partial v}\vec{\textbf{i}}+\frac{\partial v}{\partial u}\vec{\textbf{j}}. \]
%\end{frame}
%
%\begin{frame}
%We can approximate the image region $R=T(S)$ by a parallelogram determined by the secant vectors
%\[ \vec{\textbf{a}}=\vec{\textbf{r}}(u_0+\Delta uiv_0)-\vec{\textbf{r}}(u_0,v_0)\qquad \vec{\textbf{b}}=\vec{\textbf{r}}(u_0,v_0+\Delta v)-\vec{\textbf{r}}(u_0,v_0) \]
%shown in Figure
%\begin{figure}[h]
%	\includegraphics[page=904,clip,trim=20.5mm 181mm 147.5mm 67.4mm]{stewart-ccac.pdf}
%\end{figure}
%But
%\[ \vec{\textbf{r}}_u=\lim_{\Delta u\rightarrow 0}\frac{\vec{\textbf{r}}(u_0+\Delta u,v_0)-\vec{\textbf{r}}(u_0,v_0)}{\Delta u} \]
%and so $\vec{\textbf{r}}(u_0+\Delta u,v_0)-\vec{\textbf{r}}(u_0,v_0) \approx \Delta u\vec{\textbf{r}}_u$. Similarly $\vec{\textbf{r}}(u_0,v_0+\Delta v)-\vec{\textbf{r}}(u_0,v_0) \approx \Delta v\vec{\textbf{r}}_v$.
%\end{frame}
%
%\begin{frame}
%This means that we can approximate $R$ by a parallelogram determined by the vectors $\Delta u\vec{\textbf{r}}_u$ and $\Delta v\vec{\textbf{r}}_v$.
%\begin{figure}[h]
%	\includegraphics[page=904,clip,trim=20.5mm 142mm 148mm 110mm]{stewart-ccac.pdf}
%\end{figure}
%Therefore, we can approximate the area of $R$ by the area of this parallelogram, which is
%\begin{equation}\label{p904eq6}
%|(\Delta u\vec{\textbf{r}}_u)\times (\Delta v\vec{\textbf{r}}_v)|=|\vec{\textbf{r}}_u\times \vec{\textbf{r}}_v|\Delta u\Delta v.
%\end{equation}
%\end{frame}
%
%\begin{frame}
%Computing the cross product, we obtain
%\[ \vec{\textbf{r}}_u\times \vec{\textbf{r}}_v=\begin{vmatrix}
%\vec{\textbf{i}}&\vec{\textbf{j}}&\vec{\textbf{k}}\\
%&&\\
%\displaystyle \frac{\partial x}{\partial u}&\displaystyle \frac{\partial y}{\partial u}&0\\
%&&\\
%\displaystyle \frac{\partial x}{\partial v}&\displaystyle \frac{\partial y}{\partial v}&0
%\end{vmatrix}=\begin{vmatrix}
%\displaystyle \frac{\partial x}{\partial u}&\displaystyle \frac{\partial y}{\partial u}\\
%&\\
%\displaystyle \frac{\partial x}{\partial v}&\displaystyle \frac{\partial y}{\partial v}
%\end{vmatrix}\vec{\textbf{k}}=\begin{vmatrix}
%\displaystyle \frac{\partial x}{\partial u}&\displaystyle \frac{\partial x}{\partial v}\\
%&\\
%\displaystyle \frac{\partial y}{\partial u}&\displaystyle \frac{\partial y}{\partial v}
%\end{vmatrix}\vec{\textbf{k}}. \]
%
%The determinant that arises in this calculation is called the \textit{Jacobian}  of the transformation and is given a special notation.
%\end{frame}
%
\begin{frame}
\begin{definition}
	The \textbf{Jacobian} of the transformation $T$ given by $x=g(u,v)$ and $y=h(u,v)$ is
	\begin{equation}\label{p904eq7}
	\frac{\partial (x,y)}{\partial (u,v)}=\left|\begin{array}{cc}
	\displaystyle \frac{\partial x}{\partial u}&\displaystyle \frac{\partial x}{\partial v}\\
	&\\
	\displaystyle \frac{\partial y}{\partial u}&\displaystyle \frac{\partial y}{\partial v}
	\end{array}\right|=\frac{\partial x}{\partial u}\frac{\partial y}{\partial v}-\frac{\partial x}{\partial v}\frac{\partial y}{\partial u}.
	\end{equation}
\end{definition}
%With this notation we can use Equation \eqref{p904eq6} to give an approximation to the area $\Delta A$ of $R$:
%\begin{equation}\label{p904eq8} 
%\Delta A\approx \left|\frac{\partial (x,y)}{\partial (u,v)}\right|\Delta u\Delta v
%\end{equation}
%where the Jacobian is evaluated at $(u_0,v_0)$.
\end{frame}
%
%\begin{frame}
%Next we divide a region $S$ in the $uv$-plane into rectangles $S_{ij}$ and call their images in the $xy$-plane $R_{ij}$.
%\begin{figure}[h]
%	\includegraphics[page=905,clip,trim=77mm 144mm 21mm 93mm]{stewart-ccac.pdf}
%\end{figure}
%\end{frame}
%%
%\begin{frame}
%Next we divide a region $S$ in the $uv$-plane into rectangles $S_{ij}$ and call their images in the $xy$-plane $R_{ij}$.  Applying the approximation \eqref{p904eq8} to each $R_{ij}$, we approximate the double integral of $f$ over $R$ as follows:
%\begin{align*}
%\iint\limits_Rf(x,y)dA& \approx \sum_{i=1}^{m}\sum_{j=1}^nf(x_i,y_j)\Delta A\\
%& \approx \sum_{i=1}^{m}\sum_{j=1}^nf(g(u_i,v_j),h(u_i,v_j))\left|\frac{\partial (x,y)}{\partial (u,v)}\right|\Delta u\Delta v
%\end{align*}
%where the Jacobian is evaluated at $(u_i,v_j)$. Notice that this double sum is a Riemann sum for the integral
%\[\iint\limits_Sf(g(u,v),h(u,v))\left|\frac{\partial (x,y)}{\partial (u,v)}\right| du dv. \]
%
%The foregoing argument suggests that the following theorem is true.
%\end{frame}
%
\begin{frame}
\begin{theorem}[Change of Variables in Multiple Integrals]
	Suppose that  $T$ is a one-to-one $C^1$ transformation whose Jacobian is nonzero and that maps a region $S$ in the $uv$-plane onto a region $R$ in the $xy$-plane. Suppose that $f$ is continuous on $R$ and that $R$ and $S$ are Type I or Type II plane regions. Then
	\begin{equation}\label{p905eq9}
	\iint\limits_Rf(x,y)dA=\iint\limits_Sf(x(u,v),y(u,v))\left|\frac{\partial (x,y)}{\partial (u,v)}\right|  du  dv.
	\end{equation}
\end{theorem}
\end{frame}
%
\begin{frame}
\begin{block}{}
	Theorem says that we change from an integral in $x$ and $y$ to an integral in $u$ and $v$ by expressing $x$ and $y$ in terms of $u$ and $v$ and writing
	\[ dA=\left|\frac{\partial (x,y)}{\partial (u,v)}\right| du dv. \]
\end{block}
\end{frame}
%
\begin{frame}
\begin{minipage}{.49\textwidth}
	\begin{figure}[h]
		\includegraphics[page=906,clip,trim=22mm 163.5mm 148mm 32mm]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\begin{minipage}{.5\textwidth}
	As a first illustration of the theorem, we show that the formula for integration in polar coordinates is just a special case. Here the transformation $T$ from the $r\theta$-plane
	to the $xy$-plane is given by
	\begin{align*}
	x&=g(r,\theta)=r\cos \theta\\
	y&=h(r,\theta)=r\sin \theta
	\end{align*}
	and the geometry of the transformation is shown in Figure.
\end{minipage}
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate the integral $ \displaystyle \iint_{R} e^{(x+y)/(x-y)} dA $, where $ R $ is the trapezoidal with vertices $ (1,0) $, $ (2,0) $, $ (0,-2) $ and $ (0,-1) $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	Since it isn't easy to integrate $ e^{(x+y)/(x-y)} $, we make a change of variables
suggested by the form of this function:
	\[ u = x+y \quad v=x-y. \]
	These equations define a transformation $ T^{-1} $ from the $ xy $-plane to the $ uv $-plane. Theorem talks about a transformation $ T $ from the $ uv $-plane to the $ xy $-plane. It is obtained by solving previous equation for $ x $ and $ y $:
	\[ x = \frac{1}{2}(u+v) \quad y= \frac{1}{2}(u-v). \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let \qed \relax
	The Jacobian of $ T $ is
	\[ \frac{\partial (x,y)}{\partial (u,v)} = \begin{vmatrix}
	\dfrac{\partial x}{\partial u} & \dfrac{\partial x}{\partial v}\\[.3cm]
	\dfrac{\partial y}{\partial u} & \dfrac{\partial y}{\partial v}
	\end{vmatrix} = \begin{vmatrix}
	\dfrac{1}{2} & \dfrac{1}{2}\\[.3cm]
	\dfrac{1}{2} & -\dfrac{1}{2}
	\end{vmatrix} = -\frac{1}{2}. \]
	To find the region in the $ uv $-plane corresponding to $ R $, we note that the sides of $ R $ lie on the lines
	\[ y=0 \quad x-y = 2 \quad x=0 \quad x-y =1 \]
	the image lines in $ uv $-plane are
	\[ u=v \quad v=2 \quad u=-v \quad v=1. \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let \qed \relax
	\begin{minipage}{.5\textwidth}
		Thus, the region $ S $ is the trapezoidal region with vertices $ (1,1) $, $ (2,2) $, $ (-2,2) $ ve $ (-1,1) $ shown in Figure.
	\end{minipage}
	\begin{minipage}{.49\textwidth}
		\begin{figure}[h]
			\includegraphics[page=907,clip,trim=21mm 79mm 149.5mm 124.5mm]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	Since
	\[ S= \{ (u,v) | 1\le v \le 2, -v\le u \le v \} \]
	Theorem gives
	\begin{align*}
	\iint_{R} e^{(x+y)/(x-y)} dA &= \iint_{S} e^{u/v} \left| \frac{\partial (x,y)}{\partial (u,v)} \right | du dv \\
	& = \int_{1}^{2}\int_{-v}^{v} e^{u/v} \left(\frac{1}{2}\right) du dv \\
	& = \frac{1}{2} \int_{1}^{2} \left[v e^{u/v}\right]_{u=-v}^{u=v} dv\\
	& = \frac{1}{2} \int_{1}^{2} (e-e^{-1}) v dv\\
	& = \frac{3}{4}(e-e^{-1}). \qedhere
	\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate the integral $ \displaystyle \iint_{R} xy dA $ by making appropriate change of variables where $ R $ is the region bounded by the lines $ 2x-y=1 $, $ 2x-y = -3 $, $ 3x+y =1 $ ve $ 3x+y = -2 $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	We make change of variables suggested by the form of the equations enclosing the region:
	\[ u = 2x-y \quad v= 3x+y. \]
	These equations define a transformation $ T^{-1} $ from the $ xy $-plane to the $ uv $-plane. Theorem talks about a transformation $ T $ from the $ uv $-plane to the $ xy $-plane. It is obtained by solving previous equation for $ x $ and $ y $:
	\[ x = \frac{1}{5}(u+v) \quad y= \frac{1}{5}(2v-3u). \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let \qed \relax
	The Jacobian of $ T $ is
	\[ \frac{\partial (x,y)}{\partial (u,v)} = \begin{vmatrix}
	\dfrac{\partial x}{\partial u} & \dfrac{\partial x}{\partial v}\\[.3cm]
	\dfrac{\partial y}{\partial u} & \dfrac{\partial y}{\partial v}
	\end{vmatrix} = \begin{vmatrix}
	\dfrac{1}{5} & \dfrac{1}{5}\\[.3cm]
	-\dfrac{3}{5} & \dfrac{2}{5}
	\end{vmatrix} = \frac{1}{5}. \]
	To find the region in the $ uv $-plane corresponding to $ R $, we note that the sides of $ R $ lie on the lines
	\[ 2x-y=1 \quad 2x-y = -3 \quad 3x+y =1 \quad 3x+y = -2 \]
	the image lines in $ uv $-plane are
	\[ u=1 \quad u=-3 \quad v=1 \quad v=-2. \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	Since
	\[ S= \{ (u,v) | -2\le v \le 1, -3\le u \le 1 \} \]
	Theorem gives
	\begin{align*}
	\iint_{R} xy dA &= \iint_{S} \frac{1}{25}(u+v) (2v-3u) \left| \frac{\partial (x,y)}{\partial (u,v)} \right | du dv \\
	& = \frac{1}{25} \int_{-2}^{1}\int_{-3}^{1} (3u^2+2v^2-uv) \left(\frac{1}{5}\right) du dv \\
	& = \frac{1}{125} \int_{-2}^{1} \left[u^3+2v^2u-\frac{u^2}{2}v\right]_{u=-3}^{u=1} dv\\
	& = \frac{1}{125} \int_{-2}^{1} (-8v^2-4v-28) dv\\
	& = -\frac{66}{125}. \qedhere
	\end{align*}
\end{proof}
\end{frame}