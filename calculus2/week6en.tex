\section{Partial Derivatives}
\subsection{Maximum and Minimum Values}
%
\begin{frame}{\insertsubsectionhead}
\begin{definition}
A function of two variables has a \textbf{local maximum} at $(a,b)$ if $f(x,y)\leq f(a,b)$ when $(x,y)$ is near $(a,b)$. [This means that $f(x,y)\leq f(a,b)$ for all points $(x,y)$ in some disk with center $(a,b)$.] The number $f(a,b)$ is called a \textbf{local maximum value}.\medskip

If $f(x,y)\geq f(a,b)$ when $(x,y)$ is near $(a,b)$, then $f(a,b)$ is a \textbf{local minimum value}.
\end{definition}
\begin{block}{}
	If the inequalities in Definition hold for all points $(x,y)$ in the domain of $f$, then $f$ has an \textbf{absolute maximum} (or \textbf{absolute minimum}) at $(a,b)$.
\end{block}
\end{frame}
%
\begin{frame}
The graph of a function with several maxima and minima is shown in Figure. You can think of the local maxima as mountain peaks and the local minima as valley bottoms.
\begin{figure}[h]
\includegraphics[page=811,clip,trim=22mm 104.5mm 145.5mm 139mm,scale=1.8]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{theorem}
If $f$ has a local maximum or minimum at $(a,b)$ and the first-order partial derivatives of $f$ exist there, then
\[ f_x(a,b)=0 \quad\text{and}\quad f_y(a,b)=0. \]
\end{theorem} 
\end{frame}
%
\begin{frame}
\begin{block}{}
	Notice that the conclusion of Theorem can be stated in the notation of the gradient vector as
	\[ \nabla f(a,b)=\vec{0}. \]
	If we put
	\[ f_x(a,b)=0 \quad\text{and}\quad f_y(a,b)=0 \]
	in the equation of a tangent plane, we get $z=z_0$.
\end{block}
\begin{block}{}
	Thus, the geometric interpretation of Theorem is that if the graph of $f(x,y)$ has a tangent plane at a local maximum or minimum, then the tangent plane must be horizontal.
\end{block}
\end{frame}
%
\begin{frame}
\begin{block}{}
	A point $(a,b)$ is called a \textbf{critical point} (or stationary point) of $f$ if $f_x(a,b)=0$ and $f_y(a,b)=0$, or if one of these partial derivatives does not exist.
\end{block}
\begin{block}{}
	Theorem says that if $f$ has a local maximum or minimum at $(a,b)$, then $(a,b)$ is a critical point of $f$.
\end{block}
\begin{alertblock}{}
	However, as in single-variable calculus, not all critical points give rise to maxima or minima.\medskip
	
	At a critical point, a function could have a local maximum or a local minimum or neither.
\end{alertblock}
\end{frame}
%
\begin{frame}
\begin{block}{Second Derivatives Test}
Suppose the second partial derivatives of $f$ are continuous on a disk with center $(a,b)$, and suppose that $f_x(a,b)=0$ and $f_y(a,b)=0$ [that is, $(a,b)$ is a critical point of $f$]. Let
\[D=D(a,b)=f_{xx}(a,b)\cdot f_{yy}(a,b)-\left[f_{xy}(a,b)\right]^2.\]
\begin{itemize}
	\item[(a)] If $D>0$ and $f_{xx}(a,b)>0$ then $f(a,b)$ is a local minimum.
	\item[(b)] If $D>0$ and $f_{xx}(a,b)<0$ then $f(a,b)$ is a local maximum.
	\item[(c)] If $D<0$ then $f(a,b)$ is not a local maximum or minimum.
\end{itemize}
\end{block}
\end{frame}
%
\begin{frame}
\begin{alertblock}{Note 1}
In case {\usebeamercolor[fg]{structure}(c)} the point $(a,b)$ is called a \textbf{saddle point} of $f$ and the graph of $f$ crosses its tangent plane at $(a,b)$.
\end{alertblock}
\begin{alertblock}{Note 2}
If $D=0$, the test gives no information: $f$ could have a local maximum or local minimum at $(a,b)$, or $(a,b)$ could be a saddle point of $f$.
\end{alertblock}
\begin{alertblock}{Note 3}
To remember the formula for it's helpful to write it as a determinant:
\[D(x,y)=\left|\begin{matrix}f_{xx}&f_{xy}\\f_{yx}&f_{yy}\end{matrix}\right|=f_{xx}f_{yy}-(f_{xy})^2\]
\end{alertblock}
\end{frame}
%
\begin{frame}
\begin{example}
Find the local maximum and minimum values and saddle points of $f(x,y)=x^2+y^2-2x-6y+14$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
We first locate the critical points:
\[f_x=2x-2\quad f_y=2y-6.\]
Setting these partial derivatives equal to $0$, we obtain the equations
\[2x-2=0 \ \text{and} \ 2y-6=0.\]
These partial derivatives are equal to $0$ when $x=1$ and $y=3$ , so the only critical point is $ (1,3) $.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
Next we calculate the second partial derivatives and $D(x,y)$:
\[f_{xx}=2 \quad f_{xy}=0 \quad f_{yy}=2\]
\[D(x,y)=f_{xx}f_{yy}-(f_{xy})^2=2\cdot2-0=4.\]
Since $D(1,3)=4>0$ and $f_{xx}=2$, it follows from case {\usebeamercolor[fg]{structure}(a)} of the Second Derivatives Test that the $ (1,3) $ is a local minimum.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
\begin{figure}[h]
\includegraphics[page=812,clip,trim=20mm 204mm 147.5mm 32mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
Find the local maximum and minimum values and saddle points of $f(x,y)=y^2-x^2$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
We first locate the critical points:
\[f_x=-2x\quad f_y=2y.\]
Setting these partial derivatives equal to $0$, we obtain the equations
\[-2x=0 \quad\text{and} \quad 2y=0.\]
These partial derivatives are equal to $0$ when $x=0$ and $y=0$, so the only critical point is $ (0,0) $.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
Next we calculate the second partial derivatives and $D(x,y)$:
\[f_{xx}=-2 \quad f_{xy}=0 \quad f_{yy}=2\]
\[D(x,y)=f_{xx}f_{yy}-(f_{xy})^2=-2\cdot2-0=-4\]
Since $D(0,0)=-4<0$ it follows from case {\usebeamercolor[fg]{structure}(c)} of the Second Derivatives Test that the origin is a saddle point.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
\begin{figure}[h]
\includegraphics[page=812,clip,trim=20mm 125.5mm 146mm 121.5mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
Find the local maximum and minimum values and saddle points of $f(x,y)=x^4+y^4-4xy+1$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
We first locate the critical points:
\[f_x=4x^3-4y\quad f_y=4y^3-4x\]
Setting these partial derivatives equal to $ 0 $, we obtain the equations
\[x^3-y=0 \quad\text{and}\quad y^3-x=0.\]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
To solve these equations we substitute $y=x^3$ from the first equation into the second one. This gives
\begin{align*}
0&=(x^3)^3-x=x^9-x=x(x^8-1)=x(x^4-1)(x^4+1)\\
&=x(x^2-1)(x^2+1)(x^4+1)\\
&=x(x-1)(x+1)(x^2+1)(x^4+1)
\end{align*}
so there are three real roots: $x=0,1,-1$. The three critical points are $ (0,0) $, $ (1,1) $, $ (-1,-1)$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
Next we calculate the second partial derivatives and $D(x,y)$:
\[f_{xx}=12x^2 \quad f_{xy}=-4 \quad f_{yy}=12y^2\]
\[D(x,y)=f_{xx}f_{yy}-(f_{xy})^2=144x^2y^2-16\]
Since $D(0,0)=-16<0$ it follows from case {\usebeamercolor[fg]{structure}(c)} of the Second Derivatives Test that the origin is a saddle point; that is, has no local maximum or minimum at $(0,0)$.\medskip

Since $D(1,1)=128>0$ and $f_{xx}(1,1)=12>0$ we see from case {\usebeamercolor[fg]{structure}(a)} of the test that $f(1,1)=-1$ is a local minimum.\medskip

Similarly, we have $D(-1,-1)=128>0$ and $f_{xx}=12>0$ so $f(-1,-1)=-1$ is also a local minimum.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
\begin{figure}[h]
\includegraphics[page=813,clip,trim=21mm 139mm 156.5mm 100mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
A rectangular box without a lid is to be made from $12 \textrm{m}^2$  of cardboard. Find the maximum volume of such a box.
\end{example}
\begin{proof}[Solution]\let\qed\relax
	\begin{minipage}{.49\textwidth}
		\begin{figure}[ht]
			\includegraphics[page=816,clip,trim=20.5mm 166.5mm 146mm 81mm,scale=1.1]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.49\textwidth}
		Let the length, width, and height of the box (in meters) be $x$, $y$, and $z$, as shown in Figure. Then the volume of the box is
		\[V=xyz\]
	\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
We can express $V$ as a function of just two variables $x$ and $y$ by using the fact that the area of the four sides and the bottom of the box is
\[2xz+2yz+xy=12.\]
Solving this equation for $z$, we get
\[ z=\frac{12-xy}{2(x+y)} \]
so the expression for $V$ becomes
\[V=xy\frac{12-xy}{2(x+y)}=\frac{12xy-x^2y^2}{2(x+y)}.\]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
We compute the partial derivatives:
\[\frac{\partial V}{\partial x}=\frac{y^2(12-2xy-x^2)}{2(x+y)^2}\qquad \frac{\partial V}{\partial y}=\frac{x^2(12-2xy-y^2)}{2(x+y)^2}.\]
If $V$ is a maximum, then $\partial V/\partial x=\partial V/\partial y=0$, but $x=0$ or $y=0$ gives $V=0$, so we must solve the equations
\[12-2xy-x^2=0\quad 12-2xy-y^2=0.\]
These imply that $x^2=y^2$ and so $x=y$. (Note that $x$ and $y$ must both be positive in this problem.)\medskip

If we put $x=y$ in either equation we get $12-3x^2=0$, which gives
\[ x=2,\quad y=2,\quad \text{and}\quad z=1.  \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
We could use the Second Derivatives Test to show that this gives a local maximum of $V$, or we could simply argue from the physical nature of this problem that there must be an absolute maximum volume, which has to occur at a critical point of $V$, so it must occur when $x=2$, $y=2$, $z=1$. \medskip

Then $V=2\cdot 2\cdot 1=4$, so the maximum volume of the box is $4 \textrm{m}^3$.
\end{proof}
\end{frame}
%
\subsubsection{Absolute Maximum and Minimum Values}
%
\begin{frame}{\insertsubsubsectionhead}
For a function $f$ of one variable the Extreme Value Theorem says that if $f$ is continuous on a closed interval $[a,b]$, then $f$ has an absolute minimum value and an absolute maximum value.\medskip

According to the Closed Interval Method, we found these by evaluating $f$ not only at the critical numbers but also at the endpoints $a$ and $b$.
\begin{block}{}
	There is a similar situation for functions of two variables. Just as a closed interval contains its endpoints, a \textbf{closed set} in $\mathds{R}^2$ is one that contains all its boundary points. \medskip
	
	A \textbf{boundary point} of $D$ is a point $(a,b) $ such that every disk with center $(a,b)$ contains points in $D$ and also points not in $D$.
\end{block}
\end{frame}
%
\begin{frame}
For instance, the disk
\[ D=\{(x,y)|x^2+y^2\leq 1\}\]
which consists of all points on and inside the circle $x^2+y^2=1$, is a closed set because it contains all of its boundary points (which are the points on the circle
$x^2+y^2=1$).\medskip

But if even one point on the boundary curve were omitted, the set would not be closed.
\end{frame}
%
\begin{frame}
\begin{figure}[h]
\includegraphics[page=817,clip,trim=21mm 202mm 145mm 31mm,scale=1.6]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{block}{}
	A \textbf{bounded set} in $\mathds{R}^2$ is one that is contained within some disk. In other words, it is finite in extent. Then, in terms of closed and bounded sets, we can state the following counterpart of the Extreme Value Theorem in two dimensions.
\end{block}
\end{frame}
%
\begin{frame}
\begin{block}{Extreme Value Theorem for Functions of Two Variables}
If $f$ is continuous on a closed, bounded set $D$ in $\mathds{R}^2$, then $f$ attains an absolute maximum value $f(x_1,y_1)$ and an absolute minimum value $f(x_2,y_2)$ at some points $(x_1,y_1)$ and $(x_2,y_2)$ in $D$.
\end{block}
\end{frame}
%
\begin{frame}
\begin{block}{}
	To find the absolute maximum and minimum values of a continuous function $f$ on a closed, bounded set $D$:
	\begin{enumerate}
		\item Find the values of $f$ at the critical points of in $D$.
		\item Find the extreme values of $f$ on the boundary of $D$.
		\item The largest of the values from Steps {\usebeamercolor[fg]{structure}1} and {\usebeamercolor[fg]{structure}2} is the absolute maximum value; the smallest of these values is the absolute minimum value.
	\end{enumerate}
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
Find the absolute maximum and minimum values of the function $f(x,y)=x^2-2xy+2y$ on the rectangle
\[ D=\{(x,y)|0\leq x\leq 3,0\leq y \leq 2\} \]
\end{example}
\begin{proof}[Solution]\let\qed\relax
Since $f$ is a polynomial, it is continuous on the closed, bounded rectangle $D$, so there is both an absolute maximum and an absolute minimum.

According to Step {\usebeamercolor[fg]{structure}1}, we first find the critical points. These occur when
\[f_x=2x-2y=0\qquad f_y=-2x+2=0\]
so the only critical point is $ (1,1) $, and the value of $f$ there is $f(1,1)=1$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
\begin{figure}[h]
\includegraphics[page=817,clip,trim=22mm 38mm 148mm 213.5mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
In Step {\usebeamercolor[fg]{structure}2} we look at the values of $f$ on the boundary of $D$, which consists of the four line segments $L_1$, $L_2$, $L_3$, and $L_4$ shown in Figure.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
\begin{figure}[h]
\includegraphics[page=817,clip,trim=22mm 38mm 148mm 213.5mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
On $L_1$ we have $y=0$ and
\[f(x,0)=x^2\qquad 0\leq x\leq3\]
This is an increasing function of $x$, so its minimum value is $f(0,0)=0$ and its maximum value is $f(3,0)=9$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
\begin{figure}[h]
\includegraphics[page=817,clip,trim=22mm 38mm 148mm 213.5mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
On $L_2$ we have $x=3$ and
\[f(3,y)=9-4y\qquad 0\leq y\leq2\]
This is a decreasing function of $y$, so its maximum value is $f(3,0)=9$ and its minimum value is $f(3,2)=1$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
\begin{figure}[h]
\includegraphics[page=817,clip,trim=22mm 38mm 148mm 213.5mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
On $L_3$ we have $y=2$ and
\[f(x,2)=x^2-4x+4 \qquad 0\leq x \leq 3\]
By the methods in functions of one variables, or simply by observing that $f(x,2)=(x-2)^2$, we see that the minimum value of this function is $f(2,2)=0$ and the maximum value is $f(0,2)=4$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
\begin{figure}[h]
\includegraphics[page=817,clip,trim=22mm 38mm 148mm 213.5mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
Finally, on $L_4$ we have $x=0$ and
\[f(0,y)=2y \qquad 0\leq y \leq 2\]
with maximum value $f(0,2)=4$ and minimum value $f(0,0)=0$. Thus, on the boundary, the minimum value of $f$ is $0$ and the maximum is $9$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
In Step {\usebeamercolor[fg]{structure}3} we compare these values with the value $f(1,1)=1$ at the critical point and conclude that the absolute maximum value of $f$ on $D$ is $f(3,0)=9$ and the absolute minimum value is $f(0,0)=f(2,2)=0$. Figure shows the graph of $f$. \qedhere
\begin{figure}[h]
\includegraphics[page=818,clip,trim=20.5mm 191mm 145.5mm 49mm,scale=1.3]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}