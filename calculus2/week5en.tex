\section{Partial Derivatives}
\subsection{Partial Derivatives}
\subsubsection{Functions of More than Two Variables}
%
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{}
	Partial derivatives can also be defined for functions of three or more variables.
\end{block}
\begin{block}{}
	For example, if $f$ is a function of three variables $x$, $y$ and $z$ then its partial derivative with respect to $x$ is found by regarding $y$ and $z$ as constants and differentiating $f(x,y,z)$ with respect to $x$.
\end{block}
\begin{block}{}
	If $w=f(x,y,z)$, then $f_x=\partial w/\partial x$ can be interpreted as the rate of change of $w$ with respect to $x$  when $y$ and $z$ are held fixed. But we can't interpret it geometrically because the graph of $f$ lies in four-dimensional space.
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
Find $f_x$, $f_y$ and $f_z$ if
$f(x,y,z)=e^{xy}\ln z$.
\end{example}
\begin{proof}[Solution]
Holding $y$ and $z$ constant and differentiating with respect to $x$, we have
\[f_x=ye^{xy}\ln z\]
Similarly,
\[f_y=xe^{xy}\ln z \text{ and } f_z=\frac{e^{xy}}{z} \qedhere\] 
\end{proof}
\end{frame}
%
\subsubsection{Higher Derivatives}
%
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{}
	If $f$ is a function of two variables, then its partial derivatives $f_x$ and $f_y$ are also functions
	of two variables, so we can consider their partial derivatives $(f_x)_x$, $(f_x)_y$, $(f_y)_x$, $(f_y)_y$, which are called the \textbf{second partial derivatives} of $f$.
\end{block}
\end{frame}
%%
\begin{frame}
\begin{block}{}
	If $z=f(x,y)$, we use the following notation:
	\begin{align*}
	(f_x)_x&=f_{xx}=f_{11}=\frac{\partial}{\partial x}\left(\frac{\partial f}{\partial x}\right)=\frac{\partial^2f}{\partial x^2}=\frac{\partial^2 z}{\partial x^2}\\
	(f_x)_y&=f_{xy}=f_{12}=\frac{\partial}{\partial y}\left(\frac{\partial f}{\partial x}\right)=\frac{\partial^2f}{\partial y\partial x}=\frac{\partial^2 z}{\partial y\partial x}\\
	(f_y)_x&=f_{yx}=f_{21}=\frac{\partial}{\partial x}\left(\frac{\partial f}{\partial y}\right)=\frac{\partial^2f}{\partial x\partial y}=\frac{\partial^2 z}{\partial x\partial y}\\
	(f_y)_y&=f_{yy}=f_{22}=\frac{\partial}{\partial y}\left(\frac{\partial f}{\partial x}\right)=\frac{\partial^2f}{\partial y^2}=\frac{\partial^2 z}{\partial y^2}
	\end{align*}
\end{block}
\begin{block}{}
	Thus, the notation $f_{xy}$ (or $\frac{\partial^2 f}{\partial y\partial x}$) means that we first differentiate with respect to $x$ and then with respect to $y$, whereas in computing $f_{yx}$ the order is reversed.
\end{block}
\end{frame}
%%
\begin{frame}
\begin{example}
Find the second partial derivatives of $f(x,y)=x^3+x^2y^3-2y^2$.
\end{example}
\begin{proof}[Solution]
We found derivatives with respect to $x$ and $y$ are
\[f_x(x,y)=3x^2+2xy^3 \qquad f_y(x,y)=3x^2y^2-4y\]
Therefore
\begin{align*}
f_{xx}&=\frac{\partial }{\partial x}(3x^2+2xy^3)=6x+2y^3 && f_{xy}=\frac{\partial }{\partial y}(3x^2+2xy^3)=6xy^2\\
f_{yx}&=\frac{\partial }{\partial x}(3x^2 y^2-4y)=6xy^2 && f_{yy}=\frac{\partial }{\partial y}(3x^2 y^2-4y)=6x^2y-4
\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{theorem}[Clairaut Theorem]
Suppose $f$ is defined on a disk $D$ that contains the point $(a,b)$. If the functions $f_{xy}$ and $f_{yx}$ are both continuous on $D$, then
\[ f_{xy}=f_{yx}. \]
\end{theorem}
\begin{block}{}
	Partial derivatives of order $ 3 $ or higher can also be defined. For instance, using Clairaut's Theorem it can be shown that
	\[ f_{xyy}=f_{yxy}=f_{yyx} \]
	if these functions are continuous.
\end{block}
\end{frame}
%%
\begin{frame}
\begin{example}
Calculate $f_{xxyz}$ if $f(x,y,z)=\sin(3x+yz)$.
\end{example}
\begin{proof}[Solution]
\begin{align*}
f_x&=3\cos(3x+yz)\\
f_{xx}&= -9\sin(3x+yz)\\
f_{xxy}&= -9z\cos(3x+yz)\\
f_{xxyz}&=-9\cos(3x+yz)+9yz\sin(3x+yz) \qedhere
\end{align*}
\end{proof}
\end{frame}
%
\subsection{Chain Rule}
%
\begin{frame}{\insertsubsectionhead}
For functions of more than one variable, the Chain Rule has several versions, each of them giving a rule for differentiating a composite function. \medskip

The first version deals with the case where $z=f(x,y)$ and each of the variables $x$ and $y$ is, in turn, a function of a variable $t$. This means that is indirectly a function of $t$, $z=f(g(t),h(t))$, and the Chain Rule gives a formula for differentiating $z$ as a function of $t$. \medskip

We assume that $f$ is differentiable. Recall that this is the case when $f_x$ and $f_y$ are continuous.
\end{frame}
%
\begin{frame}
\begin{theorem}[The Chain Rule: Case 1]\label{p790theo2}
	Suppose that $z=f(x,y)$ is a differentiable function of $x$ and $y$, where $x=g(t)$ and $y=h(t)$ are both differentiable functions of $t$. Then $z$ is a differentiable function of $t$ and
	\[ \frac{dz}{dt}=\frac{\partial f}{\partial x}\frac{dx}{dt}+\frac{\partial f}{\partial y}\frac{dy}{dt} \]
\end{theorem}
\end{frame}
%
\begin{frame}
\begin{example}
	If $z=x^2y+3xy^4$, where $x=\sin 2t$ and $y=\cos t$ find $\dfrac{dz}{dt}$ when $t=0$.
\end{example}
\begin{proof}[Solution]
The Chain Rule gives
\begin{align*}
\frac{dz}{dt}&=\frac{\partial f}{\partial x}\frac{dx}{dt}+\frac{\partial f}{\partial y}\frac{dy}{dt} \\
&=(2xy+3y^4)(2\cos 2t)+(x^2+12xy^3)(-\sin t)
\end{align*}
It’s not necessary to substitute the expressions for $x$ and $y$ in terms of $t$. We simply observe that when $t=0$ we have $x=\sin 0=0$ and $y=\cos 0=1$. Therefore,
\[ \frac{dz}{dt}\bigg|_{t=0}=(0+3)(2\cos 0)+(0+0)(-\sin 0)=6 \qedhere\] 
\end{proof}
\end{frame}

%
%
%
%%_______________________________________________________________--
\begin{frame}
The derivative in Example can be interpreted as the rate of change of $z$ with respect to $t$ as the point $(x,y)$ moves along the curve $C$ with parametric equations $x=\sin 2t$,\ $y=\cos t$.\medskip

In particular, when $t=0$, the point $(x,y)$ is $(0,1)$ and $\displaystyle\frac{dz}{dt}=6$ is the rate of increase as we move along the curve $C$ through $(0,1)$.\medskip


If, for instance, $z=T(x,y)=x^2y+3xy^4$ represents the temperature at the point $(x,y)$, then the composite function $z=T(\sin 2t,\cos t)$ represents the temperature at points on $C$ and the derivative $\displaystyle\frac{dz}{dt}$ represents the rate at which the temperature changes along $C$.
\end{frame}
%
\begin{frame}
\begin{theorem}[The Chain Rule: Case 2]
Suppose that $z=f(x,y)$ is a differentiable function of $x$ and $y$, where $x=g(s,t)$ and $y=h(s,t)$ are differentiable functions of $s$ and $t$.

Then
\begin{align*}
\frac{\partial z }{\partial s} &= \frac{\partial z }{\partial x}\frac{\partial x }{\partial s}+\frac{\partial z }{\partial y}\frac{\partial y }{\partial s} \\
\frac{\partial z }{\partial t}&=  \frac{\partial z }{\partial x}\frac{\partial x }{\partial t}+\frac{\partial z }{\partial y}\frac{\partial y }{\partial t}
\end{align*}
\end{theorem}
\end{frame}
%
\begin{frame}
To remember the Chain Rule it is helpful to draw the tree diagram in figure below.
\begin{figure}[h]
\includegraphics[page=792,clip,trim=21mm 56.5mm 163.5mm 198mm,scale=1.8]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{example}
If $z=e^x\sin y$, where $x=st^2$ and $y=s^2t$, find $\frac{\partial z }{\partial s}$ and $\frac{\partial z }{\partial t}$.
\end{example}
\begin{proof}[Solution]
Applying Case 2 of the Chain Rule, we get
\begin{align*}
\frac{\partial z }{\partial s} &= \frac{\partial z }{\partial x}\frac{\partial x }{\partial s}+\frac{\partial z }{\partial y}\frac{\partial y }{\partial s}=(e^x\sin y)(t^2)+(e^x\cos y)(2st) \\
&= t^2e^{st^2}\sin (s^2t)+2ste^{st^2}\cos (s^2t) \\
\frac{\partial z }{\partial t}&=  \frac{\partial z }{\partial x}\frac{\partial x }{\partial t}+\frac{\partial z }{\partial y}\frac{\partial y }{\partial t}= (e^x\sin y)(2st)+(e^x\cos y)(s^2) \\
&= 2ste^{st^2}\sin (s^2t)+s^2e^{st^2}\cos (s^2t) \qedhere
\end{align*}
\end{proof}
\end{frame}

\begin{frame}
Case 2 of the Chain Rule contains three types of variables:\medskip

$s$ and $t$  are \textbf{independent} variables,\medskip

$x$ and $y$ are called \textbf{intermediate} variables,\medskip

and $z$ is the \textbf{dependent} variable.\medskip

Notice that Theorem \ref{p790theo2} has one term for each intermediate variable and each of these terms resembles the one-dimensional Chain Rule.
\end{frame}
%
\begin{frame}
\begin{block}{The Chain Rule: General Version}
Suppose that $u$ is a differentiable function of the $n$ variables $x_1,x_2,\ldots,x_n$ and each $x_j$ is a differentiable function of the $m$ variables $t_1,t_2,\ldots,t_m$. Then $u$ is a function of $t_1,t_2,\ldots,t_m$ and
\[ \dfrac{\partial u}{\partial t_i}=\dfrac{\partial u}{\partial x_1}\dfrac{\partial x_1}{\partial t_i}+\dfrac{\partial u}{\partial x_2}\dfrac{\partial x_2}{\partial t_i}+\cdots+\dfrac{\partial u}{\partial x_n}\dfrac{\partial x_n}{\partial t_i} \]
for each $i=1,2,\ldots,m$.
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
If $u=x^4y+y^2z^3$, where $x=rse^t$, $y=rs^2e^{-t}$, and $z=r^2s \sin t$, find the value of $\partial u/ \partial s$ when $r=2,s=1,t=0$.
\begin{figure}[h]
\includegraphics[page=793,clip,trim=19.3mm 105.7mm 153.3mm 151.2mm,scale=1.3]{stewart-ccac.pdf}
\end{figure}
\end{example}
\end{frame}
\begin{frame}
\begin{proof}[Solution]
We have
\begin{align*}
\dfrac{\partial u}{\partial s}&= \dfrac{\partial u}{\partial x}\dfrac{\partial x}{\partial s}+\dfrac{\partial u}{\partial y}\dfrac{\partial y}{\partial s}+\dfrac{\partial u}{\partial z}\dfrac{\partial z}{\partial s}\\
&= (4x^3y)(re^t)+(x^4+2yz^3)(2rse^{-t})+(3y^2z^2)(r^2\sin t)
\end{align*}
When  $r=2,s=1,t=0$, we have $x=2,y=2,z=0$, so
\[ \dfrac{\partial u}{\partial s}=(64)(2)+(16)(4)+(0)(0)=192. \qedhere \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
If $g(s,t)=f(s^2-t^2,t^2-s^2)$ and $f$ is differentiable, show that $g$ satisfies the equation
\[ t\dfrac{\partial g}{\partial s}+s\dfrac{\partial g}{\partial t}=0 \]
\end{example}
\begin{proof}[Solution]\let\qed\relax
Let $x=s^2-t^2$ and $y=t^2-s^2$. Then $g(s,t)=f(x,y)$ and the chain rule gives
\begin{align*}
\dfrac{\partial g}{\partial s}&= \dfrac{\partial f}{\partial x}\dfrac{\partial x}{\partial s}+\dfrac{\partial f}{\partial y}\dfrac{\partial y}{\partial s}=\dfrac{\partial f}{\partial x}(2s)+\dfrac{\partial f}{\partial y}(-2s)\\
\dfrac{\partial g}{\partial t}&= \dfrac{\partial f}{\partial x}\dfrac{\partial x}{\partial t}+\dfrac{\partial f}{\partial y}\dfrac{\partial y}{\partial t}=\dfrac{\partial f}{\partial x}(-2t)+\dfrac{\partial f}{\partial y}(2t)
\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Therefore
\[ t\dfrac{\partial g}{\partial s}+s\dfrac{\partial g}{\partial t}= \left(2st\dfrac{\partial f}{\partial x}-2st\dfrac{\partial f}{\partial y}\right)+\left(-2st\dfrac{\partial f}{\partial x}+2st\dfrac{\partial f}{\partial y}\right)=0 \qedhere \]
\end{proof}
\end{frame}

\subsubsection{Implicit Differentiation}
\begin{frame}{\insertsubsubsectionhead}
We suppose that an equation of the form $F(x,y)=0$ defines $y$ implicitly as a differentiable function of $x$, that is, $y=f(x)$, where $F(x,f(x))=0$ for all $x$ in the domain of $f$.\medskip

If $F$ is differentiable, we can apply Case 1 of the Chain Rule to differentiate both sides of the equation $F(x,y)=0$ with respect to $x$.\medskip

Since both $x$ and $y$ are functions of $x$, we obtain
\begin{equation*}
\frac{\partial F }{\partial x}\frac{dx }{dx}+\frac{\partial F }{\partial y}\frac{dy }{dx} =0
\end{equation*}
\end{frame}
%
\begin{frame}
\begin{align*}
\frac{\partial F }{\partial x}\frac{dx }{dx}+\frac{\partial F }{\partial y}\frac{dy }{dx} =0
\end{align*}
But $\displaystyle\frac{dx}{dx}=1$, so if $\displaystyle\frac{\partial F}{\partial y}\neq 0$ we solve for $\displaystyle\frac{dy}{dx}$ and obtain
\begin{equation*}
\frac{dy}{dx}=-\dfrac{\dfrac{\partial F}{\partial x}}{\dfrac{\partial F}{\partial y}}=-\dfrac{F_x}{F_y}
\end{equation*}
\end{frame}
%
\begin{frame}
Now we suppose that $z$ is given implicitly as a function $z=f(x,y)$ by an equation of the form $F(x,y,f(x,y))=0$.\medskip

This means that $F(x,y,f(x,y))=0$ for all $(x,y)$ in the domain of $f$.\medskip

If $F$ and $f$ are differentiable, then we can use the Chain Rule to differentiate the equation $F(x,y,z)=0$ as follows:
\begin{equation}\label{denklem7}
\dfrac{\partial z}{\partial x}=-\dfrac{\dfrac{\partial F}{\partial x}}{\dfrac{\partial F}{\partial z}} \hspace{2cm} \dfrac{\partial z}{\partial y}=-\dfrac{\dfrac{\partial F}{\partial y}}{\dfrac{\partial F}{\partial z}}.
\end{equation}
\end{frame}
%
\begin{frame}
\begin{example}
Find $\partial z/\partial x$ and $\partial z/\partial y$ if $z$ is defined implicitly as a function of $x$ and $y$ by the equation
\[x^3+y^3+z^3+6 x y z=1\]
\end{example}
\begin{proof}[Solution]\let\qed\relax
To find $\partial z/\partial x$ we differentiate implicitly with respect to $x$, being careful to
treat $y$ as a constant:
\[3x^2+3z^2\frac{\partial z}{\partial x}+6yz+6xy\frac{\partial z}{\partial x}=0\]
\end{proof}
\end{frame}
%%
\begin{frame}
\begin{proof}[Solution (cont.)]
\[3x^2+3z^2\frac{\partial z}{\partial x}+6yz+6xy\frac{\partial z}{\partial x}=0\]
Solving this equation for $\partial z/\partial x$, we obtain
\[\frac{\partial z}{\partial x}=-\frac{x^2+2yz}{z^2+2xy}\]
Similarly, implicit differentiation with respect to $y$ gives
\[\frac{\partial z}{\partial y}=-\frac{y^2+2xz}{z^2+2xy} \qedhere\]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
Find $\dfrac{\partial z}{\partial y}$ and $\dfrac{\partial z}{\partial x}$ if $x^3+y^3+z^3+6xyz=1$.
\end{example}
\begin{proof}[Solution]
Let $F(x,y,z)=x^3+y^3+z^3+6xyz-1$. Then, from Equations \eqref{denklem7}, we have
\begin{align*}
\frac{\partial z}{\partial x}&= -\frac{F_x}{F_z}=-\frac{3x^2+6yz}{3z^2+6xy}=-\frac{x^2+2yz}{z^2+2xy} \\
\frac{\partial z}{\partial y}&= -\frac{F_y}{F_z}=-\frac{3y^2+6xz}{3z^2+6xy}=-\frac{y^2+2xz}{z^2+2xy} \qedhere
\end{align*}
\end{proof}
\end{frame}
%
\subsection{Directioanl Derivatives and the Gradient Vector}
%
\subsubsection{Directional Derivatives}
%
\begin{frame}{\insertsubsubsectionhead}
The weather map in Figure shows a contour map of the temperature function $T(x,y)$ for the states of California and Nevada.
\begin{figure}[h]
\includegraphics[page=798,clip,trim=97mm 68mm 47mm 155mm,scale=1.1]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
The level curves, or isothermals, join locations with the same temperature.\medskip

The partial derivative $T_x$ at a location such as $ A $ is the rate of change of temperature with respect to distance if we travel east from $ A $.\medskip

But what if we want to know the rate of change of temperature when we travel southeast, or in some other direction?
\begin{block}{}
	In this section we introduce a type of derivative, called a \textbf{directional derivative}, that enables us to find the rate of change of a function of two or more variables in any direction.
\end{block}
\end{frame}
%
\begin{frame}
Recall that if $z=f(x,y)$, then the partial derivatives $f_x$ and $f_y$ are defined as
\begin{equation}\label{11.6.eqn1}
\begin{split}
f_x(x_0,y_0)&=\lim_{h\rightarrow 0}\frac{f(x_0+h,y_0)-f(x_0,y_0)}{h}\\
f_y(x_0,y_0)&=\lim_{h\rightarrow 0}\frac{f(x_0,y_0+h)-f(x_0,y_0)}{h}
\end{split}
\end{equation}
and represent the rates of change of $z$ in the $x$ and $y$-directions, that is, in the directions of the unit vectors $\vec{i}$ and $\vec{j}$.
\end{frame}
%
\begin{frame}
Suppose that we now wish to find the rate of change of $z$ at $(x_0,y_0)$ in the direction of an arbitrary unit vector $\vec{u}=\langle a,b\rangle$.
\begin{definition}
	The \textbf{directional derivative} of $f$ at $(x_0,y_0)$ in the direction of a unit vector $\vec{u}=\langle a,b\rangle$ is
	\begin{equation}\label{11.6.eqn2}
	D_{\vec{u}} f(x_0,y_0)=\lim_{h\rightarrow 0}\frac{f(x_0+ha,y_0+hb)-f(x_0,y_0)}{h}
	\end{equation}
	if this limit exists.
\end{definition}
\end{frame}
%
\begin{frame}
\begin{block}{}
	By comparing definition with equations \eqref{11.6.eqn1}, we see that if
	\[ \vec{u}=\vec{i}=\langle 1,0\rangle \quad\text{then}\quad D_{\vec{i}}f=f_x \]
	and if
	\[ \vec{u}=\vec{j}=\langle 0,1\rangle \quad\text{then}\quad D_{\vec{j}}f=f_y.\]
\end{block}
\begin{alertblock}{}
	In other words, the partial derivatives of $f$ with respect to $x$ and $y$ are just special cases of the directional derivative.
\end{alertblock}
\end{frame}
%
\begin{frame}
\begin{example}
Use the weather map in Figure to estimate the value of the directional derivative of the temperature function at Reno in the southeasterly direction.
\vspace{-.3cm}
\begin{figure}[h]
\includegraphics[page=800,clip,trim=97mm 141mm 47mm 80mm,scale=1.1]{stewart-ccac.pdf}
\end{figure}
\end{example}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution]
The unit vector directed toward the southeast is $\vec{u}=(\vec{i}-\vec{j})/\sqrt{2}$, but we won't need to use this expression. We start by drawing a line through Reno toward the southeast. We approximate the directional derivative $D_uT$ by the average rate of change of the temperature between the points where this line intersects the isothermals $T=50$ and $T=60$. The temperature at the point southeast of Reno is $T=60^\circ\mathrm{F}$ and the temperature at the point northwest of Reno is $T=50^\circ\mathrm{F}$. The distance between these points looks to be about $75\, \mathrm{mi}$. So the rate of change of the temperature in the southeasterly direction is
\[D_uT\approx \frac{60-50}{75}=\frac{10}{75}\approx 0.13^\circ\mathrm{F}/\mathrm{mi} \qedhere\]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{theorem}
If $f$ is a differentiable function of $x$ and $y$, then $f$ has a directional derivative in the direction of any unit vector $\vec{u}=\langle a,b\rangle$ and
\begin{equation}\label{11.6.eqn3}
D_{\vec{u}}f(x,y)=f_x(x,y)a+f_y(x,y)b
\end{equation}
\end{theorem}
\end{frame}
%
\subsubsection{The Gradient Vector}
%
\begin{frame}{\insertsubsubsectionhead}
Notice from Theorem \ref{11.6.eqn3} that the directional derivative can be written as the dot product
of two vectors:
\begin{equation}\label{11.6.eqn7}
\begin{split}
D_{\vec{u}}f(x,y)&=f_x(x,y)a+f_y(x,y)b\\
&=\Big\langle f_x(x,y),f_y(x,y)\Big\rangle\cdot \langle a,b\rangle\\
&=\Big\langle f_x(x,y),f_y(x,y)\Big\rangle\cdot \vec{u}
\end{split}
\end{equation}
The first vector in this dot product occurs not only in computing directional derivatives
but in many other contexts as well. So we give it a special name (the gradient
of $f$) and a special notation ($\operatorname{grad}f$ or $\nabla f$, which is read “del $f$”).
\end{frame}
%
\begin{frame}
\begin{definition}
If $f$ is a function of two variables $x$ and $y$, then the \textbf{gradient} of
$f$ is the vector function $\nabla f$ defined by
\begin{equation*}
\nabla f(x,y)=\Big\langle f_x(x,y),f_y(x,y)\Big\rangle =\frac{\partial f}{\partial x}\vec{i}+\frac{\partial f}{\partial y}\vec{j}
\end{equation*}
\end{definition}
\end{frame}
%
\begin{frame}
\begin{example}
If $f(x,y)=\sin x+e^{xy}$ then,
 \[ \nabla f(x,y)=\langle f_x,f_y\rangle =\langle \cos x+ye^{xy},xe^{xy}\rangle \]
and
 \[ \nabla f(0,1)=\langle 2,0\rangle\]
\end{example}
\end{frame}
%
\begin{frame}
\begin{block}{}
	With this notation for the gradient vector, we can rewrite the expression \eqref{11.6.eqn7} for the directional derivative as
	\begin{equation}\label{11.6.eqn9}
	D_{\vec{u}}f(x,y)=\nabla f(x,y)\cdot \vec{u}
	\end{equation}
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
Find the directional derivative of the function $f(x,y)=x^2y^3-4y$ at the point $(2,-1)$ in the direction of the vector $\vec{v}=2\vec{i}+5\vec{j}$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
We first compute the gradient vector at $(2,-1)$:
\begin{align*}
\nabla f(x,y)&=2xy^3\vec{i}+(3x^2y^2-4)\vec{j}\\
\nabla f(-2,1)&=-4\vec{i}+8\vec{j}
\end{align*}
Note that $\vec{v}$ is not a unit vector, but since $|\vec{v}|=\sqrt{29}$, the unit vector in the direction
of $\vec{v}$ is
\[\vec{u}=\frac{\vec{v}}{|\vec{v}|}=\frac{2}{\sqrt{29}}\vec{i}+\frac{5}{\sqrt{29}}\vec{j}\]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution(cont.)]
Therefore, by Equation \eqref{11.6.eqn9}, we have
\begin{align*}
D_{\vec{u}}f(2,-1)=\nabla f(2,-1)\cdot \vec{u}&\displaystyle =(-4\vec{i}+8\vec{j})\cdot \left(\frac{2}{\sqrt{29}}\vec{i}+\frac{5}{\sqrt{29}}\vec{j}\right)\\
&\displaystyle =\frac{-4\cdot 2+8\cdot 5}{\sqrt{29}}=\frac{32}{\sqrt{29}} \qedhere
\end{align*}
\end{proof}
\end{frame}
%
\subsubsection{Functions of Three Variables}
%
\begin{frame}{\insertsubsubsectionhead}
For functions of three variables we can define directional derivatives in a similar manner. Again $D_{\vec{u}}f(x,y,z)$ can be interpreted as the rate of change of the function in the direction of a unit vector $\vec{u}$.
\begin{definition}
	The \textbf{directional derivative} of $f$ at $(x_0,y_0,z_0)$ in the direction of
	a unit vector $\vec{u}=\langle a,b,c\rangle$ is
	\begin{equation*}
	D_{\vec{u}}f(x_0,y_0,z_0)=\lim_{h\rightarrow 0}\frac{f(x_0+ha,y_0+hb,z_0+hc)-f(x_0,y_0,z_0)}{h}
	\end{equation*}
	if this limit exists.
\end{definition}
\begin{block}{}
	If $f(x,y,z)$ is differentiable and $\vec{u}=\langle a,b,c\rangle$, then
	\begin{equation}\label{11.6.eqn12}
	D_{\vec{u}}f(x,y,z)=f_x(x,y)a+f_y(x,y,z)_b+f_z(x,y,z)c.
	\end{equation}
\end{block}
\end{frame}
%
\begin{frame}
For a function $f$ of three variables, the \textbf{gradient vector}, denoted by $\nabla f$ or \textbf{grad} $f$, is
\[ \nabla f(x,y,z)=\Big\langle f_x(x,y,z),f_y(x,y,z),f_z(x,y,z)\Big\rangle \]
or, for short,
\[ \nabla f=\langle f_x,f_y,f_z\rangle =\frac{\partial f}{\partial{x}}\vec{i}+\frac{\partial f}{\partial{y}}\vec{j}+\frac{\partial f}{\partial{z}}\vec{k} \]
\begin{block}{}
	Then, just as with functions of two variables, Formula \eqref{11.6.eqn12} for the directional derivative can be rewritten as
	\begin{equation}\label{11.6.eqn14}
	D_{\vec{u}}f(x,y,z)=\nabla f(x,y,z)\cdot \vec{u}
	\end{equation}
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
If $f(x,y,z)=x\sin (yz)$,
\begin{itemize}
\item[(a)] find the gradient of $f$ and
\item[(b)] find the directional
derivative of $f$ at $(1,3,0)$ in the direction of $\vec{v}=\vec{i}+2\vec{j}-\vec{k}$.
\end{itemize}
\end{example}
\begin{proof}[Solution]\let\qed\relax
\begin{itemize}
\item[(a)] The gradient of $f$ is
\begin{align*}
\nabla f(x,y,z)&\displaystyle =\Big\langle f_x(x,y,z),\;f_y(x,y,z),\;f_z(x,y,z)\Big\rangle \\
&=\Big\langle \sin (yz),\;xz\cos (yz),\;xy\cos (yz)\Big\rangle
\end{align*}
\end{itemize}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
\begin{itemize}
\item[(b)] At $(1,3,0)$ we have $\nabla f(1,3,0)=\langle 0,0,3\rangle$. The unit vector in the direction of $\vec{v}=\vec{i}+2\vec{j}-\vec{k}$ is
\[ \vec{u}=\frac{1}{\sqrt{6}}\vec{i}+\frac{2}{\sqrt{6}}\vec{j}-\frac{1}{\sqrt{6}}\vec{k} \]
Therefore, Equation \eqref{11.6.eqn14} gives
\begin{align*}
D_{\vec{u}}f(1,3,0)&\displaystyle =\nabla f(1,3,0)\cdot \vec{u}\\
&=3\vec{k}\cdot \left( \frac{1}{\sqrt{6}}\vec{i}+\frac{2}{\sqrt{6}}\vec{j}-\frac{1}{\sqrt{6}}\vec{k} \right)\\
&=3\left( -\frac{1}{\sqrt{6}} \right)=-\sqrt{\frac{3}{2}} \qedhere
\end{align*}
\end{itemize}
\end{proof}
\end{frame}