\section{Multiple Integrals}
%
\subsection{Triple Integrals}
%
\begin{frame}{\insertsubsectionhead}
Just as we defined single integrals for functions of one variable and double integrals for functions of two variables, so we can define triple integrals for functions of three variables. Let's first deal with the simplest case where $ f $ is defined on a rectangular box:
\[ B = \{ (x,y,z) \vert a \le x \le b, c \le y \le d, r \le z \le s \}. \]
\end{frame}
%
\begin{frame}
\begin{minipage}{.41\textwidth}
	\begin{figure}
		\includegraphics[page=883,clip,trim=21mm 116.5mm 146mm 107mm]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\begin{minipage}{.58\textwidth}
	The first step is to divide $ B $ into sub-boxes. We do this by dividing the interval $ [a, b] $ into $ l $ subintervals $ [x_{i-1}, x_i] $ of equal width $ \Delta x $, dividing $ [c, d] $ into $ m $ subintervals of width $ \Delta y $, and dividing $ [r, s] $ into $ n $ subintervals of width $ \Delta z $. The planes through the endpoints of these subintervals parallel to the coordinate planes divide the box $ B $ into $ lmn $ sub-boxes
	\[ B_{ijk} = [x_{i-1}, x_i] \times [y_{i-1}, y_i] \times [z_{i-1}, z_i]. \]
	Each sub-box has volume $ \Delta V = \Delta x \Delta y \Delta z $.
\end{minipage}
\end{frame}
%
\begin{frame}
Then we form the \textbf{triple Riemann sum}
\[ \sum_{i=1}^{l} \sum_{j=1}^{m} \sum_{k=1}^{n} f (x_{ijk}^{*},y_{ijk}^{*},z_{ijk}^{*}) \Delta V \]
where the sample point $ (x_{ijk}^{*},y_{ijk}^{*},z_{ijk}^{*}) $ is in $ B_{ijk} $. By analogy with the definition of a double integral, we define the triple integral as the limit of the triple Riemann sums.
\begin{definition}{}
	The \textbf{triple integral} of $ f $ over the box $ B $ is
	\[ \iiint_{B}f(x,y,z) dV = \lim_{l,m,n \to \infty} \sum_{i=1}^{l} \sum_{j=1}^{m} \sum_{k=1}^{n} f (x_{ijk}^{*},y_{ijk}^{*},z_{ijk}^{*}) \Delta V \]
	if limit exists.
\end{definition}
\end{frame}
%
\begin{frame}
Just as for double integrals, the practical method for evaluating triple integrals is to express them as iterated integrals as follows.
\begin{theorem}[Fubini's Theorem for Triple Integrals]
	If $ f $ is continuous on the rectangular box $ B = [a, b] \times [c, d] \times [r, s] $, then
	\[ \iiint_{B}f(x,y,z) dV = \int_{a}^{b} \int_{c}^{d} \int_{r}^{s} f(x,y,z) dx dy dz \]
\end{theorem}
\end{frame}
%
\begin{frame}
The iterated integral on the right side of Fubini's Theorem means that we integrate first with respect to $ x $ (keeping $ y $ and $ z $ fixed), then we integrate with respect to $ y $ (keeping $ z $ fixed), and finally we integrate with respect to $ z $. There are five other possible orders in which we can integrate, all of which give the same value. For instance, if we integrate with respect to $ y $, then $ z $, and then $ x $, we have
\[ \iiint_{B}f(x,y,z) dV = \int_{c}^{d} \int_{r}^{s} \int_{a}^{b} f(x,y,z) dy dz dx \]
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate the triple integral $ \displaystyle \iiint_{B} xyz^2 dV $, where $ B $ is the rectangular box given by $ B = \{ (x,y,z) \vert 0 \le x \le 0, -1 \le y \le 2, 0 \le z \le 3 \} $.
\end{example}
\begin{proof}[Solution]
	We could use any of the six possible orders of integration. If we choose to integrate with respect to $ x $, then $ y $, and then $ z $, we obtain
	\begin{align*}
	\iiint_{B} xyz^2 dV &= \int_{0}^{3} \int_{-1}^{2} \int_{0}^{1} xyz^2 dx dy dz = \int_{0}^{3} \int_{-1}^{2} \left[\frac{x^2yz^2}{2}\right]_{x=0}^{x=1} dy dz\\
	&= \int_{0}^{3} \int_{-1}^{2} \left[\frac{yz^2}{2}\right] dy dz = \int_{0}^{3} \left[\frac{y^2z^2}{4}\right]_{y=-1}^{y=2} dz\\
	&= \int_{0}^{3} \left[\frac{3z^2}{4}\right] dz = \left[\frac{z^3}{4}\right] = \frac{27}{4}. \qedhere
	\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
Now we define the \textbf{triple integral over a general bounded region $ \bm{E} $} in three-dimensional space (a solid) by much the same procedure that we used for double integrals. We enclose $ E $ in a rectangular box $ B $. Then we define a function $ F $ so that it agrees with $ f $ on $ E $ but is $ 0 $ for points in $ B $ that are outside $ E $. By definition,
\[ \iiint_{E} f(x,y,z) dV = \iiint_{B} f(x,y,z) dV. \]
\end{frame}
%
\begin{frame}
We restrict our attention to continuous functions $ f $ and to certain simple types of regions. A solid region E is said to be of \textbf{Type 1} if it lies between the graphs of two continuous functions of $ x $ and $ y $, that is,
\[ E = \left\{ (x,y,z) \vert (x,y) \in D, u_1(x,y) \le z \le u_2(x,y) \right\} \]
where $ D $ is the projection of $ E $ onto the $ xy $-plane as shown in Figure. Notice that the upper boundary of the solid $ E $ is the surface with equation $ z = u_2(x,y) $, while the lower boundary is the surface $ z = u_1(x,y) $.
\begin{figure}
	\includegraphics[page=884,clip,trim=20mm 70mm 148mm 174.5mm,scale=1.3]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{block}{}
	It can be shown that if $ E $ is a Type 1 region, then
	\[ \iiint_{E} f(x,y,z) dV = \iint_{D} \left[\int_{u_1(x,y)}^{u_2(x,y)} f(x,y,z) dz \right] dA. \]
\end{block}
The meaning of the inner integral on the right side is that $ x $ and $ y $ are held fixed, and therefore $ u_1(x, y) $ and $ u_2(x, y) $ are regarded as constants, while $ f(x, y, z) $ is integrated with respect to $ z $.
\end{frame}
%
\begin{frame}
\begin{figure}
	\includegraphics[page=885,clip,trim=22mm 212mm 146mm 31mm,scale=1.05]{stewart-ccac.pdf}
\end{figure}
\begin{block}{}
	In particular, if the projection $ D $ of $ E $ onto the $ xy $-plane is a \textbf{Type I plane} region, then
	\[ E = \left\{ (x,y,z) \vert a \le x \le b, g_1(x) \le y \le g_2(x), u_1(x,y) \le z \le u_2(x,y) \right\} \]
	and
	\[ \iiint_{E} f(x,y,z) dV = \int_{a}^{b} \int_{g_1(x)}^{g_2(x)} \int_{u_1(x,y)}^{u_2(x,y)} f(x,y,z) dz dy dx. \]
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate $ \displaystyle \iiint_{E} z dV $, where $ E $ is the solid tetrahedron bounded by the four planes $ x=0 $, $ y=0 $, $ z=0 $, and $ x + y + z = 1 $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	\begin{minipage}{.39\textwidth}
		\begin{figure}
			\includegraphics[page=885,clip,trim=21.5mm 93.5mm 152mm 152mm,scale=1]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.59\textwidth}
		When we set up a triple integral it's wise to draw two diagrams: one of the solid region $ E $ and one of its projection $ D $ on the $ xy $-plane. The lower boundary of the tetrahedron is the plane $ z = 0 $ and the upper boundary is the plane $ x + y + z = 1 $, so we use $ u_1(x,y) = 0 $ and $ u_2(x,y) = 1-x-y $.
	\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution]
	Notice that the planes $ x + y + z = 1 $ and $ z = 0 $ intersect in the line $ x + y = 1 $ in the $ xy $-plane. So the projection of $ E $ is the triangular region shown in Figure, and we have
	\[ E = \left\{ (x,y,z) \vert 0 \le x \le 1, 0 \le y \le 1-x, 0\le z \le 1-x-y \right\}. \]
	\begin{minipage}{.25\textwidth}
		\begin{figure}
			\includegraphics[page=885,clip,trim=24mm 46.5mm 158.5mm 199.5mm,scale=1]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.74\textwidth}
		This description of $ E $ as a Type 1 region enables us to evaluate the integral as follows:
		\begin{align*}
		\iiint_{E} f(x,y,z) dV & = \int_{0}^{1} \int_{0}^{1-x} \int_{0}^{1-x-y} z dz dy dx \\
		& = \frac{1}{2}\int_{0}^{1} \int_{0}^{1-x} (1-x-y)^2 dy dx \\
		& = \frac{1}{6}\int_{0}^{1} (1-x)^3 dx = \frac{1}{24}. \qedhere
		\end{align*}
	\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
A solid region $ E $ is of \textbf{Type 2} if it is of the form
\[ E = \left\{ (x,y,z) \vert (y,z) \in D, u_1(y,z) \le x \le u_2(y,z) \right\} \]
where, this time, $ D $ is the projection of $ E $ onto the $ yz $-plane. The back surface is $ x = u_1(y,z) $, the front surface is $ x = u_2(y,z) $, and we have
\[ \iiint_{E} f(x,y,z) dV = \iint_{D} \left[\int_{u_1(y,z)}^{u_2(y,z)}f(x,y,z)dx\right]dA. \]
\begin{figure}
	\includegraphics[page=886,clip,trim=21.5mm 208mm 147mm 32mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
Finally \textbf{Type 3} region is of the form
\[ E = \left\{ (x,y,z) \vert (x,z) \in D, u_1(x,z) \le y \le u_2(x,z) \right\} \]
where, this time, $ D $ is the projection of $ E $ onto the $ xz $-plane. The left surface is $ y = u_1(x,z) $, the right surface is $ y = u_2(x,z) $, and we have
\[ \iiint_{E} f(x,y,z) dV = \iint_{D} \left[\int_{u_1(x,z)}^{u_2(x,z)}f(x,y,z)dy\right]dA. \]
\begin{figure}
	\includegraphics[page=886,clip,trim=20mm 150.5mm 147.5mm 91mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate $ \displaystyle \iiint_{E} \sqrt{x^2+z^2}dV $, where $ E $ is the region bounded by the paraboloid $ y = x^2 + z^2 $ and the plane $ y = 4 $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	If we regard the solid $ E $ as a Type 1 region, then we need to consider its projection $ D_1 $ onto the $ xy $-plane, which is the parabolic region in the Figure.
	\begin{figure}
		\includegraphics[page=886,clip,trim=72.5mm 90.5mm 93mm 152.5mm,scale=1]{stewart-ccac.pdf}\hfil
		\includegraphics[page=886,clip,trim=147mm 93mm 34mm 152mm,scale=1]{stewart-ccac.pdf}
	\end{figure}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let \qed \relax
	From $ y = x^2 + z^2 $ we obtain $ z = \mp \sqrt{y-x^2} $, so the lower boundary surface of $ E $ is $ z = - \sqrt{y-x^2} $ and the upper surface is $ z = \sqrt{y-x^2} $. Therefore, the description of $ E $ as a Type 1 region is
	\[ E = \left\{ (x,y,z) \vert -2 \le x \le 2, x^2 \le y \le 4, -\sqrt{y-x^2} \le z \le \sqrt{y-x^2} \right\} \]
	and so we obtain
	\[ \iiint_{E} \sqrt{x^2 + z^2} dV = \int_{-2}^{2} \int_{x^2}^{4} \int_{-\sqrt{y-x^2}}^{\sqrt{y-x^2}} \sqrt{x^2 + z^2} dz dy dx. \]
	Although this expression is correct, it is extremely difficult to evaluate.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let \qed \relax
	\begin{minipage}{.3\textwidth}
		\begin{figure}
			\includegraphics[page=887,clip,trim=26mm 218mm 154.5mm 32.5mm,scale=1]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.69\textwidth}
		Let's instead consider $ E $ as a Type 3 region. As such, its projection $ D_3 $ onto $ xz $-plane is the disk $ x^2 + z^2 \le 4 $ shown in Figure. Then the left boundary of $ E $ is the paraboloid $ y = x^2 + z^2 $ and the right boundary is the plane $ y = 4 $, so taking $ u_1(x,z) = x^2 + z^2 $ and $ u_2(x,z) = 4 $, we have
	\end{minipage}
	\begin{align*}
	\iiint_{E} \sqrt{x^2 + z^2} dV & = \iint_{D_3} \left[ \int_{x^2+z^2}^{4} \sqrt{x^2+z^2} dy \right]dA\\
	& = \iint_{D_3} (4-x^2-z^2)\sqrt{x^2+z^2} dA.
	\end{align*}
	Although this integral could be written as
	\[ \int_{-2}^{2} \int_{-\sqrt{4-x^2}}^{4-x^2} (4-x^2-z^2)\sqrt{x^2+z^2}dzdx \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	it's easier to convert to polar coordinates in the $ xz $-plane: $ x = r \cos \theta $, $ z = r \sin \theta $. This gives
	\begin{align*}
	\iiint_{E} \sqrt{x^2 + z^2} dV & = \iint_{D_3} (4-x^2-z^2)\sqrt{x^2+z^2} dA \\
	& = \int_{0}^{2\pi} \int_{0}^{2} (4-r^2) r r dr d\theta \\
	& = \int_{0}^{2\pi} \int_{0}^{2} (4r^2-r^4) dr d\theta\\
	& = \frac{128 \pi}{15}. \qedhere
	\end{align*}
\end{proof}
\end{frame}
%
\subsubsection{Applications of Triple Integrals}
%
\begin{frame}{\insertsubsubsectionhead}
Recall that if $ f(x) \ge 0 $, then the single integral $ \int_{a}^{b}f(x)dx $ represents the area under the curve $ y = f(x) $ from $ a $ to $ b $, and if $ f(x,y) \ge 0 $, then the double integral $ \iint_{D}f(x,y)dA $ represents the volume under the surface $ z = f(x,y) $ and above $ D $.\medskip

The corresponding interpretation of a triple integral $ \iiint_{E}f(x,y,z)dV $, where $ f(x,y,z) \ge 0 $, is not very useful because it would be the ``hypervolume" of a four-dimensional object and, of course, that is very difficult to visualize. (Remember that $ E $ is just the domain of the function $ f $; the graph of $ f $ lies in four-dimensional space.)
\end{frame}
%
\begin{frame}
\begin{block}{}
	Let's begin with the special case where $ f(x,y,z) = 1 $ for all points in $ E $. Then the triple integral does represent the volume of $ E $:
	\[ \iiint_{E}dV. \]
\end{block}
\begin{exampleblock}{}
	For example, you can see this in the case of a Type 1 region by putting $ f(x,y,z) = 1 $ in a triple integral:
	\[ \iiint_{E}1dV = \iint_{D} \left[\int_{u_1(x,y)}^{u_2(x,y)}dz\right]dA = \iint_{D} [u_2(x,y)-u_1(x,y)]dA \]
	and from the previous sections we know this represents the volume that lies between the surfaces $ z = u_1(x,y) $ and $ z = u_2(x,y) $.
\end{exampleblock}
\end{frame}
%
\begin{frame}
\begin{example}
	Use a triple integral to find the volume of the tetrahedron $ T $ bounded by the planes $ x + 2y + z = 2 $, $ x = 2y $, $ x = 0 $, and $ z = 0 $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	\begin{figure}
		\includegraphics[page=888,clip,trim=73.5mm 121.5mm 98.5mm 105mm,scale=1]{stewart-ccac.pdf}\hfil
		\includegraphics[page=888,clip,trim=139mm 132mm 36mm 113mm,scale=1]{stewart-ccac.pdf}
	\end{figure}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	The lower boundary of $ T $ is the plane $ z = 0 $ and upper boundary is the plane $ x + 2y + z = 2 $, that is, $ z = 2-x-2y $. Therefore, we have
	\begin{align*}
	V(T) & = \iiint_{T}dV = \int_{0}^{1} \int_{x/2}^{1-x/2} \int_{0}^{2-x-2y}dzdydx\\
	& = \int_{0}^{1} \int_{x/2}^{1-x/2} (2-x-2y) dydx = \frac{1}{3}. \qedhere
	\end{align*}
\end{proof}
\begin{alertblock}{}
	Notice that it is not necessary to use triple integrals to compute volumes. They simply give an alternative method for setting up the calculation.
\end{alertblock}
\end{frame}
%
\subsection{Triple Integrals in Cylindrical and Spherical Coordinates}
%
\subsubsection{Cylindrical Coordinates}
%
\begin{frame}{\insertsubsectionhead}{\insertsubsubsectionhead}
In the \textbf{cylindrical coordinate system}, a point $ P $ in three-dimensional space is represented by the ordered triple $ (r,\theta,z) $, where $ r $ and $ \theta $ are polar coordinates of the projection of $ P $ onto $ xy $-plane and $ z $ is the directed distance from the $ xy $-plane to $ P $.

\begin{minipage}{.37\textwidth}
	\includegraphics[page=893,clip,trim=21mm 88.5mm 151mm 156mm,scale=1]{stewart-ccac.pdf}
\end{minipage}
\begin{minipage}{.62\textwidth}
	To convert from cylindrical to rectangular coordinates we use the equations
	\[ x = r \cos \theta \quad y = r \sin \theta \quad z = z \]
	whereas to convert from rectangular to cylindrical coordinates we use
	\[ r^2 = x^2 + y^2 \quad \tan \theta = \frac{y}{x} \quad z = z. \]
\end{minipage}
\end{frame}
%
\begin{frame}
Suppose that $ E $ is a Type 1 region whose projection $ D $ on the $ xy $-plane is conveniently described in polar coordinates. In particular, suppose that $ f $ is continuous and
\[ E = \{ (x,y,z) \vert (x,y) \in D, u_1(x,y) \le z \le u_2 (x,y) \} \]
where $ D $ is given in polar coordinates by
\[ D = \{ (r,\theta) \vert \alpha \le \theta \le \beta, h_1(\theta) \le r \le h_2 (\theta) \}. \]
\begin{figure}
	\includegraphics[page=893,clip,trim=109mm 31.2mm 52mm 201mm,scale=1.05]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{block}{}
	Then, the formula for \textbf{triple integration in cylindrical coordinates} is given by
	\begin{align*}
	\iiint_{E} &f(x,y,z) dV =\\
	&\int_{\alpha}^{\beta} \int_{h_1(\theta)}^{h_2(\theta)} \int_{u_1(r\cos\theta,r\sin\theta)}^{u_2(r\cos\theta,r\sin\theta)} f(r\cos\theta,r\sin\theta,z)rdzdrd\theta.
	\end{align*}
\end{block}
\begin{alertblock}{}
	It is worthwhile to use this formula when $ E $ is a solid region easily described in cylindrical coordinates, and especially when the function $ f(x, y, z) $ involves the expression $ x^2 + y^2 $.
\end{alertblock}
\end{frame}
%
\begin{frame}
\begin{example}
	A solid $ E $ lies within the cylinder $ x^2 + y^2 = 1 $, below the plane $ z = 4 $, and above the paraboloid $ z = 1 - x^2 - y^2 $. Then, evaluate the integral $ \displaystyle \iiint_{E} \sqrt{x^2 + y^2}dV $.
\end{example}
\begin{proof}[Solution]
	\begin{minipage}{.27\textwidth}
		\begin{figure}
			\includegraphics[page=894,clip,trim=26mm 89mm 149mm 134mm,scale=.8]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.72\textwidth}
		In cylindrical coordinates the cylinder is $ r = 1 $ and the paraboloid is $ z = 1 - r^2 $, so we can write
		\[ E = \{ (r,\theta,z) \vert 0 \le \theta \le 2\pi, 0 \le r \le 1, 1-r^2 \le z \le 4 \} \]
		Therefore,
		\begin{align*}
		\iiint_{E} \sqrt{x^2 + y^2}dV & = \int_{0}^{2\pi} \int_{0}^{1} \int_{1-r^2}^{4} rrdzdrd\theta\\
		& = \frac{12\pi}{5} \qedhere
		\end{align*}
	\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate $ \displaystyle \int_{-2}^{2} \int_{-\sqrt{4-x^2}}^{\sqrt{4-x^2}} \int_{\sqrt{x^2+y^2}}^{2} (x^2+y^2)dzdydx $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	This iterated integral is a triple integral over the solid region
	{\small
	\[ E = \left\{ (x,y,z) \vert -2\le x \le 2, -\sqrt{4-x^2} \le y \le \sqrt{4-x^2}, \sqrt{x^2+y^2} \le z \le 2 \right\} \]}
	and the projection of $ E $ onto the $ xy $-plane is the disk $ x^2 + y^2 \le 4 $. The lower surface of $ E $ is the cone $ z = \sqrt{x^2+y^2} $ and its upper surface is the plane $ z = 2 $. 
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	This region has a much simpler description in cylindrical coordinates:
	\[ E = \left\{ (r,\theta,z) \vert 0\le \theta \le 2\pi, 0 \le r \le 2, r \le z \le 2 \right\}. \]
	\begin{minipage}{.35\textwidth}
		\begin{figure}
			\includegraphics[page=895,clip,trim=21mm 209.5mm 151.5mm 32mm,scale=1]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.64\textwidth}
		Therefore, we have
		\begin{align*}
		\int_{-2}^{2} &\int_{-\sqrt{4-x^2}}^{\sqrt{4-x^2}} \int_{\sqrt{x^2+y^2}}^{2} (x^2+y^2)dzdydx \\
		& = \int_{0}^{2\pi} \int_{0}^{2} \int_{r}^{2} r^2 r dz dr d\theta \\
		& = \frac{16\pi}{5}. \qedhere
		\end{align*}
	\end{minipage}
\end{proof}
\end{frame}
%
\subsubsection{Spherical Coordinates}
%
\begin{frame}{\insertsubsubsectionhead}
The \textbf{spherical coordinates} $ (\rho, \theta, \phi) $ of a point $ P $ in space are shown in Figure, where $ \rho = \lvert OP\rvert $ is the distance from the origin to $ P $, $ \theta $ is the same angle as in cylindrical coordinates, and $ \phi $ is the angle between the positive $ z $-axis and the line segment $ OP $. Note that $ \rho \ge 0 $ and $ 0 \le \phi \le \pi $.
\begin{minipage}{.37\textwidth}
	\begin{figure}
		\includegraphics[page=696,clip,trim=20.5mm 64mm 151.5mm 166mm,scale=1]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\begin{minipage}{.62\textwidth}
	The relationship between rectangular and spherical coordinates can be seen from Figure. From triangles $ OPQ $ and $ OPP' $ we have
	\[ z = \rho \cos \phi \quad r = \rho \sin \phi. \]
	But, $ x = r \cos \theta $ and $ y = r \sin \theta $. 
\end{minipage}
\end{frame}
%
\begin{frame}
So to convert from spherical to rectangular coordinates, we use the equations
\[ x = \rho \sin \phi \cos \theta \quad y = \rho \sin \phi \sin \theta \quad z = \rho \cos \phi. \]
Also, the distance formula shows that
\[ \rho^2 = x^2 + y^2 + z^2. \]
We use this equation in converting from rectangular to spherical coordinates.
\end{frame}
%
\begin{frame}
In this coordinate system the counterpart of a rectangular box is a \textbf{spherical wedge}
\[ E = \{ (\rho,\theta,\phi) \vert a \le \rho \le b, \alpha \le \theta \le \beta, c \le \phi \le d \} \]
where $ a \ge 0 $, $ \beta - \alpha \le 2\pi $, and $ d - c \le \pi $. 
\begin{block}{}
	Then, the formula for triple integration in spherical coordinates is given by
	\begin{align*}
	\iiint_{E}& f(x,y,z)dV \\
	&= \int_{c}^{d} \int_{\alpha}^{\beta} \int_{a}^{b} f(\rho \sin \phi \cos \theta,\rho \sin \phi \sin \theta,\rho \cos \phi) \rho^2 \sin \phi d\rho d\theta d\phi.
	\end{align*}
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate $ \displaystyle \iiint_{B} e^{(x^2+y^2+z^2)^{3/2}}dV $, where $ B $ is the unit ball $ B = \{(x,y,z)\vert x^2+y^2+z^2\le 1 \} $.
\end{example}
\begin{proof}[Solution]
	Since the boundary of $ B $ is a sphere, we use spherical coordinates:
	\[ B = \{ (\rho, \theta, \phi) \vert 0 \le \rho \le 1, 0 \le \theta \le 2\pi, 0 \le \phi \le \pi \}. \]
	In addition, spherical coordinates are appropriate because $ x^2+y^2+z^2 = \rho^2 $. Thus,
	\begin{align*}
	\iiint_{B} e^{(x^2+y^2+z^2)^{3/2}}dV & = \int_{0}^{\pi} \int_{0}^{2\pi} \int_{0}^{1} e^{(\rho^2)^{3/2}}\rho^2 \sin \phi d\rho d\theta d\phi\\
	& = \frac{4\pi}{3}(e-1). \qedhere
	\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{alertblock}{Note}
	It would have been extremely awkward to evaluate the integral in previous Example without spherical coordinates. In rectangular coordinates the iterated integral would have been
	\[ \int_{-1}^{1} \int_{-\sqrt{1-x^2}}^{\sqrt{1-x^2}} \int_{-\sqrt{1-x^2-y^2}}^{\sqrt{1-x^2-y^2}} e^{(x^2+y^2+z^2)^{3/2}} dz dy dx. \]
\end{alertblock}
\end{frame}
%
\begin{frame}
\begin{example}
	Use spherical coordinates to find the volume of the solid that lies above the cone $ z = \sqrt{x^2+y^2} $ and below the sphere $ x^2 + y^2 + z^2 = z $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	\begin{minipage}{.39\textwidth}
		\begin{figure}
			\includegraphics[page=897,clip,trim=110.5mm 92mm 58.5mm 145mm,scale=.9]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.59\textwidth}
		Notice that the sphere passes through the origin and has center $ (0,0,\frac{1}{2}) $. We write the equation of the sphere in spherical coordinates as
		\[ \rho^2 = \rho\cos\phi \quad \text{or} \quad \rho = \cos \phi. \]
		The equation of the cone can be written as
	\end{minipage}
	\[ \rho\cos\phi = \sqrt{\rho^2\sin^2 \phi \cos^2 \theta + \rho^2\sin^2 \phi \sin^2 \theta} = \rho\sin\phi.\]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let \qed \relax
	This gives $ \sin \phi = \cos \phi $, or $ \phi = \pi/4 $. Therefore, the description of the solid $ E $ in spherical coordinates is
	\[ E = \{ (\rho,\theta,\phi) \vert 0 \le \theta \le 2\pi, 0 \le \phi \le \pi/4, 0 \le \rho \le \cos \phi \}. \]
	\begin{figure}
		\includegraphics[page=898,clip,trim=50.5mm 151.5mm 130.5mm 80mm,scale=.9]{stewart-ccac.pdf}\hfil
		\includegraphics[page=898,clip,trim=105.5mm 151.5mm 75.5mm 80mm,scale=.9]{stewart-ccac.pdf}\hfil
		\includegraphics[page=898,clip,trim=159.5mm 151.5mm 22mm 80mm,scale=.9]{stewart-ccac.pdf}
		
		$ \rho $ varies from $ 0 $ to $ \cos\phi $ \hfil $ \phi $ varies from $ 0 $ to $ \pi/4 $ \hfil $ \theta $ varies from $ 0 $ to $ 2\pi $
	\end{figure}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	The volume of $ E $ is
	\begin{align*}
	V(E) & = \iiint_{E} dV = \int_{0}^{2\pi} \int_{0}^{\pi/4} \int_{0}^{\cos \phi} \rho^2 \sin \phi d\rho d\phi d\theta\\
	& = \int_{0}^{2\pi} d\theta \int_{0}^{\pi/4} \sin \phi \left[\frac{\rho^3}{3}\right]_{\rho=0}^{\rho=\cos\phi} d\phi\\
	& = \frac{2\pi}{3} \int_{0}^{\pi/4} \sin \phi \cos^3 \phi d\phi\\
	& = \frac{\pi}{8}. \qedhere
	\end{align*}
\end{proof}
\end{frame}