\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{color}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{dsfont}
\usepackage{cancel}
\usepackage{tikz} 
\usepackage{tkz-tab}
\usepackage{multicol}
\usepackage[left=1cm,top=1cm,right=1cm,bottom=2cm]{geometry}
\theoremstyle{plain}
\newtheorem{remark}{Remark}
\newtheorem{example}{Example}
\newtheorem{definition}{Definition}
\title{MAT 1002 Calculus II \hspace{1.4cm} \insertframenumber / \inserttotalframenumber}
\author{İstatistik/Kimya}

\begin{document}

\section{Vectors and the Geometry of Space}
%
\subsection{Three-Dimensional Coordinate Systems}
%
   
	In order to represent points in space, we first choose a fixed point $O$ (the origin) and three directed lines through $O$ that are perpendicular to each other, called the \textbf{coordinate axes} and labeled the $x-$axis, $y-$axis, and $z-$axis.
	
	\begin{minipage}{.5\textwidth}
		Usually we think of the $x-$ and $y-$axes as being horizontal and the $z-$axis as being vertical, and we draw the orientation of the axes as in Figure.
	\end{minipage}
	\begin{minipage}{.4\textwidth}
%		 
			\begin{center}
				\includegraphics[page=647,clip,trim=22mm 154mm 147mm 91mm]{stewart-ccac.pdf}
			\end{center}
%		
	\end{minipage}
 
%
 
The three coordinate axes determine the three \textbf{coordinate planes} illustrated in Figure.

\begin{minipage}{.5\textwidth}
	The \textbf{$xy-$plane} is the plane that contains the $x-$ and $y-$axes;
	
	the \textbf{$yz-$plane} contains the $y-$ and $z-$ axes;
	
	the \textbf{$xz-$plane} contains the $x-$ and $z-$ axes.
\end{minipage}
\begin{minipage}{.4\textwidth}
	 
		\begin{center}
			\includegraphics[page=647,clip,trim=80mm 62.5mm 84mm 177mm]{stewart-ccac.pdf}
		\end{center}
	
\end{minipage}

These three coordinate planes divide space into eight parts, called \textbf{octants}. The \textbf{first octant}, in the foreground,
is determined by the positive axes.
 
%
 
	 
		\begin{center}
			\includegraphics[page=647,clip,trim=146mm 62.5mm 27mm 179mm,scale=1.2]{stewart-ccac.pdf}
		\end{center}
	
 
%
 
	Now if $ P $ is any point in space, let $ a $ be the (directed) distance from the $ yz- $plane to $P$, let $ b $ be the distance from the $ xz- $plane to $P$, and let $ c $ be the distance from the $ xy- $plane to $P$.
	 
		\begin{center}
			\includegraphics[page=648,clip,trim=20mm 192mm 147mm 54.5mm,scale=1.2]{stewart-ccac.pdf}
		\end{center}
	
 
%
 
	We represent the point $ P $ by the ordered triple $ (a,b,c) $ of real numbers and we call $ a,b,c $ the \textbf{coordinates} of $P$; $ a $ is the $ x- $coordinate, $ b $ is the $ y- $coordinate, and is $ c $ the $ z- $coordinate.
	
	\begin{minipage}{.5\textwidth}
		Thus, to locate the point $ (a,b,c) $ we can start at the origin $O$ and move $ a $ units along the $ x- $axis, then $ b $ units parallel to the $ y- $axis, and then $ c $ units parallel to the $ z- $axis as in Figure.
	\end{minipage}
	\begin{minipage}{.4\textwidth}
		 
			\begin{center}
				\includegraphics[page=648,clip,trim=20mm 192mm 147mm 54.5mm]{stewart-ccac.pdf}
			\end{center}
		
	\end{minipage}
 
%
 
	The point $ P(a,b,c) $ determines a rectangular box as in Figure. If we drop a perpendicular from $ P $ to the $ xy- $plane, we get a point $ Q $ with coordinates $ (a,b,0) $ called the \textbf{projection} of $ P $ on the $ xy- $plane.
	
	\begin{minipage}{.5\textwidth}
		Similarly, $ R(0,b,c) $ and $ S(a,0,c) $ are the projections of $ P $ on the $ yz$-plane and $ xz$-plane, respectively.
	\end{minipage}
	\begin{minipage}{.4\textwidth}
		 
			\begin{center}
				\includegraphics[page=648,clip,trim=20mm 109mm 144mm 126mm]{stewart-ccac.pdf}
			\end{center}
		
	\end{minipage}
 
%
 
	The Cartesian product
	\[\mathds{R}\times\mathds{R}\times\mathds{R}=\{(x,y,z)\Big\vert x,y,x\in\mathds{R}\} \]
	is the set of all ordered triples of real numbers and is denoted by $ \mathds{R}^3 $.
	
	We have given a one-to-one correspondence between points $ P $ in space and ordered triples $ (a,b,c) $  in $ \mathds{R}^3 $.
	
	It is called a \textbf{three-dimensional rectangular coordinate system}.
	
	Notice that, in terms of coordinates, the first octant can be described as the set of points whose coordinates are all
	positive.
	
	In two-dimensional analytic geometry, the graph of an equation involving $ x $ and $ y $ is a \textbf{curve} in $ \mathds{R}^2 $. In three-dimensional analytic geometry, an equation in $ x,y, z $ represents a \textbf{surface} in $ \mathds{R}^3 $.
 
%
 
\begin{example}
	What surfaces in $ \mathds{R}^3 $ are represented by the following equations?
	\[ (a)\quad z=3\qquad\qquad (b)\quad y=5 \]
\end{example}
\begin{proof}[Solution]\let \qed \relax
	(a)\quad The equation $ z=3 $ represents the set
	\[  \{(x,y,z)\Big|z=3\} \]
	which is the set of all points in $ \mathds{R}^3 $ whose $ z$-coordinate is $ 3 $.
	
	\begin{minipage}{.55\textwidth}
		This is the horizontal plane that is parallel to the $ xy$-plane and three units above it as in Figure.
	\end{minipage}
	\begin{minipage}{.4\textwidth}
		 
			\begin{center}
				\includegraphics[page=649,clip,trim=36mm 200mm 130mm 47mm,scale=.9]{stewart-ccac.pdf}
			\end{center}
		
	\end{minipage}
\end{proof}
 
%
 
\begin{proof}[Solution (cont...)]
	(b)\quad  The equation $ y=5 $ represents the set of all points in $ \mathds{R}^3 $ whose $ y$-coordinate is 5.
	
	\begin{minipage}{.55\textwidth}
		This is the vertical plane that is parallel to the $ xz$-plane and five units to the right of it as in Figure. \qedhere
	\end{minipage}
	\begin{minipage}{.4\textwidth}
		 
			\begin{center}
				\includegraphics[page=649,clip,trim=97mm 197.5mm 83mm 44mm]{stewart-ccac.pdf}
			\end{center}
		
	\end{minipage}
\end{proof}
 
%
 
\textbf{Not}\\
	When an equation is given, we must understand from the context whether it represents a curve in $ \mathds{R}^2 $ or a surface in $ \mathds{R}^3 $. In Example, $ y=5 $ represents a plane in $ \mathds{R}^3 $, but of course can also represent a line in $ \mathds{R}^2 $ if we are dealing with two dimensional
	analytic geometry.
		\begin{center}
			\includegraphics[page=649,clip,trim=147mm 207mm 27mm 47mm,scale=1.3]{stewart-ccac.pdf}
		\end{center}
 
%
 
\begin{example}
	Describe and sketch the surface in $\mathds{R}^3 $ represented by the equation $ y=x $.
\end{example}
\begin{proof}[Solution]
	The equation represents the set of all points in $ \mathds{R}^3 $ whose $ x $- and $ y $-coordinates
	are equal, that is,
	\[ \{(x,x,z)\Big|x,z\in\mathds{R}\} .\]
	
	\begin{minipage}{.55\textwidth}
		This is a vertical plane that intersects the $ xy$-plane in the line $ y=x,\; z=0 $. The portion of this plane that lies in the first octant is sketched in Figure. \qedhere
	\end{minipage}
	\begin{minipage}{.4\textwidth}
		 
		\begin{center}
				\includegraphics[page=649,clip,trim=24mm 99mm 159.5mm 139mm,scale=.9]{stewart-ccac.pdf}
		\end{center}
		
	\end{minipage}
\end{proof}
 
%
%
 
\textbf{Distance Formula in Three Dimensions}\\
	The distance $ |P_1P_2| $ between the points
	$ P_1(x_1,y_1,z_1) $ and $ P_2(x_2,y_2,z_2) $ is
	\[ |P_1P_2|=\sqrt{(x_1-x_2)^2+(y_1-y_2)^2+(z_1-z_2)^2}. \]

 
%
 
\begin{example}
The distance from the point $ P(2,-1,7) $ to the point $ Q(1,-3,5) $ is
\[ |PQ|=\sqrt{(1-2)^2+(-3+1)^2+(5-7)^2}=\sqrt{1+4+4}=3 .\]
\end{example}
 
%
 
\begin{example}
Find an equation of a sphere with radius $ r $ and center $ C(h,k,l) $.
\end{example}
\begin{proof}[Solution]
\begin{minipage}{.49\textwidth}
 
	\begin{center}
		\includegraphics[page=650,clip,trim=19mm 128mm 147.5mm 103mm]{stewart-ccac.pdf}
	\end{center}

\end{minipage}
\begin{minipage}{.5\textwidth}
	By definition, a sphere is the set of all points $ P(x,y,z) $ whose distance from $ C $ is $ r $. Thus, $ P $ is on tne sphere if and only if $ |PC| = r $. Squaring bpth sides, we have $ |PC|^2 = r^2 $ or
	\[ (x-h)^2+(y-k)^2+(z-l)^2=r^2. \qedhere \]
\end{minipage}
\end{proof}
 
%
 
\textbf{Equation of a Sphere}\\
An equation of a sphere with center $ C(h,k,l) $ and radius $ r $ is
\[ (x-h)^2+(y-k)^2+(z-l)^2=r^2. \]
In particular, if the center is the origin $ O $, then an equation of the sphere is
\[ x^2+y^2+z^2=r^2. \]

 
%
 
\begin{example}
Show that $ x^2+y^2+z^2+4x-6y+2z+6=0 $ is the equation of a sphere, and find its center and radius.
\end{example}
\begin{proof}[Solution]
We can rewrite the given equation in the form of an equation of a sphere if we complete squares:
\begin{align*}
(x^2+4x+4)+(y^2-6y+9)+(z^2+2z+1)&=-6+4+9+1\\
(x+2)^2+(y-3)^2+(z+1)^2&=8
\end{align*}
Comparing this equation with the standard form, we see that it is the equation of a sphere with center $ (-2,3,-1) $ and radius $ \sqrt{8}=2\sqrt{2} $.
\end{proof}
 
%
 
\begin{example}
What region in $ \mathds{R}^3 $ is represented by the inequalities $ 1\le  x^2+y^2+z^2\le 4,\quad z\le 0 $?
\end{example}
\begin{proof}[Solution]
\begin{minipage}{.3\textwidth}
 
\begin{center}
	\includegraphics[page=651,clip,trim=20mm 209mm 146.5mm 31.5mm,scale=.74]{stewart-ccac.pdf}
\end{center}

\end{minipage}
\begin{minipage}{.6\textwidth}
The inequalities $ 1\le  x^2+y^2+z^2\le 4 $ can be rewritten as
\[ 1\le  \sqrt{x^2+y^2+z^2}\le 2  \]
so they represent the points $(x,y,z$) whose distance from the origin is at least $ 1 $ and at most $ 2 $. But we are also given that $ z\le 0 $, so the points lie on or below the $xy$-plane. Thus, the given inequalities represent the region that lies between (or on) the spheres $ x^2+y^2+z^2=1 $ and $ x^2+y^2+z^2=4 $ and beneath (or on) the $ xy$-plane.\qedhere
\end{minipage}
\end{proof}
 
%
\subsection{Vectors}
%
  
The term \textbf{vector} is used by scientists to indicate a quantity (such as displacement or velocity or force) that has both magnitude and direction.

A vector is often represented by an arrow or a directed line segment.

The length of the arrow represents the magnitude of the vector and the arrow points in the direction of the vector.

We denote a vector by putting an arrow above the letter ($ \vec{v} $).
 
%
 
For instance, suppose a particle moves along a line segment from point $ A $ to point $ B $. The corresponding \textbf{displacement vector} $ \vec{v} $, shown in Figure, has \textbf{initial point} $ A $ (the tail) and \textbf{terminal point} $ B $ (the tip) and we indicate this by writing $ \vec{v}=\vec{AB} $.

\begin{center}
	\includegraphics[page=652,clip,trim=21mm 66mm 150mm 190.5mm]{stewart-ccac.pdf}
\end{center}

Notice that the vector $ \vec{u}=\vec{CD} $ has the same length and the same direction as $\vec{v}$ even though it is in a different position. We say that $ \vec{u}\mbox{ and } \vec{v} $ are equivalent (or equal) and we write $ \vec{u}=\vec{v} $. The \textbf{zero vector}, denoted by $ \vec{0} $, has length 0. It is the only vector with no specific direction.
 
%
\subsubsection{Combining Vectors}
%
Suppose a particle moves from $ A $  to $ B $, so its displacement vector is $ \vec{AB} $. Then the particle changes direction and moves from $ B $ to $ C $, with displacement vector $ \vec{BC} $.

\begin{center}
	\includegraphics[page=653,clip,trim=21mm 223mm 160.5mm 31.5mm]{stewart-ccac.pdf}
\end{center}

The combined effect of these displacements is that the particle has moved from $ A $ to $ C $. The resulting displacement vector $ \vec{AC} $ is called the sum of $ \vec{AB} $ and $ \vec{BC} $ and we write
\[ \vec{AC}=\vec{AB}+\vec{BC}. \]
 
%
 
\textbf{Vector Addition}\\
If $ \vec{u} $ and $ \vec{v} $ are vectors positioned so the initial point of $ \vec{v} $ is at the terminal point of $ \vec{u} $, then the \textbf{sum} $ \vec{u}+\vec{v} $ is the vector from the initial point of $ \vec{u} $ to the terminal point of $ \vec{v} $.

\begin{center}
	\includegraphics[page=653,clip,trim=21.5mm 189mm 169mm 67.5mm]{stewart-ccac.pdf}
\end{center}

 
%
 
\textbf{Scalar Multiplication}\\
If $ c $ is a scalar and $ \vec{v} $ is a vector, then the \textbf{scalar multiple} $ c\vec{v} $ is the vector whose length is $ |c| $ times the length of $ \vec{v} $ and whose direction is the same as $ \vec{v} $ if $ c>0 $ and is opposite to $ \vec{v} $ if $ c<0 $. If $ c=0 $ or $ \vec{v}=\vec{0} $, then  $ c\vec{v}=\vec{0} $.

\begin{center}
	\includegraphics[page=654,clip,trim=20mm 207mm 155mm 31.5mm]{stewart-ccac.pdf}
\end{center}
 
%
 
In particular, the vector $-\vec{v}=(-1)\vec{v}$ has the same length as $\vec{v}$ but points in the opposite direction. We call it
the negative of $\vec{v}$.

By the difference $\vec{u}-\vec{v}$ of two vectors we mean
\[ \vec{u}-\vec{v}=\vec{u}+(-\vec{v}). \]
 
%
\subsubsection{Components}
%
\begin{minipage}{.44\textwidth}

\begin{center}
	\includegraphics[page=654,clip,trim=20mm 38mm 152.5mm 177mm,scale=1.2]{stewart-ccac.pdf}
\end{center}

\end{minipage}
\begin{minipage}{.55\textwidth}
If we place the initial point of a vector $\vec{a}$ at the origin of a rectangular coordinate system, then the terminal point of has coordinates of the form $(a_1,a_2)$ or $(a_1,a_2,a_3)$, depending on whether our coordinate system is two- or three-dimensional.
These coordinates are called the components of $\vec{a}$ and we write
\[ \vec{a}=\langle a_1,a_2\rangle   \text{ or } \vec{a}=\langle a_1,a_2,a_3\rangle \]
We use the notation $ \langle a_1,a_2\rangle $ for the ordered pair that refers to a vector so as not to confuse it with the ordered pair $(a_1,a_2)$ that refers to a point in the plane.
\end{minipage}
 
%

\begin{center}
	\includegraphics[page=655,clip,trim=74.5mm 212mm 96.5mm 32mm,scale=1.2]{stewart-ccac.pdf}
\end{center}

For instance, the vectors shown in Figure are all equivalent to the vector $\vec{OP}=\langle3,2\rangle$ whose terminal point is $P(3,2)$.

What they have in common is that the terminal point is reached from the initial point by a displacement of three units to the
right and two upward.

We can think of all these geometric vectors as representations of the algebraic vector $\vec{a}=\langle3,2\rangle$.

The particular representation $\vec{OP}$ from the origin to the point $P(3,2)$ is called the \textbf{position vector} of the point $P$.
 
%
 

Given the points $A(x_1,y_1,z_1)$ and $B(x_2,y_2,z_2)$ , the vector with $\vec{a}$ representation $\vec{AB}$ is
\begin{equation}\label{p655eq1}
\vec{a}=\Big\langle (x_2-x_1),(y_2-y_1),(z_2-z_1)\Big\rangle.
\end{equation}
 
%
 
\begin{example}
Find the vector represented by the directed line segment with initial point $A(2,-3,4)$ and terminal point $B(-2,1,1)$.
\end{example}
\begin{proof}[Solution]
	By \eqref{p655eq1}, the vector corresponding to $\vec{AB}$ is
	\[ \vec{a}=\Big\langle -2-2 , 1-(-3) , 1-4\Big\rangle=\langle -4,4,-3\rangle. \qedhere \]
\end{proof}
 
%
 
The \textbf{magnitude} or \textbf{length} of the vector $\vec{v}$ is the length of any of its representations and is denoted by the symbol $|\vec{v}|$ or $||\vec{v}||$.

By using the distance formula to compute the length of a segment $OP$, we obtain the following formulas.

The length of the two-dimensional vector $ \vec{a} =\langle a_1,a_2\rangle $ is
\[ |\vec{a}|=\sqrt{a_1^{2} +a_2^{2} } \text{ dir.} \]


The length of the three-dimensional vector $\vec{a}=\langle a_1,a_2,a_3\rangle  $ is
\[ |\vec{a}|=\sqrt{a_1^2+a_2^2+a_3^2} \text{ dir.} \]

 
%
 

If $\vec{a}=\langle a_1,a_2\rangle  \text{ and }  \vec{b}=\langle b_1,b_2\rangle  \text{ then}$
\[ \vec{a}+\vec{b}=\langle a_1+b_1,a_2+b_2\rangle \quad \vec{a}-\vec{b}=\langle a_1-b_1,a_2-b_2\rangle \]
\[ c\vec{a}=\langle ca_1,ca_2\rangle. \]
Similarly, for three-dimensional vectors,
\begin{align*}
\langle a_1,a_2,a_3\rangle+\langle b_1,b_2,b_3\rangle&=\langle a_1+b_1,a_2+b_2,a_3+b_3\rangle\\
\langle a_1,a_2,a_3\rangle-\langle b_1,b_2,b_3\rangle&=\langle a_1-b_1,a_2-b_2,a_3-b_3\rangle\\
c\langle a_1,a_2,a_3\rangle&=\langle ca_1,ca_2,ca_3\rangle.
\end{align*}

 
%
 
\begin{example}
If $\vec{a}=\langle4,0,3\rangle$ and $\vec{b}=\langle-2,1,5\rangle$ find $|\vec{a}|$ and the vectors $\vec{a}+\vec{b}$, $\vec{a}-\vec{b}$, $3\vec{b}$ and $2\vec{a}+5\vec{b}$.
\end{example}
\begin{proof}[Solution]
$ \begin{aligned}
|\vec{a}|&=\sqrt{4^2+0^2+3^2}=\sqrt{25}=5\\
\vec{a}+\vec{b}&=\langle 4,0,3\rangle+\langle -2,1,5\rangle\\
&=\langle 4-2,0+1,3+5\rangle=\langle 2,1,8\rangle\\
\vec{a}-\vec{b}&=\langle 4,0,3\rangle-\langle -2,1,5\rangle\\
&=\langle 4+2,0-1,3-5\rangle=\langle 6,-1,-2\rangle\\
3\vec{b}&=3\langle -2,1,5\rangle=\langle 3.(-2),3.1,3.5\rangle=\langle -6,3,15\rangle\\
2\vec{a}+5\vec{b}&=2\langle 4,0,3\rangle+5\langle -2,1,5\rangle\\
&=\langle 8,0,6\rangle+\langle -10,5,25\rangle=\langle -2,5,31\rangle.  \qedhere
\end{aligned} $ 
\end{proof}
 
%
 
We denote by $V_2$ the set of all two-dimensional vectors and by $V_3$ the set of all three-dimensional vectors.

More generally, we will later need to consider the set $V_n$ of all $n$-dimensional vectors.

An $n$-dimensional vector is an ordered $n$-tuple:
\[ \vec{a}=\langle a_1,a_2,\ldots,a_n\rangle \]
where $a_1,a_2,\ldots,a_n$ are real numbers that are called the components of $\vec{a}$.

Addition and scalar multiplication are defined in terms of components just as for the cases $n=2$ and $n=3$.
 
%
 
\textbf{Properties of Vectors}\\
If  $\vec{a}$, $\vec{b}$ and $\vec{c}$, are vectors in $V_n$ and  $c$ and $d$ are scalars, then
\begin{multicols}{2}
\begin{enumerate}
\item $ \vec{a}+\vec{b}=\vec{b}+\vec{a}$
\item $ \vec{a}+(\vec{b}+\vec{c})=(\vec{a}+\vec{b})+\vec{c} $
\item $ \vec{a}+\vec{0}=\vec{a} $
\item $ \vec{a}+(-\vec{a})=\vec{0} $
\item $ c(\vec{a}+\vec{b})=c\vec{a}+c\vec{b} $
\item $ (c+d)\vec{a}=c\vec{a}+d\vec{a} $
\item $ (cd)\vec{a}=c(d\vec{a}) $
\item $ 1\vec{a}=\vec{a} $		
\end{enumerate}
\end{multicols}
 
%
 
Three vectors in $V_3$ play a special role. These are
\[ \vec{i}=\langle 1,0,0 \rangle,\quad \vec{j}=\langle0,1,0\rangle,\quad \vec{k}=\langle0,0,1\rangle. \]
Then $\vec{i}$, $\vec{j}$ and $\vec{k}$ are vectors that have length 1 and point in the directions of the positive $x-$, $y-$ and $z-$axes. Similarly, in two dimensions we define $\vec{i}= \langle 1,0 \rangle,\quad \vec{j}= \langle0,1\rangle $.

\begin{center}
	\includegraphics[page=657,clip,trim=140mm 146mm 31mm 105.5mm]{stewart-ccac.pdf}
\end{center}

 
%
 
If $\vec{a}=\langle a_1,a_2,a_3\rangle$, then we can write
\begin{align*}
\vec{a}& =\langle a_1,a_2,a_3\rangle=\langle a_1,0,0\rangle+\langle 0,a_2,0\rangle+\langle 0,0,a_3\rangle \\
& = a_1\langle 1,0,0\rangle+a_2\langle 0,1,0\rangle+a_3\langle 0,0,1\rangle\\
\vec{a} & =a_1\vec{i}+a_2\vec{j}+a_3\vec{k}.
\end{align*}
Thus, any vector in $V_3$ can be expressed in terms of the standard basis vectors $\vec{i},\vec{j},\vec{k}$. For instance,
\[ \langle 1,-2,6\rangle=\vec{i}-2\vec{j}+6\vec{k}. \]
 
%
 
\begin{example}
If $\vec{a}=\vec{i}+2\vec{j}-3\vec{k}$  and $\vec{b}=4\vec{i}+7\vec{k}$ express the vector $2\vec{a}+3\vec{b}$ in terms of $\vec{i},\vec{j}$ and $\vec{k}$.
\end{example}
\begin{proof}[Solution]
Using Properties 1, 2, 5, 6, and 7 of vectors, we have
\begin{align*}
2\vec{a}+3\vec{b} &=2(\vec{i}+2\vec{j}-3\vec{k})+3(4\vec{i}+7\vec{k})\\
&=2\vec{i}+4\vec{j}-6\vec{k}+12\vec{i}+21\vec{k}=14\vec{i}+4\vec{j}+15\vec{k} \qedhere
\end{align*}
\end{proof}
 
%
 
A unit vector is a vector whose length is $ 1 $.

For instance, $\vec{i}$, $\vec{j}$ and $\vec{k}$ are all unit vectors.

In general, if $\vec{a} \neq 0 $, then the unit vector that has the same direction as $\vec{a}$ is
\begin{equation}\label{p658eq4}
\vec{u}=\frac{1}{|a|}a = \frac{a}{|a|}.
\end{equation}
 
%
 
\begin{example}
Find the unit vector in the direction of the vector $ 2\vec{i}-\vec{j}-2\vec{k} $.
\end{example}
\begin{proof}[Solution]
The given vector has length
\[ |2\vec{i}-\vec{j}-2\vec{k}|=\sqrt{2^2+(-1)^2+(-2)^2}=\sqrt{9}=3 \]
so, by Equation \eqref{p658eq4}, the unit vector with the same direction is
\[ \frac{1}{3}(2\vec{i}-\vec{j}-2\vec{k})=\frac{2}{3}\vec{i}-\frac{1}{3}\vec{j}-\frac{2}{3}\vec{k}. \qedhere \]
\end{proof}
 
\subsection{Dot Product}
%
  
\begin{definition}
	The \textbf{dot product} of two nonzero vectors $\vec{a}$ and $\vec{b}$ is the number.
	\[ \vec{a}.\vec{b}=|\vec{a}||\vec{b}|\cos\theta \]
	where $\theta$ is the angle between $\vec{a}$ and $\vec{b}$, $0\leq \theta <\pi$. (So $\theta$ is the smaller angle
	between the vectors when they are drawn with the same initial point.)
	
	If either $\vec{a}$ or $\vec{b}$ is 0, we define $\vec{a}.\vec{b}=0$.
\end{definition}
The result of computing $\vec{a}.\vec{b}$ is not a vector.

It is a real number, that is, a scalar. For this reason, the dot product is sometimes called the \textbf{scalar product}.
 
%
 
Two nonzero vectors $\vec{a}$ and $\vec{b}$ are called \textbf{perpendicular} or \textbf{orthogonal} if the angle between them is $\theta=\pi/2$.

For such vectors we have
\[ \vec{a}.\vec{b}=|\vec{a}||\vec{b}|\cos(\pi/2)=0 \]
conversely if $\vec{a}.\vec{b}=0$, then $\cos \theta=0$, so $\theta=\pi/2$.

The zero vector is considered to be perpendicular to all vectors.

Therefore, two vectors $\vec{a}$ and $\vec{b}$ are orthogonal if and only if $\vec{a}.\vec{b}=0$.
 
%
\subsubsection{The Dot Product in Component Form}
%

Suppose we are given two vectors in component form: $\vec{a}=<a_1,a_2,a_3>$ and $\vec{b}=<b_1,b_2,b_3>$.

The dot product of $\vec{a}$ and $\vec{b}$ is
\[ \vec{a}.\vec{b}=a_1b_1+a_2b_2+a_3b_3 \]
 
%
 
\begin{example}
\begin{align*}
\langle 2,4\rangle\cdot\langle 3,-1\rangle&=2(3)+4(-1)=2\\
\langle -1,7,4\rangle\cdot\langle 6,2,-\frac{1}{2}\rangle&=(-1)6+7(2)+4(-\frac{1}{2})=6\\
(\vec{i}+2\vec{j}-3\vec{k})\cdot(2\vec{j}-\vec{k})&=1(0)+2(2)+(-3)(-1)=7
\end{align*}
\end{example}
 
%
 
\begin{example}
Show that $2\vec{i}+2\vec{j}-\vec{k}$ is perpendicular to $ 5\vec{i}-4\vec{j}+2\vec{k} $.
\end{example}
\begin{proof}[Solution]
	Since
	\[ (2\vec{i}+2\vec{j}-\vec{k})\cdot(5\vec{i}-4\vec{j}+2\vec{k})=2(5)+2(-4)+(-1)2=0 \]
these vectors are perpendicular.
\end{proof}
 
%
 
\textbf{Properties of the Dot Product}\\
	If $\vec{a}$, $\vec{b}$ and $\vec{c}$ are vectors in $V_3$ and $k$ is a scalar, then
	\begin{multicols}{2}
		\begin{enumerate}
			\item $ \vec{a}\cdot\vec{a}=|\vec{a}|^2 $
			\item $ \vec{a}\cdot\vec{b}=\vec{b}\cdot\vec{a} $
			\item $ \vec{0}\cdot\vec{a}=0 $
			\item $ (k\vec{a})\cdot\vec{b}=k(\vec{a}\cdot\vec{b})=\vec{a}\cdot(k\vec{b}) $
			\item $ \vec{a}\cdot(\vec{b}+\vec{c})=\vec{a}\cdot\vec{b}+\vec{a}\cdot\vec{c} $
		\end{enumerate}
	\end{multicols}

 
%
\subsection{The Cross Product}
%
  
The cross product of two vectors $\vec{a}$ and $\vec{b}$, unlike the dot product, is a vector.

For this reason it is also called the \textbf{vector product}. We will see that $\vec{a} \times \vec{b}$ is useful in geometry because it is perpendicular to both $\vec{a}$ and $\vec{b}$.
\begin{definition}
	If $\vec{a}$ and $\vec{b}$ are nonzero three-dimensional vectors, the \textbf{cross product}
	of $\vec{a}$ and $\vec{b}$ is the vector
	\[ \vec{a}\times \vec{b}=(|\vec{a}||\vec{b}|\sin\theta)\vec{n} \]
	where $\theta$ is the angle between $\vec{a}$ and $\vec{b}$, $0\leq \theta <\pi$, and $\vec{n}$ is a unit vector perpendicular
	to both $\vec{a}$ and $\vec{b}$ whose direction is given by the \textbf{right-hand rule.}
\end{definition}
 
%
 
\textbf{right-hand rule:}If the fingers of your right hand curl through the angle $\theta$ from $\vec{a}$ and $\vec{b}$, then your thumb points in the direction of $\vec{n}$.

\begin{center}
	\includegraphics[page=668,clip,trim=20.5mm 141mm 147mm 100.5mm,scale=1.2]{stewart-ccac.pdf}
\end{center}

 
%
 
If either $\vec{a}$ or $\vec{b}$ is $\vec{0}$, then we define to be $\vec{0}$.

Because $\vec{a} \times \vec{b}$ is a scalar multiple of $\vec{n}$, it has the same direction as $\vec{n}$ and so

$\vec{a} \times \vec{b}$ is orthogonal to both $\vec{a}$ and $\vec{b}.$

Two nonzero vectors  are \textbf{parallel} if and only if the angle between them is \emph{$ 0 $ or $\pi$}. In either case, $\sin \theta=0$ and so $\vec{a} \times \vec{b}=0$.


Two nonzero vectors $\vec{a}$ and $\vec{b}$ are parallel if and only if $\vec{a}\times \vec{b}=\vec{0}$.

 
%
 
\begin{example}
Find $\vec{i}\times \vec{j}$ and $\vec{j}\times \vec{i}$.
\end{example}
\begin{proof}[Çözüm]
The standard basis vectors $\vec{i}$ and $\vec{j}$ both have length $ 1 $ and the angle between them is $\pi/2$.

By the right-hand rule, the unit vector perpendicular to $\vec{i}$ and $\vec{j}$ is $\vec{n}=\vec{k}$

\begin{minipage}{.3\textwidth}

\begin{center}
	\includegraphics[page=669,clip,trim=21mm 180.5mm 160.5mm 60mm]{stewart-ccac.pdf}
\end{center}

\end{minipage}
\begin{minipage}{.69\textwidth}
	So,
	\[ \vec{i}\times \vec{j}=(|\vec{i}|| \vec{j}|\sin(\pi/2)\vec{k})=\vec{k}. \]
	But if we apply the right-hand rule to the vectors $\vec{j}$ and $\vec{i}$ (in that order), we see that
	$\vec{n}$ points downward and so $\vec{n}=-\vec{k}$. Thus
	\[ \vec{j} \times \vec{i}=-\vec{k}. \qedhere \]
\end{minipage}
\end{proof}
 
%
 
From previous example we see that
\[ \vec{i}\times \vec{j} \neq \vec{j} \times \vec{i} \]
so the cross product is not commutative. Similar reasoning shows that
\begin{align*}
\vec{j}\times \vec{k}=\vec{i} & \qquad \vec{k}\times \vec{j}=-\vec{i} \\
\vec{k}\times \vec{i}=\vec{j} & \qquad \vec{i}\times \vec{k}=-\vec{j}.
\end{align*}
In general, the right-hand rule shows that
\[\vec{b} \times \vec{a}= - \vec{a} \times \vec{b}.\]
Another algebraic law that fails for the cross product is the associative law for multiplication; that is, in general,
\[ (\vec{a}\times \vec{b})\times \vec{c}\neq \vec{a}\times( \vec{b}\times \vec{c}). \]
 
%
 
\textbf{Properties of the Cross Product}
If $\vec{a}$, $ \vec{b}$ and $\vec{c}$ are vectors and $ c $ is a scalar, then
\begin{enumerate}
\item $ \vec{a}\times \vec{b}=-\vec{b}\times \vec{a} $
\item $ (c\vec{a})\times \vec{b}= c(\vec{a}\times \vec{b})=\vec{a}\times (c\vec{b}) $
\item $ \vec{a}\times (\vec{b}+\vec{c})=\vec{a}\times \vec{b}+\vec{a}\times \vec{c} $
\item $ (\vec{a}+\vec{b})\times\vec{c})=\vec{a}\times \vec{c}+\vec{b}\times \vec{c} $
\end{enumerate}

 
%
\subsubsection{The Cross Product in Component Form}
%

In order to make this expression for $\vec{a}\times \vec{b}$ easier to remember, we use the notation of determinants.

We see that the cross product of $\vec{a}=a_1\vec{i}+a_2\vec{j}+a_3\vec{k}$ and $\vec{b}=b_1\vec{i}+b_2\vec{j}+b_3\vec{k}$ is
\[ \vec{a}\times \vec{b}= 
\begin{vmatrix}
\vec{i} & \vec{j} & \vec{k}\\
a_{1} & a_{2} &a_{3}\\
b_{1} & b_{2} &b_{3}
\end{vmatrix} \]
 
%
 
A determinant of order 3 can be defined in terms of second-order determinants as follows:
\[ \begin{vmatrix}
a_{1} & a_{2} &a_{3}\\
b_{1} & b_{2} &b_{3}\\
c_{1} & c_{2} &c_{3}
\end{vmatrix} = a_1
\begin{vmatrix}
b_{2} &b_{3} \\
c_{2} &c_{3}
\end{vmatrix} +a_2
\begin{vmatrix}
b_{1} &b_{3} \\
c_{1} &c_{3}
\end{vmatrix} + a_3
\begin{vmatrix}
b_{1} &b_{2} \\
c_{1} &c_{2}
\end{vmatrix}. \]
A determinant of order 2 is defined by
\[ \begin{vmatrix}
a & b \\
c & d
\end{vmatrix} = ad-bc. \]
 
%
 
\begin{example}
If $\vec{a}=\langle 1,3,4\rangle$ and $\vec{b}=\langle 2,7,-5\rangle$ then
\begin{align*}
\vec{a}\times \vec{b} & = \begin{vmatrix}
\vec{i} & \vec{j} & \vec{k}\\
1 & 3 &4\\
2 & 7 &-5
\end{vmatrix} \\
& = \begin{vmatrix}
3 &4 \\
7 &-5
\end{vmatrix}\vec{i}- \begin{vmatrix}
1 &4 \\
2 &-5
\end{vmatrix}\vec{j}+ \begin{vmatrix}
1 &3 \\
2 &7
\end{vmatrix} \vec{k} \\
& = (-15-28)\vec{i}-(-5-8)\vec{j}+(7-6)\vec{k}\\
& = -43\vec{i}+13\vec{j}+\vec{k}.
\end{align*}
\end{example}
 

\end{document}