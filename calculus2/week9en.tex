\section{Multiple Integrals}
%
\subsection{Double Integrals over General Regions}
%
\begin{frame}{\insertsubsectionhead}{}
We suppose that $D$ is a bounded region, which means that can be enclosed in a rectangular region $R$ as in Figure. Then, we define a new function with domain by
\begin{equation*}
F(x,y)=
\begin{cases}
f(x,y),&(x,y)\in{}D\\
0,&(x,y)\in{}R\backslash{}D.
\end{cases}
\end{equation*}
\begin{figure}[htb]
  \centering
    \includegraphics[page=854,scale=1.1,clip,trim=140mm 42mm 30mm 202mm]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%%
\begin{frame}{}{}
\begin{definition}{}
If the double integral of $F$ exists over $R$, then we define the \textbf{double integral of $\bm{f}$ over $\bm{D}$} by
\begin{equation}
\iint_{D}f(x,y){d}A=\iint_{R}F(x,y){d}A.\label{s12ss3eqn1}
\end{equation}
\end{definition}
\begin{block}{}
	Definition in \eqref{s12ss3eqn1} makes sense because $R$ is a rectangle and so $\iint_{R}F(x,y){d}A$ has been previously defined. The procedure that we have used is reasonable because the values of $F(x,y)$ are $0$ when $(x,y)$ lies outside $D$ and so they contribute nothing to the integral. This means that it doesn't matter what rectangle $R$ we use as long as it contains $ D $.
\end{block}
\end{frame}
%%
\begin{frame}{}{}
In the case where $f(x,y)\ge 0$, we can still interpret $\iint_{D}f(x,y){d}A$ as the volume of the solid that lies above $D$ and under the surface $z=f(x,y) $(the graph of $f$). You can see that this is reasonable by comparing the graphs of and in Figures and remembering that $\iint_{R}F(x,y){d}A$ is the volume under the graph of $F$.
    \begin{figure}[htb]
      \centering
      \includegraphics[page=855,scale=1.2,clip,trim=21mm 216mm 149mm 31mm]{stewart-ccac.pdf}
      \includegraphics[page=855,scale=1.2,clip,trim=21mm 168mm 149mm 78mm]{stewart-ccac.pdf}
    \end{figure}
\end{frame}
%%
\begin{frame}{}{}
\begin{block}{}
	A plane region $D$ is said to be of \textbf{Type I} if it lies between the graphs of two continuous functions of $x$, that is,
	\begin{equation}
	D=\{(x,y):\ a\leq{}x\leq{}b,\ g_{1}(x)\leq{}y\leq{}g_{2}(x)\},\label{s12ss3eqn2}
	\end{equation}
	where $g_{1}$ and $g_{2}$ are continuous on $[a,b]$.
\end{block}
\begin{figure}[htb]
  \centering
  \includegraphics[page=855,scale=0.8,clip,trim=25mm 96mm 146.5mm 150mm]{stewart-ccac.pdf}
  \includegraphics[page=855,scale=0.8,clip,trim=86mm 96mm 89.5mm 150mm]{stewart-ccac.pdf}
  \includegraphics[page=855,scale=0.8,clip,trim=25mm 96mm 139mm 150mm]{stewart-ccac.pdf}
\end{figure}
\begin{block}{}
If $f$ is continuous on a \textbf{Type I} region $D$ given by \eqref{s12ss3eqn2},
then
\begin{equation}
\iint_{D}f(x,y){d}A
=\int_{a}^{b}\int_{g_{1}(x)}^{g_{2}(x)}f(x,y){d}y{d}x.\label{s12ss3eqn3}
\end{equation}
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{block}{}
	We also consider plane regions of \textbf{Type II}, which can be expressed as
	\begin{equation}
	D=\{(x,y):\ c\leq{}y\leq{}d,\ h_{1}(y)\leq{}x\leq{}h_{2}(y)\},\label{s12ss3eqn4}
	\end{equation}
	where $h_{1}$ and $h_{2}$ are continuous on $[c,d]$.
\end{block}
\begin{figure}[htb]
  \centering
  \includegraphics[page=856,scale=0.95,clip,trim=21mm 166mm 147mm 84mm]{stewart-ccac.pdf}
  \includegraphics[page=856,scale=0.95,clip,trim=21mm 136mm 147mm 117mm]{stewart-ccac.pdf}
\end{figure}
\begin{block}{}
If $f$ is continuous on a \textbf{Type II} region $D$ given by \eqref{s12ss3eqn4}, then
\begin{equation}
\iint_{D}f(x,y){d}A
=\int_{c}^{d}\int_{h_{1}(y)}^{h_{2}(y)}f(x,y){d}x{d}y.\label{s12ss3eqn5}
\end{equation}
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}\label{s12ss3exm1}
Evaluate $\displaystyle\iint_{D}(x+2y){d}A$, where $D$ is the region bounded by the parabolas $y=2x^{2}$ and $y=1+x^{2}$.
\begin{figure}[htb]
	\centering
	\includegraphics[page=856,scale=0.9,clip,trim=21mm 70mm 148mm 164mm]{stewart-ccac.pdf}
\end{figure}
\end{example}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[{Solution}]
The parabolas intersect when $2x^{2}=1+x^{2}$, that is, $x^{2}=1$, so $x=\pm1$. We note that the region $D$, sketched in Figure, is a Type I region but not a Type II region and we can write
\begin{equation*}
D=\{(x,y):\ -1\leq{}x\leq1,\ 2x^{2}\leq{}y\leq1+x^{2}\}.
\end{equation*}
Since the lower boundary is $y=2x^{2}$ and the upper boundary is $y=1+x^{2}$,
\eqref{s12ss3eqn3} gives
\begin{equation*}
\iint_{D}(x+2y){d}A
=\int_{-1}^{1}\int_{2x^{2}}^{1+x^{2}}(x+2y){d}y{d}x
=\frac{32}{15}.\qedhere
\end{equation*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{alertblock}{Note}
When we set up a double integral as in {Example~\ref{s12ss3exm1}}, it is essential to draw a diagram. Often it is helpful to draw a vertical arrow as in Figure. Then the limits of integration for the \textbf{inner integral} can be read from the diagram as follows:\medskip

The arrow starts at the lower boundary $y=g_{1}(x)$, which gives the lower limit in the integral, and the arrow ends at the upper boundary $y=g_{2}(x)$, which gives the upper limit of integration. \medskip

For a Type II region the arrow is drawn horizontally from the left boundary to the right boundary.
\end{alertblock}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Find the volume of the solid that lies under the paraboloid $z=x^{2}+y^{2}$ and above the region $D$ in the $xy$-plane bounded by the line $y=2x$ and the parabola $y=x^{2}$.
\end{example}
\begin{proof}[{Solution~1}]
	\begin{minipage}{.29\textwidth}
		\begin{figure}[htb]
			\centering
			\includegraphics[page=857,scale=0.9,clip,trim=25mm 170mm 154mm 65mm]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.69\textwidth}
		From Figure, we see that is a Type I region and
		\begin{equation*}
		D=\{(x,y):\ 0\leq{}x\leq2,\ x^{2}\leq{}y\leq{}2x\}.
		\end{equation*}
		Therefore, the volume under $z=x^{2}+y^{2}$ and above $D$ is
		\begin{align*}
		V&=\iint_{D}\left(x^{2}+y^{2}\right){d}A\\
		&=\int_{0}^{2}\int_{x^{2}}^{2x}\left(x^{2}+y^{2}\right){d}y{d}x
		=\frac{216}{35}.\qedhere
		\end{align*}
	\end{minipage}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution~2]
	\begin{minipage}{.29\textwidth}
		\begin{figure}[htb]
			\centering
			\includegraphics[page=857,scale=0.9,clip,trim=25mm 106mm 154mm 129mm]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.69\textwidth}
		From Figure, we see that is a Type II region and
		\begin{equation}
		D=\biggl\{(x,y):\ 0\leq{}y\leq4,\ \frac{1}{2}y\leq{}x\leq\sqrt{y}\biggr\}.\nonumber
		\end{equation}
		Therefore, another expression for $V$ is
		\begin{align*}
		V&=\iint_{D}\left(x^{2}+y^{2}\right){d}A\\
		&=\int_{0}^{4}\int_{\frac{1}{2}y}^{\sqrt{y}}\left(x^{2}+y^{2}\right){d}x{d}y
		=\frac{216}{35}.\qedhere
		\end{align*}
	\end{minipage}
\end{proof}
\end{frame}
%%
\subsubsection{Properties of Double Integrals}
%
\begin{frame}{\insertsubsubsectionhead}{}
\begin{block}{Properties of Double Integrals}
Assume that all of the following integrals exist.
\begin{enumerate}\itemsep6pt
\item $\iint_{D}[f(x,y)+g(x,y)]{d}A=\iint_{D}f(x,y){d}A+\iint_{D}g(x,y){d}A$.
\item $\iint_{D} cf(x,y) dA=c\iint_{D} f(x,y)dA$.
\item If $f(x,y)\geq{}g(x,y)$ for all $(x,y)\in D$, then $\iint_{D}f(x,y){d}A\ge \iint_{D}g(x,y){d}A$.
\item If $D=D_{1}\cup D_{2}$, where $D_{1}$ and $D_{2}$ don't overlap except perhaps on their boundaries, then $\iint_{D}f(x,y){d}A=\iint_{D_{1}}f(x,y){d}A+\iint_{D_{2}}f(x,y){d}A$.
\item $\iint_{D} 1 dA=\text{A}(D)$, where $\text{A}(D)$ denotes the area of $D$.
\item If $m\le f(x,y)\le M$ for all $(x,y)\in D$, then $m\text{A}(D)\le \iint_{D}f(x,y){d}A\le M\text{A}(D)$.
\end{enumerate}
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Evaluate $\iint_{D}xy{d}A$, where $D$ is the region bounded by the line $y=x-1$ and the parabola $y^{2}=2x+6$.
\end{example}
\begin{proof}[{Solution}] \let \qed \relax
The region is shown in Figure. Again, $D$ is both Type I and Type II, but the description of as a Type I region is more complicated because the lower boundary consists of two parts. Therefore, we prefer to express as a Type II
region: $D=\bigl\{(x,y):\ -2\leq{}y\leq4,\ \frac{1}{2}y^{2}-3\leq{}x\leq{}y+1\bigr\}$.
\begin{figure}[htb]
  \centering
  \includegraphics[page=858,scale=0.8,clip,trim=77mm 165mm 87mm 71.5mm]{stewart-ccac.pdf}
  \includegraphics[page=858,scale=0.8,clip,trim=137mm 165mm 26mm 71mm]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]
	Then, we compute
	\begin{equation*}
	\iint_{D}xy{d}A
	=\int_{-2}^{4}\int_{\frac{1}{2}y^{2}-3}^{y+1}xy{d}x{d}y
	=36.
	\end{equation*}
	If we had expressed as a Type I region using Figure, then we would have obtained
	\begin{equation*}
	\iint_{D}xy{d}A
	=\int_{-3}^{-1}\int_{-\sqrt{2x+6}}^{\sqrt{2x+6}}xy{d}y{d}x
	+\int_{-1}^{5}\int_{x-1}^{\sqrt{2x+6}}xy{d}y{d}x
	\end{equation*}
	but this would have involved more work than the other method.
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Find the volume of the tetrahedron bounded by the planes $x+2y+z=2$, $x=2y$, $x=0$, and $z=0$.
\end{example}
\begin{proof}[{Solution}]\let \qed \relax
\begin{minipage}{.59\textwidth}
	In a question such as this, it's wise to draw two diagrams: one of the three-dimensional solid and another of the plane region $D$ over which it lies.
	
	Figure shows the tetrahedron $T$ bounded by the coordinate planes $x=0$, $z=0$, the vertical plane $x=2y$, and the plane $x+2y+z=2$.	
\end{minipage}
\begin{minipage}{.39\textwidth}
	\begin{figure}[htb]
		\centering
		\includegraphics[page=858,scale=1,clip,trim=20mm 37mm 151mm 189mm]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]
	\begin{minipage}{.39\textwidth}
		\begin{figure}[htb]
			\centering
			\includegraphics[page=859,scale=0.8,clip,trim=28mm 212mm 145.5mm 32mm]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.59\textwidth}
		Since the plane $x+2y+z=2$ intersects the $xy$-plane (whose equation is $z=0$) in the line $x+y=2$, we see that $T$  lies above the triangular region $D$ in the $xy$-plane bounded by the lines $x=2y$, $x+2y=2$, and $x=0$.
	\end{minipage}
The plane $x+2y+z=2$ can be written as $z=2-x-2y$, so the required volume lies under the graph of the function $z=2-x-2y$ and above $D=\bigl\{(x,y):\ 0\leq{}x\leq{}1,\ \frac{1}{2}x\leq{}y\leq1-\frac{1}{2}x\bigr\}$. Therefore,
\begin{equation*}
V=\iint_{D}(2-x-2y){d}A
=\int_{0}^{1}\int_{\frac{x}{2}}^{1-\frac{x}{2}}(2-x-2y){d}y{d}x
=\frac{1}{3}. \qedhere
\end{equation*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Evaluate the iterated integral $\displaystyle\int_{0}^{1}\int_{x}^{1}\sin(y^{2}){d}y{d}x$.
\end{example}
\begin{proof}[{Solution}]\let \qed \relax
If we try to evaluate the integral as it stands, we are faced with the task of first evaluating $\int{}\sin(y^{2}){d}y$. But it is not possible to do so in finite terms since is not an elementary function. Therefore, we must change the order of integration. This is accomplished by first expressing the given iterated integral as a double integral.
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]\let \qed \relax
\begin{figure}[htb]
  \centering
  \includegraphics[page=859,scale=0.9,clip,trim=26mm 127mm 151mm 117mm]{stewart-ccac.pdf}
\end{figure}
Using \eqref{s12ss3eqn3} backwards, we have
\begin{equation*}
\int_{0}^{1}\int_{x}^{1}\sin(y^{2}){d}y{d}x
=\iint_{D}\sin(y^{2}){d}A,
\end{equation*}
where $D=\{(x,y):\ 0\leq{}x\leq1,\ x\leq{}y\leq1\}$.
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]
\begin{minipage}{.5\textwidth}
	Then, from Figure, we see that an alternative description of $D$ is
	\begin{equation*}
	D=\{(x,y):\ 0\leq{}y\leq1,\ 0\leq{}x\leq{}y\}.
	\end{equation*}
\end{minipage}
\begin{minipage}{.49\textwidth}
	\begin{figure}[htb]
		\centering
		\includegraphics[page=859,scale=0.9,clip,trim=23.5mm 70mm 151mm 173mm]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}

This enables us to use \eqref{s12ss3eqn5} to express the double integral as an iterated integral in the reverse order:
\begin{equation*}
\iint_{D}\sin(y^{2}){d}A
=\int_{0}^{1}\int_{0}^{y}\sin(y^{2}){d}x{d}y
=\frac{1}{2}\left(1-\cos(1)\right).\qedhere
\end{equation*}
\end{proof}
\end{frame}
%%
\subsubsection{Polar Coordinates}
%
\begin{frame}{\insertsubsubsectionhead}{}
Polar coordinates offer an alternative way of locating points in a plane.
They are useful because, for certain types of regions and curves,
polar coordinates provide very simple descriptions and equations.
The principal applications of this idea occur in multi-variable calculus:
the evaluation of double integrals and the derivation of Kepler's laws of planetary motion.
\end{frame}
%
\subsubsection{Curves in Polar Coordinates}
%
\begin{frame}{\insertsubsubsectionhead}{}
The connection between polar and Cartesian coordinates can be seen from Figure, in which the pole corresponds to the origin and the polar axis coincides with the positive $x$-axis.
\begin{figure}[htb]
  \centering
  \includegraphics[page=1059,scale=1.2,clip,trim=21mm 99mm 149mm 148mm]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{block}{}
	If the point has Cartesian coordinates $(x,y)$ and polar coordinates $(r,\theta)$, then
	\begin{equation}
	x=r\cos(\theta) \quad\text{and}\quad y=r\sin(\theta).\label{s12ss3sss3eqn1}
	\end{equation}
	From \eqref{s12ss3sss3eqn1}, we can find the Cartesian coordinates of a point when the polar coordinates are known. To find $r$ and $\theta$ when $x$ and $y$ are known, we use
	\begin{equation}
	x^{2}+y^{2}=r^{2} \quad\text{and}\quad \tan(\theta)=\frac{y}{x}.\label{s12ss3sss3eqn2}
	\end{equation}
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Plot the points whose polar coordinates are given.
\begin{multicols}{4}
	\begin{enumerate}
		\item $(1,\frac{5\pi}{4})$
		\item $(2,3\pi)$
		\item $(2,-\frac{2\pi}{3})$
		\item $(-3,\frac{3\pi}{4})$\\
	\end{enumerate}
\end{multicols}
\end{example}
\begin{proof}[{Solution}]
The points are plotted in Figures.
In part {\usebeamercolor[fg]{structure}4}, the point is located three units from the pole in the fourth quadrant because the angle is in the second quadrant and is negative. \qedhere
\begin{figure}[htb]
  \centering
  \includegraphics[page=1059,scale=0.8,clip,trim=21mm 214mm 161mm 45mm]{stewart-ccac.pdf}
  \includegraphics[page=1059,scale=0.8,clip,trim=67mm 205mm 113mm 45mm]{stewart-ccac.pdf}
  \includegraphics[page=1059,scale=0.8,clip,trim=109mm 205mm 69mm 45mm]{stewart-ccac.pdf}
  \includegraphics[page=1059,scale=0.8,clip,trim=159mm 205mm 26mm 45mm]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Convert the point $(2,\frac{2\pi}{3})$ from polar to Cartesian coordinates.
\end{example}
\begin{proof}[{Solution}]
Since $r=2$ and $\theta=\frac{\pi}{3}$, \eqref{s12ss3sss3eqn1} gives
\begin{align*}
x=&r\cos(\theta)=2\cos\left(\frac{\pi}{3}\right)=2\cdot\frac{1}{2}=1,\\
y=&r\sin(\theta)=2\sin\left(\frac{\pi}{3}\right)=2\cdot\frac{\sqrt{3}}{2}=\sqrt{3}.
\end{align*}
Therefore, the point is $\left(1,\sqrt{3}\right)$ in Cartesian coordinates.
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}\label{s12ss3sss3exm3}
Represent the point with Cartesian coordinates $(1,-1)$ in terms of polar coordinates.
\end{example}
\begin{proof}[{Solution}]
If we choose $r$ to be positive, then \eqref{s12ss3sss3eqn2} gives
\begin{align*}
r=&\sqrt{x^{2}+y^{2}}=\sqrt{1^{2}+(-1)^{2}}=\sqrt{2},\\
\tan(\theta)=&\frac{y}{x}=\frac{(-1)}{1}=-1.
\end{align*}
Since the point $(1,-1)$ lies in the fourth quadrant, we can choose $\theta=-\frac{\pi}{4}$ or $\theta=\frac{7\pi}{4}$. Thus, one possible answer is $\left(\sqrt{2},-\frac{\pi}{4}\right)$, another is $\left(\sqrt{2},\frac{7\pi}{4}\right)$.
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{alertblock}{Remark}
\eqref{s12ss3sss3eqn2} does not uniquely determine $\theta$ when $x$ and $y$ are given because, as $\theta$ increases through the interval $0\leq\theta<2\pi$, each value of $\tan(\theta)$ occurs twice. Therefore, in converting from Cartesian to polar coordinates,
it's not good enough just to find $r$ and $\theta$ that satisfy \eqref{s12ss3sss3eqn2}. As in {Example~\ref{s12ss3sss3exm3}}, we must choose $\theta$ so that the point $(r,\theta)$ lies in the correct quadrant.
\end{alertblock}
\begin{block}{}
	The \textbf{graph of a polar equation} $r=f(\theta)$, or more generally $F(r,\theta)=0$, consists of all points $P$ that have at least one polar representation $(r,\theta)$ whose coordinates satisfy the equation.
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
What curve is represented by the polar equation $r=2$?
\end{example}
\begin{proof}[{Solution}]
The curve consists of all points $(r,\theta)$ with $r=2$. Since $r$ represents the distance from the point to the pole, the curve $r=2$ represents the circle with center $O$ and radius $2$. In general, the equation $r=a$ represents a circle with center $O$ and radius $|a|$. \qedhere
\begin{figure}[htb]
  \centering
  \includegraphics[page=1060,scale=0.9,clip,trim=111mm 53mm 59mm 184mm]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Sketch the polar curve $\theta=1$.
\end{example}
\begin{proof}[{Solution}]
This curve consists of all points $(r,\theta)$ such that the polar angle $\theta$ is $1$ radian. It is the straight line that passes through $O$ and makes an angle of $1$ radian with the polar axis. Notice that the points $(r,1)$ on the line with $r>0$ are in the first quadrant, whereas those with $r<0$ are in the third quadrant. \qedhere
\begin{figure}[htb]
  \centering
  \includegraphics[page=1061,scale=0.75,clip,trim=21mm 205mm 150mm 31mm]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%%
\subsection{Double Integrals in Polar Coordinates}
%
\begin{frame}{\insertsubsectionhead}{}
Suppose that we want to evaluate a double integral $\iint_{R}f(x,y){d}A$, where $R$ is one of the regions shown in Figure. In either case the description of in terms of rectangular coordinates is rather complicated but $R$ is easily described using polar coordinates.
\begin{figure}[htb]
  \centering
  \includegraphics[page=863,scale=0.95,clip,trim=88mm 184mm 92mm 66mm]{stewart-ccac.pdf}
  \includegraphics[page=863,scale=0.9,clip,trim=137mm 180mm 28mm 66mm]{stewart-ccac.pdf}
\end{figure}
Recall that the polar coordinates of a point are related to the rectangular coordinates by the equations $x^{2}+y^{2}=r^{2}$, $x=r\cos(\theta)$ and $y=r\sin(\theta)$. The regions in Figure are special cases of a \textbf{polar rectangle}
\begin{equation*}
R=\{(x,y): a\le r\le b, \alpha\le \theta\le \beta\}.
\end{equation*}
\end{frame}
%%
\begin{frame}{}{}
\begin{block}{Change to Polar Coordinates in a Double Integral}
If $f$ is continuous on a polar rectangle $R$ given by $0\leq{}a\leq{}r\leq{}b$, $\alpha\leq\theta\leq\beta$, where $0\leq\beta-\alpha\leq2\pi$, then
\begin{equation}
\iint_{R}f(x,y){d}A=\int_{\alpha}^{\beta}\int_{a}^{b}f\left(r\cos(\theta),r\sin(\theta)\right)r{d}r{d}\theta.\label{s12ss4eqn1}
\end{equation}
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Evaluate $\iint_{R}(3x+4y^{2}){d}A$, where $R$ is the region in the upper half-plane bounded by the circles $x^{2}+y^{2}=1$ and $x^{2}+y^{2}=4$.
\end{example}
\begin{proof}[{Solution}]
The region $R$ can be described as $R=\{(x,y):\ y\geq0,\ 1\leq{}x^{2}+y^{2}\leq4\}$. It is the half-ring, $R=\{(r,\theta):\ 1\leq{}r\leq2,\ 0\leq\theta\leq\pi\}$. Therefore, by \eqref{s12ss4eqn1},
\begin{equation*}
\iint_{R}(3x+4y^{2}){d}A
=\int_{0}^{\pi}\int_{1}^{2}\left(3r\cos(\theta)+4r^{2}\sin^{2}(\theta)\right)r{d}r{d}\theta
=\frac{15\pi}{2}.\qedhere
\end{equation*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Find the volume of the solid bounded by the plane $z=0$ and the paraboloid $z=1-x^{2}-y^{2}$.
\end{example}
\begin{proof}[{Solution}] \let \qed \relax
If we put $z=0$ in the equation of the paraboloid, we get $x^{2}+y^{2}=1$. This means that the plane intersects the paraboloid in the circle $x^{2}+y^{2}=1$, so the solid lies under the paraboloid and above the circular disk $D$ given by $x^{2}+y^{2}\leq1$.
\begin{figure}[htb]
  \centering
  \includegraphics[page=865,scale=0.9,clip,trim=21mm 80mm 156mm 167mm]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	Since $z=1-r^{2}$ and $D=\{(r,\theta):\ 0\leq{}r\leq1,\ 0\leq\theta\leq2\pi\}$,
	the volume is:
	\begin{equation*}
	V=\iint_{D}\left(1-x^{2}-y^{2}\right){d}A
	=\int_{0}^{2\pi}\int_{0}^{1}\left(1-r^{2}\right)r{d}r{d}\theta
	=\frac{\pi}{2}.\qedhere
	\end{equation*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{minipage}{.6\textwidth}
	What we have done so far can be extended to the more complicated type of region shown in Figure. It's similar to the Type II rectangular regions considered in previous sections.
\end{minipage}
\begin{minipage}{.39\textwidth}
	\begin{figure}[htb]
		\centering
		\includegraphics[page=866,scale=1,clip,trim=23mm 206mm 147mm 37mm]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\begin{block}{}
If $f$ is continuous on a polar region of the form
\begin{equation*}
D=\{(r,\theta):\ \alpha\leq\theta\leq\beta,\ h_{1}(\theta)\leq{}r\leq{}h_{2}(\theta)\},\nonumber
\end{equation*}
then
\begin{equation}\label{eq:formula3}
\iint_{D}f(x,y){d}A=\int_{\alpha}^{\beta}\int_{h_{1}(\theta)}^{h_{2}(\theta)}f\bigl(r\cos(\theta),r\sin(\theta)\bigr)r{d}r{d}\theta.
\end{equation}
\end{block}
\end{frame}
%
\begin{frame}{}
\begin{example}
	Find the volume of the solid that lies under the paraboloid $ z = x^2 + y^2 $, above the $ xy $-plane, and inside the cylinder $ x^2 + y^2 = 2x $.
\end{example}
\begin{proof}[Solution]\let \qed\relax
	\begin{minipage}{.35\textwidth}
		\begin{figure}
			\includegraphics[page=866,scale=1,clip,trim=30mm 103mm 147mm 142.5mm]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.64\textwidth}
		The solid lies above the disk $ D $ whose boundary circle has equation $ x^2 + y^2 = 2x $ or, after completing the square,
		\[ (x-1)^2 + y^2 = 1. \]
	\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}{}
\begin{proof}[Solution (cont.)]
	In polar coordinates we have $  x^2 + y^2 = r^2 $ and $ x = r \cos \theta $ so the boundary circle becomes $ r^2 = 2r\cos \theta $, or $ r = 2\cos \theta $. Thus, the disk $ D $ is given by
	\[ D = \left\{ (r,\theta) \vert -\pi/2 \le \theta \le \pi/2, 0 \le r \le 2 \cos \theta \right\} \]
	\begin{minipage}{.4\textwidth}
		\begin{figure}
			\includegraphics[page=866,scale=1.1,clip,trim=20.5mm 39mm 148.5mm 193.5mm]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.59\textwidth}
		and, by Formula \eqref{eq:formula3}, we have
		\begin{align*}
		V & = 8 \iint_{D} (x^2+y^2)dA \\
		& = \int_{-\pi/2}^{\pi/2} \int_{0}^{2\cos \theta} r^2 r dr d\theta\\
		& = \int_{-\pi/2}^{\pi/2} \left[\frac{r^4}{4}\right]_{0}^{2\cos \theta} dr d\theta\\
		& = 4\int_{-\pi/2}^{\pi/2} \cos^4\theta d\theta = \frac{3\pi}{2}. \qedhere
		\end{align*}
	\end{minipage}
\end{proof}
\end{frame}