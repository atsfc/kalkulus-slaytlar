\section{Infinite Sequences and Series}
\subsection{Sequences}

\begin{frame}{\insertsectionhead}
\begin{block}{}
	A \textbf{sequence} can be thought of as a list of numbers written in a definite order:
	\[ a_1,a_2,a_3,\ldots,a_n,\ldots. \]
\end{block}
For example, in the sequence
\[ 2,4,6,8,10,\ldots,2n,\ldots \]
$a_1=2$ is called the \textbf{first term}, $a_2=4$ is the \textbf{second term} and in general $a_n=2n$ is the $n$th term.
\end{frame}


\begin{frame}
\begin{block}{}
	We will deal exclusively with infinite sequences.

	So each term $a_n$ will have a successor $a_{n+1}.$
\end{block}
\begin{block}{}
	Notice that for every positive integer $n$ there is a corresponding number $a_{n}$ and so a sequence can be defined as a function whose domain is the set of positive integers ($ \mathds{N}^+ $).

	But we usually write $ a_n $ instead of the function notation $ f(n) $ for the value of the function at the number $n.$
\end{block}
\begin{block}{}
	The sequence $\{a_{1},a_{2},\cdots,a_{n},\cdots\}$ is also denoted by $ \{a_n\} \text{ or } \{a_n\}_{n=1}^{\infty} $.
\end{block}
\end{frame}


\begin{frame}
\begin{example}
In the following examples we give three description of sequences: one by using the preceding notation, another by using defining formula, and  a third by writing out the terms of the sequence.
{\small
\begin{align*}
&\left\{\frac{n}{n+1}\right\}_{n=1}^{\infty} && a_{n}=\frac{n}{n+1} && \left\{\frac{1}{2},\frac{2}{3},\frac{3}{4},\cdots,\frac{n}{n+1},\cdots\right\} \\
&\left\{(-1)^{n}\frac{n}{3^{n}}\right\}_{n=1}^{\infty} && a_{n}=(-1)^{n}\frac{n}{3^{n}} &&\left\{-\frac{1}{3},\frac{2}{9},-\frac{3}{27},\cdots,(-1)^{n}\frac{n}{3^{n}},\cdots\right\} \\
&\bigl\{\sqrt{n-3}\bigr\}_{n=3}^{\infty} && a_{n}=\sqrt{n-3}, n\ge 3 && \left\{0,1,\sqrt{2},\cdots,\sqrt{n-3},\cdots\right\} \\
&\left\{\cos\bigl(\frac{n\pi}{6}\bigr)\right\}_{n=0}^{\infty} && a_{n}=\cos\bigl(\frac{n\pi}{6}\bigr), n\geq0 && \left\{1,\frac{\sqrt{3}}{2},\frac{1}{2},\cdots,\cos\bigl(\frac{n\pi}{6}\bigr),\cdots\right\}
\end{align*}}
\textbf{Notice that $\bm{n}$ doesn't have to start at 1.}
\end{example}
\end{frame}

\begin{frame}
A sequence such as $\displaystyle a_n=\frac{n}{n+1},$ can be pictured either by plotting its terms on a number line, or by plotting its graph as in the following Figure
\begin{figure}[htb]
\includegraphics[page=564,scale=1.1,clip,trim=75mm 115mm 92mm 151mm]{stewart-ccac.pdf}
\includegraphics[page=564,scale=1.1,clip,trim=138mm 115mm 30mm 132mm]{stewart-ccac.pdf}
\end{figure}
From Figure it appears that the terms of the sequence $ a_n=\dfrac{n}{n+1} $ are approaching $ 1 $ as $ n $ becomes large.
\end{frame}

\begin{frame}
In fact, the difference
\[ 1-\frac{n}{n+1}=\frac{1}{n+1} \]
can be made as small as we like by taking $ n $ sufficiently large. We indicate this by
writing
\[ \lim_{n\to \infty}\frac{n}{n+1}=1. \]
\begin{block}{}
	In general, the notation
	\[ \lim_{n\to \infty}a_n=L \]
	means that the terms of the sequence $ \{a_n\} $ approach $ L $ as $ n $ becomes large.
\end{block}
\end{frame}

\begin{frame}
\begin{definition}
A sequence $ \{a_n\} $ has the \textbf{limit} $ L $ and we write
\[ \lim_{n\to\infty} a_n=L\quad\text{or}\quad a_n\to L\text{ as }n\to\infty \]
if we can make the terms $ a_n $ as close to $ L $ as we like by taking $ n $ sufficiently large. If $ \lim\limits_{n\to\infty} a_n $ exists, we say the sequence \textbf{converges} (or is \textbf{convergent}). Otherwise, we say the sequence \textbf{diverges} (or is \textbf{divergent}).
\end{definition}
The following Figures illustrates the Definition by showing the graphs of two sequences with $\lim\limits_{n\to\infty}a_{n}=L$.
\begin{figure}
	\includegraphics[page=565,scale=.9,clip,trim=54mm 170mm 100.5mm 84mm]{stewart-ccac.pdf}\quad
	\includegraphics[page=565,scale=.9,clip,trim=127mm 170mm 21mm 84mm]{stewart-ccac.pdf}
\end{figure}
\end{frame}

\begin{frame}
If you compare this Definition with definition of the limit of functions you will see that the only difference between $ \lim\limits_{n\to\infty} a_n=L $ and $ \lim\limits_{x\to\infty} f(x)=L $ is that $ n $ is required to be an integer.
\begin{figure}[h]
	\centering
	\includegraphics[page=565,scale=1.1,clip,trim=82mm 96mm 25mm 157mm]{stewart-ccac.pdf}
\end{figure}
\begin{theorem}
If $ \lim\limits_{x\to\infty} f(x)=L $ and $ f(n)=a_n $ when  $ n $ is an integer, then $ \lim\limits_{n\to\infty} a_n=L $.
\end{theorem}
\end{frame}


\begin{frame}
In particular, since we know that $ \lim\limits_{x\to\infty} \dfrac{1}{x^{r}}=0 $ when $ r>0, $ we have
\begin{equation*}
\lim\limits_{n\to\infty} \frac{1}{n^{r}}=0\quad \text{if } r>0.
\end{equation*}
\begin{block}{}
If  $ a_n $ becomes large as $ n$  becomes large, we use the notation
\[ \lim\limits_{n\to\infty} a_n=\infty. \]
In this case the sequence $ \{a_n\} $ is divergent, but in a special way. We say that $ \{a_n\} $ diverges to $ \infty $.	
\end{block}
\end{frame}
%
%
%
\begin{frame}
\begin{block}{The Limit Laws for convergent sequences:}
If $ \{a_n\} $ and $ \{b_n\} $ are convergent sequences and $ c $ is a constant, then
\begin{itemize}
\item $ \lim\limits_{n\to\infty} (a_n\pm b_n)=\lim\limits_{n\to\infty} a_n\pm\lim\limits_{n\to\infty} b_n $
\item $ \lim\limits_{n\to\infty} (ca_n)=c\lim\limits_{n\to\infty} a_n $
\item $ \lim\limits_{n\to\infty} (a_n b_n)=\lim\limits_{n\to\infty} a_n\cdot \lim\limits_{n\to\infty} b_n $
\item $ \lim\limits_{n\to\infty} \dfrac{a_n}{b_n}=\dfrac{\lim\limits_{n\to\infty} a_n}{\lim\limits_{n\to\infty} b_n} \quad  $ (if $ \lim\limits_{n\to\infty} b_n\neq 0 $)
\item $ \lim\limits_{n\to\infty} a_n^{p}=\left[\lim\limits_{n\to\infty} a_n\right]^{p} \quad $ (if $ p>0 $ and $ a_n>0 $)
\end{itemize}
\end{block} 
\end{frame}

\begin{frame}
\begin{figure}
	\includegraphics[page=566,scale=1.4,clip,trim=23mm 113mm 147mm 133mm]{stewart-ccac.pdf}
\end{figure}
The Squeeze Theorem can also be adapted for sequences as follows
\begin{theorem}\label{squeezethm}
If $ a_n\le  b_n\le c_n $ for $ n\ge n_0 $ and $ \lim\limits_{n\to\infty} a_n=\lim\limits_{n\to\infty} c_n=L $,
then
\[ \lim\limits_{n\to\infty} b_n=L. \]
\end{theorem}
\end{frame}

\begin{frame}
Another useful fact about limits of sequences is given by the following theorem, which follows from the Squeeze Theorem, since we have $-|a_{n}|\leq{}a_{n}\leq|a_{n}|$.
\begin{theorem}\label{p566thm4}
If $ \lim\limits_{n\to\infty} |a_n| = 0 $ then $ \lim\limits_{n\to\infty} a_n = 0. $
\end{theorem}
\end{frame}



\begin{frame}
\begin{example}
Find $ \lim\limits_{n\to\infty} \dfrac{n}{n+1}$.
\end{example}
\begin{proof}[Solution]
 Divide numerator and denominator by the highest power of $ n $ that occurs in the denominator and then use the Limit Laws.
\begin{align*}
\lim\limits_{n\to\infty} \dfrac{n}{n+1}&= \lim\limits_{n\to\infty} \frac{\cancelto{1}{n}}{\cancel{n}\left(1+\frac{1}{n}\right)}\\
&=\frac{\lim\limits_{n\to\infty} 1}{\lim\limits_{n\to\infty} 1 + \lim\limits_{n\to\infty} \frac{1}{n}}\\
&=\dfrac{1}{1+0}=1 \qedhere
\end{align*}
\end{proof}
\end{frame}
%
%
%
\begin{frame}
\begin{example}
 Calculate $ \lim\limits_{n\to\infty} \dfrac{\ln n}{n} $
\end{example}
 \begin{proof}[Solution]
Notice that both numerator and denominator approach infinity as $ n\to\infty $.
 We can’t apply l’Hospital’s Rule directly because it applies not to sequences but to functions of a real variable.
 However, we can apply l’Hospital’s Rule to the related function $ f(x)=\ln x/x $ and obtain
 \[ \lim\limits_{x\to\infty} \frac{\ln x}{x}=\lim\limits_{x\to\infty} \frac{1/x}{1}=0\]
 Therefore, we have
 \[ \lim\limits_{n\to\infty} \frac{\ln n}{n}=0 \qedhere \]
 \end{proof}
\end{frame}
%

\begin{frame}
\begin{example}
Determine whether the sequence $ \{a_n\}=\{(-1)^{n}\} $ is convergent or divergent.
\end{example}
\begin{proof}[Solution]
 If we write out the terms of the sequence, we obtain
\[ \{-1,1,-1,1,-1,1,-1,1,-1,\ldots\} \]
Since the terms oscillate between $ 1 $ and $ -1 $ infinitely often, $ a_n $ does not approach any number. Thus, $ \lim\limits_{n\to\infty} (-1)^{n} $ does not exist; that is, the sequence $ \{(-1)^{n}\} $ is divergent.
\end{proof}
\end{frame}
%
%
%
\begin{frame}
\begin{example}
Evaluate the limit of the sequence $ \{a_n\}=\left\{\dfrac{(-1)^{n}}{n}\right\} $ if it exists.
\end{example}
\begin{proof}[Solution]
\[ \lim\limits_{n\to\infty} \left|\frac{(-1)^{n}}{n}\right| = \lim\limits_{n\to\infty} \frac{1}{n}=0 \]
Therefore, by Theorem \ref{p566thm4},
\[ \lim\limits_{n\to\infty} \frac{(-1)^{n}}{n}=0. \qedhere \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
For what values of $r$ is the sequence $\{r^{n}\}$ convergent?
\end{example}
\begin{proof}[Solution]\let \qed \relax
We know that $\lim\limits_{x\to\infty}a^{x}=\infty$ for $a>1$ and $\lim\limits_{x\to\infty}a^{x}=0$ for $0<a<1$. Therefore, putting $a=r$ and using Theorem \ref{squeezethm}, we have
\begin{equation*}
\lim_{n\to\infty}r^{n}=
\begin{cases}
\infty,&r>1\\
0,&0<r<1
\end{cases}
\end{equation*}
For the cases $r=1$ and $r=0$ we have
\begin{equation*}
\lim_{n\to\infty}1^{n}=\lim_{n\to\infty}1=1\quad\text{and}\quad\lim_{n\to\infty}0^{n}=\lim_{n\to\infty}0=0.
\end{equation*}
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution(cont.)]
If $-1<r<0$ and $0<|r|<1$, so
\begin{equation}
\lim_{n\to\infty}\bigl|r^{n}\bigr|
=\lim_{n\to\infty}|r|^{n}=0\nonumber
\end{equation}
and therefore $\lim\limits_{n\to\infty}r^{n}=0$ by {Theorem \ref{p566thm4}}.
If $r<-1$ or $ r=-1 $,then $\{r^{n}\}$ diverges.
Figure shows the graphs for various values of $r$. \qedhere
\begin{figure}[htb]
 \centering
 \includegraphics[page=568,scale=1.1,clip,trim=78mm 98mm 87mm 141mm]{stewart-ccac.pdf} \includegraphics[page=568,scale=1.1,clip,trim=138mm 98mm 28mm 141mm]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}

%%
%
\begin{frame}
\begin{block}{}
The sequence $\{r^{n}\}$ is convergent if $-1<r\leq1$
and divergent for all other values of $r$.
\begin{equation*}
\lim_{n\to\infty}r^{n}
=
\begin{cases}
0,&-1<r<1\\
1,&r=1
\end{cases}.
\end{equation*}
\end{block}
\end{frame}

\begin{frame}
\begin{definition}
A sequence $ \{a_n\} $ is called \textbf{increasing} if $ a_n<a_{n+1} $ for all $ n\geq 1 ,$ that is,
\[ a_1<a_2<a_3<\cdots \]
It is called \textbf{decreasing} if $ a_n>a_{n+1} $ for all  $ n\geq 1 .$ 

It is called \textbf{monotonic} if it is either increasing or decreasing.
\end{definition}
\begin{example}
	The sequence $ \left\{\dfrac{3}{n+5}\right\} $ is decreasing because
	\[ a_n=\frac{3}{n+5},\quad a_{n+1}=\dfrac{3}{(n+1)+5}=\frac{3}{n+6} \]
	
	for all $ n\ge 1.$ (The right side is smaller because it has a larger denominator.)
\end{example}
\end{frame}
%
%
\begin{frame}
\begin{example}
Show that the sequence $ \left\{\dfrac{n}{n^2+1}\right\} $ is decreasing.
\end{example}
\begin{proof}[Solution]
Consider the function $ f(x)=\dfrac{x}{x^2+1} $. If we use first derivative test we have
\[ f'(x)=\frac{1\cdot (x^2+1)-x\cdot 2x}{(x^2+1)^2}=\frac{1-x^2}{(x^2+1)^2}. \]
Since  $ f'(x)<0 $ when $ x\geq 1 $ the function $ f(x) $ is decreasing for $ x\geq 1. $ $ a_n=f(n) $ implies that the sequence $ \left\{ a_n \right\} $ is also decreasing.
\end{proof}
\end{frame}
%
%
%
\begin{frame}
\begin{definition}
A sequence $ \{a_n\} $ is \textbf{bounded above} if there is a number $ M $ such that
\[ a_n\leq M \text{ for all } n\geq 1 .\]
It is \textbf{bounded below} if there is a number $ m $ such that
\[ a_n\geq m \text{ for all } n\geq 1 . \]
If it is bounded above and below, then $ \{a_n\} $ is a \textbf{bounded sequence}.
\end{definition}
For instance, the sequence $ \{a_n\}=\big\{n\big\}_{n=1}^{\infty} $ is bounded below $ (a_n>0) $ but not above. \\\medskip
The sequence $ \{a_n\}=\left\{\dfrac{n}{n+1}\right\} $ an is bounded because $ 0< a_n< 1 $ for all $ n. $
\end{frame}
%
%
\begin{frame}
\begin{theorem}[Monotonic Sequence Theorem]
Every bounded, monotonic sequence is convergent.
\end{theorem}
\end{frame}
%
%
%
\begin{frame}
\begin{example}
 Investigate the sequence $a_n=\dfrac{2^n}{3^{n+1}}.$
\end{example}
\begin{proof}[Solution]
 We begin by computing the first several terms:
\[\left\{\frac{2}{9},\frac{4}{27},\frac{8}{81},\frac{16}{243},\frac{32}{729},\ldots\right\}\]
It is seen that the terms take the values between $ 0 $ and $\frac{2}{9}$, so the sequence is bounded. We need to check if the sequence $ \{a_n\} $ is monotonic. Since
\[a_{n+1}-a_n=\frac{2^{n+1}}{3^{n+2}}-\frac{2^n}{3^{n+1}}=\frac{2^n}{3^{n+1}}\left(\frac{2}{3}-1\right)=-\frac{1}{3}\frac{2^n}{3^{n+1}}<0\]
the sequence is decreasing. 
By Monotoinc Sequence Theorem we say that the sequence  $a_n=\frac{2^n}{3^{n+1}}$ is convergent.
\end{proof}
\end{frame}

\subsection{Series}

\begin{frame}{\insertsubsectionhead}
\begin{block}{}
	If we try to add the terms of an infinite sequence $\{a_n\}_{n=1}^\infty$ we get an expression of the form
	\begin{equation}\label{dizi}
	a_1+a_2+a_3+\ldots+a_n+\ldots
	\end{equation}
	which is called an \textbf{infinite series} (or just a \textbf{series}) and is denoted, for short, by the symbol
	\[\sum_{n=1}^\infty a_n \qquad \text{ or } \qquad \sum a_n.\]
\end{block}
\end{frame}
%
%
%
\begin{frame}{}
\begin{block}{}
	But does it make sense to talk about the sum of infinitely many terms?
\end{block}
It would be impossible to find a finite sum for the series
\[1+2+3+4+5+\ldots+n+\ldots\]
because if we start adding the terms we get the cumulative sums  $ 1 $, $ 3 $, $ 6 $, $ 10 $, $ 15 $, $ 21 $,$\ldots$ and, the $n$th term,\[ \frac{n(n+1)}{2} \] which becomes very large as $ n $ increases.
\end{frame}
%
%
\begin{frame}{}
\begin{minipage}{.65\textwidth}
	However, if we start to add the terms of the series
	\[\frac{1}{2}+\frac{1}{4}+\frac{1}{8}+\frac{1}{16}+\frac{1}{32}+\frac{1}{64}+\ldots+\frac{1}{2^n}+\ldots\]
	we get
	\[\frac{1}{2},\;\frac{3}{4},\;\frac{7}{8},\;\frac{15}{16},\;\frac{31}{32},\;\frac{63}{64},\ldots,1-\frac{1}{2^n},\ldots.\]
\end{minipage}
\begin{minipage}{.2\textwidth}
	\begin{figure}[h]
		\includegraphics[page=574,clip,trim=22.5mm 186mm 144.5mm 37.5mm,scale=.8]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}

The Table shows that as we add more and more terms, these partial sums become closer and closer to $ 1 $.

In fact, by adding sufficiently many terms of the series we can make the partial sums as close as we like to $ 1 $. So it seems reasonable to say that the sum of this infinite series is $ 1 $ and to write
\[\sum_{n=1}^\infty \frac{1}{2^n}=\frac{1}{2}+\frac{1}{4}+\frac{1}{8}+\frac{1}{16}+\ldots+\frac{1}{2^n}+\ldots=1.\]
\end{frame}
%
%
\begin{frame}{}
\begin{block}{}
	We use a similar idea to determine whether or not a general series \eqref{dizi} has a sum. We consider the \textbf{partial sums}
	\begin{align*}
	s_1&=a_1\\
	s_2&=a_1+a_2\\
	s_3&=a_1+a_2+a_3\\
	s_4&=a_1+a_2+a_3+a_4
	\end{align*}
	and, in general,
	\[s_n=a_1+a_2+a_3+\ldots+a_n=\sum_{i=1}^n a_i.\]
	These partial sums form a new sequence $\{s_n\}$, which may or may not have a limit.
	
	If $ \lim\limits_{n\to \infty} s_n=s$ exists (as a finite number), then, as in the preceding example, we call it the sum of the infinite series $\sum a_n$.
\end{block}
\end{frame}
%
%
\begin{frame}
\begin{definition}
Given a series $\displaystyle \sum_{n=1}^\infty a_n=a_1+a_2+\ldots ,$ let $\{s_n\}$ denote its $ n $th partial sum:
\[s_n=\sum_{i=1}^n a_i=a_1+a_2+\ldots+a_n\]
If the sequence  $\{s_n\}$ is convergent and $\displaystyle\lim_{n \to \infty} s_n$ exists as a real number, then the $\sum a_n$ series is called \textbf{convergent} and we write
\[a_1+a_2+\ldots+a_n+\ldots=s \quad \text{ or } \quad \sum_{n=1}^\infty a_n =s\]

The $s$ number is called the \textbf{sum} of the series. If the sequence $\{s_n\}$ is divergent, then the series is called \textbf{divergent}.
\end{definition}
\end{frame}
%
%
%
\begin{frame}
\begin{block}{}
Thus, the sum of a series is the limit of the sequence of partial sums.	
\end{block}
\begin{block}{}
	So when we write $\displaystyle\sum_{n=1}^\infty a_n=s$ we mean that by adding sufficiently many terms of the series we can get as close as we like to the number $s.$ Notice that
	\[\sum_{n=1}^\infty a_n=\lim_{n\to \infty}\sum_{i=1}^n a_i.\]
\end{block}
\end{frame}

\begin{frame}
\begin{example}[Geometric Series]
	An important example of an infinite series is the {geometric series}
	\begin{equation*}
		a+ar+ar^{2}+\cdots+ar^{n-1}+\cdots=\sum_{n=1}^{\infty}ar^{n-1}
	\end{equation*}
	Each term is obtained from the preceding one by multiplying it by the common ratio $r$. (We have already considered the special case where $a=\frac{1}{2}$ and $r=\frac{1}{2}$.)
	
	If $r=1$, then $s_{n}=a+a+\cdots+a=na\to\pm\infty$. Since $\lim\limits_{n\to\infty}s_{n}$ doesn't exist, the geometric series diverges in this case. If $r\ne 1$, we have
	\begin{align}
		s_{n}=a+ar+\cdots+ar^{n-1} \quad\text{and}\quad rs_{n}=ar+ar^{2}+\cdots+ar^{n}.\nonumber
	\end{align}
\end{example}
\end{frame}

%

\begin{frame}{}{}
\begin{exampleblock}{}
Subtracting these equations, we get
\begin{equation}
	s_{n}-rs_{n}=a-ar^{n} \implies s_{n}=\frac{a(1-r^{n})}{1-r}.\label{s1ss2exm1eq1}
\end{equation}
If $-1<r<1$, we know that $r^{n}\to 0$ as $n\to\infty$, so
\begin{equation*}
	\lim_{n\to\infty}s_{n}=\lim_{n\to\infty}\frac{a(1-r^{n})}{1-r}=\frac{a}{1-r}-\frac{a}{1-r}\lim_{n\to\infty}r^{n}=\frac{a}{1-r}.
\end{equation*}
Thus, when $|r|<1$ the geometric series is convergent and its sum is $\dfrac{a}{1-r}$.

If $r\le -1$ or $r>1$, the sequence $\{r^{n}\}$ is divergent and so, by \eqref{s1ss2exm1eq1}, $\lim\limits_{n\to\infty}s_{n}$ does not exist. Therefore, the geometric series diverges in those cases.
\end{exampleblock}
\end{frame}
%
\begin{frame}{}{}
We summarize the results of Example as follows.
\begin{block}{}
The geometric series
\begin{equation*}
\sum_{n=1}^{\infty}ar^{n-1}=a+ar+ar^{2}+\cdots
\end{equation*}
is \textbf{convergent} if $|r|<1$ and its sum
\begin{equation*}
\sum_{n=1}^{\infty}ar^{n-1}=\frac{a}{1-r}.
\end{equation*}
If $|r|\geq1$, the geometric series is \textbf{divergent}.
\end{block}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}
Is the series $ \displaystyle\sum_{n=1}^{\infty}2^{2n}3^{1-n}$ convergent or divergent?
\end{example}
\begin{proof}[Solution]
Let's rewrite the $ n $th term of the series in the form:
\begin{equation*}
\sum_{n=1}^{\infty}2^{2n}3^{1-n}=\sum_{n=1}^{\infty}\frac{4^{n}}{3^{n-1}}=\sum_{n=1}^{\infty}4\biggl(\frac{4}{3}\biggr)^{n-1}.
\end{equation*}
We recognize this series as a Geometric Series with $a=4$ and $r=\frac{4}{3}$. Since $r>1$, the series diverges.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}
Find the sum of the series $\displaystyle \sum_{n=0}^{\infty}x^{n}$, where $|x|<1$.
\end{example}
\begin{proof}[Solution]
Notice that this series starts with $n=0$ and so the first term is $x^{0}=1$. (With series, we adopt the convention that $x^{0}=1$ even when $x=0$.) Thus
\begin{equation*}
\sum_{n=0}^{\infty}x^{n}=1+x+x^{2}+\cdots.
\end{equation*}
This is a Geometric Series with $a=1$ and $r=x$. Since $|r|=|x|<1$, it converges and its sum is
\begin{equation*}
\sum_{n=0}^{\infty}x^{n}=\frac{1}{1-x}.\qedhere
\end{equation*}
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}
Show that the series $\displaystyle\sum_{n=1}^{\infty}\frac{1}{n(n+1)}$ is convergent, and find its sum.
\end{example}
\begin{proof}[Solution]\let \qed \relax
This is not a geometric series, so we go back to the definition of a convergent series and compute the partial sums.
\begin{equation*}
s_{n}=\sum_{i=1}^{n}\frac{1}{i(i+1)}
=\frac{1}{1\cdot2}+\frac{1}{2\cdot3}+\cdots+\frac{1}{n\cdot(n+1)}.
\end{equation*}
We can simplify this expression if we use the partial fraction decomposition
\begin{equation*}
\frac{1}{i(i+1)}=\frac{1}{i}-\frac{1}{i+1}.
\end{equation*}
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]
Thus, we have
\begin{align*}
s_{n}&=\sum_{i=1}^{n}\frac{1}{i(i+1)}=\sum_{i=1}^{n}\biggl(\frac{1}{i}-\frac{1}{i+1}\biggr)\\
=&\biggl(1-\frac{1}{2}\biggr)+\biggl(\frac{1}{2}-\frac{1}{3}\biggr)+\dots+\biggl(\frac{1}{n}-\frac{1}{n+1}\biggr)\\
=&1-\frac{1}{n+1}
\end{align*}
and so
\begin{equation*}
\lim_{n\to\infty}s_{n}=\lim_{n\to\infty}\biggl(1-\frac{1}{n+1}\biggr)=1-0=1.
\end{equation*}
Therefore, the given series is convergent and
\begin{equation*}
\sum_{n=1}^{\infty}\frac{1}{n(n+1)}=1. \qedhere
\end{equation*}
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}

\begin{example}[Harmonic Series]
Show that the {harmonic series} $\sum\limits_{n=1}^{\infty}\dfrac{1}{n}$
is divergent.
\end{example}
\begin{proof}[Solution]
Let us consider the partial sums $s_{2},s_{4},s_{8},\cdots$ and show that they become large.
{\small
\begin{align*}
s_{2}=&1+\frac{1}{2}\\
s_{4}=&1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}
>1+\frac{1}{2}+\frac{1}{4}+\frac{1}{4}=1+\frac{2}{2}\\
s_{8}=&1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}+\frac{1}{5}+\frac{1}{6}+\frac{1}{7}+\frac{1}{8}\\
>&1+\frac{1}{2}+\frac{1}{4}+\frac{1}{4}+\frac{1}{8}+\frac{1}{8}+\frac{1}{8}+\frac{1}{8}
=1+\frac{3}{2}
\end{align*}}
In general, $s_{2^{n}}>1+\frac{n}{2}$ for $n=1,2,\ldots$. This shows that $s_{2^{n}}\to\infty$ as $n\to\infty$ and so is divergent. Therefore, the harmonic series diverges.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}

\begin{theorem}\label{s1ss2thm1} %P.~578, Teorem~6
If the series is convergent $\displaystyle\sum_{n=1}^{\infty}a_{n}$,
then $\lim\limits_{n\to\infty}a_{n}=0$.
\end{theorem}

\begin{alertblock}{Note 1} %P.~578, Not~1
With any {series} $\sum{}a_{n}$ we associate two {sequences}: the sequence $\{s_{n}\}$ of its partial sums and the sequence $\{a_{n}\}$ of its terms. If $\sum{}a_{n}$ is convergent, then the limit of the sequence $\{s_{n}\}$ is (the sum of the series) and, as {Theorem~\ref{s1ss2thm1}} asserts, the limit of the sequence $\{a_{n}\}$ is $0$.
\end{alertblock}
\end{frame}

\begin{frame}
\begin{alertblock}{Note 2} %P.~578, Not~2
The converse of {Theorem~\ref{s1ss2thm1}} is not true in general. If $\lim\limits_{n\to\infty}a_{n}=0$, we cannot conclude that is convergent. Observe that for the Harmonic Series $\sum\frac{1}{n}$ we have $a_{n}=\frac{1}{n}\to0$ as $n\to\infty$, but we know that $\sum\frac{1}{n}$ is divergent.
\end{alertblock}
\begin{theorem}[The Test for Divergence]
If $\lim\limits_{n\to\infty}a_{n}$ does not exist or if $\lim\limits_{n\to\infty}a_{n}\neq0$, then the series $\sum\limits_{n=1}^{\infty}a_{n}$ is divergent.
\end{theorem}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}
Show that the series $ \displaystyle\sum_{n=1}^{\infty}\frac{n^{2}}{5n^{2}+4}$ diverges.
\end{example}
\begin{proof}[Solution]
\begin{equation*}
\lim_{n\to\infty}a_{n}=\lim_{n\to\infty}\frac{n^{2}}{5n^{2}+4}=\lim_{n\to\infty}\frac{1}{5+\frac{4}{n^{2}}}=\frac{1}{5}\ne 0.
\end{equation*}
So the series diverges by the Test for Divergence.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{alertblock}{Note 3} %P.~578, Not~3
If we find that $\lim\limits_{n\to\infty}a_{n}\ne 0$, we know that $\sum{}a_{n}$ is divergent. If we find that $\lim\limits_{n\to\infty}a_{n}=0$, we know nothing about the convergence or divergence of $\sum{}a_{n}$. Remember the warning in {\usebeamercolor[fg]{alerted text}Note 2}: If $\lim\limits_{n\to\infty}a_{n}=0$, the series $\sum a_{n}$ might converge or it might diverge.
\end{alertblock}
\begin{theorem}\label{s1ss2thm3} %P.~579, Teorem~8
If $\sum{}a_{n}$ and $\sum{}b_{n}$ are convergent series,
then so are the series $\sum{}ca_{n}$ (where $c$ is a constant),
$\sum{}(a_{n}+b_{n})$, and $\sum{}(a_{n}-b_{n})$,
and the followings hold.
\begin{enumerate}
\item $\sum\limits_{n=1}^{\infty}ca_{n}=c\sum\limits_{n=1}^{\infty}a_{n}$.
\item $\sum\limits_{n=1}^{\infty}(a_{n}+b_{n})=\sum\limits_{n=1}^{\infty}a_{n}+\sum\limits_{n=1}^{\infty}b_{n}$.
\item $\sum\limits_{n=1}^{\infty}(a_{n}-b_{n})=\sum\limits_{n=1}^{\infty}a_{n}-\sum\limits_{n=1}^{\infty}b_{n}$.
\end{enumerate}

\end{theorem}

\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{alertblock}{Note 4} %P.~580, Not~4
A finite number of terms doesn't affect the convergence or divergence of a series.
For instance, suppose that we were able to show that the series
\begin{equation*}
\textstyle
\sum\limits_{n=4}^{\infty}\frac{n}{n^{3}+1}
\end{equation*}
is convergent. Since
\begin{equation*}
\textstyle
\sum\limits_{n=1}^{\infty}\frac{n}{n^{3}+1}=\frac{1}{2}+\frac{2}{9}+\frac{3}{28}+\sum\limits_{n=4}^{\infty}\frac{n}{n^{3}+1}
\end{equation*}
it follows that the entire series $\sum_{n=1}^{\infty}\frac{n}{n^{3}+1}$ is convergent.
Similarly, if it is known that the series $\sum_{n=N+1}^{\infty}a_{n}$ converges,
then the full series
\begin{equation*}
\textstyle
\sum\limits_{n=1}^{\infty}a_{n}=\sum\limits_{n=1}^{N}a_{n}+\sum\limits_{n=N+1}^{\infty}a_{n}
\end{equation*}
is also convergent.
\end{alertblock}
\end{frame}
%
%%
%
\subsection{The Integral and Comparison Tests}

\begin{frame}{\insertsubsectionhead}{}
In general, it is difficult to find the exact sum of a series. We were able to accomplish this for geometric series and the series $\sum\frac{1}{n(n+1)}$ because in each of those cases we could find a simple formula for the $n$th partial sum $s_{n}$. But usually it is not easy to compute $\lim\limits_{n\to\infty}s_{n}$. Therefore, in this section and the next we develop tests that enable us to determine whether a series is convergent or divergent without explicitly finding its sum.\medskip

In this section we deal only with series with positive terms, so the partial sums are increasing. In view of the Monotonic Sequence Theorem, to decide whether a series is convergent or divergent, we need to determine whether the partial sums are bounded or not.
\end{frame}
%
\subsubsection{Testing with an Integral}
%
\begin{frame}{\insertsubsubsectionhead}{}
\begin{theorem}[The Integral Test]
Suppose $f$ is a continuous, positive, decreasing function on $[1,\infty)$ and let $a_{n}=f(n)$. Then the series $\sum_{n=1}^{\infty}a_{n}$ is convergent if and only if the improper integral $\int_{1}^{\infty}f(x)dx$ is convergent. In other words:
\begin{itemize}
\item[(a)] If $\int_{1}^{\infty}f(x)dx$ is convergent, then $\sum_{n=1}^{\infty}a_{n}$ is convergent.
\item[(b)] If $\int_{1}^{\infty}f(x)dx$ is divergent, then $\sum_{n=1}^{\infty}a_{n}$ is divergent.
\end{itemize}
\end{theorem}
\begin{alertblock}{Note 5} %P.~585, Not~1
When we use the Integral Test it is not necessary to start the series or the integral at $n=1$. For instance, in testing the series $\sum_{k=4}^{\infty}\frac{1}{(n-3)^{2}}$ we use $\int_{4}^{\infty}\frac{1}{(x-3)^{2}}d{}x$. Also, it is not necessary that $f$ be always decreasing. What is important is that be {ultimately} decreasing, that is, decreasing for $x$ larger than some number $N$. Then $\sum_{n=N}^{\infty}$ is convergent, so $\sum_{n=1}^{\infty}$ is convergent by {\usebeamercolor[fg]{alerted text}Note 4}
\end{alertblock}
\end{frame}

\begin{frame}
\begin{example}\label{s2ss1sss1exm1} %P.~585, \"{O}rnek~2
For what values of $p$ is the series $ \displaystyle\sum_{n=1}^{\infty}\frac{1}{n^{p}}$ convergent?
\end{example}
\begin{proof}[Solution]
If $p<0$, then $\lim\limits_{n\to\infty}\frac{1}{n^{p}}=\infty$.
If $p=0$, then $\lim\limits_{n\to\infty}\frac{1}{n^{p}}=1$.
In either case $\lim\limits_{n\to\infty}\frac{1}{n^{p}}\neq0$,
so the given series diverges by the Test for Divergence.

If $p>0$, then the function is clearly continuous, positive, and decreasing on $[1,\infty)$.

We found before that $\int_{1}^{\infty}\frac{1}{x^{p}}dx$ converges if $p>1$
and diverges if $p\leq1$.

It follows from the Integral Test that the series $\sum_{n=1}^{\infty}\frac{1}{n^{p}}$ converges if $p>1$
and diverges if $0<p\leq1$.
(For $p=1$, this series is the Harmonic Series) 
\end{proof}
\end{frame}

%%
%
\begin{frame}{}{}

The series in the previous Example is called the $p$-series.
It is important in the rest of this chapter,
so we summarize the results of Example for future reference as follows.
\begin{block}{}
The $p$-series $\displaystyle\sum_{n=1}^{\infty}\frac{1}{n^{p}}$ is convergent if $p>1$,
and divergent if $p\leq1$.
\end{block}

\end{frame}

%

\subsubsection{Testing by Comparing}

\begin{frame}{\insertsubsubsectionhead}{}

\begin{theorem}[The Comparison Test]
Suppose that $\sum{}a_{n}$ and $\sum{}b_{n}$ are series with positive terms.
\begin{enumerate}
\item If $\sum{}b_{n}$ is convergent and $a_{n}\leq{}b_{n}$ for all $n$,
then $\sum{}a_{n}$ is also convergent.
\item If $\sum{}b_{n}$ is divergent and $a_{n}\geq{}b_{n}$ for all $n$,
then $\sum{}a_{n}$ is also divergent.
\end{enumerate}
\end{theorem}
\begin{alertblock}{Note 6}
Although the condition $a_{n}\leq{}b_{n}$ or $a_{n}\geq{}b_{n}$ in the Comparison Test is given for all $n$,
we need verify only that it holds for $n\geq{}N$,
where $N$ is some fixed integer,
because the convergence of a series is not affected by a finite number of terms.
\end{alertblock}
\end{frame}

%

\begin{frame}{}{}
\begin{example}
Determine whether the series $\sum\limits_{n=1}^{\infty}\frac{5}{2n^{2}+4n+3}$ converges or diverges.
\end{example}
\begin{proof}[Solution]
For large $n$ the dominant term in the denominator is $2n^{2}$,
so we compare the given series with the series $\sum\frac{5}{2n^{2}}$.
Observe that
\begin{equation*}
\textstyle
\frac{5}{2n^{2}+4n+3}<\frac{5}{2n^{2}}
\end{equation*}
because the left side has a bigger denominator. We know that
\begin{equation*}
\textstyle
\sum\limits_{n=1}^{\infty}\frac{5}{2n^{2}}
=\frac{5}{2}\sum\limits_{n=1}^{\infty}\frac{1}{n^{2}}
\end{equation*}
is convergent ($p$-series with $p=2>1$). Therefore $\sum\limits_{n=1}^{\infty}\frac{5}{2n^{2}+4n+3}$ is convergent by part (a) of the Comparison Test.
\end{proof}
\end{frame}

\begin{frame}{}{}
\begin{example}
Test the series $\displaystyle\sum_{n=1}^{\infty}\frac{\ln(n)}{n}$ for convergence or divergence.
\end{example}
\begin{proof}[Solution]
Observe that $\ln(n)>1$ for $n\geq3$ and so
\begin{equation}
\frac{\ln(n)}{n}>\frac{1}{n},\quad{}n\geq3.\nonumber
\end{equation}
We know that is divergent ($p$-series with $p=1$).
Thus, the given series is divergent by the Comparison Test.
\end{proof}
\end{frame}