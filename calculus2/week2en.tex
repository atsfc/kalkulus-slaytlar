%
\section{Infinite Sequences and Series}
\subsection{Other Convergence Tests}
%
\begin{frame}{\insertsubsectionhead}{}
The convergence tests that we have looked at so far apply only to series with positive terms. In this section we learn how to deal with series whose terms are not necessarily positive.
\end{frame}
%
\subsubsection{Alternating Series}
%
\begin{frame}{\insertsubsubsectionhead}{}
\begin{block}{}
	An \textbf{alternating series} is a series whose terms are alternately positive and negative. 
\end{block}
Here are two examples:
\begin{align*}
\sum_{n=1}^{\infty}(-1)^{n-1}\frac{1}{n}=&1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\cdots,\\
\sum_{n=1}^{\infty}(-1)^{n}\frac{n}{n+1}=&-\frac{1}{2}+\frac{2}{3}-\frac{3}{4}+\cdots.
\end{align*}
\begin{block}{}
	We see from these examples that the $n$th term of an alternating series is of the form
	\begin{equation*}
	a_{n}=(-1)^{n-1}b_{n} \quad\text{or}\quad a_{n}=(-1)^{n}b_{n}
	\end{equation*}
	where $b_{n}$ is a positive number. (In fact, $b_{n}=|a_{n}|$.)
\end{block}
\end{frame}
%
\begin{frame}{}{}
The following test says that if the terms of an alternating series decrease to $0$
in absolute value, then the series converges.
\begin{theorem}[The Alternating Series Test]
If the alternating series
\begin{equation*}
\sum_{n=1}^{\infty}(-1)^{n-1}b_{n}=b_{1}-b_{2}+b_{3}-b_{4}+\cdots,\quad{}b_{n}>0
\end{equation*}
satisfies the followings.
\begin{enumerate}
\item $b_{n+1}\leq{}b_{n}$ for all $n$.
\item $\lim\limits_{n\to\infty}b_{n}=0$.
\end{enumerate}
Then, the series is convergent.
\end{theorem}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
The alternating harmonic series
\begin{equation*}
\sum_{n=1}^{\infty}\frac{(-1)^{n-1}}{n}=1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\cdots
\end{equation*}
satisfies the followings.
\begin{enumerate}
\item $b_{n+1} < b_{n}$ for all $n$ because $\frac{1}{n+1}<\frac{1}{n}$.
\item $\lim\limits_{n\to\infty}b_{n}=\lim\limits_{n\to\infty}\frac{1}{n}=0$.
\end{enumerate}
So the series is convergent by the Alternating Series Test.
\end{example}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
The series $ \displaystyle\sum_{n=1}^{\infty}\frac{(-1)^{n}3n}{4n-1}$ is alternating, but
\begin{equation*}
\lim_{n\to\infty}b_{n}=\lim_{n\to\infty}\frac{3n}{4n-1}=\frac{3}{4}\neq0.
\end{equation*}
so one of the conditions of the Alternating Series Test is not satisfied.

Instead, we look at the limit of the $n$th term of the series:
\begin{equation*}
\lim_{n\to\infty}\frac{(-1)^{n}3n}{4n-1}
\end{equation*}
This limit does not exist, so the series diverges by the Test for Divergence.
\end{example}
\end{frame}
%
\subsubsection{Absolute Convergence}
%
\begin{frame}{\insertsubsubsectionhead}{}
Given any series $\sum{}a_{n}$, we can consider the corresponding series
\begin{equation}
\sum_{n=1}^{\infty}|a_{n}|=|a_{1}|+|a_{2}|+\cdots\nonumber
\end{equation}
whose terms are the absolute values of the terms of the original series.
\begin{definition}[Absolute Convergence]
A series $\sum{}a_{n}$ is called {absolutely convergent}
if the series of absolute values $\sum|a_{n}|$ is convergent.
\end{definition}
\begin{alertblock}{}
	Notice that if $\sum{}a_{n}$ is a series with positive terms, then $|a_{n}|=a_{n}$ and so absolute convergence is the same as convergence.
\end{alertblock}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
The series
\begin{equation}
\sum_{n=1}^{\infty}\frac{(-1)^{n-1}}{n^{2}}=1-\frac{1}{4}+\frac{1}{9}-\frac{1}{16}+\cdots\nonumber
\end{equation}
is absolutely convergent because
\begin{equation*}
\sum_{n=1}^{\infty}\left|\frac{(-1)^{n-1}}{n^{2}}\right|=\sum_{n=1}^{\infty}\frac{1}{n^{2}}=1+\frac{1}{4}+\frac{1}{9}+\frac{1}{16}+\cdots
\end{equation*}
is a convergent $p$-series ($p=2$).
\end{example}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
We know that the alternating harmonic series
\begin{equation*}
\sum_{n=1}^{\infty}\frac{(-1)^{n-1}}{n}=1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\cdots
\end{equation*}
is convergent, but it is not absolutely convergent because the corresponding series of absolute values is
\begin{equation*}
\sum_{n=1}^{\infty}\left|\frac{(-1)^{n-1}}{n}\right|=\sum_{n=1}^{\infty}\frac{1}{n}=1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}+\cdots
\end{equation*}
which is the harmonic series ($p$-series with $p=1$) and is therefore divergent.
\end{example}
\end{frame}
%
\begin{frame}{}{}
Previous Example shows that it is possible for a series to be convergent but not absolutely convergent. However, the following theorem shows that absolute convergence implies convergence.
\begin{theorem}
If a series $\sum{}a_{n}$ is absolutely convergent, then it is convergent.
\end{theorem}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
Determine whether the series $\sum\limits_{n=1}^{\infty}\frac{\cos(n)}{n^{2}}=\frac{\cos(1)}{1}+\frac{\cos(2)}{4}+\cdots$  is convergent or divergent.
\end{example}
\begin{proof}[Solution]
This series has both positive and negative terms, but it is not alternating. (The first term is positive, the next three are negative, and the following three are positive. The signs change irregularly.) We can apply the Comparison Test to the series of absolute values
\begin{equation*}
\sum\limits_{n=1}^{\infty}\left|\frac{\cos(n)}{n^{2}}\right|=\sum\limits_{n=1}^{\infty}\frac{|\cos(n)|}{n^{2}}.
\end{equation*}
Since $|\cos(n)|\leq1$ for all $n$, we have $\frac{|\cos(n)|}{n^{2}}\leq\frac{1}{n^{2}}$. We know that is convergent ($p$-series with $p=2$ and Comparison Test). Thus, the given series is absolutely convergent and therefore convergent.
\end{proof}
\end{frame}
%
\subsubsection{The Ratio Test}
%
\begin{frame}{\insertsubsubsectionhead}{}
The following test is very useful in determining whether a given series is absolutely convergent.
\begin{theorem}[The Ratio Test]\label{s1ss4sss3thm1}
\begin{itemize}
\item If $\lim\limits_{n\to\infty}\left|\frac{a_{n+1}}{a_{n}}\right|=L<1$, then the series $\sum\limits_{n=1}^{\infty}a_{n}$ is absolutely convergent (and therefore convergent).
\item If $\lim\limits_{n\to\infty}\left|\frac{a_{n+1}}{a_{n}}\right|=L>1$ or $\lim\limits_{n\to\infty}\left|\frac{a_{n+1}}{a_{n}}\right|=\infty$, then the series $\sum\limits_{n=1}^{\infty}a_{n}$ is divergent.
\end{itemize}
\end{theorem}
\end{frame}
%
\begin{frame}{}{}
\begin{alertblock}{Note 1}
If $\lim\limits_{n\to\infty}\left|\frac{a_{n+1}}{a_{n}}\right|=1$, the Ratio Test gives no information. For instance, for the convergent $\sum\frac{1}{n^{2}}$ series we have
\begin{equation*}
\left|\frac{a_{n+1}}{a_{n}}\right|=\frac{\frac{1}{(n+1)^{2}}}{\frac{1}{n^{2}}}=\frac{1}{\left(1+\frac{1}{n}\right)^{2}}\to 1 \quad\text{as}\ n\to\infty
\end{equation*}
whereas for the divergent series $\sum\frac{1}{n}$ we have
\begin{equation*}
\left|\frac{a_{n+1}}{a_{n}}\right|=\frac{\frac{1}{n+1}}{\frac{1}{n}}=\frac{1}{1+\frac{1}{n}}\to 1 \quad\text{as}\ n\to\infty.
\end{equation*}
Therefore, if $\lim\limits_{n\to\infty}\left|\frac{a_{n+1}}{a_{n}}\right|=1$, the series $\sum{}a_{n}$ might converge or it might diverge. In this case the Ratio Test fails and we must use some other test.
\end{alertblock}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
Test the series $\displaystyle\sum_{n=1}^{\infty}(-1)^{n}\frac{n^{3}}{3^{n}}$ for absolute convergence.
\end{example}
\begin{proof}[Solution]
We use the Ratio Test with $a_{n}=(-1)^{n}\frac{n^{3}}{3^{n}}$:
\begin{align*}
\left|\frac{a_{n+1}}{a_{n}}\right|=&\left|\frac{(-1)^{n+1}\frac{(n+1)^{3}}{3^{n+1}}}{(-1)^{n}\frac{n^{3}}{3^{n}}}\right|=\frac{(n+1)^{3}}{3^{n+1}}\frac{3^{n}}{n^{3}}\\
=&\frac{1}{3}\left(1+\frac{1}{n}\right)^{3}\to\frac{1}{3}<1\quad\text{as}\ n\to\infty.
\end{align*}
Thus, by the Ratio Test, the given series is absolutely convergent and therefore convergent.
\end{proof}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
Test the convergence of the series $\displaystyle\sum_{n=1}^{\infty}\frac{n^{n}}{n!}$.
\end{example}
\begin{proof}[Solution]
Since the terms $a_{n}=\frac{n^{n}}{n!}$ are positive, we don't need the absolute value signs.
\begin{align*}
\frac{a_{n+1}}{a_{n}}=&\frac{\frac{(n+1)^{n+1}}{(n+1)!}}{\frac{n^{n}}{n!}}=\frac{(n+1)(n+1)^{n}}{(n+1)n!}\frac{n^{n}}{n!}\\
=&\biggl(1+\frac{1}{n}\biggr)^{n}\to e>1\quad\text{as}\ n\to\infty.
\end{align*}
Since $e>1$, the given series is divergent by the Ratio Test.
\end{proof}
\end{frame}
%
\begin{frame}{}{}
\begin{alertblock}{Note 2}
Although the Ratio Test works in previous Example, another method is to use the Test for Divergence. Since
\begin{equation}
a_{n}=\frac{n^{n}}{n!}=\frac{n\times{}n\times\cdots\times{}n}{1\times{}2\times\cdots\times{}n}\geq{}n\nonumber
\end{equation}
it follows that $a_{n}$ does not approach $0$ as $n\to\infty$. Therefore, the given series is divergent by the Test for Divergence.
\end{alertblock}
\end{frame}
%
\subsection{Power Series}
%
\begin{frame}{\insertsubsectionhead}{}
\begin{block}{}
	A \textbf{power series} is a series of the form
	\begin{equation}
	\sum_{n=0}^{\infty}c_{n}x^{n}=c_{0}+c_{1}x+c_{2}x^{2}+\cdots\label{s1ss5eqn1}
	\end{equation}
	where $x$ is a variable and the $c_{n}$'s are constants called the \textbf{coefficients} of the series. For each fixed $x$, the series \eqref{s1ss5eqn1} is a series of constants that we can test for convergence or divergence. A power series may converge for some values of $x$ and diverge for other values of $x$. The sum of the series is a function
	\begin{equation*}
	f(x)=c_{0}+c_{1}x+c_{2}x^{2}+\cdots+c_{n}x^{n}+\cdots
	\end{equation*}
	whose domain is the set of all $x$ for which the series converges. Notice that $f$ resembles a polynomial. The only difference is that has infinitely many terms.
	\end{block}
\end{frame}
%
\begin{frame}{}{}
For instance, if we take $c_{n}=1$ for all $n$,
the power series becomes the geometric series
\begin{equation}
\sum_{n=0}^{\infty}x^{n}=1+x+x^{2}+\cdots+x^{n}+\cdots=\frac{1}{1-x}\nonumber
\end{equation}
which converges when $-1<x<1$ and diverges when $|x|\geq1$.
\begin{block}{}
	More generally, a series of the form
	\begin{equation}
	\sum_{n=0}^{\infty}c_{n}(x-a)^{n}
	=c_{0}+c_{1}(x-a)+c_{2}(x-a)^{2}+\cdots\label{s1ss5eqn2}
	\end{equation}
	is called a {power series in $(x-a)$} or a {power series centered at $a$} or a {power series about $a$}. Notice that in writing out the term corresponding to $n=0$ in \eqref{s1ss5eqn1} and \eqref{s1ss5eqn2} we have adopted the convention that even when $x=a$. Notice also that when $x=a$ all of the terms are $0$ for $n\geq1$ and so the power series \eqref{s1ss5eqn2} always converges when $x=a$.
\end{block}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
For what values of $x$ is the series $\displaystyle\sum_{n=0}^{\infty}n!x^{n}$ convergent?
\end{example}
\begin{proof}[Solution]
We use the Ratio Test. If we let $a_{n}$, as usual, denote the $n$th term of the series, then $a_{n}=n!x^{n}$. If $x\neq0$, we have
\begin{equation*}
\lim_{n\to\infty}\left|\frac{a_{n+1}}{a_{n}}\right|=\lim_{n\to\infty}\left|\frac{(n+1)!x^{n+1}}{n!x^{n}}\right|=\lim_{n\to\infty}(n+1)|x|=\infty.
\end{equation*}
By the Ratio Test, the series diverges when $x\ne 0$. Thus, the given series converges only when $x=0$.
\end{proof}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
For what values of $x$ does the series $\sum_{n=1}^{\infty}\frac{(x-3)^{n}}{n}$ converge?
\end{example}
\begin{proof}[Solution]\let \qed \relax
Let $a_{n}=\frac{(x-3)^{n}}{n}$. Then
\begin{align*}
\left|\frac{a_{n+1}}{a_{n}}\right| =&\left|\frac{(x-3)^{n+1}}{n+1}\frac{n}{(x-3)^{n}}\right|\\
=&\frac{1}{1+\frac{1}{n}}|x-3|\to|x-3|\quad\text{as}\ n\to\infty.
\end{align*}
By the Ratio Test, the given series is absolutely convergent, and therefore convergent when $|x-3|<1$, and divergent when $|x-3|>1$. Now
\begin{equation*}
|x-3|<1\iff-1<x-3<1\iff2<x<4
\end{equation*}
so the series converges when $2<x<4$, and diverges if $x<2$ or $x>4$. 
\end{proof}
\end{frame}
%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]
The Ratio Test gives no information when $|x-3|=1$ so we must consider $x=2$ and $x=4$ separately. If we put $x=4$ in the series, it becomes $\sum\frac{1}{n}$, the harmonic series, which is divergent. If $x=2$, the series is $\sum\frac{(-1)^{n}}{n}$, which converges by the Alternating Series Test. Thus, the given power series converges for $2\le x<4$.
\end{proof}
\end{frame}
%
\begin{frame}{}{}
\begin{theorem}
For a given power series there are only three possibilities:
\begin{enumerate}
\item\label{s1ss5thm1it1} The series converges only when $x=a$.
\item\label{s1ss5thm1it2} The series converges for all $x$.
\item\label{s1ss5thm1it3} There is a positive number such that the series converges if $|x-a|<R$ and diverges if $|x-a|>R$.
\end{enumerate}
\end{theorem}
\end{frame}
%
\begin{frame}
\begin{block}{}
	The number in case \eqref{s1ss5thm1it3} is called the \textbf{radius of convergence} of the power series. 
	
	By convention, the radius of convergence is $R=0$ in case \eqref{s1ss5thm1it1} and $R=\infty$ in case \eqref{s1ss5thm1it2}.
\end{block}
\begin{block}{}
	 The \textbf{interval of convergence} of a power series is the interval that consists of all values of $x$ for which the series converges. 
	 
	 In case \eqref{s1ss5thm1it1} the interval consists of just a single point $a$. In case \eqref{s1ss5thm1it2} the interval is $(-\infty,\infty)$. In case \eqref{s1ss5thm1it3} note that the inequality $|x-a|<R$ can be rewritten as $a-R<x<a+R$.
\end{block}
\begin{alertblock}{}
	When $x$ is an \textbf{endpoint} of the interval, that is, $x=a\pm{}R$, anything can happen; the series might converge at one or both endpoints or it might diverge at both endpoints. 
	
	Thus, in case \eqref{s1ss5thm1it3} there are four possibilities for the interval of convergence:
	\begin{equation}
	(a-R,a+R),\quad (a-R,a+R],\quad [a-R,a+R)\quad\text{or}\quad [a-R,a+R].\nonumber
	\end{equation}
\end{alertblock}
\end{frame}
%
\begin{frame}{}{}
\begin{table}
	\begin{tabular}{|c|c|c|c|}\hline
		Series & Radius of Convergence & Interval of Convergence \\ \hline
		$\sum\limits_{n=0}^{\infty}x^{n}$ & $R=1$ & $(-1,1)$ \\ \hline
		$\sum\limits_{n=0}^{\infty}n!x^{n}$ & $R=0$ & $\{0\}$ \\ \hline
		$\sum\limits_{n=0}^{\infty}\frac{(x-3)^{n}}{n}$ & $R=1$ & $[2,4)$\\ \hline
	\end{tabular}
\end{table}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
Find the radius of convergence and interval of convergence of the series $\sum\limits_{n=0}^{\infty}\frac{(-3)^{n}x^{n}}{\sqrt{n+1}}$.
\end{example}
\begin{proof}[Solution]\let \qed \relax
Let $a_{n}=\frac{(-3)^{n}x^{n}}{\sqrt{n+1}}$. Then
\begin{align*}
\left|\frac{a_{n+1}}{a_{n}}\right|&=\left|\frac{(-3)^{n+1}x^{n+1}}{\sqrt{n+2}}\frac{\sqrt{n+1}}{(-3)^{n}x^{n}}\right|=\left|-3x\sqrt{\frac{n+1}{n+2}}\right|\\
&=3|x|\sqrt{\frac{n+1}{n+2}}\to 3|x|\quad\text{as}\ n\to\infty.
\end{align*}
By the Ratio Test, the given series converges if $3|x|<1$, and diverges if $3|x|>1$. Thus, it converges if $|x|<\frac{1}{3}$, and diverges if $|x|>\frac{1}{3}$. This means that the radius of convergence is $R=\frac{1}{3}$.
\end{proof}
\end{frame}
%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]
We know the series converges in the interval $(-\frac{1}{3},\frac{1}{3})$, but we must now test for convergence at the endpoints of this interval. If $x=-\frac{1}{3}$, the series becomes
\begin{equation*}
\sum_{n=0}^{\infty}\frac{(-3)^{n}\bigl(-\frac{1}{3}\bigr)^{n}}{\sqrt{n+1}} =\sum_{n=0}^{\infty}\frac{1}{\sqrt{n+1}}
\end{equation*}
which diverges. (Use the Integral Test or simply observe that it is a $p$-series with $p=\frac{1}{2}<1$.) If $x=\frac{1}{3}$, the series is
\begin{equation*}
\sum_{n=0}^{\infty}\frac{(-3)^{n}\bigl(\frac{1}{3}\bigr)^{n}}{\sqrt{n+1}}=\sum_{n=0}^{\infty}\frac{(-1)^{n}}{\sqrt{n+1}}
\end{equation*}
which converges by the Alternating Series Test. Therefore, the power series converges when $-\frac{1}{3}<x\leq\frac{1}{3}$, so the interval of convergence is $\bigl(-\frac{1}{3},\frac{1}{3}\bigr]$.
\end{proof}
\end{frame}
%
\subsection{Representations of Functions as Power Series}
%
\begin{frame}{\insertsubsectionhead}{}
In this section we learn how to represent certain types of functions as sums of power series by manipulating geometric series or by differentiating or integrating such a series. 

You might wonder why we would ever want to express a known function as a sum of infinitely many terms. 

We will see later that this strategy is useful for integrating functions that don't have elementary antiderivatives, for solving differential equations, and for approximating functions by polynomials. (Scientists do this to simplify the expressions they deal with; computer scientists do this to represent functions on calculators and computers.)
\end{frame}
%
\begin{frame}{}{}
\begin{block}{}
	We start with an equation that we have seen before:
	\begin{equation}
	\sum_{n=0}^{\infty}x^{n}=1+x+x^{2}+\cdots+x^{n}+\cdots=\frac{1}{1-x},\quad|x|<1.\label{s1ss6eqn1}
	\end{equation}
\end{block}
We first encountered this equation in previous examples, where we obtained it by observing that the series is a geometric series with $a=1$ and $r=x$. But here our point of view is different. We now regard \eqref{s1ss6eqn1} as expressing the function $f(x)=\dfrac{1}{1-x}$ as a sum of a power series.
\end{frame}
%
\begin{frame}{}{}
\begin{example}
Express $\frac{1}{1+x^{2}}$ as the sum of a power series and find the interval of convergence.
\end{example}
\begin{proof}[Solution]
Replacing $x$ by $-x^{2}$ in \eqref{s1ss6eqn1}, we have
\begin{align*}
\frac{1}{1+x^{2}}=&\frac{1}{1-(-x^{2})}=\sum_{n=0}^{\infty}(-x^{2})^{n}=\sum_{n=0}^{\infty}(-1)^{n}x^{2n}\\
=&1-x^{2}+x^{4}-x^{6}+\cdots.
\end{align*}
Because this is a geometric series, it converges when $|-x^{2}|<1$, that is, $x^{2}<1$, or $|x|<1$. Therefore, the interval of convergence is $(-1,1)$. (Of course, we could have determined the radius of convergence by applying the Ratio Test, but that much work is unnecessary here.)
\end{proof}
\end{frame}
%
\begin{frame}{}{}
\begin{example}
Find a power series representation of $\dfrac{x^{3}}{x+2}$.
\end{example}
\begin{proof}[Solution]
We write
\begin{align*}
\frac{x^{3}}{x+2}=\frac{x^{3}}{2}\frac{1}{1-\bigl(-\frac{x}{2}\bigr)}=\frac{x^{3}}{2}\sum_{n=0}^{\infty}\bigl(-\frac{x}{2}\bigr)^{n}=\frac{x^{3}}{2}\sum_{n=0}^{\infty}\frac{(-1)^{n}}{2^{n}}x^{n}
\end{align*}
This series converges when $|-\frac{x}{2}|<1$, that is, $|x|<2$.
So the interval of convergence is $(-2,2)$.
\end{proof}
\end{frame}