\section{Vector Functions}
%
\subsection{Vector Functions and Space Curves}
%
\begin{frame}{\insertsectionhead}{\insertsubsectionhead}
\begin{block}{}
	In general, a function is a rule that assigns to each element in the domain an element in the range.
\end{block}
\begin{block}{}
	A \textbf{vector-valued function}, or \textbf{vector function}, is simply a function whose domain is a set of real numbers and whose range is a set of vectors.
\end{block}
\begin{block}{}
	We are most interested in vector functions $\vec{r}$ whose values are three-dimensional vectors.\medskip
	
	This means that for every number $t$ in the domain of $\vec{r}$ there is a unique vector $V_3$ in denoted by $\vec{r}(t)$.
\end{block}
\end{frame}
%
\begin{frame}
\begin{block}{}
	If $f(t)$, $g(t)$ and $h(t)$ are the components of the vector $\vec{r}(t)$, then $f(t),\,g(t)$ and $h(t)$ are real-valued functions called the \textbf{component functions} of $\vec{r}$ and we can write
	\[\vec{r}(t)=\Big\langle f(t),g(t),h(t)\Big\rangle=f(t)\vec{i}+g(t)\vec{j}+h(t)\vec{k}.\]
\end{block}
\begin{block}{}
	We use the letter $t$ to denote the independent variable because it represents time in most applications of vector functions.
\end{block}
\end{frame}
%
\begin{frame}
\begin{block}{}
	The \textbf{limit} of a vector function $\vec{r}$ is defined by taking the limits of its component functions as follows:
	
	If $\vec{r}(t)=\langle f(t),g(t),h(t)\rangle$,then
	\begin{equation}\label{p705eq1}
	\lim_{t\rightarrow a}\vec{r}(t)=\left\langle  \lim_{t\rightarrow a}f(t),\lim_{t\rightarrow a}g(t),\lim_{t\rightarrow a}h(t)\right\rangle
	\end{equation}
	provided the limits of the component functions exist.
\end{block}
\begin{alertblock}{Note}
	If $\displaystyle \lim_{t\rightarrow a}\vec{r}(t)=\vec{L}$, this definition is equivalent to saying that the length and direction of the vector $\vec{r}(t)$ approach the length and direction of the vector $\vec{L}$.
\end{alertblock}
\begin{block}{}
	Limits of vector functions obey the same rules as limits of real-valued functions.
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
	Find $\displaystyle \lim_{t\rightarrow 0}\vec{r}(t)$, where $\displaystyle \vec{r}(t)=(1+t^3)\vec{i}+te^{-t}\vec{j}+\frac{\sin t}{t}\vec{k}$.
\end{example}
\begin{proof}[Solution]
	According to Definition \ref{p705eq1}, the limit of $\vec{r}$ is the vector whose components are the limits of the component functions of $\vec{r}$:
	\begin{align*}
	\lim_{t\rightarrow 0}\vec{r}(t)&=\lim_{t\rightarrow 0}\left[(1+t^3)\right]\vec{i}+\lim_{t\rightarrow 0}\left[te^{-t}\right]\vec{j}+\lim_{t\rightarrow 0}\left[\frac{\sin t}{t}\right]\vec{k}\\
	&=\vec{i}+\vec{k} \qedhere
	\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{block}{}
	A vector function $\vec{r}$ is \textbf{continuous} at $a$ if
	\[\lim_{t\rightarrow a}\vec{r}(t)=\vec{r}(a).\]
\end{block}
\begin{block}{}
	In view of Definition \ref{p705eq1}, we see that is $\vec{r}$ continuous at $a$ if and only if its component functions $f$, $g$ and $h$ are continuous at $a$.
\end{block}
\begin{block}{}
	There is a close connection between continuous vector functions and space curves. Suppose that $f,\, g$ and $h$ are continuous real-valued functions on an interval $I$. Then the set $C$ of all points $(x,y,z)$ in space, where
	\begin{equation}\label{p706eq2}
	x=f(t)\quad y=g(t)\quad z=h(t)
	\end{equation}
	and $t$ varies throughout the interval $I$, is called a \textbf{space curve}.
\end{block}
\begin{block}{}
	The equations in \eqref{p706eq2} are called \textbf{parametric equations of $\bm{C}$} and $t$ is called a \textbf{parameter}.
\end{block}
\end{frame}
%
\begin{frame}
We can think of $C$ as being traced out by a moving particle whose position at time $t$ is $(f(t),g(t),h(t))$.

\begin{minipage}{.49\textwidth}
	\begin{figure}[h]
		\includegraphics[page=706,clip,trim=20mm 134mm 147mm 110.5mm,scale=1.1]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\begin{minipage}{.5\textwidth}
	If we now consider the vector function $\vec{r}(t)=\Big\langle  f(t),g(t),h(t)\Big\rangle$, then $\vec{r}(t)$ is the position vector of the point $P(f(t),g(t),h(t))$ on $C$.
	
	Thus, any continuous vector function $\vec{r}$ defines a space curve $C$ that is traced out by the tip of the moving vector $\vec{r}(t)$, as shown in Figure.
\end{minipage}
\end{frame}
%
\begin{frame}
\begin{example}
	Sketch the curve whose vector equation is
	\[\vec{r}(t)=\cos t \vec{i}+\sin t \vec{j}+t \vec{k}.\]
\end{example}
\begin{proof}[Solution]\let \qed \relax
	The parametric equations for this curve are
	\[x=\cos t\quad y=\sin t\quad z=t.\]
	Since $x^2+y^2=\cos ^2t+\sin ^2t=1$, the curve must lie on the circular cylinder $x^2+y^2=1$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	\begin{minipage}{.49\textwidth}
		\begin{figure}[h]
			\includegraphics[page=707,clip,trim=26mm 203mm 148.5mm 30mm,scale=1.1]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.5\textwidth}
		The point $(x,y,z)$ lies directly above the point $(x,y,0)$, which moves counterclockwise around the circle in the $xy$-plane. Since $z=t$, the curve spirals upward around the cylinder as $t$ increases. The curve, shown in Figure, is called a \textbf{helix}. \qedhere
	\end{minipage}
\end{proof}
\end{frame}
%
\subsection{Derivatives of Vector Functions}
%
\subsubsection{Derivatives}
%
\begin{frame}{\insertsubsectionhead}{\insertsubsubsectionhead}
	\begin{block}{}
		The \textbf{derivative} $ \vec{r}\ ' $ of a vector function $ \vec{r} $ is defined in much the same way as for real-valued functions:
		\[ \frac{d \vec{r}}{dt} = \lim_{h\rightarrow 0} \frac{\vec{r}(t+h)-\vec{r}(t)}{h} \]
		if this limit exists.
	\end{block}
	\begin{theorem}
		If
		\[ \vec{r}(t)=\Big\langle  f(t),g(t),h(t)\Big\rangle=f(t)\vec{i}+g(t)\vec{j}+h(t)\vec{k}, \]
		where $f$, $g$ and $h$ are differentiable functions, then
		\[ \vec{r}\ '(t)=\Big\langle  f'(t),g'(t),h'(t)\Big\rangle=f'(t)\vec{i}+g'(t)\vec{j}+h'(t)\vec{k}. \]
	\end{theorem}
\end{frame}
%
\begin{frame}
\begin{example}
	Find the derivative of $\displaystyle \vec{r}(t)=(1+t^3) \vec{i}+te^{-t} \vec{j}+\sin 2t \vec{k}$.
\end{example}
\begin{proof}[Solution]
	According to Theorem, we differentiate each component of $\vec{r}$:
	\[\vec{r}\ '(t)=3t^2 \vec{i}+(1-t)e^{-t} \vec{j}+2\cos 2t \vec{k}. \qedhere\]
\end{proof}
\end{frame}
%
\subsubsection{Differentiation Rules}
%
\begin{frame}{\insertsubsubsectionhead}
\begin{theorem}
	Suppose $\vec{u}$ and $\vec{v}$ are differentiable vector functions, $c$ is a scalar, and $f$ is a real-valued function. Then
	\begin{enumerate}
		\item $\displaystyle \frac{d}{dt}[\vec{u}(t)+\vec{v}(t)]=\vec{u}\ '(t)+\vec{v}\ '(t)$ 
		\item $\displaystyle \frac{d}{dt}[c\vec{u}(t)]=c\vec{u}\ '(t)$ 
		\item $\displaystyle \frac{d}{dt}[f(t)\vec{u}(t)]=f '(t)\vec{u}(t)+f(t)\vec{u}\ '(t)$ 
		\item $\displaystyle \frac{d}{dt}[\vec{u}(t)\cdot \vec{v}(t)]=\vec{u}\ '(t)\cdot\vec{v}(t)+\vec{u}(t)\cdot\vec{v}\ '(t)$ 
		\item $\displaystyle \frac{d}{dt}[\vec{u}(t)\times\vec{v}(t)]=\vec{u}\ '(t)\times\vec{v}(t)+\vec{u}(t)\times\vec{v}\ '(t)$ 
		\item $\displaystyle \frac{d}{dt}[\vec{u}(f(t))]=f '(t)\vec{u}\ '(f(t)).\text{ (chain rule)}$
	\end{enumerate}
\end{theorem}
\end{frame}
%
\subsubsection{Integrals}
%
\begin{frame}{\insertsubsubsectionhead}
	\begin{block}{}
		The definite integral of a continuous vector function $\vec{r}(t)$ can be defined in much the same way as for real-valued functions \underline{except that the integral is a vector.}
	\end{block}
	\begin{block}{}
		But then we can express the integral of $\vec{r}$ in terms of the integrals of its component functions $f$, $g$ and $h$ as follows.
		\[\int_a^b \vec{r}(t)dt=\left(\int_a^b f(t)dt\right)\vec{i}+\left(\int_a^b g(t)dt\right)\vec{j}+\left(\int_a^b h(t)dt\right)\vec{k}\]
	\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
	If $\vec{r}(t)=2\cos t \vec{i}+\sin t \vec{j}+2t \vec{k}$, then
	\begin{align*}
	\int \vec{r}(t) dt&=\left( \int 2\cos t dt\right)\vec{i}+\left( \int \sin t dt\right)\vec{j}+\left( \int 2t dt\right)\vec{k}\\
	&=2\sin t \vec{i}-\cos t \vec{j}+t^2 \vec{k}+\vec{C}
	\end{align*}
	where $\vec{C}$ is a vector constant of integration, and
	\[\int_0^{\frac{\pi}{2}} \vec{r}(t) dt=\left[2\sin t \vec{i}-\cos t \vec{j}+t^2 \vec{k}\right]_0^{\frac{\pi}{2}}=2 \vec{i}+ \vec{j}+\frac{\pi ^2}{4} \vec{k}.\]
\end{example}
\end{frame}
%
\subsection{Arc Length}
%
\begin{frame}{\insertsubsectionhead}
Suppose that the curve has the vector equation
\[ \vec{r}(t)=\Big\langle  f(t),g(t),h(t)\Big\rangle,\quad a\le t \le b \]
or, equivalently, the parametric equations
\[ x=f(t),\quad y=g(t),\quad z=h(t) \]
where $f'$, $g'$ and $h'$ are continuous.
\end{frame}
%
\begin{frame}
If the curve is traversed exactly once as $t$ increases from $a$ to $b$, then it can be shown that its \textbf{length} is
\begin{align*}
L & = \int_a^b \sqrt{[f'(t)]^2+[g'(t)]^2+[h'(t)]^2} dt\\
& = \int_a^b \sqrt{\left(\frac{dx}{dt}\right)^2+\left(\frac{dy}{dt}\right)^2+\left(\frac{dz}{dt}\right)^2} dt.
\end{align*}
Notice that both of the arc length formulas can be put into the more compact form
\begin{equation}\label{p717eq3}
L=\int_a^b | \vec{r}\ '(t)| dt.
\end{equation}
\end{frame}
%
\begin{frame}
\begin{example}
	Find the length of the arc of the circular helix with vector equation $\vec{r}(t)=\cos t \vec{i}+\sin t \vec{j}+t \vec{k}$ from the point $(1,0,0)$ to the point $(1,0,2\pi)$.
\end{example}
\begin{proof}[Solution]
Since $\vec{r}\ '(t)=-\sin t \vec{i}+\cos t \vec{j}+\vec{k}$ we have
\[| \vec{r}\ '(t)| =\sqrt{(-\sin t)^2+\cos ^2t+1}=\sqrt{2} \]
The arc from $(1,0,0)$ to $(1,0,2\pi)$ is described by the parameter interval $0\le t\le 2\pi$ and so, from Formula \eqref{p717eq3}, we have
\[L=\int_0^{2\pi} | \vec{r}\ '(t)| dt=\int_0^{2\pi}\sqrt{2} dt=2\sqrt{2}\pi. \qedhere \]
\end{proof}
\end{frame}
%
\section{Vector Calculus}
%
\subsection{Vector Fields}
%
\begin{frame}{\insertsectionhead}{\insertsubsectionhead}
In general, a vector field is a function whose domain is a set of points in $ \mathds{R}^{2} $ (or $ \mathds{R}^{3} $) and whose range is a set of vectors in $ V_2 $ (or $ V_3 $).
\begin{definition}
	Let $ D $ be a set in $ \mathds{R}^{2} $ (a plane region). A \textbf{vector field on} $ \mathds{R}^{2} $ is a function $ \vec{F} $ that assigns to each point $ (x,y) $ in $ D $ a two-dimensional vector $ \vec{F}(x,y) $.
\end{definition}
\end{frame}
%
\begin{frame}
\begin{minipage}{.49\textwidth}
\begin{figure}[h]
	\includegraphics[page=918,clip,trim=21mm 103mm 149.5mm 143.5mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
\end{minipage}
\begin{minipage}{.5\textwidth}
	The best way to picture a vector field is to draw the arrow representing the vector $ \vec{F}(x,y) $ starting at the point $ (x,y) $. Of course, it's impossible to do this for all points $ (x,y) $, but we can gain a reasonable impression of $ \vec{F} $ by doing it for a few representative points in $ D $ as in Figure.
\end{minipage}

Since $ \vec{F}(x,y) $ is a two-dimensional vector, we can write
it in terms of its \textbf{component functions} $ P $ and $ Q $ as follows:
\[ \vec{F}(x,y) = P(x,y) \vec{i} + Q(x,y) \vec{j} = \langle P(x,y), Q(x,y) \rangle \]
or, for short,
\[ \vec{F} = P \vec{i} + Q \vec{j}. \]
Notice that $ P $ and $ Q $ are scalar functions of two variables and are sometimes called
\textbf{scalar fields} to distinguish them from vector fields.
\end{frame}
%
\begin{frame}
\begin{definition}
Let $ E $ be a subset of $ \mathds{R}^{3} $. A \textbf{vector field} on $ \mathds{R}^{3} $ is a function $ \vec{F} $ that assigns to each point $ (x,y,z) $ in $ E $ a three-dimensional vector $ \vec{F}(x,y,z) $.
\end{definition}
A vector field $ \vec{F} $ on $ \mathds{R}^{3} $ is pictured in Figure. We can express it in terms of its component functions $ P $, $ Q $ and $ R $ as
\[ \vec{F}(x,y,z) = P(x,y,z) \vec{i} + Q(x,y,z) \vec{j} + R(x,y,z) \vec{k}. \]
\begin{figure}[h]
\includegraphics[page=918,clip,trim=21mm 49mm 150.5mm 200.5mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
\begin{frame}
\begin{block}{}
	As with the vector functions, we can define continuity of vector fields and show that $ \vec{F} $ is continuous if and only if its component functions $ P $, $ Q $ and $ R $ are continuous.
\end{block}
\begin{block}{}
	We sometimes identify a point $ (x,y,z) $ with its position vector $ \vec{x} = \langle x,y,z \rangle $ and write $ \vec{F}(\vec{x}) $ instead of $ \vec{F}(x,y,z) $. Then $ \vec{F} $ becomes a function that assigns a vector $ \vec{F}(\vec{x}) $ to a vector $ \vec{x} $.
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
A vector field on $ \mathds{R}^{2} $ is defined by
\[ \vec{F}(x,y) = -y \vec{i} + x \vec{j}. \]
Describe $ \vec{F} $ by sketching some of the vectors $ \vec{F}(x,y) $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
\begin{minipage}{.44\textwidth}
\begin{figure}[h]
\includegraphics[page=919,clip,trim=23mm 151mm 149mm 84.5mm]{stewart-ccac.pdf}
\end{figure}
\end{minipage}
\begin{minipage}{.55\textwidth}
Since $ \vec{F}(1,0) = \vec{j} $, we draw the vector $ \vec{j} = \langle 0,1 \rangle $ starting at the point $ (1,0) $ in Figure. Since $ \vec{F}(0,1) = -\vec{i} $, we draw the vector $ \langle -1,0 \rangle $ with starting point $ (0,1) $. Continuing in this way, we draw a number of representative vectors to represent the vector field in Figure.
\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
It appears that each arrow is tangent to a circle with center the origin. To confirm this, we take the dot product of the position vector $ \vec{x} = x \vec{i} + y \vec{j} $ with the vector $ \vec{F}(\vec{x}) = \vec{F}(x,y) $:
\begin{align*}
\vec{x} \cdot \vec{F}(\vec{x}) & = (x\vec{i} + y \vec{j}) \cdot (-y \vec{i} + x \vec{j})\\
& = -xy + yx = 0.
\end{align*}
This shows that $ \vec{F}(x,y) $ is perpendicular to the position vector $ \langle x,y \rangle $ and is therefore tangent to a circle with center the origin and radius $ |\vec{x}| = \sqrt{x^{2}+y^{2}} $. Notice also that
\[ |\vec{F}(x,y)| = \sqrt{(-y)^{2}+x^{2}} = \sqrt{x^{2}+y^{2}} = |\vec{x}| \]
so the magnitude of the vector $ \vec{F}(x,y) $ is equal to the radius of the circle.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
Sketch the vector field on $ \mathds{R}^{3} $ given by $ \vec{F}(x,y,z) = z \vec{k} $.
\end{example}
\begin{proof}[Solution]
\begin{minipage}{.49\textwidth}
\begin{figure}[h]
\includegraphics[page=920,clip,trim=20mm 203.5mm 148mm 32mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
\end{minipage}
\begin{minipage}{.5\textwidth}
The sketch is shown in Figure. Notice that all vectors are vertical and point upward above the $ xy $-plane or downward below it. The magnitude increases
with the distance from the $ xy $-plane. \qedhere
\end{minipage}
\end{proof}
\end{frame}
%
\subsubsection{Gradient Fields}
%
\begin{frame}{\insertsubsubsectionhead}
If $ f $ is a scalar function of two variables, recall that its gradient $ \nabla f $ (or $ \operatorname{grad} f $) is defined by
\[ \nabla f(x,y) = f_x(x,y) \vec{i} + f_y(x,y) \vec{j}. \]
\begin{block}{}
	Therefore, $ \nabla f $ is really a vector field on $ \mathds{R}^{2} $ and is called a gradient vector field. Likewise, if is a scalar function of three variables, its gradient is a vector field on $ \mathds{R}^{3} $ given by
	\[ \nabla f(x,y,z) = f_x(x,y,z) \vec{i} + f_y(x,y,z) \vec{j} + f_z(x,y,z) \vec{k}. \]
\end{block}
\begin{block}{}
	A vector field $ \vec{F} $ is called a \textbf{conservative vector field} if it is the gradient of some scalar function, that is, if there exists a function $ f $ such that $ \vec{F} = \nabla f $. In this situation
	$ f $ is called a \textbf{potential function} for $ \vec{F} $.
\end{block}
\end{frame}
%
\subsection{Line Integrals}
%
\begin{frame}{\insertsubsectionhead}
In this section we define an integral that is similar to a single integral except that
instead of integrating over an interval $ [a,b] $, we integrate over a curve $ C $. 
We start with a plane curve $ C $ given by the parametric equations
\[ x=x(t) \quad y=y(t) \quad a\le t \le b \]
or, equivalently, by the vector equation $ \vec{r}(t) = x(t) \vec{i} + y(t) \vec{j} $, and we assume that $ C $ is a smooth curve.

[This means that $ \vec{r}\ ' $ is continuous and $ \vec{r}\ ' (t) \ne 0 $.]
\end{frame}
%
\begin{frame}
\begin{minipage}{.45\textwidth}
\begin{figure}[h]
\includegraphics[page=924,clip,trim=22mm 98.5mm 148mm 136.5mm]{stewart-ccac.pdf}
\end{figure}
\end{minipage}
\begin{minipage}{.54\textwidth}
If we divide the parameter interval $ [a,b] $ into $ n $ subintervals $ [t_{i-1},t_{i}] $ of equal width
and we let $ x_i = x(t_i) $ and $ y_i = y(t_i) $, then the corresponding points $ P_i(x_i,y_i) $ divide $ C $ into $ n $ subarcs with lengths $ \Delta s_1 $, $ \Delta s_2 $, \ldots , $ \Delta s_n $. We choose any point $ P^{*}_{i}(x^{*}_i,y^{*}_i) $ in the $ i $th subarc. (This corresponds to a point $ t^{*}_{i} $ in $ [t_{i-1},t_{i}] $.)
\end{minipage}

Now if $ f $ is
any function of two variables whose domain includes the curve $ C $, we evaluate $ f $ at the point $ (x^{*}_i,y^{*}_i) $, multiply by the length $ \Delta s_i $ of the subarc, and form the sum
\[ \sum_{i=1}^{n} f(x^{*}_i,y^{*}_i)\Delta s_i   \]
which is similar to a Riemann sum. Then we take the limit of these sums and make
the definition by analogy with a single integral.
\end{frame}
%
\begin{frame}
\begin{definition}
	If $ f $ is defined on a smooth curve $ C $ given by 
	\[ x=x(t) \quad y=y(t) \quad a\le t \le b \]
	then, the \textbf{line integral of $ \bm{f} $ along $ \bm{C} $} is
	\[ \int_{C} f(x,y) ds = \lim\limits_{n\to\infty} \sum_{i=1}^{n} f(x^{*}_{i},y^{*}_{i})\Delta s_i \]
	if the limit exists.
\end{definition}
\end{frame}
%
\begin{frame}
We know that the length of $ C $ is
\[ L= \int_{a}^{b} \sqrt{\left(\frac{dx}{dt}\right)^{2}+\left(\frac{dy}{dt}\right)^{2}}dt. \]
A similar type of argument can be used to show that if $ f $ is a continuous function, then the limit in Definition always exists and the following formula can be used to evaluate the line integral:
\begin{equation}\label{p925eq3}
\int_{C}f(x,y)ds = \int_{a}^{b}f(x(t),y(t)) \sqrt{\left(\frac{dx}{dt}\right)^{2}+\left(\frac{dy}{dt}\right)^{2}}dt.
\end{equation}
\end{frame}
%
\begin{frame}
\begin{example}
Evaluate $ \displaystyle \int_{C} (2+x^2y)ds $, where $ C $ is the upper half of the unit circle $ x^2+y^2 =1 $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	In order to use Formula \eqref{p925eq3} we first need parametric equations to represent $ C $. Recall that the unit circle can be parametrized by means of the equations
	\[ x=\cos t \quad y= \sin t \]
	and the upper half of the circle is described by the parameter interval $ 0 \le t \le \pi $.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Therefore, Formula \eqref{p925eq3} gives
\begin{align*}
\int_{C} (2+x^2y)ds & = \int_{0}^{\pi} (2+\cos^{2} t \sin t) \sqrt{\left(\frac{dx}{dt}\right)^{2}+\left(\frac{dy}{dt}\right)^{2}}dt\\
& = \int_{0}^{\pi} (2+\cos^{2} t \sin t) \sqrt{\sin^{2} t+\cos^{2} t}dt\\
& = \int_{0}^{\pi} (2+\cos^{2} t \sin t) dt\\
& = \left[2t-\frac{\cos^{3} t}{3}\right]_{0}^{\pi} = 2\pi + \frac{2}{3}. \qedhere
\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{figure}[h]
\includegraphics[page=926,clip,trim=22mm 215mm 148.5mm 32.5mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
Suppose now that $ C $ is a \textbf{piecewise-smooth curve}; that is, $ C $ is a union of a finite number of smooth curves $ C_1, C_2,\ldots,C_n $, where, as illustrated in Figure, the initial point of $ C_{i+1} $ is the terminal point of $ C_i $. Then we define the integral of $ f $ along $ C $ as the sum of the integrals of $ f $ along each of the smooth pieces of $ C $:
\[ \int_{C} f(x,y) ds = \int_{C_1} f(x,y) ds + \int_{C_2} f(x,y) ds + \cdots + \int_{C_n} f(x,y) ds \]
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate $ \displaystyle \int_{C} 2x ds $, where $ C $ consists of the arc $ C_1 $ of the parabola $ y=x^2 $ from $ (0,0) $ to $ (1,1) $ followed by the vertical line segment $ C_2 $ from $ (1,1) $ to $ (1,2) $.
\end{example}
\begin{proof}[Solution]\let \qed \relax
\begin{minipage}{.29\textwidth}
\begin{figure}[h]
\includegraphics[page=926,clip,trim=22mm 164mm 163mm 83mm]{stewart-ccac.pdf}
\end{figure}
\end{minipage}
\begin{minipage}{.7\textwidth}
The curve $ C $ is shown in Figure. $ C_1 $ is the graph of a function of $ x $, so we can choose $ x $ as the parameter and the equations for $ C_1 $ become
\[ x=x \quad y=x^2 \quad 0\le x\le 1. \]
\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)] \let \qed \relax
Therefore
\begin{align*}
\int_{C_1} 2x ds & = \int_{0}^{1} 2x \sqrt{\left(\frac{dx}{dx}\right)^{2}+\left(\frac{dy}{dx}\right)^{2}}dx\\
& = \int_{0}^{1} 2x \sqrt{1+4x^2}dx = \frac{1}{6}(1+4x^2)^{3/2}\big|_{0}^{1} = \frac{5\sqrt{5}-1}{6}.
\end{align*}
On $ C_2 $ we choose $ y $ as the parameter, so the equations of $ C_2 $ are
\[ x=1 \quad y=y \quad 1\le y \le 2. \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Thus
\begin{align*}
\int_{C_2} 2x ds & = \int_{1}^{2} 2(1) \sqrt{\left(\frac{dx}{dy}\right)^{2}+\left(\frac{dy}{dy}\right)^{2}}dy\\
& = \int_{1}^{2} 2 dy = 2
\end{align*}
Therefore
\[ \int_{C} 2x ds = \int_{C_1} 2x ds + \int_{C_2} 2x ds = \frac{5\sqrt{5}-1}{6} +2. \qedhere \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{block}{}
	Two other line integrals are obtained by replacing $ \Delta s_i $ by either $ \Delta x_i = x_i - x^{i-1} $ or $ \Delta y_i = y_i - y^{i-1} $ in Definition. They are called the \textbf{line integrals of $ \bm{f} $ along with $ \bm{C} $ respect to $ \bm{x} $ and $ \bm{y} $}:
	\begin{align}
	\int_{C} f(x,y) dx &= \lim\limits_{n\to\infty} \sum_{i=1}^{n} f(x^{*}_{i}, y^{*}_{i}) \Delta x_i \label{p927eq5}\\
	\int_{C} f(x,y) dy &= \lim\limits_{n\to\infty} \sum_{i=1}^{n} f(x^{*}_{i}, y^{*}_{i}) \Delta y_i \label{p927eq6}
	\end{align}
\end{block}
\begin{block}{}
	When we want to distinguish the original line integral $ \displaystyle \int_{C} f(x,y) ds $ from those in
	Equations \eqref{p927eq5} and \eqref{p927eq6}, we call it the \textbf{line integral with respect to arc length}.
\end{block}
\end{frame}
%
\begin{frame}
\begin{block}{}
	The following formulas say that line integrals with respect to $ x $ and $ y $ can also be evaluated by expressing everything in terms of $ t $: $ x=x(t) $, $ y=y(t) $, $ dx=x'(t)dt $, $ dy=y'(t)dt $.
	\begin{equation}\label{p927eq7}
	\begin{split}
	\int_{C} f(x,y) dx &= \int_{a}^{b} f(x(t),y(t))x'(t)dt\\
	\int_{C} f(x,y) dy &= \int_{a}^{b} f(x(t),y(t))y'(t)dt
	\end{split}
	\end{equation}
\end{block}
\begin{block}{}
	It frequently happens that line integrals with respect to $ x $ and $ y $ occur together. When this happens, it's customary to abbreviate by writing
	\[ \int_{C} P(x,y) dx + \int_{C} Q(x,y) dy = \int_{C} P(x,y) dx + Q(x,y) dy .\]
\end{block}
\end{frame}
%
\begin{frame}
When we are setting up a line integral, sometimes the most difficult thing is to think of a parametric representation for a curve whose geometric description is given. 
\begin{block}{}
	In particular, we often need to parametrize a line segment, so it's useful to remember that a vector representation of the line segment that starts at $ \vec{r}_{0} $ and $ \vec{r}_{1} $ ends at is given by
	\begin{equation}\label{p928eq8}
	\vec{r}(t) = (1-t) \vec{r}_{0} + t \vec{r}_{1} \quad 0 \le t \le 1.
	\end{equation}
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
	Evaluate $ \displaystyle \int_{C} y^2 dx + x dy $, where
\begin{itemize}
\item[(a)] $ C=C_1 $ is the line segment from $ (-5,-3) $ to $ (0,2) $ and
\item[(b)] $ C=C_2 $ is the arc of the parabola $ x=4-y^2 $ from $ (-5,-3) $ to $ (0,2) $.
\end{itemize}
\end{example}
\begin{proof}[Solution]\let \qed \relax
\begin{minipage}{.39\textwidth}
\begin{figure}[h]
\includegraphics[page=928,clip,trim=20mm 149mm 151mm 94.5mm]{stewart-ccac.pdf}
\end{figure}
\end{minipage}
\begin{minipage}{.6\textwidth}
\begin{itemize}
\item[(a)] A parametric representation for the line segment is
\[ x=5t-5 \quad y= 5t-3 \quad 0 \le t \le 1. \]
(Use Equation \eqref{p928eq8} with $ \vec{r}_{0} = \langle -5, -3 \rangle $ and $ \vec{r}_{1} = \langle 0,2 \rangle $.)
\end{itemize}
\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)] \let \qed \relax
\begin{itemize}
\item[ ] Then $ dx = 5 dt $, $ dy = 5 dt $ and Formula \eqref{p927eq7} gives
\begin{align*}
\int_{C_1} y^2 dx + x dy & = \int_{0}^{1} (5t-3)^{2} 5dt + (5t-5) 5 dt\\
& = 5 \int_{0}^{1} (25t^2-25t+4)dt\\
& =5 \left[ \frac{25t^3}{3} - \frac{25t^2}{2} + 4t \right]_{0}^{1} = -\frac{5}{6}.
\end{align*}
\item[(b)] Since the parabola is given as a function of $ y $, let's take $ y $ as the parameter and
write $ C_2 $ as
\[ x=4-y^2 \quad y=y \quad -3\le y \le 2. \]
\end{itemize}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
\begin{itemize}
\item[ ] Then $ dx = -2y dy $ and by Formula \eqref{p927eq7} we have
\begin{align*}
\int_{C_2} y^2 dx + x dy &= \int_{-3}^{2} y^2(-2y) dy + (4-y^2)dy\\
& = \int_{-3}^{2} (-2y^3 -y^2+4)dy\\
& = \left[-\frac{y^4}{2}-\frac{y^3}{3}+4y\right]_{-3}^{2} = \frac{245}{6}. \qedhere
\end{align*}
\end{itemize}
\end{proof}
\end{frame}