\section{Line Integrals}
%
\subsection{Line Integrals in Space}
%
\begin{frame}{\insertsubsectionhead}
We now suppose that $C$ is a smooth space curve given by the parametric equations
\[ x=x(t)\quad y=y(t) \quad z=z(t)\quad a\le t\le b \]
or by a vector equation $\vec{r}(t)=x(t)\vec{i}+y(t)\vec{j}+z(t)\vec{k}$. If $f$ is a function of three variables that is continuous on some region containing $C$, then we define the  \textbf{line integral of $\bm{f}$ along $\bm{C}$} (with respect to arc length) in manner similar to that for plane curves:
\[ \int\limits_{C}f(x,y,z)ds=\lim\limits_{n\to \infty}\sum\limits_{i=1}^{n}f(x_i*,y_i*,z_i*)\Delta s_i \]
\end{frame}
%
\begin{frame}
We evaluate  it using a formula:
\begin{equation}\label{p929_eqn09}
\int_C f(x,y,z)ds=\int_{a}^{b}f(x(t),y(t),z(t))\sqrt{\left(\dfrac{dx}{dt}\right)^2+\left(\dfrac{dy}{dt}\right)^2+\left(\dfrac{dz}{dt}\right)^2}dt
\end{equation}
Observe that the integral in the above equation can be written in the more compact vector notation
\[ \int\limits_{a}^{b}f(\vec{r}(t))|\vec{r}\ '(t)|dt \]
For the special case $f(x,y,z)=1$, we get 
\[ \int_Cds=\int\limits_{a}^{b}|\vec{r}\ '(t)|dt=L \]
where $L$ is the length of the curve $C$.
\end{frame}
%
\begin{frame}
Line integrals along $C$ with respect to $x,y,z$ can also be defined. For example,
\begin{align*}
\int\limits_{C}f(x,y,z)dz&= \lim\limits_{n\to \infty}\sum\limits_{i=1}^{n}f(x_i*,y_i*,z_i*)\Delta z_i\\
&= \int\limits_{a}^{b}f(x(t),y(t),z(t))z'(t)dt
\end{align*}
Therefore, as with line integrals in the plane , we evaluate integrals of the form
\begin{equation}\label{p930_eqn10}
\int_CP(x,y,z)dx+Q(x,y,z)dy+R(x,y,z)dz
\end{equation}
by expressing everything $(x,y,z,dx,dy,dz)$ in terms of the parameter $t$.
\end{frame}
%
\begin{frame}
\begin{example}
Evaluate $\int_Cy\sin zds$, where $C$ is the circular helix given by the equations $x=\cos t$, $y=\sin t$, $z=t$, $0\leqslant t \leqslant 2\pi$
\begin{figure}
\includegraphics[page=930,clip,trim=18mm 117mm 153mm 94mm,scale=1]{stewart-ccac.pdf}
\end{figure}
\end{example}
\end{frame}

\begin{frame}
\begin{proof}[Solution]
Formula \eqref{p929_eqn09}  gives
\begin{align*}
\int_Cy\sin zds&= \int\limits_{0}^{2\pi}(\sin t) \sin t \sqrt{\left(\dfrac{dx}{dt}\right)^2+\left(\dfrac{dy}{dt}\right)^2+\left(\dfrac{dz}{dt}\right)^2}dt\\
&= \int\limits_{0}^{2\pi}\sin ^2t\sqrt{\sin ^2t+\cos ^2t+1}dt\\
&= \sqrt{2}\int_{0}^{2\pi}\dfrac{1}{2}(1-\cos 2t)dt=\dfrac{\sqrt{2}}{2}\left[t-\dfrac{1}{2}\sin 2t\right]_0^{2\pi}=\sqrt{2}\pi
\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
Evaluate $\int_C ydx+zdy+xdz$ where $C$ consists of the line segment $C_1$ from $(2,0,0)$ to $(3,4,5)$ followed by the vertical line segment $C_2$ from $(3,4,5)$ to $(3,4,0)$.
\end{example}
\begin{proof}[Solution]\let \qed \relax
	\begin{minipage}{.39\textwidth}
		\begin{figure}
			\includegraphics[page=930,clip,trim=20.5mm 55mm 156mm 188mm,scale=1]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.59\textwidth}
		We write $C_1$ as
		\begin{align*}
		\vec{r}(t)&=(1-t)\langle 2,0,0 \rangle+t \langle 3,4,5 \rangle \\ 
		&= \langle 2+t,4t,5t \rangle
		\end{align*}
		or, in parametric form, as
		\[ x=2+t \quad y=4t \quad z=5t \quad 0\le t\le 1 \]
	\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qedsymbol\relax
Thus 
\begin{align*}
\int_{C_1}ydx+zdy+xdz&= \int\limits_{0}^{1}(4t)dt+(5t)4dt+(2+t)5dt\\
&=\int\limits_{0}^{1}(10+29t)dt=\left. 10t+29\dfrac{t^2}{2}\right]_0^1=24.5
\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution(cont.)]
Likewise $C_2$ can be written in the form
\[ \mathbf{r}(t)=(1-t)<3,4,5>+t<3,4,0>=<3,4,5-5t> \]
or
\[ x=3 \quad y=4 \quad z=5-5t \quad 0\leqslant t \leqslant 1 \]
Then $dx=0=dy$, so
\[ \int_{C_2}ydx+zdy+xdz=\int\limits_{0}^{1}3(-5)dt=-15 \]
Adding the values of these integrals, we obtain
\[   \int_{C}ydx+zdy+xdz=24.5-15=9.5\]
\end{proof}
\end{frame}

\subsection{Line Integrals of Vector Fields}
\begin{frame}\frametitle{Line Integrals of Vector Fields}
Suppose that $\mathbf{F}=P\mathbf{i}+Q\mathbf{j}+R\mathbf{k}$ is a continuous force field on $\mathds{R}^3$. We wish to compute the work done by this force in moving a particle along a smooth curve $C$.  We define the work $W$ done by the force field $\mathbf{F}$
\begin{equation}\label{p931_eqn12}
W=\int_{C}\mathbf{F}(x,y,z)\cdot \mathbf{T}(x,y,z)ds=\int_C\mathbf{F}\cdot \mathbf{T} ds
\end{equation}
where $\mathbf{T}(x,y,z)$ is the unit tangent vector at the point $(x,y,z)$ on $C$.\\
Equation \eqref{p931_eqn12} says that work is the line integral with respect to arc length of the tangential component of the force.
\end{frame}

\begin{frame}
If the curve $C$ is given by the vector equation $\mathbf{r}(t)=x(t)\mathbf{i}+y(t)\mathbf{j}+z(t)\mathbf{k}$, then $\mathbf{T}(t)=\mathbf{r}'(t)/|\mathbf{r}'(t)|$, so using the Equation \eqref{p929_eqn09} we can rewrite Equation \eqref{p931_eqn12} in the form
\begin{align*}
W&= \int\limits_{a}^{b}\left[\mathbf{F}(\mathbf{r}(t))\cdot \dfrac{\mathbf{r}'(t)}{|\mathbf{r}'(t)|}\right]|\mathbf{r}'(t)|dt\\
&= \int\limits_{a}^{b} \mathbf{F}(\mathbf{r}(t))\cdot \mathbf{r}'(t) dt
\end{align*}
This integral is often abbreviated as $\int_C\mathbf{F}\cdot d\mathbf{r}$ and occurs in other areas of physics as well. Therefore, we make the following definition for the line integral of any continuous vector field.
\end{frame}

\begin{frame}
\begin{definition}
Let $\mathbf{F}$ be a continuous vector field defined on a smooth curve $C$ given by a vector function $\mathbf{r}(t)$, $a\leqslant t \leqslant b$. Then the line integral of $\mathbf{F}$ along $C$ is
\[ \int_C\mathbf{F}\cdot d\mathbf{r}= \int_C\mathbf{F}(\mathbf{r}(t))\cdot \mathbf{r}'(t) dt=\int_C\mathbf{F}\cdot \mathbf{T} ds \]
\end{definition}
\end{frame}


\begin{frame}
\begin{example}
Evaluate $\int_C\mathbf{F}\cdot d\mathbf{r}$, where $\mathbf{F}(x,y,z)=xy\mathbf{i}+yz\mathbf{j}+zx\mathbf{k}$ and $C$ is the twisted cubic given by 
\[ x=t \quad y=t^2 \quad z=t^3 \quad 0\leqslant t \leqslant 1 \]
\end{example}
\end{frame}
\begin{frame}
\begin{proof}[Solution]
We have
\begin{align*}
\mathbf{r}(t)&= t\mathbf{i}+t^2\mathbf{j}+t^3\mathbf{k}\\
\mathbf{r}'(t)&= 1\mathbf{i}+2t\mathbf{j}+3t^2\mathbf{k}\\
\mathbf{F}(\mathbf{r}(t))&= t^3\mathbf{i}+t^5\mathbf{j}+t^4\mathbf{k}
\end{align*}
Thus 
\begin{align*}
\int_C\mathbf{F}\cdot d\mathbf{r}&= \int\limits_{0}^{1}\mathbf{F}(\mathbf{r}(t))\cdot \mathbf{r}'(t) dt\\
&= \int\limits_{0}^{1}(t^3+5t^6)dt=\left.\dfrac{t^4}{4}+\dfrac{5t^7}{7}\right]_0^1=\dfrac{27}{28} \qedhere
\end{align*}
\end{proof}
\end{frame}

\begin{frame}
Finally, we note the connection between line integrals of vector fields and line integrals of scalar fields. Suppose the vector field $\mathbf{F}$ on $\mathds{R}^3$ is given in component form by the equation $\mathbf{F}=P\mathbf{i}+Q\mathbf{j}+R\mathbf{k}$. Hence,
\begin{align*}
\int_C\mathbf{F}\cdot d\mathbf{r}&= \int\limits_{a}^{b}\mathbf{F}(\mathbf{r}(t))\cdot \mathbf{r}'(t) dt\\
&=\int\limits_{a}^{b}(P\mathbf{i}+Q\mathbf{j}+R\mathbf{k})\cdot(x'(t)\mathbf{i}+y'(t)\mathbf{j}+z'(t)\mathbf{k})dt\\
&=\int\limits_{a}^{b}[P(x(t),y(t),z(t))x'(t)\\
&+Q(x(t),y(t),z(t))y'(t)+R(x(t),y(t),z(t))z'(t)]dt
\end{align*}
But this last integral is precisely the line integral in \eqref{p930_eqn10}. Therefore, we have
\[  \int_C\mathbf{F}\cdot d\mathbf{r}=\int_C Pdx+Qdy+Rdz \quad \text{where} \quad \mathbf{F}=P\mathbf{i}+Q\mathbf{j}+R\mathbf{k}\]
\end{frame}

\subsection{The Fundamental Theorem for Line Integrals}

\begin{frame}\frametitle{The Fundamental Theorem for Line Integrals}
Recall that the fundamental theorem of calculus can be written as 
\begin{equation}\label{p936_eqn1}
\int\limits_{a}^{b}F'(x)dx=F(b)-F(a)
\end{equation}
where $F'$ is continuous on $[a,b]$. 
The following theorem can be regarded as a version of the Fundamental Theorem for line integrals.
\begin{theorem}
Let $C$ be a smooth curve given by the vector function $\mathbf{r}(t)$, $a\leqslant t\leqslant b$. Let $f$ be a differentiable function of two or three variables whose gradient vector $\nabla f$ is continuous on $C$. Then
\[ \int_C \nabla f \cdot d\mathbf{r}=f(\mathbf{r}(b))- f(\mathbf{r}(a))\]
\end{theorem}
\end{frame}

\begin{frame}
\begin{block}{Note}
Theorem says that we can evaluate the line integral of a conservative vector field (the gradient vector field of the potential function $f$) simply by knowing the value of $f$ at the endpoints of $C$. In fact, Theorem  says that the line integral of $\nabla f$ is the total change in $f$. If $f$ is a function of two variables and $C$ is a plane curve with initial point $A(x_1,y_1)$ and terminal point $B(x_2,y_2)$, then Theorem becomes
\[  \int_C \nabla f \cdot d\mathbf{r}=f(x_2,y_2)-f(x_1,y_1) \]
If $f$ is a function of three variables and $C$ is a space curve joining the point  $A(x_1,y_1,z_1)$ to the point $B(x_2,y_2,z_2)$, then we have
\[  \int_C \nabla f \cdot d\mathbf{r}=f(x_2,y_2,z_2)-f(x_1,y_1,z_1) \]
\end{block}
\end{frame}

\begin{frame}
\begin{figure}
\includegraphics[page=936,clip,trim=81.1mm 28.42mm 27.45mm 210mm,scale=1]{stewart-ccac.pdf}
\end{figure}
\end{frame}

\subsection{Independence of Path}
\begin{frame}
\frametitle{Independence of Path}
Suppose $C_1$ and $C_2$ are two piecewise-smooth curves (which are called paths) that have the same initial point $A$ and terminal point $B$. We know that in general, $\int_{C_1}\mathbf{F}\cdot d\mathbf{r}\neq \int_{C_2}\mathbf{F}\cdot d\mathbf{r}$. But one implication of fundamental Theorem is that
\[ \int_{C_1} \nabla f \cdot d\mathbf{r}=\int_{C_2} \nabla f \cdot d\mathbf{r} \]
whenever $\nabla f$ is continuous. In other words, the line integral of a conservative vector field depends only on the initial point and terminal point of a curve.\\
In general, if $\mathbf{F}$ is a continuous vector field with domain $D$, we say that the line integral $\int_{C}\mathbf{F}\cdot d\mathbf{r}$ is independent of path if $\int_{C_1}\mathbf{F}\cdot d\mathbf{r}=\int_{C_2}\mathbf{F}\cdot d\mathbf{r}$ for any two paths $C_1$  and $C_2$ in $D$ that have the same initial and terminal points. With this terminology we can say that line integrals of conservative vector fields are independent of path.
\end{frame}


\begin{frame}
A curve is called \textbf{closed} if its terminal point coincides with its initial point, that is,
$\mathbf{r}(a)=\mathbf{r}(b)$.
\begin{figure}
\includegraphics[page=938,clip,trim=17.31mm 216.13mm 143.39mm 33.25mm,scale=1]{stewart-ccac.pdf}
\end{figure}
\end{frame}

\begin{frame}
\begin{theorem}
$\int_C\mathbf{F}\cdot d\mathbf{r}$ is independent of path in $D$ if and only if $\int_C\mathbf{F}\cdot d\mathbf{r}=0$ for every closed path $C$ in $D$.
\end{theorem}
The physical interpretation
is that the work done by a conservative force field  as it moves an object around a closed path is $0$.
\end{frame}

\begin{frame}
\begin{theorem}
Suppose $\mathbf{F}$ is a vector field that is continuous on an open connected region $D$. If $\int_C\mathbf{F}\cdot d\mathbf{r}$ is independent of path in $D$, then $\mathbf{F}$ is a conservative vector field on $D$; that is, there exists a function $f$ such that $\nabla f= \mathbf{F}$
\end{theorem}
The theorem says that the only vector fields that are independent of path are conservative. We assume that $D$ is \textbf{open}, which means that for every point $P$ in $D$ there is a disk with center $P$ that lies entirely in $D$. In addition, we assume that $D$ is \textbf{connected}. This means that any two points in $D$ can be joined by a path that lies in $D$.
\end{frame}

\begin{frame}
\begin{theorem}
If $\mathbf{F}(x,y)=P(x,y)\mathbf{i}+Q(x,y)\mathbf{j}$ is a conservative field, where $P$ and $Q$ have continuous first-order partial derivatives on a domain $D$, then throughout $D$ we have
\[ \dfrac{\partial P}{\partial y}=\dfrac{\partial Q}{\partial x} \]
\end{theorem}
\end{frame}

\begin{frame}
\begin{theorem}
Let $\mathbf{F}=P\mathbf{i}+Q\mathbf{j}$ be a vector field on an open simply-connected region $D$. Suppose that $P$ and $Q$ have continuous first-order derivatives and
\[ \dfrac{\partial P}{\partial y}=\dfrac{\partial Q}{\partial x} \quad \textrm{throughout } D \]
Then $\mathbf{F}$ is conservative.
\end{theorem}
A \textbf{simply-connected region} in the plane is a connected region $D$ such that every simple closed curve in $D$ encloses only point that are in $D$. Intuitively speaking, a simply-connected region contains no hole and can't consist of two separate pieces.
\end{frame}

\begin{frame}
\begin{figure}
\includegraphics[page=940,clip,trim=29.78mm 234.83mm 156mm 30.48mm,scale=1.5]{stewart-ccac.pdf}
\caption{simply-connected region}
\end{figure}
\begin{figure}
\includegraphics[page=940,clip,trim=16.62mm 207.125mm 143.85mm 50.10mm,scale=1]{stewart-ccac.pdf}
\caption{regions that are not simply-connected}
\end{figure}
\end{frame}

\begin{frame}
\begin{example}
Determine whether or not the vector field
\[ \mathbf{F}(x,y)=(x-y)\mathbf{i}+(x-2)\mathbf{j} \]
is conservative.
\end{example}
\begin{proof}[Solution]
Let $P(x,y)=x-y$ and $Q(x,y)=x-2$. Then
\[\dfrac{\partial P}{\partial y}=-1 \quad \dfrac{\partial Q}{\partial x}=1  \]
Since $\partial P/ \partial y \neq \partial Q/ \partial x$, $\mathbf{F}$ is not conservative.
\end{proof}
\end{frame}

\begin{frame}
\begin{example}
Determine whether or not the vector field
\[ \mathbf{F}(x,y)=(3+2xy)\mathbf{i}+(x^2-3y^2)\mathbf{j} \]
is conservative.
\end{example}
\begin{proof}[Solution]
Let $P(x,y)=3+2xy$ and $Q(x,y)=x^2-3y^2$. Then
\[\dfrac{\partial P}{\partial y}=2x= \dfrac{\partial Q}{\partial x}  \]
Also, the domain of $\mathbf{F}$ is the entire plane, which is open and simply-connected. Therefore, $\mathbf{F}$ is conservative.
\end{proof}
\end{frame}

\begin{frame}
\begin{example}
\begin{itemize}
\item[a)]If  $\mathbf{F}(x,y)=(3+2xy)\mathbf{i}+(x^2-3y^2)\mathbf{j}$, find a function $f$ such that $\mathbf{F}=\nabla f$.
\item[b)] Evaluate the line integral $\int_C \mathbf{F}\cdot d\mathbf{r}$, where $C$ is the curve given by $\mathbf{r}(t)=e^t\sin t \mathbf{i}+e^t\cos t \mathbf{j}$, $0\leqslant t \leqslant \pi$.
\end{itemize}
\end{example}
\end{frame}

\begin{frame}
\begin{proof}[Solution]\let\qedsymbol\relax
\begin{itemize}
\item[a)] From previous example we know that $\mathbf{F}$ is conservative and so there exist a function $f$ with $\nabla f= \mathbf{F}$, that is,
\begin{equation}\label{pg941eqn7}
f_x(x,y)=3+2xy
\end{equation}
\begin{equation}\label{pg941eqn8}
f_y(x,y)=x^2-3y^2
\end{equation}
Integrating \eqref{pg941eqn7} with respect to $x$, we obtain
\begin{equation}\label{pg941eqn9}
f(x,y)=3x+x^2y+g(y)
\end{equation}
Notice that the constant of integration is a constant with respect to $x$, that is, a function
of $y$, which we have called $g(y)$. Next we differentiate both sides of \eqref{pg941eqn9} with
respect to $y$:
\begin{equation}\label{pg941eqn10}
f_y(x,y)=x^2+g'(y)
\end{equation}
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution(cont.)]\let\qedsymbol\relax
Comparing \eqref{pg941eqn8} and \eqref{pg941eqn10}, we see that
\[ g'(y)=-3y^2 \] 
Integrating with respect to $y$, we have 
\[ g(y)=-y^3+K \]
where $K$ is a constant. Putting this in \eqref{pg941eqn9}, we have
\[ f(x,y)=3x+x^2y-y^3+K \]
as the desired potential function.
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution(cont.)]
\begin{itemize}
\item[b)] We have to know the initial and terminal points of $C$, namely, $\mathbf{r}(0)=(0,1)$ and $\mathbf{r}(\pi)=(0,-e^{\pi})$. In the expression for $f(x,y)$ in part (a), any value of the constant $K$ will do, so let's choose $K=0$. Then we have
\begin{eqnarray*}
\int_C \mathbf{F}\cdot d\mathbf{r}&=& \int_C \nabla f \cdot d\mathbf{r}=f(0,-e^{\pi})-f(0,1)\\
&=& e^{3\pi}-(-1)=e^{3\pi}+1
\end{eqnarray*}
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\begin{example}
If $\mathbf{F}(x,y,z)=y^2\mathbf{i}+(2xy+e^{3z})\mathbf{j}+3ye^{3z}\mathbf{k}$, find a function $f$ such that $\nabla f= \mathbf{F}$
\end{example}
\begin{proof}[Solution]\let\qedsymbol\relax
If there is such a function $f$, then
\begin{eqnarray}
f_x(x,y,z)&=& y^2 \label{pg941eqn11}\\
f_y(x,y,z)&=& 2xy+e^{3z} \label{pg941eqn12}\\
f_z(x,y,z)&=& 3ye^{3z} \label{pg941eqn13}
\end{eqnarray}
Integrating \eqref{pg941eqn11} with respect to $x$, we get 
\begin{equation}\label{pg942eqn14}
f(x,y,z)xy^2+g(y,z)
\end{equation}
where $g(y,z)$ is a constant with respect to $x$. 
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution(cont.)]
Then differentiating \eqref{pg942eqn14} with respect to $y$, we have 
\[ f_y(x,y,z)=2xy+g_y(y,z) \]
and comparing with \eqref{pg941eqn12} gives
\[ g_y(y,z)=e^{3z} \]
 Thus, $g(y,z)=ye^{3z}+h(z)$ and rewrite \eqref{pg942eqn14} as
 \[ f(x,y,z)=xy^2+ye^{3z}+h(z) \]
 Finally differentiating with respect to $z$ and comparing with \eqref{pg941eqn13}, we obtain $h'(z)=0$ and therefore, $h(z)=K$, a constant. The desired function is
 \[ f(x,y,z)=xy^2+ye^{3z}+K \]
 It is easily verified that $\nabla f = \mathbf{F}$
\end{proof}
\end{frame}









