%%
\section{Partial Derivatives}
%
\subsection{Lagrange Multipliers}
%
\begin{frame}{\insertsubsectionhead}{}
In a previous example we maximized a volume function $V=xyz$ subject to the constraint $2xz+2yz+xy=12$, which expressed the side condition that the surface area was $12\,\textrm{m}^{2}$.
\begin{block}{}
	In this section, we present Lagrange's method for maximizing or minimizing a general function $f(x,y,z)$ subject to a constraint (or side condition) of the form $g(x,y,z)=k$.
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{block}{Method of Lagrange Multipliers}
To find the maximum and minimum values of $f(x,y,z)$ subject to the constraint $g(x,y,z)=k$ (assuming that these extreme values exist):
\begin{enumerate}
\item Find all values of $x$, $y$, $z$, and $\lambda$ such that
\begin{align*}
\nabla f(x,y,z)&=\lambda\nabla g(x,y,z),\\
g(x,y,z)&=k.
\end{align*}
\item Evaluate $f$ at all the points $(x,y,z)$ that result from Step {\usebeamercolor[fg]{structure}1}. The largest of these values is the maximum value of $f$; the smallest is the minimum value of $f$.
\end{enumerate}
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
A rectangular box without a lid is to be made from $12\,\textrm{m}^{2}$ of cardboard. Find the maximum volume of such a box.
\end{example}
\begin{proof}[{Solution}]\let \qed \relax
We let $x$, $y$ and $z$ be the length, width and height, respectively, of the box in meters. Then, we wish to maximize
\begin{equation*}
V=xyz
\end{equation*}
subject to the constraint
\begin{equation*}
g(x,y,z)=2xz+2yz+xy=12.
\end{equation*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]\let \qed \relax
Using the method of Lagrange multipliers, we look for values of $x$, $y$, $z$ and $\lambda$ such that $\nabla{}V=\lambda\nabla{}g$ and $g(x,y,z)=12$. This gives the equations
\begin{equation*}
V_{x}=\lambda{}g_{x},\quad
V_{y}=\lambda{}g_{y},\quad
V_{z}=\lambda{}g_{z},\quad
2xz+2yz+xy=12,
\end{equation*}
which become
\begin{gather}
yz=\lambda(2z+y),\label{s11ss8exm1slneqn1}\\
xz=\lambda(2z+x),\label{s11ss8exm1slneqn2}\\
xy=\lambda(2x+2y),\label{s11ss8exm1slneqn3}\\
2xz+2yz+xy=12.\label{s11ss8exm1slneqn4}
\end{gather}
There are no general rules for solving systems of equations. Sometimes some ingenuity is required.
\end{proof}
\end{frame}
%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]\let \qed \relax
In the present example, you might notice that if we multiply \eqref{s11ss8exm1slneqn1} by $x$, \eqref{s11ss8exm1slneqn2} by $y$, and \eqref{s11ss8exm1slneqn3} by $z$, then the left sides of these equations will be identical.
\begin{gather}
xyz=\lambda(2xz+xy),\label{s11ss8exm1slneqn5}\\
xyz=\lambda(2yz+xy),\label{s11ss8exm1slneqn6}\\
xyz=\lambda(2xz+2yz).\label{s11ss8exm1slneqn7}
\end{gather}
We observe that $\lambda\neq0$ because $\lambda=0$ would imply $yz=xz=xy=0$ from \eqref{s11ss8exm1slneqn1}, \eqref{s11ss8exm1slneqn2} and \eqref{s11ss8exm1slneqn3} and this would contradict \eqref{s11ss8exm1slneqn4}.
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]
Therefore, from \eqref{s11ss8exm1slneqn5} and \eqref{s11ss8exm1slneqn6}, we have
\begin{equation*}
2xz+xy=2yz+xy,
\end{equation*}
which gives $xz=yz$. But $z\neq0$ (since $z=0$ would give $V=0$), so $x=y$. From \eqref{s11ss8exm1slneqn6} and \eqref{s11ss8exm1slneqn7}, we have
\begin{equation*}
2yz+xy=2xz+2yz,
\end{equation*}
which gives $2xz=xy$ and so (since $x\neq0$) $y=2z$. If we now put $x=y=2z$ in \eqref{s11ss8exm1slneqn4}, we get
\begin{equation*}
4z^{2}+4z^{2}+4z^{2}=12.
\end{equation*}
Since $x$, $y$, $z$ and are all positive, we therefore have $z=1$, $x=2$ and $y=2$ as before. Thus, $V=4$.
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Find the extreme values of $f(x,y)=x^{2}+2y^{2}$ on the circle $x^{2}+y^{2}=1$.
\end{example}
\begin{proof}[{Solution}]\let \qed \relax
We are asked for the extreme values of $f$ subject to the constraint $g(x,y)=x^{2}+y^{2}=1$. Using Lagrange multipliers, we solve the equations $\nabla{}f=\lambda\nabla{}g$, $g(x,y)=1$, which can be written as
\begin{equation}
f_{x}=\lambda{}g_{x},\quad
f_{y}=\lambda{}g_{y},\quad
g(x,y)=1\nonumber
\end{equation}
or as
\begin{gather}
2x=2x\lambda,\label{s11ss8exm2slneqn1}\\
4y=2y\lambda,\label{s11ss8exm2slneqn2}\\
x^{2}+y^{2}=1.\label{s11ss8exm2slneqn3}
\end{gather}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)] \let \qed \relax
From \eqref{s11ss8exm2slneqn1}, we have $x=0$ or $\lambda=1$. If $x=0$, then \eqref{s11ss8exm2slneqn3} gives $y=\pm 1$. If $\lambda=1$, then $y=0$ from \eqref{s11ss8exm2slneqn2}, so then \eqref{s11ss8exm2slneqn3} gives $x=\pm 1$. Therefore, $f$ has possible extreme values at the points $(0,1),(0,-1),(1,0)$ and $(-1,0)$. Evaluating $f$ at these four points, we find that
\begin{equation}
f(0,1)=2,\quad
f(0,-1)=2,\quad
f(1,0)=1,\quad
f(-1,0)=1.\nonumber
\end{equation}
Therefore, the maximum value of $f$ on the circle $x^{2}+y^{2}=1$ is $f(0,\pm1)=2$ and the minimum value is $f(\pm1,0)=1$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	\begin{figure}[htb]
		\includegraphics[page=824,scale=1.1,clip,trim=23mm 37mm 149.5mm 190mm]{stewart-ccac.pdf}
		\includegraphics[page=825,scale=1.2,clip,trim=22mm 183.mm 145.5mm 56.5mm]{stewart-ccac.pdf}
	\end{figure}
	Checking Figure, we see that these values look reasonable.
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Find the points on the sphere $x^{2}+y^{2}+z^{2}=4$ that are closest to and farthest from the point $(3,1,-1)$.
\end{example}
\begin{proof}[{Solution}]\let \qed \relax
The distance from a point $(x,y,z)$ to the point $(3,1,-1)$ is
\begin{equation*}
d=\sqrt{(x-3)^{2}+(y-1)^{2}+(z+1)^{2}}
\end{equation*}
but the algebra is simpler if we instead maximize and minimize the square of the distance:
\begin{equation*}
d^{2}=(x-3)^{2}+(y-1)^{2}+(z+1)^{2}.
\end{equation*}
The constraint is that the point $(x,y,z)$ lies on the sphere, that is,
\begin{equation*}
g(x,y,z)=x^{2}+y^{2}+z^{2}=4.
\end{equation*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)] \let \qed \relax
According to the method of Lagrange multipliers, we solve $\nabla{}f=\lambda\nabla{}g$, $g=4$. This gives
\begin{gather}
2(x-3)=2x\lambda,\label{s11ss8exm3slneqn1}\\
2(y-1)=2y\lambda,\label{s11ss8exm3slneqn2}\\
2(z+1)=2z\lambda,\label{s11ss8exm3slneqn3}\\
x^{2}+y^{2}+z^{2}=4.\label{s11ss8exm3slneqn4}
\end{gather}
The simplest way to solve these equations is to solve for $x,y$ and $z$ in terms of from \eqref{s11ss8exm3slneqn1}, \eqref{s11ss8exm3slneqn2} and \eqref{s11ss8exm3slneqn3}, and then substitute these values into \eqref{s11ss8exm3slneqn4}. From \eqref{s11ss8exm3slneqn1}, we have
\begin{equation*}
x-3=x\lambda\quad\text{or}\quad x(1-\lambda)=3 \quad\text{or}\quad x=\frac{3}{1-\lambda}.
\end{equation*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution (cont.)]\let \qed \relax
Note that $1-\lambda\neq0$ because $\lambda=1$ is impossible from \eqref{s11ss8exm3slneqn1}. Similarly, \eqref{s11ss8exm3slneqn2} and \eqref{s11ss8exm3slneqn3} give
\begin{equation*}
y=\frac{1}{1-\lambda} \quad\text{and}\quad z=-\frac{1}{1-\lambda}.
\end{equation*}
Therefore, from \eqref{s11ss8exm3slneqn4}, we have
\begin{equation*}
\frac{3^{2}}{(1-\lambda)^{2}}+\frac{1^{2}}{(1-\lambda)^{2}}+\frac{(-1)^{2}}{(1-\lambda)^{2}}=4,
\end{equation*}
which gives $(1-\lambda)^{2}=\frac{11}{4}$ so $\lambda=1\pm\frac{\sqrt{11}}{2}$. These values of $\lambda$, then give the corresponding points $(x,y,z)$:
\begin{equation*}
\left(\tfrac{6}{\sqrt{11}},\tfrac{2}{\sqrt{11}},-\tfrac{2}{\sqrt{11}}\right)\quad\text{and}\quad\left(-\tfrac{6}{\sqrt{11}},-\tfrac{2}{\sqrt{11}},\tfrac{2}{\sqrt{11}}\right).
\end{equation*}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	\begin{minipage}{.49\textwidth}
		\begin{figure}[htb]
			\centering
			\includegraphics[page=826,scale=1.2,clip,trim=20mm 180mm 146.5mm 50mm]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
	\begin{minipage}{.49\textwidth}
		It's easy to see that has a smaller value at the first of these points, so the closest point is $\left(\frac{6}{\sqrt{11}},\frac{2}{\sqrt{11}},-\frac{2}{\sqrt{11}}\right)$ and the farthest is $\left(-\frac{6}{\sqrt{11}},-\frac{2}{\sqrt{11}},\frac{2}{\sqrt{11}}\right)$. \qedhere
	\end{minipage}
\end{proof}
\end{frame}
%%
\section{Multiple Integrals}
%
\subsection{Double Integrals over Rectangles}
%
\begin{frame}{\insertsectionhead}{\insertsubsectionhead}
In much the same way that our attempt to solve the area problem led to the definition of a definite integral, we now seek to find the volume of a solid and in the process we arrive at the definition of a double integral.
\end{frame}
%%
\subsubsection{Volumes and Double Integrals}
%
\begin{frame}{\insertsubsubsectionhead}{}
In a similar manner, we consider a function of two variables defined on a closed rectangle
\begin{equation*}
R=[a,b]\times[c,d]=\{(x,y)\in\mathds{R}^{2}:\ a\le x\leq{}b,\ c\le y\le d\}
\end{equation*}

\begin{minipage}{.49\textwidth}
and we first suppose that $f(x,y)\geq0$. The graph of $f$ is a surface with equation $z=f(x,y)$. Let $S$ be the solid that lies above $R$ and under the graph of $f$, that is,
\end{minipage}
\begin{minipage}{.49\textwidth}
	\begin{figure}[htb]
		\centering
		\includegraphics[page=840,scale=1,clip,trim=20mm 203.5mm 146.5mm 39mm]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\begin{equation*}
S=\{(x,y,z)\in\mathds{R}^{3}:\ 0\leq{}z\leq{}f(x,y),\ (x,y)\in{}R\}
\end{equation*}
Our goal is to find the volume of $S$.	
\end{frame}
%%
\begin{frame}{}{}
The first step is to divide the rectangle $R$ into subrectangles. We do this by dividing the interval $[a,b]$ into $m$ subintervals $[x_{i-1},x_{i}]$ of equal width $\Delta{}x=\frac{b-a}{m}$ and dividing $[c,d]$ into $n$ subintervals $[y_{j-1},y_{j}]$ of equal width $\Delta{}y=\frac{d-c}{n}$. By drawing lines parallel to the coordinate axes through the endpoints of these subintervals as in Figure, we form the subrectangles $R_{ij}=[x_{i-1},x_{i}]\times[y_{j-1},y_{j}]$ each with area $\Delta{}A=\Delta{}x\Delta{}y$.
\begin{figure}[htb]
	\centering
	\includegraphics[page=840,scale=.95,clip,trim=84mm 98mm 34mm 123mm]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%%
\begin{frame}{}{}
If we choose a \textbf{sample point} $(x_{ij}^{\ast},y_{ij}^{\ast})$ in each $R_{ij}$, then we can approximate the part of $S$ that lies above each $R_{ij}$ by a thin rectangular box (or {\textquotedblleft}column{\textquotedblright}) with base $R_{ij}$ and height $f(x_{ij}^{\ast},y_{ij}^{\ast})$ as shown in Figure. The volume of this box is the height of the box times the area of the base rectangle:
\begin{equation*}
f(x_{ij}^{\ast},y_{ij}^{\ast})\Delta{}A.
\end{equation*}
\begin{figure}[htb]
	\centering
	\includegraphics[page=841,scale=.95,clip,trim=21mm 190mm 116mm 32mm]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%%
\begin{frame}{}{}
If we follow this procedure for all the rectangles and add the volumes of the corresponding boxes, we get an approximation to the total volume of $S$:
\begin{equation}
V\approx\sum_{i=1}^{m}\sum_{j=1}^{n}f(x_{ij}^{\ast},y_{ij}^{\ast})\Delta{}A.\label{s12ss1sss1eqn1}
\end{equation}
This double sum means that for each subrectangle, we evaluate at the chosen point and multiply by the area of the subrectangle, and then we add the results.
\begin{figure}[htb]
	\centering
	\includegraphics[page=841,scale=0.89,clip,trim=115mm 193.5mm 21.5mm 31.5mm]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%%
\begin{frame}{}{}
Our intuition tells us that the approximation given in \eqref{s12ss1sss1eqn1} becomes better as $m$ and $n$ become larger and so we would expect that
\begin{equation}
V=\lim_{m,n\to\infty}\sum_{i=1}^{m}\sum_{j=1}^{n}f(x_{ij}^{\ast},y_{ij}^{\ast})\Delta{}A.\label{s12ss1sss1eqn2}
\end{equation}
We use the expression in \eqref{s12ss1sss1eqn2} to define the \textbf{volume} of the solid that lies under the graph of and above the rectangle.
\end{frame}
%%
\begin{frame}{}{}
Limits of the type that appear in \eqref{s12ss1sss1eqn2} occur frequently, not just in finding volumes but in a variety of other situations as well as we will see in next section even when $f$ is not a positive function. So we make the following definition.
\begin{definition}[Double Integral]
The {double integral} of $f$ over the rectangle $R$ is
\begin{equation*}
\iint_{R}f(x,y){d}A
=\lim_{m,n\to\infty}\sum_{i=1}^{m}\sum_{j=1}^{n}f(x_{ij}^{\ast},y_{ij}^{\ast})\Delta{}A
\end{equation*}
provided the limit exists.
\end{definition}
\end{frame}
%%
\subsection{Iterated Integrals}
%
\begin{frame}{\insertsubsectionhead}{}
\begin{example}
Evaluate the iterated integrals.
	\begin{enumerate}
		\item $\displaystyle\int_{0}^{3}\int_{1}^{2}x^{2}y{d}y{d}x$.
		\item $\displaystyle\int_{1}^{2}\int_{0}^{3}x^{2}y{d}x{d}y$.
	\end{enumerate}
\end{example}
\begin{proof}[{Solution}]\let \qed \relax
\begin{enumerate}
\item Here we first integrate with respect to $y$:
    \begin{equation*}
    \int_{0}^{3}\int_{1}^{2}x^{2}y{d}y{d}x=\int_{0}^{3}x^{2}\frac{y^{2}}{2}\bigg|_{y=1}^{y=2}{d}x=\int_{0}^{3}\frac{3}{2}x^{2}{d}x=\frac{x^{3}}{2}\bigg|_{0}^{3}=\frac{27}{2}.
    \end{equation*}
\end{enumerate}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	\begin{enumerate}
		\setcounter{enumi}{1}
		\item Here we first integrate with respect to $x$:
		\begin{equation*}
		\int_{1}^{2}\int_{0}^{3}x^{2}y{d}x{d}y
		=\int_{1}^{2}\frac{x^{3}}{3}y\bigg|_{x=0}^{x=3}{d}y
		=\int_{1}^{2}9y{d}y
		=\frac{9}{2}y^{2}\bigg|_{1}^{2}
		=\frac{27}{2}.\qedhere
		\end{equation*}
	\end{enumerate}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
The following theorem gives a practical method for evaluating a double integral by expressing it as an iterated integral (in either order).
\begin{theorem}[Fubini's Theorem]
If is continuous on the rectangle
\begin{equation*}
R=\{(x,y):\ a\leq{}x\leq{}b,\ c\leq{}y\leq{}d\},
\end{equation*}
then
\begin{equation*}
\iint_{R}f(x,y){d}A=\int_{a}^{b}\int_{c}^{d}f(x,y){d}y{d}x=\int_{c}^{d}\int_{a}^{b}f(x,y){d}x{d}y.
\end{equation*}
More generally, this is true if we assume that $f$ is bounded on $R$, $f$ is discontinuous only on a finite number of smooth curves, and the iterated integrals exist.
\end{theorem}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}\label{s12ss2exm2}
Evaluate the double integral $\displaystyle\iint_{R}(x-3y^{2})dA$, where $R=\{(x,y):\ 0\leq{}x\leq2,\ 1\leq{}y\leq2\}$.
\begin{figure}[htb]
	\centering
	\includegraphics[page=851,scale=1.4,clip,trim=21mm 130mm 146.5mm 116.5mm]{stewart-ccac.pdf}
\end{figure}
\end{example}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[{Solution~1}]
Applying Fubini's Theorem with respect to $y$ first, we have
\begin{align*}
\iint_{R}(x-3y^{2}){d}A=&\int_{0}^{2}\int_{1}^{2}(x-3y^{2}){d}y{d}x=\int_{0}^{2}\Bigl[xy-y^{3}\Bigr]_{y=1}^{y=2}{d}x\\
=&\int_{0}^{2}(x-7){d}x=\biggl[\frac{x^{2}}{2}-7\biggr]_{0}^{2}=-12.\qedhere
\end{align*}
\end{proof}
%
\begin{proof}[{Solution~2}]
Again applying Fubini's Theorem, but this time integrating with respect to $x$ first, we have
\begin{align*}
\iint_{R}(x-3y^{2}){d}A=&\int_{1}^{2}\int_{0}^{2}(x-3y^{2}){d}x{d}y=\int_{1}^{2}\biggl[\frac{x^{2}}{2}-3xy^{2}\biggr]_{x=0}^{x=2}{d}y\\
=&\int_{1}^{2}\bigl(2-6y^{2}\bigr){d}x=\Bigl[2y-2y^{3}\Bigr]_{1}^{2}=-12.\qedhere
\end{align*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}\label{s12ss2exm3}
	\begin{minipage}{.49\textwidth}
		Evaluate $\displaystyle\iint_{R}y\sin(xy){d}A$, where $R=[1,2]\times[0,\pi]$.
	\end{minipage}
	\begin{minipage}{.49\textwidth}
		\begin{figure}[htb]
			\centering
			\includegraphics[page=851,scale=1.2,clip,trim=21mm 38mm 148mm 210mm]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
\end{example}
\begin{proof}[{Solution~1}]
If we first integrate with respect to $x$, we get
\small
\begin{align*}
\iint_{R}y\sin(xy){d}A=&\int_{0}^{\pi}\int_{1}^{2}y\sin(xy){d}x{d}y=\int_{0}^{\pi}\left[-\cos(xy)\right]_{x=1}^{x=2}{d}y\\
=&\int_{0}^{\pi}\left[\cos(y)-\cos(2y)\right]{d}y=\biggl[\sin(y)-\frac{1}{2}\sin(2y)\biggr]_{0}^{\pi}=0.\qedhere
\end{align*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[{Solution~2}]\let \qed \relax
If we first integrate with respect to $y$, we get
\begin{equation*}
\iint_{R}y\sin(xy){d}A
=\int_{1}^{2}\int_{0}^{\pi}y\sin(xy){d}y{d}x.
\end{equation*}
To evaluate the inner integral, we use integration by parts with $u=y$ and ${d}v=\sin(xy){d}y$ and so
\begin{align*}
\int_{0}^{\pi}y\sin(xy){d}y=&-\frac{y}{x}\cos(xy)\biggr|_{y=0}^{y=\pi}+\frac{1}{x}\int_{0}^{\pi}\cos(xy){d}y\\
=&-\frac{\pi}{x}\cos(\pi{}x)+\frac{1}{x^{2}}\sin(xy)\biggr|_{y=0}^{y=\pi}\\
=&-\frac{\pi}{x}\cos(\pi{}x)+\frac{1}{x^{2}}\sin(\pi{}x).\qedhere
\end{align*}
\end{proof}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[Solution~2 (cont.)]
If we now integrate by parts with $u=\frac{1}{x}$ and ${d}v=\pi\cos(\pi{}x){d}x$ the first term of
\begin{equation*}
\int_{1}^{2}\left(-\frac{\pi}{x}\cos(\pi{}x)+\frac{1}{x^{2}}\sin(\pi{}x)\right){d}x.
\end{equation*}
Then,
\begin{equation*}
\int_{1}^{2}\left(-\frac{\pi}{x}\cos(\pi{}x)\right){d}x=-\frac{\sin(\pi{}x)}{x}\bigg|_{1}^{2}-\int_{1}^{2}\frac{1}{x^{2}}\sin(\pi{}x){d}x,
\end{equation*}
which yields
\begin{align*}
\int_{1}^{2}\left(-\frac{\pi}{x}\cos(\pi{}x)+\frac{1}{x^{2}}\sin(\pi{}x)\right){d}x=&-\frac{\sin(\pi{}x)}{x}\bigg|_{1}^{2}\\
=&\frac{\sin(2\pi)}{2}-\sin(\pi)=0.\qedhere
\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
In {Example~\ref{s12ss2exm2}}, {Solution~1} and {Solution~2} are equally straightforward, but in {Example~\ref{s12ss2exm3}}, the first solution is much easier than the second one. 
\begin{block}{}
	Therefore, when we evaluate double integrals it is wise to choose the order of integration that gives simpler integrals.
\end{block}
\end{frame}
%%
\begin{frame}{}{}
\begin{example}
Find the volume of the solid $S$ that is bounded by the elliptic paraboloid $x^{2}+2y^{2}+z=16$, the planes $x=2$ and $y=2$, and the three coordinate planes.
\begin{figure}[htb]
	\centering
	\includegraphics[page=852,scale=1.4,clip,trim=20mm 98mm 147mm 150mm]{stewart-ccac.pdf}
\end{figure}
\end{example}
\end{frame}
%%
\begin{frame}{}{}
\begin{proof}[{Solution}]
We first observe that $S$ is the solid that lies under the surface $z=16-x^{2}-2y^{2}$ and above the square $R=[0,2]\times[0,2]$. This solid was considered in a previous example, but we are now in a position to evaluate the double integral using Fubini's Theorem. Therefore,
\begin{align*}
V=&\iint_{R}\left(16-x^{2}-2y^{2}\right){d}A=\int_{0}^{2}\int_{0}^{2}\left(16-x^{2}-2y^{2}\right){d}x{d}y\\
=&\int_{0}^{2}\left[16x-\frac{x^{3}}{3}-2y^{2}x\right]_{x=0}^{x=2}{d}y=\int_{0}^{2}\left(\frac{88}{3}-4y^{2}\right){d}y\\
=&\left[\frac{88}{3}y-\frac{4}{3}y^{3}\right]_{0}^{2}=48.\qedhere
\end{align*}
\end{proof}
\end{frame}