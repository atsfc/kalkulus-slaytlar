\section{Partial Derivatives}
\subsection{Functions of Several Variables}
\subsubsection{Visiual Representations}
\begin{frame}{\insertsubsubsectionhead}
One way to visualize a function of two variables is through its graph. Recall that the graph of $f$ is the surface with equation $z=f(x,y)$.\\

Another method for visualizing functions, borrowed from mapmakers, is a \textbf{contour map} on which points of constant elevation are joined to form \textbf{contour lines}, or \textbf{level curves}.
\end{frame}
%
\begin{frame}
\begin{definition}
The \textbf{level curves} of a function $f$ of two variables are the curves with equations $f(x,y)=k$, where $k$ is a constant (in the range of $f$).
\end{definition}
\begin{block}{}
	A level curve $f(x,y)=k$ is the set of all points in the domain of $f$ at which takes on a given value $k$. In other words, it shows where the graph of $f$ has height $k$.
\end{block}
\end{frame}
%
\begin{frame}
You can see from Figure below the relation between level curves and horizontal traces. The level curves $f(x,y)=k$ are just the traces of the graph of $f$ in the horizontal plane $z=k$ projected down to the $xy$-plane. So if you draw the level curves of a function and visualize them being lifted up to the surface at the indicated height, then you can mentally piece together a picture of the graph. The surface is steep where the level curves are close together. It is somewhat flatter where they are farther apart.
\begin{figure}
\begin{center}
\includegraphics[page=752,clip,trim=18.5mm 139mm 96mm 58mm,scale=0.65]{stewart-ccac.pdf}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\begin{example}
Sketch the level curves of the function
\[ g(x,y)=\sqrt{9-x^2-y^2}=k \quad \text{for} \quad k=0,1,2,3 \]
\end{example}
\begin{proof}[Solution] \let\qed\relax
The level curves are
\[ \sqrt{9-x^2-y^2}=k \quad \text{or} \quad x^2+y^2=9-k^2 \]
This is a family of concentric circles with center $(0,0)$ and radius $\sqrt{9-k^2}$. The cases $k=0,1,2,3$ are shown in Figure below. Try to visualize these level curves lifted up to form a surface and compare with the graph of $g$ (a hemisphere).
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution (cont.)]\let \qed \relax
	In the following Figure, it is shown the counter map and the graph of
	\[ g(x,y)=\sqrt{9-x^2-y^2}=k. \]
	\includegraphics[page=753,clip,trim=100.5mm 54mm 49mm 182mm,scale=1]{stewart-ccac.pdf}
	\includegraphics[page=751,clip,trim=18mm 143mm 154mm 100mm,scale=1.2]{stewart-ccac.pdf}
\end{proof}
\end{frame}
%
\subsubsection{Functions of Three or More Variables}
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{}
	A \textbf{function of three variables}, $\bm{f}$, is a rule that assigns to each ordered triple $(x,y,z)$ in a domain $D\subset \mathds{R}^3$ \textbf{a unique} real number denoted by $f(x,y,z)$.
\end{block}
\begin{exampleblock}{}
	For instance, the temperature $T$ at a point on the surface of the Earth depends on the longitude $x$ and latitude $y$ of the point and on the time $t$, so we could write $T=f(x,y,t)$.
\end{exampleblock}
\end{frame}
%
\begin{frame}
\begin{example}
Find the domain of $f$ if
\[ f(x,y,z)=\ln(z-y)+xy \sin z. \]
\end{example}
\begin{proof}[Solution]
The expression for $f(x,y,z)$ is defined as long as $z-y>0$, so the domain of $f$ is
\[ D=\{(x,y,z)\in \mathds{R}^3|z>y\} \]
This is a \textbf{half-space} consisting of all points that lie above the plane $z=y$.
\end{proof}
\end{frame}

\begin{frame}
\begin{block}{}
	It's very difficult to visualize a function $f$ of three variables by its graph, since that would lie in a four-dimensional space.
\end{block}
\begin{block}{}
	However, we do gain some insight into $f$ by examining its \textbf{level surfaces}, which are the surfaces with equations $f(x,y,z)=k$, where $k$ is a constant. 
	
	If the point $(x,y,z)$ moves along a level surface, the value of $f(x,y,z)$ remains fixed.
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
Find the level surfaces of the function
\[ f(x,y,z)=x^2+y^2+z^2 \]
\end{example}
\begin{proof}[Solution]
	\begin{minipage}{.49\textwidth}
		\includegraphics[page=756,clip,trim=12mm 176mm 143mm 53mm,scale=1]{stewart-ccac.pdf}
	\end{minipage}
	\begin{minipage}{.49\textwidth}
		The level surfaces are $x^2+y^2+z^2=k$, where $k\ge 0$. These form a family of concentric spheres with radius $\sqrt{k}$.  Thus, as $(x,y,z)$ varies over any sphere with center $O$, the value of $f(x,y,z)$ remains fixed. \qedhere
	\end{minipage}
\end{proof}
\end{frame}
%
\section{Functions and Surfaces} 
\subsubsection{Graphs}
\begin{frame}{\insertsubsubsectionhead}
\begin{definition}
	If $f$ is a function of two variables with domain $D$, then the \textbf{graph} of $f$ is the set of all points $(x,y,z)$ in $\mathds{R}^3$ such that $z=f(x,y)$ and $(x,y)$ is in $D$.
\end{definition}
\begin{block}{}
	Just as the graph of a function $f$ of one variable is a curve $C$ with equation $y=f(x)$ so the graph of a function $f$ of two variables is a \textbf{surface} with equation $z=f(x,y)$.
\end{block}
\end{frame}
%
\begin{frame}
We can visualize the graph $S$ of $f$ as lying directly above or below its domain $D$ in the $xy$-plane.
\begin{center}
	\includegraphics[page=687,clip,trim=18mm 160mm 142mm 73mm,scale=1.3]{stewart-ccac.pdf}
\end{center}
\end{frame}
%
\begin{frame}
\begin{example}
Sketch the graph of the function $f(x,y)=6-3x-2y$.
\end{example}
\begin{proof}[Solution]
The graph of $f$ has the equation $z=6-3x-2y$ or $3x+2y+z=6$ which represents a plane. By finding the intercepts, we sketch the portion of this graph that lies in the first octant in Figure. \qedhere
\begin{figure}[h]
\includegraphics[page=687,clip,trim=18mm 105mm 146mm 130mm,scale=1]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{block}{}
	The function in previous example is a special case of the function
	\[f(x,y)=ax+by+c\]
	which is called a \textbf{linear function}.
\end{block}
\begin{block}{}
	The graph of such a function has the equation, $z=ax+by+c$ or $ax+by-z+c=0$, so it is a plane.
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
Sketch the graph of the function $f(x,y)=x^2$.
\end{example}
\begin{proof}[Solution]
Notice that, no matter what value we give $y$, the value of $f(x,y)$ is always $x^2$. The equation of the graph is $z=x^2$, which doesn’t involve $y$. This means that any vertical plane with equation $y=k$ (parallel to the $xz$-plane) intersects the graph in a curve with equation $z=x^2$, that is, a parabola. 

\begin{minipage}{.49\textwidth}
	\begin{figure}[h]
		\includegraphics[page=687,clip,trim=16mm 46mm 150mm 190mm,scale=.9]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\begin{minipage}{.5\textwidth}
	Figure shows how the graph is formed by taking the parabola $z=x^2$ in the $xz$-plane and moving it in the direction of the $y$-axis.
	
	So the graph is a surface, called a \textbf{parabolic cylinder}, made up of infinitely many shifted copies of the same parabola. \qedhere
\end{minipage}
\end{proof}
\end{frame}
%
\begin{frame}
\begin{block}{}
	In sketching the graphs of functions of two variables, it's often useful to start by determining the shapes of \textbf{cross-sections} (\textbf{slices}) of the graph.
\end{block}
\begin{exampleblock}{}
	For example, if we keep $x$ fixed by putting $x=k$ (a constant) and letting $y$ vary, the result is a function of one variable $z=f(k,y)$, whose graph is the curve that results when we intersect the surface $z=f(x,y)$ with the vertical plane $x=k$.
\end{exampleblock}
\begin{block}{}
	In a similar fashion we can slice the surface with the vertical plane $y=k$ and look at the curves $z=f(x,k)$. We can also slice with horizontal planes $z=k$.
	
	All three types of curves are called \textbf{traces} (or \textbf{cross-sections}) of the surface $z=f(x,y)$.
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
Use traces to sketch the graph of the function $f(x,y)=4x^2+y^2$
\end{example}
\begin{proof}[Solution]
The equation of the graph is $z=4x^2+y^2$. If we put $x=0$, we get $z=y^2$, so the $yz$-plane intersects the surface in a parabola. If we put $x=k$ (a constant), we get $z=4k^2+y^2$. This means that if we slice the graph with any plane parallel to the $yz$-plane, we obtain a parabola that opens upward. Similarly, if $y=k$, the trace
is $z=4x^2+k^2$, which is again a parabola that opens upward. If we put $z=k$, we get the horizontal traces $4x^2+y^2=k$, which we recognize as a family of ellipses.

\begin{minipage}{.34\textwidth}
	\begin{figure}
		\begin{center}
			\includegraphics[page=688,clip,trim=17.5mm 200.5mm 153mm 35mm,scale=.65]{stewart-ccac.pdf}
		\end{center}
	\end{figure}
\end{minipage}
\begin{minipage}{.64\textwidth}
	Knowing the shapes of the traces, we can sketch the graph of $ f $ in Figure. Because of the elliptical and parabolic traces, the surface $z=4x^2+y^2$ is called an \textbf{elliptic paraboloid}. \qedhere
\end{minipage}
\end{proof} 
\end{frame}
%
\subsubsection{Quadric Surfaces}
%
\begin{frame}{\insertsubsubsectionhead}
\begin{definition}
	The graph of a second-degree equation in three variables $x$, $y$ and $z$ is called a \textbf{quadric surface}.
\end{definition}
\end{frame}
%
%
%
%
%%_______________________________________________________________--
\begin{frame}
\begin{example}
Sketch the quadric surface with equation $ x^2+\dfrac{y^2}{9}+\dfrac{z^2}{4}=1 $.
\end{example}
\begin{proof}[Solution]\let\qed\relax
The trace in the $xy$-plane ($z=0$) is $x^2+\dfrac{y^2}{9}=1$, which we recognize as an equation of an ellipse.

In general, the horizontal trace in the plane $z=k$ is
\[x^2+\frac{y^2}{9}=1-\frac{k^2}{4}\qquad z=k\]
which is an ellipse, provided that $k^2<4$ or $-2<k<2.$
\end{proof}
\end{frame}
%
%
%
%
%%_______________________________________________________________--
\begin{frame}
\begin{proof}[Solution(cont.)]\let\qed\relax
Similarly, the vertical traces are also ellipses:
\begin{align*}
\frac{y^2}{9}+\frac{z^2}{4}&=1-k^2, \; x=k \,(\text{if } -1<k<1)\\
x^2+\frac{z^2}{4}&=1-\frac{k^2}{9}, \; y=k \,(\text{if } -3<k<3)
\end{align*}
\begin{minipage}{.41\textwidth}
	\includegraphics[page=690,clip,trim=20.5mm 133.5mm 146.5mm 112mm,scale=1]{stewart-ccac.pdf}
\end{minipage}
\begin{minipage}{.58\textwidth}
	Figure shows how drawing some traces indicates the shape of the surface. It’s called an \textbf{ellipsoid} because all of its traces are ellipses. Notice that it is symmetric with respect to each coordinate plane; this is a reflection of the fact that its equation involves only even powers of $x$, $y$ and $z$.
\end{minipage}
\end{proof}
\end{frame}
%
%
%
%%_______________________________________________________________--
\begin{frame}
\begin{proof}[Solution(cont.)]\let\qed\relax
The ellipsoid in example is \textbf{not the graph of a function} because some vertical lines (such as the $z$-axis) intersect it more than once. But the top and bottom halves are graphs of functions. In fact, if we soland the equation of the ellipsoid for $z$, we get
\[z^2=4\left(1-x^2-\frac{y^2}{9}\right)\qquad z=\pm 2 \sqrt{1-x^2-\frac{y^2}{9}}\]
\end{proof}
\end{frame}

%%_______________________________________________________________--
\begin{frame}
\begin{proof}[Solution(cont.)]\let\qed\relax
So the graphs of the functions
\[f(x,y)=2\sqrt{1-x^2-\frac{y^2}{9}} \text{ and } g(x,y)=-2 \sqrt{1-x^2-\frac{y^2}{9}}\]
are the top and bottom halves of the ellipsoid.

\includegraphics[page=691,clip,trim=50.5mm 205.5mm 100mm 32mm,scale=0.9]{stewart-ccac.pdf}
\includegraphics[page=691,clip,trim=130.5mm 205.5mm 22mm 32mm,scale=0.9]{stewart-ccac.pdf}
\end{proof}
\end{frame}
%
%
%
%
%%_______________________________________________________________--
\begin{frame}
\begin{proof}[Solution(cont.)]
The domain of both $f$ and $g$ is the set of all points $(x,y)$ such that
\[1-x^2-\frac{y^2}{9}\geq0\quad \implies \quad x^2+\frac{y^2}{9}\leq1\]
so the domain is the set of all points that lie on or inside the ellipse
\[ x^2+\dfrac{y^2}{9}=1 \qedhere \]
\end{proof}
\end{frame}
%
%
%%_______________________________________________________________--
\subsubsection{Graphs of Some Standard Quadric Surfaces}
\begin{frame}{\insertsubsubsectionhead}
Let us look through some standard quadratic surfaces that are drawn by computer.\medskip

All these surfaces are symmetric with respect to $z$-axes.\medskip

If a quadratic surface is symmetric with respect to an axis then the equation of the surface will change according to this symmetry.
\end{frame}
%
\begin{frame}
\begin{block}{Ellipsoid}
\begin{minipage}{.5\textwidth}
		\[ \frac{x^2}{a^2}+\frac{y^2}{b^2}+\frac{z^2}{c^2}=1 \]
		All traces are ellipses.
		
		If $ a=b=c $, the ellipsoid is a sphere.
\end{minipage}
\begin{minipage}{.45\textwidth}
	\begin{figure}[h]
		\includegraphics[page=691,clip,trim=30.5mm 138mm 156.5mm 122.5mm,scale=1.6]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\end{block}
\end{frame}
%%_______________________________________________________________--
\begin{frame}
\begin{block}{Cone}
	\begin{minipage}{.5\textwidth}
		\[ \frac{x^2}{a^2}+\frac{y^2}{b^2}=\frac{z^2}{c^2} \]
		Horizontal traces are ellipses.
		
		Vertical traces in the planes $ x=k $ and $ y=k $ hyperbolas if $k\neq 0 $
		but are pairs of lines if $ k=0 $.
	\end{minipage}
	\begin{minipage}{.45\textwidth}
		\begin{figure}[h]
			\includegraphics[page=691,clip,trim=117mm 127mm 72mm 121mm,scale=1.4]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
\end{block}
\end{frame}
%%_______________________________________________________________--
\begin{frame}
\begin{block}{Elliptic Paraboloid}
	\begin{minipage}{.5\textwidth}
		\[ \frac{x^2}{a^2}+\frac{y^2}{b^2}=\frac{z}{c} \]
		Horizontal traces are ellipses.
		
		Vertical traces are parabolas.
	\end{minipage}
	\begin{minipage}{.45\textwidth}
		\begin{figure}[h]
			\includegraphics[page=691,clip,trim=34mm 82mm 163mm 166mm,scale=1.5]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
\end{block}
\end{frame}
%%_______________________________________________________________--
\begin{frame}
\begin{block}{Hyperboloid of One Sheet}
	\begin{minipage}{.5\textwidth}
		\[ \frac{x^2}{a^2}+\frac{y^2}{b^2}-\frac{z^2}{c^2}=1 \]
		Horizontal traces are ellipses.
		
		Vertical traces are hyperbolas.
	\end{minipage}
	\begin{minipage}{.45\textwidth}
		\begin{figure}[h]
			\includegraphics[page=691,clip,trim=117mm 82mm 71mm 166mm,scale=1.4]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
\end{block}
\end{frame}
%%_______________________________________________________________--
\begin{frame}
\begin{block}{Hyperbolic Paraboloid}
	\begin{minipage}{.5\textwidth}
		\[ \frac{x^2}{a^2}-\frac{y^2}{b^2}=\frac{z}{c} \]
		Horizontal traces are hyperbolas.
		
		Vertical traces are parabolas.
	\end{minipage}
	\begin{minipage}{.45\textwidth}
		\begin{figure}[h]
			\includegraphics[page=691,clip,trim=24mm 42mm 151mm 212mm,scale=1.4]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
\end{block}
\end{frame}
%%_______________________________________________________________--
\begin{frame}
\begin{block}{Hyperboloid of Two Sheets}
	\begin{minipage}{.5\textwidth}
		\[ -\frac{x^2}{a^2}-\frac{y^2}{b^2}+\frac{z^2}{c^2}=1 \]
		Horizontal traces in $z=k$ are ellipses if $ k<-c$ or $ k>c $.
		
		Vertical traces are hyperbolas.
	\end{minipage}
	\begin{minipage}{.45\textwidth}
		\begin{figure}[h]
			\includegraphics[page=691,clip,trim=117mm 39mm 72mm 210mm,scale=1.4]{stewart-ccac.pdf}
		\end{figure}
	\end{minipage}
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
Classify the quadric surface $x^2+2z^2-6x-y+10=0$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
By completing the square we rewrite the equation as
\[y-1=(x-3)^2+2z^2\]
Notice that, this equation represents an eliptic paraboloid.

Here, however, the axis of the paraboloid is parallel to the $y$-axis, and it has
been shifted so that its vertex is the point $ (3,1,0) $. The traces in the plane $y=k$, $ (k>1)$ are the ellipses
\[(x-3)^2+2z^2=k-1,\quad y=k\]
The trace in the $xy$-plane is the parabola with equation $y=1+(x-3)^2$, $z=0$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution(cont.)]
 \begin{figure}[h]
 \includegraphics[page=692,clip,trim=20mm 204mm 146mm 29.5mm,scale=1.4]{stewart-ccac.pdf}
 \end{figure}
\end{proof}
\end{frame}
%
\subsection{Limits and Continuity}
%
\begin{frame}{\insertsubsectionhead}
Let's compare the behavior of the functions
\[f(x,y)=\frac{\sin (x^2+y^2)}{x^2+y^2}\;\; \text{and}\;\;g(x,y)=\frac{x^2-y^2}{x^2+y^2}\]
as $ x $ and $ y $ both approach $0$ [and therefore the point $(x,y)$ approaches the origin].
\end{frame}
%
\begin{frame}
\begin{table}[h]
\begin{figure}[h]
\includegraphics[page=760,clip,trim=19mm 146mm 113mm 80mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
\caption{$ \displaystyle f(x,y)=\frac{\sin (x^2+y^2)}{x^2+y^2} $}
\label{tbl:tablo1}
\end{table}
\end{frame}
%
%
\begin{frame}
\begin{table}[h]
\begin{figure}[h]
\includegraphics[page=760,clip,trim=109.5mm 146mm 21mm 80mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
\caption{$ \displaystyle g(x,y)=\frac{x^2-y^2}{x^2+y^2} $}
\label{tbl:tablo2}
\end{table}
\end{frame}
%
%
\begin{frame}
Tables \ref{tbl:tablo1} and Table \ref{tbl:tablo2} show values of $f(x,y)$ and $g(x,y)$, correct to three decimal places, for points near the origin. (Notice that neither function is defined at the origin.)\medskip

It appears that as $(x,y)$, approaches $(0,0)$, the values of $f(x,y)$ are approaching $1$ whereas the values $g(x,y)$ of aren't approaching any number.
\end{frame}
%
\begin{frame}
It turns out that these guesses based on numerical evidence are correct, and we write
\[\lim_{(x,y)\to (0,0)}\frac{\sin (x^2+y^2)}{x^2+y^2}=1\]
and
\[\lim_{(x,y)\to (0,0)}\frac{x^2-y^2}{x^2+y^2}=\text{does not exist.}\]
\end{frame}
%
\begin{frame}
\begin{definition}
We write
\[\lim_{(x,y)\to (a,b)}f(x,y)=L\]
and we say that the \textbf{limit of $\bm{f(x,y)}$ as $ \bm{(x,y)} $ approaches $\bm{(a,b)}$} is $ \bm{L} $ if we can
make the values of $f(x,y)$ as close to $ L $ as we like by taking the point $(x,y)$ sufficiently close to the point $(a,b)$, but not equal to $(a,b)$.
\end{definition}
\end{frame}
%
\begin{frame}
For functions of two variables the situation is not as simple because we can let
$(x,y)$ approach $(a,b)$ from an infinite number of directions in any manner whatsoever as long as $(x,y)$ stays within the domain of $f$.

\begin{minipage}{.5\textwidth}
	If we can find two different paths of approach along which the function $f(x,y)$ has different limits, then it follows that
	\[ \lim_{(x,y)\to (a,b)}f(x,y) \]
	does not exist.
\end{minipage}
\begin{minipage}{.45\textwidth}
	\begin{figure}[h]
		\includegraphics[page=761,clip,trim=24mm 210mm 148mm 44mm,scale=1.3]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\end{frame}
%
\begin{frame}
\begin{alertblock}{Remark}
\begin{itemize}
	\item If $f(x,y)\to L_1$ as $(x,y)\to (a,b)$ along a path $C_1$
	\item[] and
	\item $f(x,y)\to L_2$ as $(x,y)\to (a,b)$ along a path $C_2$, where $L_1\neq L_2$,
\end{itemize}
then \[ \lim\limits_{(x,y)\to (a,b)}f(x,y) \] does not exist.
\end{alertblock}
\end{frame}


\begin{frame}
\begin{example}
Show that $\lim\limits_{(x,y)\to (0,0)}\dfrac{x^2-y^2}{x^2+y^2}$ does not exist.
\end{example}
\begin{proof}[Solution]
Let $\displaystyle f(x,y)=\frac{x^2-y^2}{x^2+y^2}$. First let's approach $(0,0)$ along the
$x$-axis. Then $y=0$ gives $f(x,0)=x^2/x^2=1$ for all $x\neq 0$, so
\[f(x,y)\to 1\ \text{as} \ (x,y)\to (0,0) \ \text{along the $x$-axis.}\]
We now approach along the $y$-axis by putting $x=0$. Then $f(0,y)=-y^2/y^2=-1$ for
all $y\neq 0$, so
\[f(x,y)\to -1 \ \text{as} \ (x,y)\to (0,0) \ \text{along the $y$-axis.}\]
Since $f$ has two different limits along two different lines, the given limit \textbf{does not exist}. \qedhere
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
If $\displaystyle f(x,y)=\frac{xy^2}{x^2+y^4}$ does $\displaystyle \lim_{(x,y)\to (0,0)}f(x,y)$ exist?
\end{example}
\begin{proof}[Solution]\let\qed\relax
With the solution of previous example in mind, let's try to save time by letting
$(x,y)\to (0,0)$ along any nonvertical line through the origin.

Then $y=mx$, where $m$ is the slope, and
\[f(x,y)=f(x,mx)=\frac{x(mx)^2}{x^2+(mx)^4}=\frac{m^2x^3}{x^2+m^4x^4}=\frac{m^2x}{1+m^4x^2}\]
So $f(x,y)\to 0$ as $(x,y)\to (0,0)$ along $y=mx$.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Thus, $f(x,y)$ has the same limiting value along every nonvertical line through the origin.

But that does not show that the given limit is $0$, for if we now let
$(x,y)\to (0,0)$ along the parabola $x=y^2$, we have
\[f(x,y)=f(y^2,y)=\frac{y^2y^2}{(y^2)^2+y^4}=\frac{y^4}{2y^4}=\frac{1}{2}\]
so
$f(x,y)\to\frac{1}{2}$ as $(x,y)\to (0,0)$ along $x=y^2$. Since different paths lead to different limiting values, the given limit does not exist.
\end{proof}
\end{frame}
%
\subsubsection{Continuity}
%
\begin{frame}{\insertsubsubsectionhead}
Recall that evaluating limits of continuous functions of a single variable is easy. 

It can be accomplished by direct substitution because the defining property of a continuous function is
\[  \lim_{x\to a}f(x)=f(a). \]
Continuous functions of two variables are also defined by the direct substitution property.
\end{frame}
%
\begin{frame}
\begin{definition}
A function $f$ of two variables is called \textbf{continuous at} $\bm{(a,b)}$ if
\[\lim_{(x,y)\to (a,b)}f(x,y)=f(a,b)\]
We say $\bm{f}$ \textbf{is continuous on} $\bm{D}$ if $f$ is continuous at every point $(a,b)$ in $D$.
\end{definition}
\end{frame}
%
\begin{frame}
\begin{block}{}
	The intuitive meaning of continuity is that if the point $(x,y)$ changes by a small amount, then the value of changes $f(x,y)$ by a small amount.\medskip

	This means that a surface that is the graph of a continuous function has no hole or break.
\end{block}
\begin{exampleblock}{}
	Using the properties of limits, you can see that sums, differences, products, and quotients of continuous functions are continuous on their domains.
\end{exampleblock}
\end{frame}
%
\begin{frame}
\begin{example}
Let $ g(x,y)= \begin{cases}
             \displaystyle \frac{x^2-y^2}{x^2+y^2}, & (x,y) \neq (0,0) \\
             0, & (x,y)=(0,0) 
           \end{cases} $.

Here $g$ is defined at $(0,0)$ but is still discontinuous at $0$ because
\[ \lim_{(x,y)\to (0,0)}g(x,y) \]
does not exist.
\end{example}
\end{frame}
%
\subsection{Partial Derivatives}
%
\begin{frame}{\insertsubsectionhead}
\begin{block}{}
	In general, if $f$ is a function of two variables $x$ and $y$, suppose we let only $x$ vary while keeping $y$ fixed, say $y=b$, where $b$ is a constant. \medskip
	
	Then we are really considering a function of a single variable $x$, namely, $g(x)=f(x,b)$. If $g$ has a derivative at $a$, then we call it the \textbf{partial derivative of with respect to $\bm{x}$ at $\bm{(a,b)}$} and denote it by $\bm{f_x(a,b)}$. Thus
	\begin{equation}\label{fninkismiturevi}
	f_x(a,b)=g'(a) \ \text{where} \ g(x)=f(x,b).
	\end{equation}
\end{block}
\end{frame}
%%
\begin{frame}
\begin{block}{}
	By the definition of a derivative, we have
	\[g'(a)=\lim_{h\to 0}\frac{g(a+h)-g(a)}{h}\]
	and so Equation \eqref{fninkismiturevi} becomes
	\begin{equation}\label{xegore}
	f_x(a,b)=\lim_{h\to 0}\frac{f(a+h,b)-f(a,b)}{h}
	\end{equation}
\end{block}
\end{frame}
%%
\begin{frame}
\begin{block}{}
	Similarly, the \textbf{partial derivative of $\bm{f}$ with respect to $\bm{y}$ at $\bm{(a,b)}$}, denoted by $f_y(a,b) $, is obtained by keeping fixed and finding the ordinary derivative at $b$ of the function $G(y)=f(a,y)$:
	\begin{equation}\label{yegore}
	f_y(a,b)=\lim_{h\to 0}\frac{f(a,b+h)-f(a,b)}{h}.
	\end{equation}
\end{block}
\end{frame}
%%

\begin{frame}
\begin{block}{}
	If we now let the point $(a,b)$ vary in Equations \eqref{xegore} and \eqref{yegore}, $f_x$ and $f_y$ become functions of two variables. \medskip
	
	If $f$ is a function of two variables, its \textbf{partial derivatives} are the functions $f_x$ and $f_y$ defined by
	\begin{align*}
	f_x(x,y)=\lim_{h\to 0} \frac{f(x+h,y)-f(x,y)}{h}\\
	f_y(x,y)=\lim_{h\to 0} \frac{f(x,y+h)-f(x,y)}{h}
	\end{align*}
\end{block}
\end{frame}
%%
%%
%%
%%
%%%_______________________________________________________________--
\begin{frame}
\begin{block}{Notations for Partial Derivatives}
	If $z=f(x,y)$, we write
	\begin{align*}
	f_x(x,y)&=f_x=\frac{\partial f}{\partial x}=\frac{\partial}{\partial x}f(x,y)=\frac{\partial z}{\partial x}=f_1=D_1f=D_x f\\
	f_y(x,y)&=f_y=\frac{\partial f}{\partial y}=\frac{\partial}{\partial y}f(x,y)=\frac{\partial z}{\partial y}=f_2=D_2f=D_y f
	\end{align*}
\end{block}
\end{frame}
%%
%%
%%
%%
%%%_______________________________________________________________--
\begin{frame}
\begin{block}{Rule for Finding Partial Derivatives of $z=f(x,y)$}
	\begin{itemize}
		\item To find $f_x$, regard $y$ as a constant and differentiate $f(x,y)$ with respect to $x$.
		\item To find $f_y$, regard $x$ as a constant and differentiate $f(x,y)$ with respect to $y$.
	\end{itemize}
\end{block}
\end{frame}


\begin{frame}
\begin{example}
 If $f(x,y)=x^3+x^2y^3-2y^2$, find
$f_x(2,1)$ and $f_y(2,1)$.
\end{example}
\begin{proof}[Solution]
 Holding $y$ constant and differentiating with respect to $x$, we get
\[f_x(x,y)=3x^2+2 x y^3\]
and so
\[f_x(2,1)=3\cdot 2^2+2\cdot 2\cdot 1^3=16\]
Holding $x$ constant and differentiating with respect to $y$, we get
\[f_y(x,y)=3x^2y^2-4y\]
\[f_y(2,1)=3\cdot 2^2\cdot 1^2-4\cdot 1=8 \qedhere \]
\end{proof}
\end{frame}


\begin{frame}
\begin{example}
If $f(x,y)=4-x^2-2y^2$, find $f_x(1,1)$ and $f_y(1,1)$ and interpret these
numbers as slopes.
\end{example}
\begin{proof}[Solution]
 We have
\begin{align*}
f_x(x,y)=-2x & \text{ and }  f_y(x,y)=-4y\\
f_x(1,1)=-2 & \text{ and }  f_y(1,1)=-4 \qedhere
\end{align*}
\end{proof}
\end{frame}

\begin{frame}
\begin{example}
 If $\displaystyle f(x,y)=\sin\left(\frac{x}{1+y}\right)$, calculate $\displaystyle\frac{\partial f}{\partial x}$ and $\displaystyle \frac{\partial f}{\partial y}$.
\end{example}
\begin{proof}[Solution]
Using the Chain Rule for functions of one variable, we have
\begin{align*}
\frac{\partial f}{\partial x}&=\cos\left(\frac{x}{1+y}\right)\cdot\frac{\partial}{\partial x}\left(\frac{x}{1+y}\right)=\cos\left(\frac{x}{1+y}\right)\cdot\frac{1}{1+y}\\
\frac{\partial f}{\partial y}&=\cos\left(\frac{x}{1+y}\right)\cdot\frac{\partial}{\partial y}\left(\frac{x}{1+y}\right)=-\cos\left(\frac{x}{1+y}\right)\cdot\frac{x}{(1+y)^2} \qedhere
\end{align*}
\end{proof}
\end{frame}

\begin{frame}
\begin{example}
Find $\partial z/\partial x$ and $\partial z/\partial y$ if $z$ is defined implicitly as a function of $x$ and $y$ by the equation
\[x^3+y^3+z^3+6 x y z=1\]
\end{example}
\begin{proof}[Solution]
To find $\partial z/\partial x$ we differentiate implicitly with respect to $x$, being careful to treat $y$ as a constant:
\[3x^2+3z^2\frac{\partial z}{\partial x}+6yz+6xy\frac{\partial z}{\partial x}=0.\]
Similarly, to find $\partial z/\partial y$ we differentiate implicitly with respect to $y$, being careful to treat $x$ as a constant:
\[3y^2+3z^2\frac{\partial z}{\partial y}+6xz+6xy\frac{\partial z}{\partial y}=0.\qedhere\]
\end{proof}
\end{frame}
