
\section{Infinite Sequences and Series}\label{s1} %P.~562

\subsection{Sequences}\label{s1ss1} %P.~563

\begin{frame}{\insertsectionhead}{\insertsubsectionhead}

A \emph{sequence} can be thought of as a list of numbers written in a definite order:
\begin{equation}
a_{1},a_{2},\cdots,a_{n},\cdots\nonumber
\end{equation}
The number $a_{1}$ is called the first term, $a_{2}$ is the second term,
and in general $a_{n}$ is the $n$th term.
We will deal exclusively with infinite sequences and so each term $a_{n}$ will have a successor $a_{n+1}$.

Notice that for every positive integer $n$ there is a corresponding number $a_{n}$
and so a sequence can be defined as a function whose domain is the set of positive integers.
But we usually write $a_{n}$ instead of the function notation $f(n)$ for the value of the function at the number $n$.

The sequence $\{a_{1},a_{2},\cdots,a_{n},\cdots\}$ is also denoted by $\{a_{n}\}$ or $\{a_{n}\}_{n=1}^{\infty}$.

\end{frame}

%

\begin{frame}{}{}
\begin{example}\label{s1ss1exm1} %P.~563, \"{O}rnek~1
Some sequences can be defined by giving a formula for the $n$th term.
In the following examples we give three descriptions of the sequence:
one by using the preceding notation, another by using the defining formula,
and a third by writing out the terms of the sequence.
Notice that $n$ doesn't have to start at $1$.
\begin{align*}
&\bigl\{\frac{n}{n+1}\bigr\}_{n=1}^{\infty} && a_{n}=\frac{n}{n+1} && \bigl\{\frac{1}{2},\frac{2}{3},\frac{3}{4},\cdots,\frac{n}{n+1},\cdots\bigr\} \\
&\bigl\{(-1)^{n}\frac{n}{3^{n}}\bigr\} && a_{n}=(-1)^{n}\frac{n}{3^{n}} && \bigl\{-\frac{1}{3},\frac{2}{9},-\frac{3}{27},\cdots,(-1)^{n}\frac{n}{3^{n}},\cdots\bigr\} \\
&\bigl\{\sqrt{n-3}\bigr\}_{n=3}^{\infty} && a_{n}=\sqrt{n-3}, n\geq2 && \bigl\{0,1,\sqrt{2},\cdots,\sqrt{n-3},\cdots\bigr\} \\
&\bigl\{\cos\bigl(\frac{n\pi}{6}\bigr)\bigr\}_{n=0}^{\infty} && a_{n}=\cos\bigl(\frac{n\pi}{6}\bigr), n\geq0 && \bigl\{1,\frac{\sqrt{3}}{2},\frac{1}{2},\cdots,\cos\bigl(\frac{n\pi}{6}\bigr),\cdots\bigr\}
\end{align*}
\end{example}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{definition}\label{s1ss1dfn1} %P.~565, Tan{\i}m~1
A sequence $\{a_{n}\}$ has the \emph{limit $L$} and we write
\begin{equation}
\lim_{n\to\infty}a_{n}=L
\quad\text{or}\quad
n\to\infty\ \text{as}\ a_{n}\to{}L\nonumber
\end{equation}
if we can make the terms as close to $L$ as we like by taking $n$ sufficiently large.
If exists, we say the sequence \emph{converges} (or is \emph{convergent}).
Otherwise, we say the sequence \emph{diverges} (or is \emph{divergent}).
\end{definition}
%P.~565, Sekil~3
\begin{figure}[htb]
\centering
\includegraphics[page=565,scale=0.8,clip,trim=51mm 168mm 100mm 84mm]{stewart-ccac.pdf}
\includegraphics[page=565,scale=0.8,clip,trim=124mm 168mm 20mm 84mm]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{theorem}\label{s1ss1thm1} %P.~565, Teorem~2
If $\lim_{x\to\infty}f(x)=L$ and $f(n)=a_{n}$ when $n$ is an integer,
then $\lim_{n\to\infty}a_{n}=L$.
\end{theorem}
%P.~565, Sekil~4
\begin{figure}[htb]
  \centering
  \includegraphics[page=565,clip,trim=79mm 95mm 25mm 157mm]{stewart-ccac.pdf}
  \caption{}\label{s1ss1fig2}
\end{figure}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{block}{Limit Laws for Convergent Sequences} %P.~566, Limit kurallar{\i}
If $\{a_{n}\}$ and $\{b_{n}\}$ are convergent sequences and is a constant,
then the following properties hold.
\begin{enumerate}
\item $\lim_{n\to\infty}(a_{n}+b_{n})=\lim_{n\to\infty}a_{n}+\lim_{n\to\infty}b_{n}$.
\item $\lim_{n\to\infty}(a_{n}-b_{n})=\lim_{n\to\infty}a_{n}-\lim_{n\to\infty}b_{n}$.
\item $\lim_{n\to\infty}ca_{n}=c\lim_{n\to\infty}a_{n}$.
\item $\lim_{n\to\infty}(a_{n}b_{n})=\lim_{n\to\infty}a_{n}\cdot\lim_{n\to\infty}b_{n}$.
\item $\lim_{n\to\infty}\frac{a_{n}}{b_{n}}=\frac{\lim_{n\to\infty}a_{n}}{\lim_{n\to\infty}b_{n}}$ if $\lim_{n\to\infty}b_{n}\neq0$.
\item $\lim_{n\to\infty}a_{n}^{p}=\bigl[\lim_{n\to\infty}a_{n}\bigr]^{p}$ if $p>0$ and $a_{n}>0$.
\end{enumerate}
\end{block}
\end{frame}
%
%%
%
\begin{frame}{}{}
The Squeeze Theorem can also be adapted for sequences as follows (see Figure).

\begin{theorem}[Squeeze Theorem]\label{s1ss1thm2} %P.~566, S{\i}k{\i}\c{s}t{\i}rma teoremi
If $a_{n}\leq{}b_{n}\leq{}c_{n}$ for $n\geq{}n_{0}$ and $\lim_{n\to\infty}a_{n}=\lim_{n\to\infty}c_{n}=L$,
then $\lim_{n\to\infty}b_{n}=L$.
\end{theorem}
%P.~566, Sekil~5
\begin{figure}[htb]
  \centering
  \includegraphics[page=566,clip,trim=20mm 111mm 147mm 133mm]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%
%%
%
\begin{frame}{}{}
Another useful fact about limits of sequences is given by the following theorem,
which follows from the Squeeze Theorem because $-|a_{n}|\leq{}a_{n}\leq|a_{n}|$.

\begin{theorem}\label{s1ss1thm3} %P.~566, Teorem~4
If $\lim_{n\to\infty}|a_{n}|=0$, then $\lim_{n\to\infty}a_{n}=0$.
\end{theorem}
In particular, since we know from ***Section 2.5*** that when we have
\begin{equation}
\lim_{n\to\infty}\frac{1}{n^{r}}=0
\quad\text{if}\ r>0.\label{s1ss1eqn1z}
\end{equation}
If becomes large as $n$ becomes large, we use the notation
\begin{equation}
\lim_{n\to\infty}a_{n}=\infty.\nonumber
\end{equation}
In this case the sequence $\{a_{n}\}$ is divergent,
but in a special way.
We say that $\{a_{n}\}$ diverges to $\infty$.
The Limit Laws given in ***Section 2.3*** also hold for the limits of sequences and their proofs are similar.
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss1exm2} %P.~566, \"{O}rnek~3
Find $\lim_{n\to\infty}\frac{n}{n+1}$.
\end{example}
\begin{proof}[Solution]
The method is similar to the one we used in ***Section 2.5***:
Divide numerator and denominator by the highest power of that occurs in the denominator and then
use the Limit Laws.
\begin{align}
\lim_{n\to\infty}\frac{n}{n+1}
=&\lim_{n\to\infty}\frac{1}{1+\frac{1}{n}}
=\frac{\lim_{n\to\infty}1}{\lim_{n\to\infty}1+\lim_{n\to\infty}\frac{1}{n}}\nonumber\\
=&\frac{1}{1+0}=1\nonumber
\end{align}
Here we used \eqref{s1ss1eqn1z} with $r=1$.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss1exm3} %P.~566, \"{O}rnek~4
Calculate $\lim_{n\to\infty}\frac{\ln(n)}{n}$.
\end{example}
\begin{proof}[Solution]
Notice that both numerator and denominator approach infinity as $n\to\infty$.
We can't apply l'Hospital's Rule directly because it applies not to sequences
but to functions of a real variable.
However, we can apply l'Hospital's Rule to the related function $f(x)=\frac{\ln(x)}{x}$ and obtain
\begin{equation}
\lim_{x\to\infty}\frac{\ln(x)}{x}
=\lim_{x\to\infty}\frac{\frac{1}{x}}{1}
=0.\nonumber
\end{equation}
Therefore, by \emph{Theorem~\ref{s1ss1thm1}} we have
\begin{equation}
\lim_{n\to\infty}\frac{\ln(n)}{n}=0.\nonumber
\end{equation}
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss1exm4} %P.~567, \"{O}rnek~5
Determine whether the sequence $a_{n}=(-1)^{n}$ is convergent or divergent.
\end{example}
\begin{proof}[Solution]
If we write out the terms of the sequence, we obtain $\{-1,1,-1,1,\cdots\}$.
The graph of this sequence is shown in Figure.
%P.~567, \c{S}ekil~6
\begin{figure}[htb]
  \centering
  \includegraphics[page=567,clip,trim=22mm 168mm 146mm 84mm]{stewart-ccac.pdf}
\end{figure}
{\vspace{-12pt}}
Since the terms oscillate between $1$ and $(-1)$ infinitely often,
does not approach any number.
Thus, $\lim_{n\to\infty}(-1)^{n}$ does not exist; that is, the sequence is divergent.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss1exm5} %P.~567, \"{O}rnek~6
Evaluate $\lim_{n\to\infty}\frac{(-1)^{n}}{n}$ if it exists.
\end{example}
\begin{proof}[Solution]
\begin{equation}
\lim_{n\to\infty}\biggl|\frac{(-1)^{n}}{n}\biggr|
=\lim_{n\to\infty}\frac{1}{n}=0.\nonumber
\end{equation}
Therefore, by \emph{Theorem~\ref{s1ss1thm3}}
\begin{equation}
\lim_{n\to\infty}\frac{(-1)^{n}}{n}=0.\nonumber
\end{equation}
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss1exm6} %P.~568, \"{O}rnek~8
For what values of $r$ is the sequence $\{r^{n}\}$ convergent?
\end{example}
\begin{proof}[Solution]
We know from ***Section 2.5***
and the graphs of the exponential functions in ***Section 1.5***
that $\lim_{x\to\infty}a^{x}=\infty$ for $a>1$
and $\lim_{x\to\infty}a^{x}=0$ for $0<x<1$.
Therefore, putting $a=r$ and using \emph{Theorem~\ref{s1ss1thm1}},
we have
\begin{equation}
\lim_{n\to\infty}r^{n}
=
\begin{cases}
\infty,&r>1\\
0,&0<r<1
\end{cases}\nonumber
\end{equation}
For the cases $r=1$ and $r=0$ we have
\begin{equation}
\lim_{n\to\infty}1^{n}
=\lim_{n\to\infty}1
=1
\quad\text{and}\quad
\lim_{n\to\infty}0^{n}
=\lim_{n\to\infty}0
=0.\nonumber
\end{equation}
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{block}{}
If $-1<r<0$ and $0<|r|<1$, so
\begin{equation}
\lim_{n\to\infty}\bigl|r^{n}\bigr|
=\lim_{n\to\infty}|r|^{n}=0\nonumber
\end{equation}
and therefore $\lim_{n\to\infty}r^{n}=0$ by \emph{Theorem~\ref{s1ss1thm3}}.
If $r\leq-1$,
then $\{r^{n}\}$ diverges as in Example~\ref{s1ss1exm4} .
Figure shows the graphs for various values of $r$.
(The case is shown in Figure.)
%P.~568, \c{S}ekil~9
\begin{figure}[htb]
  \centering
\includegraphics[page=568,scale=0.8,clip,trim=78mm 98mm 87mm 141mm]{stewart-ccac.pdf}
\includegraphics[page=568,scale=0.8,clip,trim=138mm 98mm 28mm 141mm]{stewart-ccac.pdf}
\end{figure}
\vspace{-30pt}\qed
\end{block}
\end{frame}
%
%%
%
\begin{frame}{}{}
The results of Example~\ref{s1ss1exm6} are summarized for future use as follows.
\begin{block}{}
The sequence $\{r^{n}\}$ is convergent if $-1<r\leq1$
and divergent for all other values of $r$.
\begin{equation}
\lim_{n\to\infty}r^{n}
=
\begin{cases}
0,&-1<r<1\\
1,&r=1.
\end{cases}\label{s1ss1eqn1}
\end{equation}
\end{block}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{definition}[Monotonic Sequence]\label{s1ss1dfn2} %P.~568, Tan{\i}m
A sequence $\{a_{n}\}$ is called \emph{increasing} if $a_{n}<a_{n+1}$ for all $n\geq1$,
that is, $a_{1}<a_{2}<a_{3}<\cdots$.
It is called \emph{decreasing} if $a_{n}>a_{n+1}$ for all $n\geq1$.
It is called \emph{monotonic} if it is either increasing or decreasing.
\end{definition}
\begin{example}\label{s1ss1exm7} %P.~569, \"{O}rnek~9
The sequence $\bigl\{\frac{3}{n+5}\bigr\}$ is decreasing because
\begin{equation}
\frac{3}{n+5}>\frac{3}{(n+1)+5}=\frac{3}{n+6}\nonumber
\end{equation}
for all $n\geq1$.
(The right side is smaller because it has a larger denominator.)
\end{example}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{definition}[Bounded Sequence]\label{s1ss1dfn3}
A sequence $\{a_{n}\}$ is \emph{bounded above}
if there is a number $M$ such that
\begin{equation}
a_{n}\leq{}M\quad\text{for all}\ n\geq1.\nonumber
\end{equation}
It is \emph{bounded below}
if there is a number $m$ such that
\begin{equation}
a_{n}\geq{}m\quad\text{for all}\ n\geq1.\nonumber
\end{equation}
If it is bounded above and below, then $\{a_{m}\}$ is a \emph{bounded sequence}.
\end{definition}
For instance, the sequence $a_{n}=n$ is bounded below ($a_{n}>0$) but not above.
The sequence an $a_{n}=\frac{n}{n+1}$ is bounded because $0<a_{n}<1$ for all $n$.
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{theorem}[Monotonic Sequence Theorem]\label{s1ss1thm4}
Every bounded, monotonic sequence is convergent.
\end{theorem}
\end{frame}
%
\subsection{Series}\label{s1ss2} %P.~573
%
\begin{frame}{\insertsubsectionhead}{}
If we try to add the terms of an infinite sequence $\{a_{n}\}_{n=1}^{\infty}$ we get an expression of the form
\begin{equation}
a_{1}+a_{2}+\cdots+a_{n}+\cdots,\nonumber
\end{equation}
which is called an \emph{infinite series} (or just a \emph{series}) and is denoted, for short, by the symbol
\begin{equation}
\sum_{n=1}^{\infty}a_{n}
\quad\text{or}\quad
\sum_{}a_{n}.\nonumber
\end{equation}
\end{frame}
%
%%
%
\begin{frame}{}{}
But does it make sense to talk about the sum of infinitely many terms?
It would be impossible to find a finite sum for the series
because if we start adding the terms we get the cumulative sums
\begin{equation}
1+2+\cdots+n+\cdots\nonumber
\end{equation}
and, after the $n$th term, $\frac{n(n+1)}{2}$, which becomes very large as $n$ increases.
\end{frame}
%
%%
%
\begin{frame}{}{}
However, if we start to add the terms of the series
\begin{equation}
\frac{1}{2}+\frac{1}{4}+\cdots+\frac{1}{2^{n}}+\cdots\nonumber
\end{equation}
we get $\frac{1}{2},\frac{3}{4},\cdots,1-\frac{1}{2^{n}},\cdots$. The table shows that as we add more and
more terms, these partial sums become closer and closer to $1$.
\begin{table}
  \centering
  \begin{tabular}{cc}
    $n$ & Sum of first $n$ terms \\ \hline
    $1$ & $0.500000000$ \\
    %$2$ & $0.750000000$ \\
    %$3$ & $0.875000000$ \\
    $5$ & $0.968750000$ \\
    $10$ & $0.999023438$ \\
    %$20$ & $0.999999046$ \\
    $30$ & $0.999999999$
  \end{tabular}
  \caption{\empty}
  \label{s1ss2tbl1}
\end{table}
\vspace{-12pt}
In fact, by adding sufficiently many terms of the series
we can make the partial sums as close as we like to $1$.
So it seems reasonable to say that the sum of this infinite series is $1$ and to write
\begin{equation}
\sum_{n=1}^{\infty}\frac{1}{2^{n}}
=\frac{1}{2}+\frac{1}{4}+\cdots+\frac{1}{2^{n}}+\cdots
=1.\nonumber
\end{equation}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{definition}[Sequence of Partial Sums]\label{s1ss2dfn1} Given a series $\sum_{n=1}^{\infty}a_{n}=a_{1}+a_{2}+\cdots$,
let $s_{n}$ denote its $n$th partial sum:
\begin{equation}
s_{n}=\sum_{i=1}^{n}a_{i}=a_{1}+a_{2}+\cdots+a_{n}.\nonumber
\end{equation}
If the sequence is convergent and exists as a real number,
then the series is called  \emph{convergent} and we write
\begin{equation}
a_{1}+a_{2}+\cdots+a_{n}+\cdots=s
\quad\text{or}\quad
\sum_{n=1}^{\infty}a_{n}=s.\nonumber
\end{equation}
The number is called the sum of the series.
If the sequence $\{s_{n}\}$ is divergent,
then the series is called  \emph{divergent}.
\end{definition}
\end{frame}
%
%%
%\em
\begin{frame}{}{}
\begin{example}[Geometric Series]\label{s1ss2exm1} %P.~575, \"{O}rnek~1
An important example of an infinite series is the  \emph{geometric series}
\begin{equation}
a+ar+ar^{2}+\cdots+ar^{n-1}+\cdots=\sum_{n=1}^{\infty}ar^{n-1}\nonumber
\end{equation}
Each term is obtained from the preceding one by multiplying it by the common ratio $r$.
(We have already considered the special case where $a=\frac{1}{2}$ and $r=\frac{1}{2}$.)

If $r=1$, then $s_{n}=a+a+\cdots+a=na\to\pm\infty$.
Since $\lim_{n\to\infty}s_{n}$ doesn't exist,
the geometric series diverges in this case.
If $r\neq1$, we have
\begin{align}
s_{n}=a+ar+\cdots+ar^{n-1}
\quad\text{and}\quad
rs_{n}=ar+ar^{2}+\cdots+ar^{n}.\nonumber
\end{align}
Subtracting these equations, we get
\begin{equation}
s_{n}-rs_{n}=a-ar^{n}
\implies
s_{n}=\frac{a(1-r^{n})}{1-r}.\label{s1ss2exm1eq1}
\end{equation}
\end{example}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{exampleblock}{}
If $-1<r<1$, we know from ***(8.1.6)*** that $r^{n}\to0$ as $n\to\infty$, so
\begin{equation}
\lim_{n\to\infty}s_{n}
=\lim_{n\to\infty}\frac{a(1-r^{n})}{1-r}
=\frac{a}{1-r}-\frac{a}{1-r}\lim_{n\to\infty}r^{n}
=\frac{a}{1-r}.\nonumber
\end{equation}
Thus, when $|r|<1$ the geometric series is convergent
and its sum is $\frac{a}{1-r}$.
If $r\leq-1$ or $r>1$, the sequence $\{r^{n}\}$ is divergent by ***(8.1.6)*** and so, by \eqref{s1ss2exm1eq1},
$\lim_{n\to\infty}s_{n}$ does not exist.
Therefore, the geometric series diverges in those cases.
\end{exampleblock}
\end{frame}
%
%%
%
\begin{frame}{}{}
We summarize the results of Example~\ref{s1ss2exm1} as follows.
\begin{block}{}
The geometric series
\begin{equation}
\sum_{n=1}^{\infty}ar^{n-1}=a+ar+ar^{2}+\cdots\label{s1ss2eqn1}
\end{equation}
is convergent if $|r|<1$ and its sum
\begin{equation}
\sum_{n=1}^{\infty}ar^{n-1}=\frac{a}{1-r}.\nonumber
\end{equation}
If $|r|\geq1$, the geometric series is divergent.
\end{block}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss2exm2} %P.~576, \"{O}rnek~3
Is the series $\sum_{n=1}^{\infty}2^{2n}3^{1-n}$ convergent or divergent?
\end{example}
\begin{proof}[Solution]
Let's rewrite the term of the series in the form:
\begin{equation}
\sum_{n=1}^{\infty}2^{2n}3^{1-n}
=\sum_{n=1}^{\infty}\frac{4^{n}}{3^{n-1}}
=\sum_{n=1}^{\infty}4\biggl(\frac{4}{3}\biggr)^{n-1}.\nonumber
\end{equation}
We recognize this series as a geometric series with $a=4$ and $r=\frac{4}{3}$.
Since $r>1$,
the series diverges by Example~\ref{s1ss2exm1}.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss2exm3} %P.~576, \"{O}rnek~5
Find the sum of the series $\sum_{n=0}^{\infty}x^{n}$, where $|x|<1$.
\end{example}
\begin{proof}[Solution]
Notice that this series starts with $n=0$ and so the first term is $x^{0}=1$.
(With series, we adopt the convention that $x^{0}=1$ even when $x=0$.)
Thus
\begin{equation}
\sum_{n=0}^{\infty}x^{n}=1+x+x^{2}+\cdots.\nonumber
\end{equation}
This is a geometric series with $a=1$ and $r=x$.
Since $|r|=|x|<1$, it converges and Example~\ref{s1ss2exm1} gives
\begin{equation}
\sum_{n=0}^{\infty}x^{n}=\frac{1}{1-x}.\nonumber
\end{equation}
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss2exm4} %P.~577, \"{O}rnek~6
Show that the series $\sum_{n=1}^{\infty}\frac{1}{n(n+1)}$ is convergent, and find its sum.
\end{example}
\begin{proof}[Solution] \let \qed \relax
This is not a geometric series,
so we go back to the definition of a convergent series and compute the partial sums.
\begin{equation}
s_{n}=\sum_{i=1}^{n}\frac{1}{i(i+1)}
=\frac{1}{1\cdot2}+\frac{1}{2\cdot3}+\cdots+\frac{1}{n\cdot(n+1)}.\nonumber
\end{equation}
We can simplify this expression if we use the partial fraction decomposition
\begin{equation}
\frac{1}{i(i+1)}=\frac{1}{i}-\frac{1}{i+1}\nonumber
\end{equation}
(see ***Section~5.7***).
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{block}{}
Thus, we have
\begin{align}
s_{n}=&\sum_{i=1}^{n}\frac{1}{i(i+1)}
=\sum_{i=1}^{n}\biggl(\frac{1}{i}-\frac{1}{i+1}\biggr)\nonumber\\
=&\biggl(1-\frac{1}{2}\biggr)+\biggl(\frac{1}{2}-\frac{1}{3}\biggr)+\dots+\biggl(\frac{1}{n}-\frac{1}{n+1}\biggr)\nonumber\\
=&1-\frac{1}{n+1}\nonumber
\end{align}
and so
\begin{equation}
\lim_{n\to\infty}s_{n}=\lim_{n\to\infty}\biggl(1-\frac{1}{n+1}\biggr)=1-0=1.\nonumber
\end{equation}
Therefore, the given series is convergent and
\begin{equation}
\sum_{n=1}^{\infty}\frac{1}{n(n+1)}=1.\tag*{\qed}
\end{equation}
\end{block}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}[Harmonic Series]\label{s1ss2exm5} %P.~577, \"{O}rnek~7
Show that the  \emph{harmonic series}
\begin{equation}
\sum_{n=1}^{\infty}\frac{1}{n}\nonumber
\end{equation}
is divergent.
\end{example}
\begin{proof}[Solution]
For this particular series it's convenient to consider the partial sums $s_{2},s_{4},s_{8},\cdots$ and show that they become large.
\begin{align}
s_{2}=&1+\frac{1}{2}\nonumber\\
s_{4}=&1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}
>1+\frac{1}{2}+\frac{1}{4}+\frac{1}{4}=1+\frac{2}{2}\nonumber\\
s_{8}=&1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}+\frac{1}{5}+\frac{1}{6}+\frac{1}{7}+\frac{1}{8}\nonumber\\
>&1+\frac{1}{2}+\frac{1}{4}+\frac{1}{4}+\frac{1}{8}+\frac{1}{8}+\frac{1}{8}+\frac{1}{8}
=1+\frac{3}{2}\nonumber
\end{align}
In general, $s_{2^{n}}>1+\frac{n}{2}$ for $n=1,2,\cdots$.
This shows that $s_{2^{n}}\to\infty$ as $n\to\infty$ and so is divergent.
Therefore, the harmonic series diverges.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{theorem}\label{s1ss2thm1} %P.~578, Teorem~6
If the series is convergent $\sum_{n=1}^{\infty}a_{n}$,
then $\lim_{n\to\infty}a_{n}=0$.
\end{theorem}
\begin{remark}\label{s1ss2rmk1} %P.~578, Not~1
With any series $\sum{}a_{n}$ we associate two sequences:
the sequence $\{s_{n}\}$ of its partial sums and the sequence $\{a_{n}\}$ of its terms.
If $\sum{}a_{n}$ is convergent, then the limit of the sequence $\{s_{n}\}$ is (the sum of the series) and,
as  \emph{Teorem~\ref{s1ss2thm1}} asserts, the limit of the sequence $\{a_{n}\}$ is $0$.
\end{remark}
\begin{remark}\label{s1ss2rmk2} %P.~578, Not~2
The converse of  \emph{Teorem~\ref{s1ss2thm1}} is not true in general.
If $\lim_{n\to\infty}a_{n}=0$, we cannot conclude that is convergent.
Observe that for the harmonic series $\sum\frac{1}{n}$
we have $a_{n}=\frac{1}{n}\to0$ as $n\to\infty$,
but we showed in Example~\ref{s1ss2exm5} that $\sum\frac{1}{n}$ is divergent.
\end{remark}
\begin{theorem}[The Test for Divergence]\label{s1ss2thm2} %P.~578, Iraksakl{\i}k testi
If $\lim_{n\to\infty}a_{n}$ does not exist or if $\lim_{n\to\infty}a_{n}\neq0$,
then the series $\sum_{n=1}^{\infty}a_{n}$ is divergent.
\end{theorem}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s1ss2exm6} %P.~578, \"{O}rnek~8
Show that the series $\sum_{n=1}^{\infty}\frac{n^{2}}{5n^{2}+4}$ diverges.
\end{example}
\begin{proof}[Solution]
\begin{equation}
\lim_{n\to\infty}a_{n}
=\lim_{n\to\infty}\frac{n^{2}}{5n^{2}+4}
=\lim_{n\to\infty}\frac{1}{5+\frac{4}{n^{2}}}
=\frac{1}{5}\neq0.\nonumber
\end{equation}
So the series diverges by the Test for Divergence.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{remark}\label{s1ss2rmk3} %P.~578, Not~3
If we find that $\lim_{n\to\infty}a_{n}\neq0$,
we know that $\sum{}a_{n}$ is divergent.
If we find that $\lim_{n\to\infty}a_{n}=0$,
we know nothing about the convergence or divergence of $\sum{}a_{n}$.
Remember the warning in Remark~\ref{s1ss2rmk2}:
If $\lim_{n\to\infty}a_{n}=0$,
the series $\sum{}a_{n}$ might converge or it might diverge.
\end{remark}
\begin{theorem}\label{s1ss2thm3} %P.~579, Teorem~8
If $\sum{}a_{n}$ and $\sum{}b_{n}$ are convergent series,
then so are the series $\sum{}ca_{n}$ (where $c$ is a constant),
$\sum{}(a_{n}+b_{n})$, and $\sum{}(a_{n}-b_{n})$,
and the followings hold.
\begin{enumerate}
\item $\sum_{n=1}^{\infty}ca_{n}=c\sum_{n=1}^{\infty}a_{n}$.
\item $\sum_{n=1}^{\infty}(a_{n}+b_{n})=\sum_{n=1}^{\infty}a_{n}+\sum_{n=1}^{\infty}b_{n}$.
\item $\sum_{n=1}^{\infty}(a_{n}-b_{n})=\sum_{n=1}^{\infty}a_{n}-\sum_{n=1}^{\infty}b_{n}$.
\end{enumerate}
\end{theorem}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{remark}\label{s1ss2rmk4} %P.~580, Not~4
A finite number of terms doesn't affect the convergence or divergence of a series.
For instance, suppose that we were able to show that the series
\begin{equation}
\sum_{n=4}^{\infty}\frac{n}{n^{3}+1}\nonumber
\end{equation}
is convergent.
Since
\begin{equation}
\sum_{n=1}^{\infty}\frac{n}{n^{3}+1}
=\frac{1}{2}+\frac{2}{9}+\frac{3}{28}+\sum_{n=4}^{\infty}\frac{n}{n^{3}+1}\nonumber
\end{equation}
it follows that the entire series $\sum_{n=1}^{\infty}\frac{n}{n^{3}+1}$ is convergent.
Similarly, if it is known that the series $\sum_{n=N+1}^{\infty}a_{n}$ converges,
then the full series
\begin{equation}
\sum_{n=1}^{\infty}a_{n}=\sum_{n=1}^{N}a_{n}+\sum_{n=N+1}^{\infty}a_{n}\nonumber
\end{equation}
is also convergent.
\end{remark}
\end{frame}
%
%%
%
\subsection{The Integral and Comparison Tests}\label{s1ss3} %P.~583
%
\begin{frame}{\insertsubsectionhead}{}
In general, it is difficult to find the exact sum of a series.
We were able to accomplish this for geometric series and the series $\sum\frac{1}{n(n+1)}$
because in each of those cases we could find a simple formula for the $n$th partial sum $s_{n}$.
But usually it is not easy to compute $\lim_{n\to\infty}s_{n}$.
Therefore, in this section and the next we develop tests that enable us to determine
whether a series is convergent or divergent without explicitly finding its sum.

In this section we deal only with series with positive terms,
so the partial sums are increasing.
In view of the Monotonic Sequence Theorem,
to decide whether a series is convergent or divergent,
we need to determine whether the partial sums are bounded or not.
\end{frame}
%
%%
%
\subsubsection{Testing with an Integral}\label{s2ss1sss1} %P.~583
%
\begin{frame}{\insertsubsubsectionhead}{}
\begin{theorem}[The Integral Test]\label{s2ss1sss1thm1} %P.~584, \.{I}ntegral Testi
Suppose $f$ is a continuous, positive, decreasing function on $[1,\infty)$ and let $a_{n}=f(n)$.
Then the series $\sum_{n=1}^{\infty}a_{n}$ is convergent
if and only if the improper integral $\int_{1}^{\infty}f(x)dx$ is convergent.
In other words:
\begin{enumerate}
\item If $\int_{1}^{\infty}f(x)dx$ is convergent,
    then $\sum_{n=1}^{\infty}a_{n}$ is convergent.
\item If $\int_{1}^{\infty}f(x)dx$ is divergent,
    then $\sum_{n=1}^{\infty}a_{n}$ is divergent.
\end{enumerate}
\end{theorem}
\begin{remark}\label{s2ss1sss1rmk1} %P.~585, Not~1
When we use the Integral Test it is not necessary to start the series or the integral at $n=1$. For instance, in testing the series $\sum_{k=4}^{\infty}\frac{1}{(n-3)^{2}}$ we use $\int_{4}^{\infty}\frac{1}{(x-3)^{2}} dx$. Also, it is not necessary that $f$ be always decreasing. What is important is that be \emph{ultimately} decreasing, that is, decreasing for $x$ larger than some number $N$. Then $\sum_{n=N}^{\infty}$ is convergent, so $\sum_{n=1}^{\infty}$ is convergent by Remark~\ref{s1ss2rmk4} of  Section~\ref{s1}.\ref{s1ss2}.
\end{remark}
\end{frame}
%
%%
%
\begin{frame}{}{}

\begin{example}\label{s2ss1sss1exm1} %P.~585, \"{O}rnek~2
For what values of $p$ is the series $\sum_{n=1}^{\infty}\frac{1}{n^{p}}$ convergent?
\end{example}
\begin{proof}[Solution]
If $p<0$, then $\lim_{n\to\infty}\frac{1}{n^{p}}=\infty$.
If $p=0$, then $\lim_{n\to\infty}\frac{1}{n^{p}}=1$.
In either case $\lim_{n\to\infty}\frac{1}{n^{p}}\neq0$,
so the given series diverges by the Test for Divergence ***[see (8.2.7)]***.
If $p>0$, then the function is clearly continuous, positive, and decreasing on $[1,\infty)$.
We found in ***Chapter 5 [see (5.10.2)]*** that $\int_{1}^{\infty}\frac{1}{x^{p}}dx$ converges if $p>1$
and diverges if $p\leq1$.
It follows from the Integral Test that the series $\sum_{n=1}^{\infty}\frac{1}{n^{p}}$ converges if $p>1$
and diverges if $0<p\leq1$.
(For $p=1$, this series is the harmonic series discussed in Example~\ref{s1ss2exm5} in  \emph{Section~\ref{s1}.\ref{s1ss2}}.)
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
The series in Example~\ref{s1ss2exm5} is called the $p$-series.
It is important in the rest of this chapter,
so we summarize the results of Example~\ref{s1ss2exm5} for future reference as follows.
\begin{block}{}
The $p$-series $\sum_{n=1}^{\infty}\frac{1}{n^{p}}$ is convergent if $p>1$,
and divergent if $p\leq1$.
\end{block}
\end{frame}
%
%%
%
\subsubsection{Testing by Comparing}\label{s2ss1sss2} %P.~586
%
\begin{frame}{\insertsubsubsectionhead}{}
\begin{theorem}[The Comparison Test]\label{s2ss1sss2thm1} %P.~586, Kar\c{s}{\i}la\c{s}t{\i}rma Testi
Suppose that $\sum{}a_{n}$ and $\sum{}b_{n}$ are series with positive terms.
\begin{enumerate}[(a)]
\item\label{s2ss1sss2thm1it1} If $\sum{}b_{n}$ is convergent and $a_{n}\leq{}b_{n}$ for all $n$,
    then $\sum{}a_{n}$ is also convergent.
\item\label{s2ss1sss2thm1it2} If $\sum{}b_{n}$ is divergent and $a_{n}\geq{}b_{n}$ for all $n$,
    then $\sum{}a_{n}$ is also divergent.
\end{enumerate}
\end{theorem}
\begin{remark} %P.~587
Although the condition $a_{n}\leq{}b_{n}$ or $a_{n}\geq{}b_{n}$ in the Comparison Test is given for all $n$,
we need verify only that it holds for $n\geq{}N$,
where $N$ is some fixed integer,
because the convergence of a series is not affected by a finite number of terms. 
\end{remark}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s2ss1sss2exm1} %P.~587, \"{O}rnek~3
Determine whether the series $\sum_{n=1}^{\infty}\frac{5}{2n^{2}+4n+3}$ converges or diverges.
\end{example}
\begin{proof}[Solution]
For large $n$ the dominant term in the denominator is $2n^{2}$,
so we compare the given series with the series $\sum\frac{5}{2n^{2}}$.
Observe that
\begin{equation}
\frac{5}{2n^{2}+4n+3}<\frac{5}{2n^{2}}\nonumber
\end{equation}
because the left side has a bigger denominator.
(In the notation of the Comparison Test,
$a_{n}$ is the left side and $b_{n}$ is the right side.)
We know that
\begin{equation}
\sum_{n=1}^{\infty}\frac{5}{2n^{2}}
=\frac{5}{2}\sum_{n=1}^{\infty}\frac{1}{n^{2}}\nonumber
\end{equation}
is convergent ($p$-series with $p=2>1$).
Therefore
$\sum_{n=1}^{\infty}\frac{5}{2n^{2}+4n+3}$
is convergent by part  \emph{\eqref{s2ss1sss2thm1it1}} of the Comparison Test.
\end{proof}
\end{frame}
%
%%
%
\begin{frame}{}{}
\begin{example}\label{s2ss1sss2exm2} %P.~587, \"{O}rnek~4
Test the series $\sum_{n=1}^{\infty}\frac{\ln(n)}{n}$ for convergence or divergence.
\end{example}
\begin{proof}[Solution]
Observe that $\ln(n)>1$ for $n\geq3$ and so
\begin{equation}
\frac{\ln(n)}{n}>\frac{1}{n},\quad{}n\geq3.\nonumber
\end{equation}
We know that is divergent ($p$-series with $p=1$).
Thus, the given series is divergent by the Comparison Test.
\end{proof}
\end{frame}