\section{silinecek}
\subsection{Improper Integrals}

\begin{frame}{\insertsubsectionhead}
\begin{block}{}
	In defining a definite integral $\displaystyle \int_a^bf(x)\;dx$ we dealt with a function $f$ defined on a finite interval $[a,b]$ and we assumed that $f$ does not have an infinite discontinuity.\medskip
	
	In this section we extend the concept of a definite integral to the case where the \textbf{interval is infinite} and also to the case where $f$ has an infinite \textbf{discontinuity} in $[a,b]$. \medskip
	
	In either case the integral is called an \textbf{improper integral}.
\end{block}

\end{frame}
%
%
%---------------------------197
\subsubsection{Type 1: Infinite Intervals}
%
\begin{frame}{\insertsubsubsectionhead}
	\begin{block}{Definition of an Improper Integral of Type 1}
		\begin{itemize}
			\item[(a)] If $\displaystyle \int_a^t f(x)dx$ exists for every number $t\geq a$, then
			\[\int_a^\infty f(x)dx=\lim_{t\to \infty}\int_a^tf(x)dx\]
			provided this limit exists (as a finite number).
		\end{itemize}
	\end{block}
\end{frame}
%
%
\begin{frame}
\begin{block}{Definition of an Improper Integral of Type 1 (cont.)}
	\begin{itemize}
		\item[(b)] If $\displaystyle \int_t^b f(x)\;dx$ exists for every number $t\leq b$, then
		\[\int_{-\infty}^b f(x)dx=\lim_{t\to -\infty}\int_t^bf(x)dx\]
		provided this limit exists (as a finite number).
	\end{itemize}
	The improper integrals
	$\displaystyle \int_a^{\infty}f(x)dx$ and $\displaystyle\int_{-\infty}^b f(x)dx$ 
	are called \textbf{convergent} if the
	corresponding limit exists and \textbf{divergent} if the limit does not exist.
\end{block}
\end{frame}
%---------------------------199
%
%
%
%---------------------------200
%
\begin{frame}
\begin{block}{Definition of an Improper Integral of Type 1 (cont.)}
	\begin{itemize}
		\item[(c)] If both $\displaystyle \int_a^\infty f(x)dx$ and $\displaystyle\int_{-\infty}^a f(x)dx$ are convergent, then we define
		\[\int_{-\infty}^\infty f(x)dx=\int_{-\infty}^a f(x)dx+\int_a^\infty f(x)dx.\]
		Here any real number $a$ can be used.
	\end{itemize}
\end{block}
\end{frame}
%
%
%---------------------------201
%
\begin{frame}
	\begin{example}
		Determine whether the integral $\displaystyle\int_1^\infty \frac{1}{x}dx$ is convergent or divergent.
	\end{example}
	\begin{proof}[Solution]
		According to part {\usebeamercolor[fg]{structure}(a)} of Definition, we have
		\begin{align*}
			\int_1^\infty \frac{1}{x}dx&=\displaystyle \lim_{t\to \infty}\int_1^t\frac{1}{x}dx=\lim_{t\to \infty}\ln |x|\Big]_1^t\\
			&= \lim_{t\to \infty}(\ln t-\ln 1)=\lim_{t\to \infty}\ln t=\infty
		\end{align*}
		The limit does not exist as a finite number and so the improper integral $\displaystyle \int_1^\infty \frac{1}{x}dx$ is divergent.
	\end{proof}
\end{frame}
%
%---------------------------202
%
%
\begin{frame}
	\begin{example}
		Evaluate $\displaystyle \int_{-\infty}^0 xe^xdx$.
	\end{example}
	\begin{proof}[Solution]\let \qed \relax
		Using part {\usebeamercolor[fg]{structure}(b)} of Definition, we have
		\[\int_{-\infty}^0 xe^x dx=\lim_{t\to \infty} \int_t^0x e^x dx\]
		We integrate by parts with $u=x$, $dv=e^x dx$, so that $du=dx$, $v=e^x$:
		\[\int_t^0 x e^x dx =x e^x\Big]_t^0-\int_t^0 e^x dx=-t e^t-1+e^t\]
	\end{proof}
\end{frame}
%
%---------------------------203
%
%
\begin{frame}
	\begin{proof}[Solution (cont.)]
		\[\int_t^0x e^x dx =-t e^t-1+e^t\]
		We know that $e^t\to 0$ as $\displaystyle t\to -\infty$, and by L'Hospital's Rule we have
		\begin{align*}
			\lim_{t\to -\infty}t e^t&=\displaystyle \lim_{t\to -\infty}\frac{t}{e^{-t}}=\lim_{t\to -\infty}\frac{1}{-e^{-t}}\\
			&= \lim_{t\to -\infty}(-e^t)=0.
		\end{align*}
		Therefore
		\begin{align*}
			\int_{-\infty}^0 xe^xdx&=\displaystyle \lim_{t\to -\infty}(-te^t-1+e^t)\\
			&=-0-1+0=-1. \qedhere
		\end{align*}
	\end{proof}	
\end{frame}
%
%
%---------------------------204
%
%---------------------------205
%
\begin{frame}
	\begin{example}
		Evaluate $\displaystyle \int_{-\infty}^{\infty}\frac{1}{1+x^2}\;dx$.
	\end{example}
	\begin{proof}[Solution]\let \qed \relax
		It’s convenient to choose $a=0$ in Definition {\usebeamercolor[fg]{structure}(c)}:
		\[\int_{-\infty}^\infty \frac{1}{1+x^2}dx=\int_{\infty}^0\frac{1}{1+x^2}dx+\int_0^\infty \frac{1}{1+x^2}dx.\]
	\end{proof}
\end{frame}
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let \qed \relax
	We must now evaluate the integrals on the right side separately:
	\begin{align*}
	\int_0^\infty\frac{1}{1+x^2}dx&=\displaystyle \lim_{t\to \infty}\int_0^t\frac{dx}{1+x^2}=\lim_{t\to \infty}\tan^{-1}x\Big]_0^t\\
	&=\lim_{t\to \infty} (\tan^{-1}t+\tan^{-1}0)=\lim_{t\to \infty}\tan^{-1}t=\frac{\pi}{2}
	\end{align*}
	\begin{align*}
	\int_{-\infty} ^0\frac{1}{1+x^2}dx&=\displaystyle \lim_{t\to -\infty}\int_t^0\frac{dx}{1+x^2}dx=\lim_{t\to -\infty}\tan^{-1}x\Big]_t^0\\
	&=\displaystyle \lim_{t\to -\infty}(\tan^{-1}0-\tan^{-1}t)\\
	&=\displaystyle 0-\left(-\frac{\pi}{2}\right)=\frac{\pi}{2}.
	\end{align*}
\end{proof}	
\end{frame}
%
%---------------------------207
%
%---------------------------208
%
\begin{frame}
	\begin{proof}[Solution (cont.)]
		Since both of these integrals are convergent, the given integral is convergent and
		\[\int_{-\infty}^\infty \frac{1}{1+x^2}dx =\frac{\pi}{2}+\frac{\pi}{2}=\pi.\]
		Since $1/(1+x^2)>0$, the given improper integral can be interpreted as the area of the infinite region that lies under the curve $y=1/(1+x^2)$ and above the $x-$axis.
	\end{proof}
\end{frame}
%
%
%---------------------------209
%
\begin{frame}
	\begin{example}
		For what values of $p$ is the integral
		\[\int_1^\infty \frac{1}{x^p}\;dx\]
		convergent?
	\end{example}
	\begin{proof}[Solution]\let \qed \relax
		We know from first example that if $p=1$,  then the integral is divergent, so let's assume that $p\neq 1$. Then
		\begin{align*}
			\int_1^\infty \frac{1}{x^p}dx&\displaystyle=\lim_{t\to \infty}\int_1^t\frac{1}{x^p}dx=\lim_{t\to \infty} \left.\frac{x^{-p+1}}{-p+1}\right]_{x=1}^{x=t}\\
			&=\lim_{t\to \infty} \frac{1}{1-p}\left[\frac{1}{t^{p-1}}-1\right].
		\end{align*}
	\end{proof}
\end{frame}
%
%---------------------------210
%
%
\begin{frame}
	\begin{proof}[Solution (cont.)]
		If $p>1$, then $p-1>0$, so as $t\to \infty$, $t^{p-1}\to\infty$ and $1/t^{p-1}\to0$. Therefore
		\[\int_1^{\infty} \frac{1}{x^p}dx=\frac{1}{p-1} \quad \text{ for } \quad p>1\]
		and so the integral converges.
		But if $p<1$, then $p-1<0$ and so
		\[\frac{1}{t^{p-1}}=t^{1-p}\to \infty \quad \text{ as } \quad  t\to \infty \]
		and the integral diverges. That is,
		$\displaystyle \int_1^\infty \frac{1}{x^p}\;dx$ is convergent if $p>1$ and divergent if $p\leq1$.
	\end{proof}
\end{frame}
%
%---------------------------211
%
%
%---------------------------212
%
\subsubsection{Type 2: Discontinuous Integrands}
%
\begin{frame}{\insertsubsubsectionhead}
	\begin{block}{Definition of an Improper Integral of Type 2}
		\begin{itemize}
			\item[(a)] If $f$ is continuous on $[a,b)$ and is discontinuous at $b$, then
			\[\int_a^bf(x)dx=\lim_{t\to b^-}\int_a^tf(x)dx\]
			if this limit exists (as a finite number).
		\end{itemize}
	\end{block}
\end{frame}
%
%
%---------------------------213
%
\begin{frame}
	\begin{block}{Definition of an Improper Integral of Type 2 (cont.)}
		\begin{itemize}
			\item[(b)] If $f$ is continuous on $(a,b]$ and is discontinuous at $a$, then
			\[\int_a^bf(x)dx=\lim_{t\to a^+}\int_t^bf(x)dx\]
			if this limit exists (as a finite number).
	\end{itemize}	
			The improper integral $\displaystyle \int_a^b f(x)dx$ is called \textbf{convergent} if the corresponding limit exists and \textbf{divergent} if the limit does not exist.
	\end{block}
\end{frame}
%
%---------------------------214
%
%
\begin{frame}
	\begin{block}{Definition of an Improper Integral of Type 2 (cont.)}
		\begin{itemize}
			\item[(c)] If $f$ has a discontinuity at $c$, where $a<c<b$, and both $\displaystyle \int_a^cf(x)dx$ and $\displaystyle \int_c^bf(x)dx$ are convergent, then we define
			\[\int_a^bf(x)dx=\int_a^cf(x)dx+\int_c^bf(x)dx.\]
		\end{itemize}
	\end{block}
\end{frame}
%
%
%---------------------------215
%
\begin{frame}
	\begin{example}
		Find $\displaystyle \int_2^5 \frac{1}{\sqrt{x-2}}dx.$
	\end{example}
	\begin{proof}[Solution]
		We note first that the given integral is improper because $f(x)=1/\sqrt{x-2}$ has the vertical asymptote $x=2$. Since the infinite discontinuity occurs at the left endpoint of $[2,5]$.
		We use part {\usebeamercolor[fg]{structure}(b)} of Definition:
		\begin{align*}
		\int_2^5 \frac{dx}{\sqrt{x-2}}&\displaystyle=\lim_{t\to 2^+}\int_t^5\frac{dx}{\sqrt{x-2}} =\lim_{t\to 2^+} 2\sqrt{x-2}\Big]_t^5\\
		&\displaystyle = \lim_{t\to 2^+}2(\sqrt{3}-\sqrt{t-2}) = 2\sqrt{3}.
		\end{align*}
		Thus, the given improper integral is convergent.
	\end{proof}
\end{frame}
%
%
%---------------------------216
%
%---------------------------217
%
%
\begin{frame}
	\begin{example}
		Determine whether $\displaystyle \int_0^{\pi/2} \sec x\;dx$ converges or diverges.
	\end{example}
	\begin{proof}[Solution]
		Note that the given integral is improper because $ \lim\limits_{x\to (\pi/2)^-}\sec x=\infty$.
		\begin{align*}
			\int_0^{\pi/2}\sec x dx&=\displaystyle \lim_{t\to (\pi/2)^-}\int_0^t\sec x dx=\lim_{t\to (\pi/2)^-}\ln |\sec x +\tan x|\Big]_0^t\\
			&=\lim_{t\to (\pi/2)^-} [\ln(\sec t+\tan t)-\ln1] = \infty
		\end{align*}
		because $\sec t\to \infty$ and $\tan t\to \infty$ as $t\to (\pi/2)^-$. Thus, the given improper integral is divergent.
	\end{proof}
\end{frame}
%
%---------------------------218
%
%---------------------------219
%
%
\begin{frame}
	\begin{example}
		Evaluate $\displaystyle \int_0^3\frac{dx}{x-1}$ if possible.
	\end{example}
	\begin{proof}[Solution]\let \qed \relax
		Observe that the line $x=1$ is a vertical asymptote of the integrand. Since it occurs in the middle of the interval $[0,3]$, we must use part {\usebeamercolor[fg]{structure}(c)} of Definition with $c=1$:
		\[\int_0^3\frac{dx}{x-1}=\int_0^1\frac{dx}{x-1}+\int_1^3\frac{dx}{x-1}\]
	\end{proof}
\end{frame}
%
%
%---------------------------220
%
\begin{frame}
	\begin{proof}[Solution (cont.)]
		where
		\begin{align*}
			\int_0^1\frac{dx}{x-1}&= \lim_{t\to 1^-}\int_0^t\frac{dx}{x-1}=\lim_{t\to 1^-} \ln |x-1| \Big]_0^t\\
			&= \lim_{t\to 1^-} (\ln |t-1|-\ln|-1|)\\
			&=\lim_{t\to 1^-} \ln(1-t)=-\infty
		\end{align*}
		because $1-t\to 0^+$ as $t\to 1^-$. Thus, $ \int_0^1 dx/(x-1)$ is divergent. This implies that $\displaystyle \int_1^3 dx/(x-1)$ is divergent. We do not need to evaluate $\displaystyle \int_0^3 dx/(x-1)$.
	\end{proof}
\end{frame}
%
%---------------------------221
%
%
%
%---------------------------222
%
\begin{frame}
	\begin{alertblock}{Warning}
		If we had not noticed the asymptote $x=1$ in example above and had instead confused the integral with an ordinary integral, then we might have made the following erroneous calculation:
		\[\int_0^3\frac{dx}{x-1}=\ln|x-1|\;\Big]_0^3=\ln2-\ln1=\ln2.\]
		This is wrong because the integral is improper and must be calculated in terms of limits.
	\end{alertblock}
	\begin{alertblock}{Warning}
		From now on, whenever you meet the symbol $\displaystyle \int_a^b f(x)dx$ you must decide, by looking
		at the function $f$ on $[a,b]$, whether it is an ordinary definite integral or an improper
		integral.
	\end{alertblock}
\end{frame}
%
%
%---------------------------223
%
%---------------------------224
%
%
\begin{frame}
	\begin{example}
		Evaluate $\displaystyle \int_0^1\ln xdx$.
	\end{example}
	\begin{proof}[Solution]\let \qed \relax
		We know that the function $f(x)=\ln x$ has a vertical asymptote at $0$ since $\lim_{x\to 0^+}\ln x=-\infty$. Thus, the given integral is improper and we have
		\[\int_0^1\ln xdx=\lim_{t\to 0^+}\int_t^1\ln x dx\]
	\end{proof}
\end{frame}
%
%
%---------------------------225
%
\begin{frame}
	\begin{proof}[Solution (cont.)]\let \qed \relax
		Now we integrate by parts with $u=\ln x$, $dv=dx$, $du=dx/x$, and $v=x$:
		\begin{align*}
		\int_t^1\ln xdx &=\displaystyle x \ln x\Big]_t^1-\int_t^1 dx\\
		&= 1\ln1 -t\ln t-(1-t)\\
		&= -t\ln t -1+t.
		\end{align*}
	\end{proof}
\end{frame}
%
%
%---------------------------226
%
\begin{frame}
	\begin{proof}[Solution (cont.)]
		To find the limit of the first term we use L'Hospital's Rule:
		\begin{align*}
			\lim_{t\to 0^+}t\ln t&= \displaystyle \lim_{t\to 0^+} \frac{\ln t}{1/t}=\lim_{t\to 0^+}\frac{1/t}{-1/t^2}\\
			&=\displaystyle \lim_{t\to 0^+} (-t)=0.
		\end{align*}
		Therefore
		\begin{align*}
			\int_0^1 \ln x dx &=\displaystyle \lim_{t\to 0^+}(-t\ln t -1+t)\\
			&=-0-1+0=-1. \qedhere
		\end{align*}
	\end{proof}
\end{frame}
%
%
%---------------------------227
\subsubsection{A Comparison Test for Improper Integrals}
%
\begin{frame}{\insertsubsubsectionhead}
	Sometimes it is impossible to find the exact value of an improper integral and yet it is important to know whether it is convergent or divergent.
	\begin{block}{Comparison Theorem}
		Suppose that $f$ and $g$ are continuous functions with $f(x)\geq g(x)\geq 0$ for $x\geq a$.
		\begin{itemize}
			\item[(a)] If $\displaystyle \int_0^\infty f(x)dx$ is convergent, then $\displaystyle \int_a^\infty g(x)dx$ is convergent.
			\item[(b)] If $\displaystyle \int_a^\infty g(x)dx$ is divergent, then $\displaystyle \int_a^\infty f(x)dx$ is divergent.
		\end{itemize}
	\end{block}
\end{frame}
%
%---------------------------228
%
%
%---------------------------229
%
\begin{frame}
	\begin{alertblock}{Warning}
		Note that the reverse is not necessarily true:
		
		If $\displaystyle \int_a^\infty g(x)dx$ is convergent, $\displaystyle\int_a^\infty f(x)dx$ may or may not be convergent, and if $\displaystyle \int_a^\infty f(x)dx$ is divergent, $\displaystyle \int_a^\infty g(x)dx$ may or may not be divergent.
	\end{alertblock}
\end{frame}
%
%
%---------------------------230
%
\begin{frame}
	\begin{example}
		Show that $\displaystyle \int_0^\infty e^{-x^2}\;dx$ is convergent.
	\end{example}
	\begin{proof}[Solution]\let \qed \relax
		We can't evaluate the integral directly because the antiderivative of $e^{-x^2}$ is not an elementary function. We write
		\[\int_0^\infty e^{-x^2}dx=\int_0^1e^{-x^2}dx+\int_1^\infty e^{-x^2}dx\]
		and observe that the first integral on the right-hand side is just an ordinary definite integral.
	\end{proof}
\end{frame}
%
%
%---------------------------231
%
\begin{frame}
	\begin{proof}[Solution (cont.)]\let \qed \relax
		\[\int_0^\infty e^{-x^2}dx=\int_0^1e^{-x^2}dx+\int_1^\infty e^{-x^2}dx\]
		In the second integral we use the fact that for $x\geq 1$ we have
		\[ x^2\geq x, \]
		so
		\[ -x^2\leq -x \]
		and therefore
		\[ e^{-x^2}\leq e^{-x}. \]
	\end{proof}
\end{frame}
%
%
%---------------------------232
%
\begin{frame}
	\begin{proof}[Solution (cont.)]
		The integral of $e^{-x}$ is easy to evaluate:
		\[\int_1^\infty e^{-x}dx =\lim_{t\to \infty} \int_1^t e^{-x} dx=\lim_{t\to \infty} (e^{-1}-e^{-t})=e^{-1}\]
		Thus, taking $f(x)=e^{-x}$ and $g(x)=e^{-x^2}$ in the Comparison Theorem, we see that $\displaystyle \int_1^\infty e^{-x^2}dx$ is convergent. It follows that $\displaystyle \int_0^\infty e^{-x^2}dx$ is convergent.
	\end{proof}
\end{frame}
%
%
%---------------------------233
%
\begin{frame}
	\begin{example}
		The integral $\displaystyle \int_1^\infty \frac{1+e^{-x}}{x}dx$ is divergent by the Comparison Theorem because
		\[\displaystyle \frac{1+e^{-x}}{x}>\frac{1}{x}\]
		and $\displaystyle \int_1^{\infty}\frac{1}{x} dx $ is divergent by the first example.
	\end{example}
\end{frame}

