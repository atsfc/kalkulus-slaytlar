\section{Functions and Models}
\subsection{Inverse Functions and Logarithms}
\subsubsection{Inverse Functions}
\begin{frame}{\insertsubsectionhead}{\insertsubsubsectionhead}
\begin{definition}
A function $f$ is called a \textbf{one-to-one} function if it never takes on
the same value twice; that is,
\[ x_1\neq x_2 \implies f(x_1)\neq f(x_2).\]
\end{definition}
\end{frame}
%
%
%
\begin{frame}
If a \textbf{horizontal line} intersects the graph of $f$ in more than one point, then we see from Figure that there are numbers $x_1$ and $x_2$ such that $f(x_1)=f(x_2)$. This means that $f$ is not one-to-one.
  \begin{figure}[h]
  	\includegraphics[page=65,clip,trim=21mm 149mm 144mm 93mm,scale=1.3]{stewart-ccac.pdf}
  \end{figure}
\end{frame}
%
%
%
\begin{frame}
Therefore, we have the following geometric method for determining
whether a function is one-to-one.\\\vspace{.5cm}
\begin{block}{Horizontal Line Test}
A function is one-to-one if and only if no horizontal line intersects its graph more than once.
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
Is the function $f(x)=x^3$ one-to-one?
\end{example}
\begin{proof}[Solution 1.]
If $x_1\neq x_2$, then $x_1^3\neq x_2^3$ (two diferent numbers can't have the same cube). Therefore, by definition, $f(x)=x^3$ is one-to-one.
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution 2.]
From Figure we see that no horizontal line intersects the graph of $f(x)=x^3$
more than once. Therefore, by the Horizontal Line Test, $f$ is one-to-one. \qedhere
\begin{figure}
\includegraphics[page=65,clip,trim=21mm 96mm 154mm 151mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}

\begin{frame}
\begin{example}
Is the function $g(x)=x^2$ one-to-one?
\end{example}
\begin{proof}[Solution 1.]
This function is not one-to-one because, for instance,
\[ g(1)=1=g(-1) \]
and so $1$ and $-1$ have the same output.
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution 2.]
From Figure we see that there are horizontal lines that intersect the
graph of $g$ more than once. Therefore, by the Horizontal Line Test, $g$ is not one-to-one. \qedhere
\begin{figure}
\includegraphics[page=65,clip,trim=21mm 49mm 154mm 200mm,scale=1.5]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}


%
%
\begin{frame}
\begin{definition}
Let $f$ be a one-to-one function with domain $A$ and range $B$.

Then its \textbf{inverse} function $f^{-1}$ has domain $B$ and range $A$ and is defined by
\[f^{-1}(y)=x \iff f(x)=y\]
for any $y$ in $B$.
\end{definition}
\end{frame}
%
%
%
\begin{frame}
\begin{table}[hhhhhh]
\begin{tabular}{l}
domain of $f^{-1}$  = range of $f$ \\
range of $f^{-1}$  = domain of $f$ .
\end{tabular}
\end{table}
  \begin{figure}[h]
  	\includegraphics[page=66,clip,trim=20.5mm 226mm 154.5mm 32mm,scale=2]{stewart-ccac.pdf}
  	%\caption{}\label{p66sekil5}
  \end{figure}
\end{frame}
%
%
%
\begin{frame}
For example, the inverse function of $f(x)=x^3$ is $f^{-1}(x)=x^{1/3}$ because if $y=x^3$, then
\[f^{-1}(y)=f^{-1}(x^3)=(x^3)^{1/3}=x\]
\begin{alertblock}{CAUTION:}
	Do not mistake the $-1$ in $f^{-1}$ for an exponent. Thus $f^{-1}$ does not mean $1/f$.
\end{alertblock}
\end{frame}
%
%
%
%
\begin{frame}
The letter $x$ is traditionally used as the independent variable, so when we concentrate
on $f^{-1}$ rather than on $f$, we usually reverse the roles of $x$ and $y$ and write
\begin{equation}\label{fonksiyonuntersi}
f^{-1}(x)=y\Leftrightarrow f(y)=x.
\end{equation}
By substituting for $y$ in Definition and substituting for $x$ in \eqref{fonksiyonuntersi}, we get the following \textbf{cancellation equations}:
\begin{align*}
f^{-1}(f(x))&=x && x\in A \\
f(f^{-1}(x))&=x && x\in B.
\end{align*}
\end{frame}
%
%
%
%
\begin{frame}
\begin{block}{How to Find the Inverse Function of a One-to-One Function}
	\begin{enumerate}
		\item Write $y=f(x)$.
		\item Solve this equation for $x$ in terms of $y$ (if possible).
		\item To express $f^{-1}$ as a function of $x$, interchange $x$ and $y$. The resulting equation is $y=f^{-1}(x)$.
	\end{enumerate}
\end{block}
\end{frame}
%
%
%
%
\begin{frame}
\begin{example}
Find the inverse function of $f(x)=x^3+2$.
\end{example}
\begin{proof}[Solution.]
According to steps in previous slide, we first write
\[y=x^3+2\]
Then we solve this equation for $x$:
\begin{align*}
x^3&=y-2\\
x&=\sqrt[3]{y-2}
\end{align*}
Finally, we interchange $x$ and $y$:
\[y=\sqrt[3]{x-2}.\]
Therefore, the inverse function is $f^{-1}(x)=\sqrt[3]{x-2}.$ 
\end{proof}
\end{frame}
%
%
%
%
%
%
%
\begin{frame}
The principle of interchanging $x$ and $y$ to find the inverse function also gives us the method for obtaining the graph of $f^{-1}$ from the graph of $f$.

\begin{minipage}{.42\textwidth}
	Since $f(a)=b$ if and only if $f^{-1}(b)=a$, the point $(a,b)$ is on the graph of $f$ if and only if the point $(b,a)$ is on the graph of $f^{-1}$. But we get the point $(b,a)$ from by reflecting about the line $y=x$.
\end{minipage}\hfil
\begin{minipage}{.4\textwidth}
	\begin{figure}[h]
		\includegraphics[page=68,clip,trim=74mm 198mm 92mm 45mm,scale=1.2]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\end{frame}
%
%
%
%
\begin{frame}
The graph of $f^{-1}$ is obtained by reflecting the graph of $f$ about the line $y=x$.

	\mode<beamer>{
		\only<1>{
			\begin{figure}[!h]
				\centering
				\includegraphics[scale=.6]{pictures/w2inverseF/w2_inverseF_1}
			\end{figure}
		}
		\only<2>{
			\begin{figure}[!h]
				\centering
				\animategraphics[scale=.6, autoplay,loop]{5}{pictures/w2inverseF/w2_inverseF_}{1}{21}
			\end{figure}
		}
		\only<3>{
			\begin{figure}[!h]
				\centering
				\includegraphics[scale=.6]{pictures/w2inverseF/w2_inverseF_23}
			\end{figure}
		}
	}
	\mode<handout>{
		\begin{figure}[!h]
			\centering
			\includegraphics[scale=.6]{pictures/w2inverseF/w2_inverseF_23}
		\end{figure}
	}
\end{frame}
%
%
%
%
\begin{frame}
\begin{example}
Sketch the graphs of $f(x)=\sqrt{-1-x}$ and its inverse function using the
same coordinate axes.
\end{example}
\begin{proof}[Solution] \let \qed \relax
	First we sketch the curve $y=\sqrt{-1-x}$ (the top half of the parabola
	$y^2=-1-x,$ , or $x=-y^2-1$) and then we reflect about the line $y=x$ to get the
	graph of $f^{-1}$.
	\begin{figure}[h]
		\includegraphics[page=68,clip,trim=21mm 133mm 145mm 106mm,scale=1]{stewart-ccac.pdf}
	\end{figure}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]
As a check on our graph, notice that the expression
for $f^{-1}$ is $f^{-1}(x)=-x^2-1$, $x>0$. So the graph of $f^{-1}$ is the right half of the
parabola $y=-x^2-1$ and this seems reasonable from Figure. \qedhere
\begin{figure}[h]
    	\includegraphics[page=68,clip,trim=21mm 133mm 145mm 106mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%
%
\subsection{Trigonometry}

%%_______________________________________________________________--
\subsubsection{Angles}
\begin{frame}{\insertsubsectionhead}{\insertsubsubsectionhead}
Angles can be measured in degrees or in radians (abbreviated as \textrm{rad}). The angle given by a complete revolution contains $360^\circ$, which is the same as $2\pi$ \textrm{rad}. Therefore
\begin{equation}\label{EkC-eqn-1}
\pi\; \textrm{rad}=180^\circ
\end{equation}
and 
\begin{equation}\label{pa18eqn2}
1\; \textrm{rad}=\left(\dfrac{180}{\pi}\right)^{\circ}\approx 57.3^{\circ} \quad 1^{\circ}=\dfrac{\pi}{180}\textrm{rad}\approx0.017\; \textrm{rad}
\end{equation}
\end{frame}

\begin{frame}
\begin{example}
\begin{itemize}
\item[(a)] Find the radian measure of $60^{\circ}$.
\item[(b)]Express $5\pi /4$ \textrm{rad} in degrees.
\end{itemize}
\end{example}

\begin{proof}[Solution.]
\begin{itemize}
\item[(a)] From Equation \eqref{EkC-eqn-1} or \eqref{pa18eqn2} we see that to convert from degrees to radians we multiply by $\pi/180$. Therefore
\[ 60^{\circ}=60\left(\dfrac{\pi}{180}\right)=\dfrac{\pi}{3}\; \textrm{rad} \]
\item[(b)] To convert from radians to degrees we multiply by $180/ \pi$. Thus
\[ \dfrac{5\pi}{4}\;\textrm{rad}=\dfrac{5\pi}{4}\left(\dfrac{180}{\pi}\right)=225^{\circ} \qedhere \]
\end{itemize}
\end{proof}
\end{frame}




%%_______________________________________________________________--
\begin{frame}
In calculus we use radians to measure angles except when otherwise indicated. The
following table gives the correspondence between degree and radian measures of
some common angles.\\
\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|}
		\hline
		Degrees & $0^\circ$ & $30^\circ$ & $45^\circ$ & $60^\circ$ & $90^\circ$ & $120^\circ$ & $135^\circ$  \\
		\hline
		Radians & 0 & $\frac{\pi}{6}$ & $\frac{\pi}{4}$ & $\frac{\pi}{3}$ & $\frac{\pi}{2}$ & $\frac{2\pi}{3}$ & $\frac{3\pi}{4}$  \\
		\hline
	\end{tabular}
\end{table}
\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		Degrees & $150^\circ$ & $180^\circ$ & $270^\circ$ & $360^\circ$ \\
		\hline
		Radians & $\frac{5\pi}{6}$ & $\pi$ & $\frac{3\pi}{2}$ & $2\pi$ \\
		\hline
	\end{tabular}
\end{table}
\end{frame}





%%_______________________________________________________________--
\begin{frame}
The \textbf{standard position} of an angle occurs when we place its vertex at the origin of
a coordinate system and its initial side on the positive $x-$axis as in Figure \ref{fig:a19-sekil3-4}.

\begin{minipage}{.5\textwidth}
	\begin{figure}[h]
		\caption{$\theta>0$}
		\includegraphics[page=1019,clip,trim=73.5mm 120mm 91mm 128mm,scale=.8]{stewart-ccac.pdf}
		\label{fig:a19-sekil3-4}
	\end{figure}
\end{minipage}
\begin{minipage}{.4\textwidth}
	A \textbf{positive} angle is obtained by rotating the initial side counterclockwise until it
	coincides with the terminal side.
\end{minipage}
\begin{minipage}{.5\textwidth}
	Likewise, \textbf{negative} angles are obtained by clockwise rotation as in Figure \ref{fig:a19-sekil4}.
\end{minipage}
\begin{minipage}{.4\textwidth}
	\begin{figure}[h]
		\caption{$\theta<0$}
		\includegraphics[page=1019,clip,trim=139mm 127mm 35mm 128mm]{stewart-ccac.pdf}
		\label{fig:a19-sekil4}
	\end{figure}
\end{minipage}
\end{frame}



%%_______________________________________________________________--
\begin{frame}
Figure shows several examples of angles in standard position. Notice that different angles can have the same terminal side.
\begin{figure}[h]
   \includegraphics[page=1019,clip,trim=59.5mm 28mm 135.5mm 229mm,scale=1.4]{stewart-ccac.pdf}\hfil
   \includegraphics[page=1019,clip,trim=87mm 28mm 108mm 229mm,scale=1.4]{stewart-ccac.pdf}\hfil
   \includegraphics[page=1019,clip,trim=114.5mm 28mm 80mm 229mm,scale=1.4]{stewart-ccac.pdf}
   
   \includegraphics[page=1019,clip,trim=142mm 28mm 52.5mm 229mm,scale=1.4]{stewart-ccac.pdf}\hfil
   \includegraphics[page=1019,clip,trim=170mm 28mm 25mm 229mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{frame}



%%_______________________________________________________________--
\begin{frame}
For instance, the angles  $3\pi /4,$ $-5\pi /4$, and $11\pi /4$ have the same initial and terminal sides because

\[ \frac{3\pi}{4}-2\pi=-\frac{5\pi}{4}\qquad \frac{3\pi}{4}+2\pi=\frac{11\pi}{4} \]
and $2\pi$ \textrm{rad} represents a complete revolution.
\end{frame}
%
\subsubsection{The Trigonometric Functions}
\begin{frame}{\insertsubsubsectionhead}
For an acute angle $\theta$ the six trigonometric functions are defined as ratios of lengths of
sides of a right triangle as follows

\begin{minipage}{.35\textwidth}
	\begin{figure}[h]
		\includegraphics[page=1020,clip,trim=19mm 220mm 155mm 36mm,scale=1.1]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\begin{minipage}{.64\textwidth}
	\begin{align}
	&\sin \theta =\frac{\text{opposite}}{\text{hypotenuse}} && \displaystyle \csc \theta =\frac{\text{hypotenuse}}{\text{opposite}}\nonumber\\
	&\cos \theta =\frac{\text{adjacent}}{\text{hypotenuse}} && \displaystyle \sec \theta =\frac{\text{hypotenuse}}{\text{adjacent}}\label{EkC-eqn-4}\\
	&\tan \theta =\frac{\text{opposite}}{\text{adjacent}}   && \displaystyle \cot \theta =\frac{\text{adjacent}}{\text{opposite}}\nonumber
	\end{align}
\end{minipage}
\end{frame}
%%_______________________________________________________________--
\begin{frame}
This definition does not apply to obtuse or negative angles, so for a general angle $\theta$
in standard position we let $P(x,y)$ be any point on the terminal side of $\theta$ and we let $r$ be the distance $|OP|$
as in Figure.
\begin{figure}[h]
   \includegraphics[page=1020,clip,trim=21mm 138mm 156mm 113mm,scale=1.3]{stewart-ccac.pdf}
\end{figure}
\end{frame}
%%_______________________________________________________________--
\begin{frame}
 Then we define
\begin{columns}[c]
\column{.6\textwidth}
\begin{equation}
\begin{array}{l}
\displaystyle \sin \theta =\frac{y}{r}\quad \displaystyle \cos \theta =\frac{x}{r}\quad \displaystyle \tan \theta =\frac{y}{x} \\ \\
\displaystyle \csc \theta =\frac{r}{y}\quad \displaystyle \sec \theta =\frac{r}{x}\quad \displaystyle \cot \theta =\frac{x}{y}
\end{array}
\label{EkC-eqn-5}
\end{equation}
\column{.4\textwidth}
\begin{figure}[h]
   \includegraphics[page=1020,clip,trim=21mm 138mm 156mm 113mm]{stewart-ccac.pdf}
\end{figure}
\end{columns}
\vspace{.3cm}
Since division by $ 0 $ is not defined, $\tan \theta$ and $\sec \theta$ are undefined when $x=0$ and $\csc \theta$ and  $\cot \theta$ are undefined when $y=0$. Notice that the definitions in \eqref{EkC-eqn-4} and \eqref{EkC-eqn-5}
are consistent when $\theta$ is an acute angle.
\end{frame}

\begin{frame}
\begin{example}
Find the exact trigonometric ratios for $\theta=2\pi/3$.
\end{example}
\begin{proof}[Solution]\let \qed \relax
\begin{figure}
\centering
\includegraphics[page=1021,clip,trim=24mm 174mm 150mm 74mm,scale=1.5]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution (cont.)]
From Figure we see that a point on the terminal line for $\theta=2\pi/3$ is $P(-1,\sqrt{3})$. Therefore, taking
\[ x=-1 \quad y=\sqrt{3} \quad r=2 \]
in the definitions of the trigonometric ratios, we have
\begin{align*}
\sin \dfrac{2\pi}{3}=\dfrac{\sqrt{3}}{2} && \quad \cos \dfrac{2\pi}{3}=-\dfrac{1}{2} && \quad \tan \dfrac{2\pi}{3}=-\sqrt{3}\\
\csc \dfrac{2\pi}{3}=\dfrac{2}{\sqrt{3}} && \quad \sec \dfrac{2\pi}{3}=-2 &&\quad \cot \dfrac{2\pi}{3}=-\dfrac{1}{\sqrt{3}}
\end{align*}
\end{proof}
\end{frame}
%%_______________________________________________________________--
\begin{frame}
\begin{alertblock}{Note}
	If $\theta$ is a number, the convention is that $\sin \theta$ means the sine of the angle whose \textbf{radian} measure is $\theta$.\medskip
	
	For example, the expression $\sin 3$ implies that we are dealing with an angle of $ 3 $ \textrm{rad}. When finding a calculator approximation to this number we must remember to set our calculator in radian mode, and then we obtain
	\[ \sin 3\approx 0.14112 \]
	If we want to know the sine of the angle $3^\circ$ we would write $\sin 3^\circ$ and, with our calculator in degree mode, we find that
	\[ \sin 3^\circ \approx 0.05234 \]
\end{alertblock}
\end{frame}



%%_______________________________________________________________--
\begin{frame}
The following table gives some values of $\sin \theta$ and $\cos \theta$.
\begin{figure}[h]
   \includegraphics[page=1021,clip,trim=67mm 125mm 20mm 119mm,scale=.9]{stewart-ccac.pdf}
\end{figure}
\end{frame}



%%_______________________________________________________________--
\subsubsection{Trigonometric Identities}
\begin{frame}{\insertsubsubsectionhead}
A trigonometric identity is a relationship among the trigonometric functions. The most elementary are the following, which are immediate consequences of the definitions of the trigonometric functions.
\[\csc \theta =\frac{1}{\sin \theta}\qquad \sec \theta =\frac{1}{\cos \theta}\qquad \cot \theta = \frac{1}{\tan \theta}\]
\[\tan \theta =\frac {\sin \theta}{\cos \theta}\qquad \cot \theta = \frac{\cos \theta}{\sin \theta}\]

\end{frame}
%%_______________________________________________________________--
\begin{frame}
\[\sin ^2 \theta +\cos ^2 \theta =1\]

\[\tan ^2 \theta + 1=\sec ^2 \theta\]

\[1+\cot ^2 \theta =\csc ^2 \theta \]

\[\sin  (-\theta)=-\sin \theta \]

\[\cos (-\theta)=\cos\theta\]

\[\sin(\theta+2\pi )=\sin\theta\qquad \cos (\theta+2\pi )=\cos \theta\]
\end{frame}
%%_______________________________________________________________--
\begin{frame}
The remaining trigonometric identities are all consequences of two basic identities called the \textbf{addition formulas}:
\[\sin(\alpha+\beta)=\sin\alpha\cos\beta+\cos\alpha\sin\beta\]
\[\cos(\alpha+\beta)=\cos\alpha\cos\beta-\sin\alpha\sin\beta.\]
\end{frame}

\begin{frame}
\[\sin(\alpha-\beta)=\sin\alpha\cos\beta-\cos\alpha\sin\beta\]

\[\cos(\alpha-\beta)=\cos\alpha\cos\beta+\sin\alpha\sin\beta\]

\[\tan(\alpha+\beta)=\frac{\tan\alpha+\tan\beta}{1-\tan\alpha\tan\beta}\]

\[\tan(\alpha-\beta)=\frac{\tan\alpha-\tan\beta}{1+\tan\alpha\tan\beta}\]
\end{frame}


%%_______________________________________________________________--
\begin{frame}
\[\sin2\alpha=2\sin\alpha\cos\alpha\]

\[\cos2\alpha=\cos^2\alpha-\sin^2\alpha\]

\[\cos2\alpha=2\cos^2\alpha-1\]

\[cos2\alpha=1-2\sin^2\alpha\]

\[\cos^2\alpha=\frac{1+\cos2\alpha}{2}\]

\[\sin^2\alpha=\frac{1-\cos2\alpha}{2}\]
\end{frame}



%%_______________________________________________________________--
\begin{frame}
\begin{example}
 Find all values of $x$ in the interval $[0,2\pi ]$ such that $\sin x= \sin 2x$
\end{example}
\begin{proof}[Solution.]
Using the double-angle formula, we rewrite the given equation as
\[\sin x=2\sin x\cos x \quad \text{ or } \quad \sin x(1-2 \cos x)=0\]
Therefore, there are two possibilities:
\[  \sin x=0 \quad \mbox{ or } \quad 1-2 \cos x=0 \]
\[  x=0,\pi,2\pi \quad \mbox{ or } \quad \cos x =\frac{1}{2}\Rightarrow x=\frac{\pi}{3},\frac{5\pi}{3} \]
The given equation has five solutions: $x=0,\pi,2\pi,\frac{\pi}{3},\frac{5\pi}{3}.$
\end{proof}
\end{frame}

\subsubsection{Graphs of the Trigonometric Functions}
\begin{frame}{\insertsubsubsectionhead}
The graph of the function $f(x)=\sin x$ is obtained by plotting points for $0\leq x\leq 2 \pi$ and then using the periodic nature of the function to complete the graph. Notice that the zeros of the sine function occur at the
integer multiples of $\pi$, that is,
\[ \sin x =0 \quad \text{whenever\;} \; x=n\pi, \;\; n\; \text{an integer} \]
Because of the identity
\[ \cos x=\sin \left(x+\dfrac{\pi}{2}\right) \]
the graph of cosine is obtained by shifting the graph of sine by an amount $\pi/2$ to the left. Note that for both the sine and cosine functions the domain is $-\infty,\infty$ and the range is the closed interval $[-1,1]$ . Thus, for all values of $x$, we have
\[ -1\leq \sin x\leq 1 \quad -1\leq \cos x \leq 1. \]
\end{frame}

\begin{frame}
\begin{figure}
\centering
\includegraphics[page=1024,clip,trim=20mm 72mm 114mm 184mm,scale=1.4]{stewart-ccac.pdf}\\
\includegraphics[page=1024,clip,trim=108mm 72mm 23mm 184mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
The graphs of the remaining four trigonometric functions are shown in following figures:
\end{frame}


\begin{frame}
\begin{figure}
\centering
\includegraphics[page=1025,clip,trim=76mm 198mm 88mm 32mm,scale=1.3]{stewart-ccac.pdf}
\caption{$ y=\tan x $}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\centering
\includegraphics[page=1025,clip,trim=139mm 198mm 23mm 32mm,scale=1.3]{stewart-ccac.pdf}
\caption{$ y=\cot x $}
\end{figure}
\end{frame}
\begin{frame}
\begin{figure}
\centering
\includegraphics[page=1025,clip,trim=76mm 142mm 90mm 88mm,scale=1.3]{stewart-ccac.pdf}
\caption{$ y=\csc x $}
\end{figure}
\end{frame}


\begin{frame}
\begin{figure}
\centering
\includegraphics[page=1025,clip,trim=139mm 142mm 23mm 88mm,scale=1.3]{stewart-ccac.pdf}
\caption{$ y=\sec x $}
\end{figure}
\end{frame}

\subsubsection{Inverse Trigonometric Functions}
\begin{frame}{\insertsubsubsectionhead}
When we try to find the inverse trigonometric functions, we have a slight difficulty.\medskip

Because the trigonometric functions are not one-to-one, they don’t have inverse functions. \medskip

The difficulty is overcome by restricting the domains of these functions so that they become one-to-one.
\end{frame}

\begin{frame}
You can see from Figure that the sine function $ y=\sin(x) $ is not one-to-one (use
the Horizontal Line Test).
\begin{figure}
\centering
\includegraphics[page=1025,clip,trim=19.5mm 55mm 99.5mm 198mm,scale=1.2]{stewart-ccac.pdf}
\end{figure}
\end{frame}

\begin{frame}
But the function $ f(x)=\sin(x),\quad -\pi/2\leq x \leq \pi/2 $, is one-to-one.
\begin{figure}
\centering
\includegraphics[page=1025,clip,trim=130mm 55mm 43.5mm 198mm,scale=1.4]{stewart-ccac.pdf}
\caption{}\label{fig:figure16}
\end{figure}
\begin{block}{}
	The inverse function of this restricted sine function $ f $ exists and is denoted by $ \sin^{-1} $or $ \arcsin $. 
	
	It is called the \textbf{inverse sine} function or the \textbf{arcsine} function.
\end{block}
\end{frame}

\begin{frame}
Since the definition of an inverse function says that
\[ f^{-1}(x)=y\quad \Leftrightarrow \quad f(y)=x \]
we have
\[ \sin^{-1}(x)=y \quad \Leftrightarrow \quad  \sin(y)=x \quad \text{and} \quad -\pi/2\leq y \leq \pi/2 \]
Thus, if $ -1\leq x \leq 1 $, $ \sin^{-1}x $ is the number between $ -\pi/2 $ and  $ \pi/2 $ whose sine is $ x $.
\end{frame}

\begin{frame}
\begin{example}
	Evaluate {\usebeamercolor[fg]{block title example}(a)} $ \sin^{-1}(1/2) $ and {\usebeamercolor[fg]{block title example}(b)} $ \tan(\arcsin(1/3)) $.
\end{example}
\begin{proof}[Solution]
	\begin{itemize}
		\item[(a)] Since $ \sin (\pi/6) = 1/2 $, and $ \pi/6 $ is in between $ -\pi/2 $ and $ \pi/2 $, we have
		\[ \sin^{-1}(1/2) = \frac{\pi}{6}. \]
		\item[(b)] 
		
		\begin{minipage}{.35\textwidth}
			\begin{figure}
				\centering
				\includegraphics[page=1026,clip,trim=19.5mm 165mm 151.5mm 92.5mm,scale=.7]{stewart-ccac.pdf}
			\end{figure}
		\end{minipage}
		\begin{minipage}{.55\textwidth}
			Let $ \theta = \arcsin(1/3) $. Then we can draw a right triangle with angle $ \theta $ as in Figure and deduce from the Pythagorean Theorem that the third side has length $ \sqrt{9-1} = 2\sqrt{2} $.
		\end{minipage}
		This enables us to read from the triangle that
		\[ \tan(\arcsin (1/3)) = \tan \theta = \frac{1}{2\sqrt{2}}. \qedhere \]
	\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
The inverse sine function, $\sin^{-1}  $, has domain $ [-1,1] $ and range $ [-\pi/2,\pi/2] $, and its
graph, shown in Figure \ref{fig:figure18}, is obtained from that of the restricted sine function (Figure
\ref{fig:figure16}) by reflection about the line $ y=x $.
\begin{figure}
\centering
\includegraphics[page=1026,clip,trim=73mm 65mm 104.5mm 174mm,scale=1.3]{stewart-ccac.pdf}
\caption{}
\label{fig:figure18}
\end{figure}
\end{frame}

\begin{frame}
The tangent function can be made one-to-one by restricting it to the interval
$ (-\pi/2,\pi/2) $. 
\begin{figure}
\centering
\includegraphics[page=1026,clip,trim=124mm 57mm 47mm 167.5mm,scale=1.2]{stewart-ccac.pdf}
\caption{}
\label{fig:figure19}
\end{figure}

\end{frame}

\begin{frame}
\begin{block}{}
	Thus, the \textbf{inverse tangent function} is defined as the inverse of the function $ f(x)=\tan(x) $, $ \pi/2<x<\pi/2 $. (See Figure \ref{fig:figure19}) It is denoted by $ \tan^{-1} $ or $ \arctan $.
\end{block}
\[ \tan^{-1}(x)=y  \quad \Leftrightarrow \quad  \tan(y)=x \quad \mbox{and} \quad -\pi/2<y<\pi/2\]
\end{frame}

\begin{frame}
The inverse tangent function, $ \tan^{-1}=\arctan $, has domain $ \mathds{R} $ and its range is
$ (-\pi/2,\pi/2) $. Its graph is shown in Figure.
\begin{figure}
\centering
\includegraphics[page=1027,clip,trim=21mm 119mm 144mm 125mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{frame}

\begin{frame}
\begin{example}
	Simplify the expression $ \cos(\tan^{-1}x) $.
\end{example}
\begin{proof}[Solution]
	Let $ y = \tan^{-1} x $. Then $ \tan y = x $ and $ -\pi/2 < y < \pi/2 $. We want to find $ \cos y $ but, since $ \tan y $ is known, it is easier to find $ \sec y $ first:
	\begin{align*}
	\sec^2 y &= 1 + \tan^2 y = 1 + x^2\\
	\sec y &= \sqrt{1+x^2} \; (\text{Since } \sec y > 0 \text{ for } -\pi/2 < y < \pi/2)
	\end{align*}
	Thus
	\[ \cos (\tan^{-1} x) = \cos y = \frac{1}{\sec y} = \frac{1}{\sqrt{1+x^2}}.\qedhere \]
\end{proof}
\end{frame}
%
\subsection{Exponential Functions}
\begin{frame}{\insertsubsectionhead}
\begin{block}{}
	The function $f(x)=2^x$ is called an \textbf{exponential function} because the variable, $x$, is the exponent. 
\end{block}
\begin{alertblock}{}
	It should not be confused with the power function $g(x)=x^2$, in which the variable is the base.
\end{alertblock}
\begin{block}{}
	In general, an \textbf{exponential function} is a function of the form 
	\[ f(x)=a^x \]
	where $a$ is a positive constant.
\end{block}
\end{frame}


\begin{frame}
The graphs of $y=2^x$ and $y=(0.5)^x$ are shown in Figure below. In both cases the domain
is $(-\infty,\infty)$ and the range is $(0,\infty)$.
  \begin{figure}[h]
  	\includegraphics[page=34,clip,trim=93mm 46mm 41mm 194mm,scale=1.2]{stewart-ccac.pdf}
  \end{figure}
\end{frame}
%
%
\begin{frame}
	\centering
  \begin{figure}[h]
  	\mode<beamer>{
  		\only<1>{
  			\begin{figure}[!h]
  				\centering
  				\includegraphics[scale=.6]{pictures/w2powerFunctions/w2_powerfs_1}
  			\end{figure}
  		}
  		\only<2>{
  			\begin{figure}[!h]
  				\centering
  				\animategraphics[scale=.6, autoplay,loop]{5}{pictures/w2powerFunctions/w2_powerfs_}{1}{105}
  			\end{figure}
  		}
  		\only<3>{
  			\begin{figure}[!h]
  				\centering
  				\includegraphics[scale=.6]{pictures/w2powerFunctions/w2_powerfs_106}
  			\end{figure}
  		}
  	}
  	\mode<handout>{
  		\begin{figure}[!h]
  			\centering
  			\includegraphics[scale=.6]{pictures/w2powerFunctions/w2_powerfs_106}
  		\end{figure}
  	}
  \end{figure}
\end{frame}

\begin{frame}
	\begin{block}{Laws of Exponent}
		If $a$ and $b$ are positive numbers and $x$ and $y$ are any real numbers, then
		\begin{enumerate}
			\item $a^{x+y}=a^{x}a^{y}$.
			\item $a^{x-y}=\dfrac{a^{x}}{a^{y}}$.
			\item $(a^{x})^{y}=a^{xy}$.
			\item $(ab)^{x}=a^{x}b^{x}$.
		\end{enumerate}
		\end{block}
\end{frame}

\begin{frame}
\begin{example}
Sketch the graph of the function $y=3-2^x$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
First we reflect the graph of $y=2^x$ about the $x-$axis to get the graph of $y=-2^x$. Then we shift the graph of $y=-2^x$ upward three units to obtain the graph of $y=3-2^x$.
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
\centering
\mode<beamer>{
	\only<1>{
		\begin{figure}[!h]
			\includegraphics[scale=.5]{pictures/w2ex12/w2_ex12_1}
		\end{figure}
	}
	\only<2>{
		\begin{figure}[!h]
			\animategraphics[scale=.5, autoplay,loop]{5}{pictures/w2ex12/w2_ex12_}{1}{44}
		\end{figure}
	}
	\only<3>{
		\begin{figure}[!h]
			\includegraphics[scale=.5]{pictures/w2ex12/w2_ex12_44}
		\end{figure}
	}
}
\mode<handout>{
	\begin{figure}[!h]
		\includegraphics[scale=.5]{pictures/w2ex12/w2_ex12_44}
	\end{figure}
}
\end{proof}


\end{frame}
\subsubsection{The Number $e$}
\begin{frame}{\insertsubsubsectionhead}
The choice of a base $a$ is influenced by the way the graph of $y=a^x$ crosses the $y-$axis. Following Figure shows the tangent lines to the graphs of $y=2^x$ and $y=3^x$ at the point $(0,1)$. 
\begin{figure}
\centering
\includegraphics[page=61,clip,trim=74mm 34mm 101mm 204mm,scale=1.15]{stewart-ccac.pdf}\hfil
\includegraphics[page=61,clip,trim=132mm 34mm 44mm 204mm,scale=1.15]{stewart-ccac.pdf}
\end{figure}
If we measure the slopes of these tangent lines, we find that $m\approx 0.7$ for $y=2^x$ and $m\approx 1.1$ for $y=3^x$.
\end{frame}

\begin{frame}
Some of the formulas of calculus will be greatly simplified if we choose the base a so that the slope of the tangent line to $y=a^x$ at $(0,1)$ is exactly $1$.
\begin{figure}
\centering
\includegraphics[page=62,clip,trim=20mm 182mm 155mm 57mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}

\end{frame}

\begin{frame}
In fact, there is such a number and it is denoted by the letter $e$.\footnote{This notation was chosen by the Swiss mathematician Leonhard Euler in 1727, probably because it is the first letter of the word \textbf{exponential}.} it comes as no surprise that the number $e$ lies between $2$ and $3$ and the graph of $y=e^x$ lies between the graphs of $y=2^x$ and $y=3^x$.
\begin{figure}
\centering
\includegraphics[page=62,clip,trim=20mm 182mm 155mm 57mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{frame}

\begin{frame}
\begin{example}
Graph the function $y=\dfrac{1}{2}e^{-x}-1$ and state the domain and range.
\end{example}
\begin{proof}[Solution]\let\qed\relax
We start with the graph of $y=e^x$ and reflect about the $y-$axis to get the graph of $y=e^{-x}$. Then we compress the graph vertically by a factor of $2$ to obtain the graph of $y=\dfrac{1}{2}e^{-x}$. Finally, we shift the graph downward one unit to get the desired graph. The domain is $ \mathds{R} $ and the range is $ (-1, \infty) $. 
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
\mode<beamer>{
	\only<1>{
		\begin{figure}[!h]
			\includegraphics[scale=.5]{pictures/w2ex13/w2ex13_1}
		\end{figure}
	}
	\only<2>{
		\begin{figure}[!h]
			\animategraphics[scale=.5, autoplay,loop]{5}{pictures/w2ex13/w2ex13_}{1}{66}
		\end{figure}
	}
	\only<3>{
		\begin{figure}[!h]
			\includegraphics[scale=.5]{pictures/w2ex13/w2ex13_66}
		\end{figure}
	}
}
\mode<handout>{
	\begin{figure}[!h]
		\includegraphics[scale=.5]{pictures/w2ex13/w2ex13_66}
	\end{figure}
}
\end{proof}
\end{frame}
%
\subsubsection{Logarithmic Functions}
\begin{frame}{\insertsubsubsectionhead}
If $a>0$ and $a\neq 1$, the exponential function $f(x)=a^x$ is either increasing or decreasing and so it is one-to-one by the \textbf{Horizontal Line Test}.
\begin{block}{}
	It therefore has an inverse function $f^{-1}$, which is called the \textbf{logarithmic function with base} a and is denoted by $\log _a$.
\end{block}
\end{frame}
%
%
%
%
\begin{frame}
\begin{block}{}
If we use the formulation of an inverse function
\[f^{-1}(x)=y\iff f(y)=x\]
then we have
\[\log _ax=y\iff a^y=x.\]
\end{block}
Thus, if $0<x$, then $\log_ax,\,a$ is the exponent to which the base must be raised to give $x$.\medskip

For example, $\log_{10}0.001=-3$ because $10^{-3}=0,001$.
\end{frame}
%
%
%
%
\begin{frame}
\begin{block}{}
	The cancellation equations, when applied to $f(x)=a^x$ and $f^{-1}(x)=\log_ax$
	become
	\[\log_a(a^x)=x\,,\quad x\in \mathds{R}\]
	\[a^{\log_a x}=x\,,\quad x>0.\]
\end{block}
In particular, if we set $ x=1 $, we get
\[ \log_a (a) = 1. \]
\end{frame}
%
%
\begin{frame}
The logarithmic function $\log_a x$ has domain $(0,\infty)$ and range $\mathds{R}$. Its graph is the
reflection of the graph of $y=a^x$ about the line $y=x$.
   \begin{figure}[h]
     	\includegraphics[page=69,clip,trim=21mm 206mm 148mm 31mm,scale=1.4]{stewart-ccac.pdf}
     \end{figure}
Figure shows the case where $1<a$. (The most important logarithmic functions have base $a>1$.)
\end{frame}
%
%
\begin{frame}
\begin{minipage}{.6\textwidth}
	\mode<beamer>{
		\only<1>{
			\begin{figure}[!h]
				\includegraphics[scale=.5]{pictures/w2logFunctions/w2_log_1}
			\end{figure}
		}
		\only<2>{
			\begin{figure}[!h]
				\animategraphics[scale=.5, autoplay,loop]{5}{pictures/w2logFunctions/w2_log_}{1}{34}
			\end{figure}
		}
		\only<3>{
			\begin{figure}[!h]
				\includegraphics[scale=.5]{pictures/w2logFunctions/w2_log_34}
			\end{figure}
		}
	}
	\mode<handout>{
		\begin{figure}[!h]
			\includegraphics[scale=.5]{pictures/w2logFunctions/w2_log_34}
		\end{figure}
	}
\end{minipage}
\begin{minipage}{.35\textwidth}
	The fact that $y=a^x$ is a very rapidly increasing function for
	$0<x$ is reflected in the fact that $y=\log_ax$ is a very slowly increasing function for $1<x$.
\end{minipage}
Figure shows the graphs of $y=\log_ax$ with various values of the base $a$.

Since $\log_a1=0$, the graphs of all logarithmic functions pass through the point $(1,0)$.
\end{frame}
%
%
\begin{frame}
\begin{block}{Laws of Logarithms}
	If $x$ and $y$ are positive numbers, then
	\begin{enumerate}
		\item $\displaystyle \log_a(xy)=\log_ax+\log_ay$
		\item $\displaystyle \log_a\left(\frac{x}{y}\right)=\log_ax-\log_ay$
		\item $\displaystyle \log_a\left(x^r\right)=r\log_ax\quad(\text{where $r$ is any real number.})$
	\end{enumerate}
\end{block}
\end{frame}
%
%
%
\begin{frame}
\begin{example}
Use the laws of logarithms to evaluate $\log_2 80-\log_2 5$.
\end{example}
\begin{proof}[Solution.]
Using Law {\usebeamercolor[fg]{structure}2}, we have
\[ \log_2 80-\log_2 5=\log_2 \left(\dfrac{80}{5}\right)=\log_2 16=4 \]
because $2^4=16.$
\end{proof}
\end{frame}
%
\subsubsection{Natural Logarithm}
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{}
	The logarithm with base $e$ is called the \textbf{natural logarithm} and has a special notation:
	\[\log_e x=\ln x\]
\end{block}
The defining properties of the natural logarithm function are
\begin{equation}\label{p69eqn8}
\ln x=y\Longleftrightarrow e^y=x
\end{equation}
\[\ln (e^x)=x\qquad x\in\mathds{R}\]
\begin{equation}\label{p69eqn9}
e^{\ln x}=x\qquad x>0.
\end{equation}
\end{frame}
%
%
%
%
\begin{frame}
In particular, if we set $x=1$, we get
\[\ln e=1.\]
\begin{block}{}
	For any positive number $a$, we have
	\[\log _ax=\frac{\ln x}{\ln a},\qquad a>0,\,a\neq 1.\]
\end{block}
\end{frame}
%
%
\begin{frame}
\begin{example}
Find $x$ if $\ln x=5$.
\end{example}
\begin{proof}[Solution 1.]
From equation \eqref{p69eqn8} we see that
\[ \ln x=5 \quad \text{means} \quad e^5=x \]
Therefore, $x=e^5$.
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution 2.]
Start with the equation 
\[ \ln x=5 \]
and apply the exponential function to both sides of the equation:
\[ e^{\ln x}=e^5 \]
But the second cancellation in equation \eqref{p69eqn9} says that $e^{\ln x}=x$. Therefore, $x=e^5$.
\end{proof}
\end{frame}
%

\begin{frame}
\begin{example}
Solve the equation  $e^{5-3x}=10$.
\end{example}

\begin{proof}[Solution.]
We take natural logarithms of both sides of the equation and use equation \eqref{p69eqn9}:
\begin{align*}
\ln (e^{5-3x})&=\ln 10\\
5-3x&=\ln 10\\
3x&=5-\ln 10\\
x&= \dfrac{1}{3}(5-\ln 10)
\end{align*}
Since the natural logarithm is found on scientific calculators, we can approximate
the solution to four decimal places: $x\approx0.8991$.
\end{proof}
\end{frame}


\begin{frame}
\begin{example}
Express $\ln a+\dfrac{1}{2} \ln b$ as a single logarithm.
\end{example}
\begin{proof}[Solution.]
Using Laws {\usebeamercolor[fg]{structure}3} and {\usebeamercolor[fg]{structure}1} of logarithms, we have
\begin{align*}
\ln a+\dfrac{1}{2} \ln b&= \ln a + \ln b^{1/2}\\
&= \ln a +\ln \sqrt{b}\\
&= \ln (a\sqrt{b}). \qedhere
\end{align*}
\end{proof}
\end{frame}
%
\begin{frame}
The graphs of the exponential function $y=e^x$ and its inverse function, the natural logarithm function, are shown in Figure \ref{p71sekil13}. Because the curve $y=e^x$ crosses the $y-$axis with a slope of $ 1 $, it follows that the reflected curve $y=\ln x$ crosses the $x$-axis with a slope of $ 1 $.
      \begin{figure}[h]
      	\includegraphics[page=71,clip,trim=21mm 39mm 147mm 194mm,scale=1.2]{stewart-ccac.pdf}
      	\caption{}\label{p71sekil13}
      \end{figure}
\end{frame}
%
%
%
\begin{frame}
\begin{example}
Sketch the graph of the function $y=\ln (x-2)-1$.
\end{example}
\begin{proof}[Solution]
	\begin{minipage}{.59\textwidth}
		\mode<beamer>{
			\only<1>{
				\begin{figure}[!h]
					\includegraphics[scale=.5]{pictures/w2ex18/w2ex18_1}
				\end{figure}
			}
			\only<2>{
				\begin{figure}[!h]
					\animategraphics[scale=.5, autoplay,loop]{5}{pictures/w2ex18/w2ex18_}{1}{44}
				\end{figure}
			}
			\only<3>{
				\begin{figure}[!h]
					\includegraphics[scale=.5]{pictures/w2ex18/w2ex18_44}
				\end{figure}
			}
		}
		\mode<handout>{
			\begin{figure}[!h]
				\includegraphics[scale=.5]{pictures/w2ex18/w2ex18_44}
			\end{figure}
		}
	\end{minipage}
	\begin{minipage}{.35\textwidth}
		We start with the graph of $y=\ln x$ as given in Figure \ref{p71sekil13}. Using the
		transformations, we shift it two units to the right to get the graph
		of $y=\ln (x-2)$ and then we shift it one unit downward to get the graph of
		$y=\ln (x-2)-1$. \qedhere
	\end{minipage}
\end{proof}
\end{frame}
%
%
\begin{frame}
Although $\ln x$ is an increasing function, it grows very slowly when $1<x$. In fact, $\ln x$ grows more slowly than any positive power of $x$. To illustrate this fact, we compare the graphs of the functions $y=\ln x$ and $y=x^{1/2}=\sqrt{x}$ in Figure. You can see that initially the
graphs of and grow at comparable rates, but eventually the root function
far surpasses the logarithm.
\mode<beamer>{
	\only<1>{
		\begin{figure}[!h]
			\includegraphics[scale=.6]{pictures/w2rootVSln/w2rootVSln_1}
		\end{figure}
	}
	\only<2>{
		\begin{figure}[!h]
			\animategraphics[scale=.6, autoplay,loop]{2}{pictures/w2rootVSln/w2rootVSln_}{1}{13}
		\end{figure}
	}
	\only<3>{
		\begin{figure}[!h]
			\includegraphics[scale=.6]{pictures/w2rootVSln/w2rootVSln_13}
		\end{figure}
	}
}
\mode<handout>{
	\begin{figure}[!h]
		\includegraphics[scale=.39]{pictures/w2rootVSln/w2rootVSln_1}
		\includegraphics[scale=.39]{pictures/w2rootVSln/w2rootVSln_13}
	\end{figure}
}
\end{frame}