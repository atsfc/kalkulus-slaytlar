\section{Integral}
\subsection{The Substitution Rule}

\begin{frame}{\insertsubsectionhead}
\begin{definition}
If $u=g(x)$ is a differentiable function whose range is
an interval $I$ and $f$ is continuous on $I$, then
 \begin{equation}\label{5.5.eqn4}
 \int f(g(x))\;g'(x)\;dx=\int f(u)\; du.
 \end{equation}
\end{definition}
\end{frame}
%
%% ---------------------------------112
%
%
\begin{frame}
\begin{example}
 Find $\displaystyle \int x^3\cos (x^4+2)\;dx$.
\end{example}
\begin{proof}[Solution]
 We make the substitution $u=x^4+2$ because its differential is $du=4x^3 dx$, which, apart from the constant factor $4$, occurs in the integral. Thus,
 using $x^3 dx=du/4$ and the Substitution Rule, we have
\begin{align*}
\displaystyle \int x^3\cos (x^4+2)\;dx&\displaystyle =\int \cos u\cdot \frac{1}{4}du=\frac{1}{4}\int \cos u\;du\\
&\displaystyle =\frac{1}{4}\sin u +C\\
&\displaystyle=\frac{1}{4}\sin (x^4+2)+C
\end{align*}
Notice that at the final stage we had to return to the original variable $x$.
\end{proof}
\end{frame}
%
%
%% ---------------------------------113
%
\begin{frame}
\begin{block}{}
	The idea behind the Substitution Rule is to replace a relatively complicated integral by a simpler integral. This is accomplished by changing from the original variable $x$ to a new variable $u$ that is a function of $x$.
\end{block}
\end{frame}
%
%
%% ---------------------------------114
%
\begin{frame}

\begin{example}
Evaluate $\displaystyle\int \sqrt{2x+1}\;dx$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
Let $u=2x+1$. Then $du=2 dx,$ so $dx=du/2$. Thus, the Substitution Rule gives
\begin{align*}
\int \sqrt{2x+1}\;dx&\displaystyle=\int \sqrt{u}\;\frac{du}{2}=\frac{1}{2}\int u^{1/2}\;du\\
&=\frac{1}{2}\cdot \frac{u^{3/2}}{3/2}+C=\frac{1}{3}u^{3/2}+C\\
&=\frac{1}{3}(2x+1)^{3/2}+C.
\end{align*}
\end{proof}
\end{frame}
%
%
%% ---------------------------------115
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Another possible substitution is $u=\sqrt{2x+1}$. Then $\displaystyle du=\frac{dx}{\sqrt{2x+1}}$ so $dx=\sqrt{2x+1}\;du=u\;du$. (Or observe that $u^2=2x+1,$ so $2u\;du=2\;dx$.) Therefore
\begin{align*}
\int \sqrt{2x+1}\;dx&\displaystyle=\int u\cdot u\;du=\int u^2\;du\\
&=\frac{u^3}{3}+C\\
&=\frac{1}{3}(2x+1)^{3/2}+C. \qedhere
\end{align*}
\end{proof}
\end{frame}
%
%% ---------------------------------116
%
%
\begin{frame}
\begin{example}
 Find $\displaystyle \int \frac{x}{\sqrt{1-4x^2}}\;dx$.
\end{example}
\begin{proof}[Solution]
Let $u=1-4x^2$. Then $du=-8x\;dx$, so $x\;dx=-\frac{1}{8}du$ and
\begin{align*}
\int\frac{x}{\sqrt{1-4x^2}}\;dx&\displaystyle=-\frac{1}{8}\int \frac{1}{\sqrt{u}}\;du=-\frac{1}{8}\int u^{-1/2}\;du\\
&=-\frac{1}{8}\left(2\sqrt{u}\right)+C=-\frac{1}{4}\sqrt{1-4x^2}+C. \qedhere
\end{align*}
\end{proof} 
\end{frame}
%
%
%% ---------------------------------117
%
\begin{frame}
\begin{example}
 Calculate $\displaystyle \int e^{5x}\;dx$.
\end{example}
\begin{proof}[Solution]
If we let $u=5x$, then $du=5\;dx,$ so $dx=\frac{1}{5}du$. Therefore
 \[ \int e^{5x}\;dx=\frac{1}{5}\int e^u\;du=\frac{1}{5}e^u+C=\frac{1}{5}e^{5x}+C. \qedhere\]
\end{proof}
\end{frame}
%
%
%% ---------------------------------118
%
\begin{frame}
\begin{example}
Calculate $\displaystyle\int \tan x\, dx$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
First we write tangent in terms of sine and cosine:
 \[ \displaystyle \int \tan x\;dx=\int \frac{\sin x}{\cos x}\;dx.\]
This suggests that we should substitute $u=\cos x$, since then $du=-\sin x\;dx$ and so $\sin x \;dx=-du$:
\begin{align*}
\int \tan x\;dx&\displaystyle=\int \frac{\sin x}{\cos x}\;dx=-\int \frac{1}{u}\;du\\
&=-\ln |u|+C=-\ln |\cos x|+C.
\end{align*}
\end{proof}
\end{frame}
%
%
%% ---------------------------------119
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Since $-\ln |\cos x|=\ln \left(|\cos x|^{-1}\right)=\ln \left(1/|\cos x|\right)=\ln |\sec x|$, the result of
example can also be written as
\[ \displaystyle\int \tan x\;dx=\ln |\sec x|+C. \qedhere\]
\end{proof}
\end{frame}
%
%
%% ---------------------------------120
%
\subsubsection{Definite Integrals}
\begin{frame}{\insertsubsubsectionhead}
\begin{alertblock}{The Substitution Rule for Definite Integrals}
If $g'$ is continuous on $[a,b]$ and $f$ is continuous on the range of $u=g(x)$, then
\begin{equation}\label{5.5.eqn5}
\int _a^bf(g(x))g'(x)\;dx=\int _{g(a)}^{g(b)}f(u)\;du.
\end{equation}
\end{alertblock}
\end{frame}
%
%
%% ---------------------------------121
%
\begin{frame}
\begin{example}
Evaluate  $\displaystyle \int _0^4\sqrt{2x+1}\,dx$ using Equation \eqref{5.5.eqn5}.
\end{example}

\begin{proof}[Solution]\let\qed\relax
 Using the substitution $u=2x+1$, we get $dx=du/2$. To find the new limits of integration we note that
 \[ x=0\Rightarrow u=2\cdot 0+1=1\quad\text{ and }\quad x=4\Rightarrow u=2\cdot 4+1=9. \]
\end{proof}
\end{frame}
%
%
%% ---------------------------------122
%
\begin{frame}
\begin{proof}[Solution (cont.)]
	Therefore
	\begin{align*}
	\int_0^4 \sqrt{2x+1}\,dx& =\int _1^9\frac{1}{2}\sqrt{u}\,du =\left.\frac{1}{2}\cdot \frac{2}{3}u^{3/2}\right]_1^9\\
	&=\frac{1}{3}(9^{3/2}-1^{3/2})=\frac{26}{3}.
	\end{align*}
Observe that when using \eqref{5.5.eqn5} we do not return to the variable $x$ after integrating. We
simply evaluate the expression in $u$ between the appropriate values of $u$.
\end{proof}
\end{frame}
%
%
%% ---------------------------------123
%
\begin{frame}
\begin{example}
 Evaluate $\displaystyle \int _1^2\frac{dx}{(3-5x)^2}$.
\end{example}
\begin{proof}[Solution]
Let $u=3-5x$. Then $du=-5 dx$, so $dx=-du/5$. When $x=1$, $u=-2$ and when $x=2$, $u=-7$. Thus
\begin{align*}
\int _1^2\frac{dx}{(3-5x)^2}&=-\frac{1}{5}\int _{-2}^{-7}\frac{du}{u^2}\\
&=-\frac{1}{5}\left[-\frac{1}{u}\right]_{-2}^{-7}=\left.\frac{1}{5u}\right]_{-2}^{-7}\\
&=\frac{1}{5}\left(-\frac{1}{7}+\frac{1}{2}\right)=\frac{1}{14}. \qedhere
\end{align*}
\end{proof}
\end{frame}+
%
%% ---------------------------------124
%
%
\begin{frame}
\begin{example}
 Calculate $\displaystyle \int _1^e\frac{\ln x}{x}\,dx$.
\end{example}
\begin{proof}[Solution]
We let $u=\ln x$ because its differential $du=dx/x$ occurs in the integral. $x=1$, $u=\ln 1=0;$ when $x=e$, $u=\ln e =1$. Thus
\[ \displaystyle \int _1^e\frac{\ln x}{x}\,dx=\int _0^1u\,du=\left.\frac{u^2}{2}\right]_0^1=\frac{1}{2}. \qedhere \]
\end{proof}
\end{frame}
%
%
%% ---------------------------------125
%
\subsubsection{Symmetry}
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{Integrals of Symmetric Functions}
Suppose $f$ is continuous on $[-a,a]$.
 \begin{itemize}
 \item[(a)] If $f$ is even $\displaystyle [f(-x)=f(x)], \text{ then }\int _{-a}^af(x)\,dx=2\int _0^af(x)\,dx$.
 \item[(b)] If $f$ is odd $\displaystyle [f(-x)=-f(x)],\text{ then }\int _{-a}^af(x)\,dx=0$.
 \end{itemize}
\end{block}
\end{frame}
%
%
%% ---------------------------------126
%
\begin{frame}
\begin{example}
Since $f(x)=x^6+1$ satisfies $f(-x)=f(x)$, it is even and so
\begin{align*}
\int _{-2}^2(x^6+1)\,dx&\displaystyle=2\int _0^2(x^6+1)\,dx\\
&=2\left[\frac{1}{7}x^7+x\right]_0^2=\left(\frac{128}{7}+2\right)=\frac{284}{7}.
\end{align*}
\end{example}
\end{frame}
%
%% ---------------------------------127
%
%
\begin{frame}
\begin{example}
Since $\displaystyle f(x)=\frac{\tan x}{1+x^2+x^4}$ satisfies $f(-x)=-f(x),$ it is odd so
\[ \int _{-1}^1 \frac{\tan x}{1+x^2+x^4}\,dx=0. \]
\end{example} 
\end{frame}
%
%
%% ---------------------------------128
%
\subsection{Integration by Parts}
\begin{frame}{\insertsubsectionhead}
\begin{block}{}
	\begin{equation}\label{5.6.eqn1}
	\int f(x)g'(x)\,dx=f(x)g(x)-\int g(x)f'(x)\,dx
	\end{equation}
	Formula \eqref{5.6.eqn1} is called the \textbf{formula for integration by parts}.
\end{block}
It is perhaps easier to remember in the following notation. Let $u=f(x)$ and $v=g(x)$. Then the differentials are $dv=g'(x)dx$ and $du=f'(x)dx$, so, by the Substitution Rule, the formula for integration by parts becomes
\begin{equation}\label{5.6.eqn2}
\int u dv=uv-\int v du.
\end{equation}
\end{frame}
%
%
%% ---------------------------------129
%
\begin{frame}
\begin{example}
 Find $\displaystyle \int x\sin x\,dx$.
\end{example}
\begin{proof}[Solution]
Suppose we choose $u=x$ and $dv=\sin x dx$. Then $du=dx$ and $v=-\cos x$.
\begin{align*}
\int x\sin x\,dx&\displaystyle=x(-\cos x)-\int (-\cos x)\,dx\\
&=-x\cos x+\int \cos x\, dx\\
&=-x\cos x+\sin x+C. \qedhere
\end{align*}
\end{proof}
\end{frame}
%
%% ---------------------------------130
%
%
\begin{frame}
\begin{example}
Evaluate $\displaystyle \int \ln x\,dx$.
\end{example}
\begin{proof}[Solution]
Here $u=\ln x,\, dv=dx$, then $\displaystyle du=\frac{1}{x}dx,\,v=x$. Integrating by parts, we get
\begin{align*}
\int \ln x\,dx&\displaystyle=x\ln x-\int x\frac{dx}{x}\\
&=x\ln x-\int dx\\
&=x\ln x-x+C.
\end{align*}
Integration by parts is effective in this example because the derivative of the
function $f(x)=\ln x$ is simpler than $f$.
\end{proof}
\end{frame}
%
%
%% ---------------------------------131
%
\begin{frame}
\begin{example}
Find $\displaystyle \int x^2e^x\,dx$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
Notice that $x^2$ becomes simpler when differentiated, so we have $u=x^2,\,dv=e^x dx$. Then $du=2x dx,\,v=e^x$. Integration by parts gives
\[\int x^2e^x\,dx=x^2e^x-2\int xe^x\,dx.\]
\end{proof}
\end{frame}
%
%
%% ---------------------------------132
%
\begin{frame}
\begin{proof}[Solution (cont.)]
The integral that we obtained, $\int xe^x\,dx$, is simpler than the original integral but is still not obvious. Therefore, we use integration by parts a second time, this time with $u=x$ and $dv=e^x dx$. Then $du=dx,\,v=e^x$, and
\[ \int xe^x\,dx=xe^x-\int e^x\,dx=xe^x-e^x+C. \]
Putting this in equation above, we get
\[\int x^2e^x\,dx=x^2e^x-2\int xe^x\,dx=x^2e^x-2xe^x+2e^x+C_1,\]
where  $C_1=-2C$.
\end{proof}
\end{frame}
%
%
%% ---------------------------------133
%
\begin{frame}
\begin{example}
 Evaluate $\displaystyle \int e^x \sin x\,dx$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
Neither $e^x$ nor $\sin x$ become simpler when differentiated, but we try choosing $u=e^x$ and $dv=\sin x\,dx$ anyway. Then $du=e^x dx$ and $v=-\cos x$, so integration by parts gives
\begin{equation}\label{5.6.eqn4}
\int e^x\sin x\,dx=-e^x\cos x\,dx+\int e^x\cos x\,dx.
\end{equation}
\end{proof}
\end{frame}
%
%
%% ---------------------------------134
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
The integral that we have obtained, $\int e^x\cos x\,dx$, is no simpler than the original one. This time we use $u=e^x$ and $dv=\cos x\,dx$. Then $du=e^x\,dx$, $v=\sin x$, and
\begin{equation}\label{5.6.eqn5}
\int e^x\cos x\,dx=e^x\sin x-\int e^x\sin x\,dx
\end{equation}
However, if we put Equation \eqref{5.6.eqn5} into Equation \eqref{5.6.eqn4} we get
\[ \int e^x\sin x\,dx=-e^x\cos x+e^x\sin x-\int e^x\sin x\,dx. \qedhere \]
\end{proof}
\end{frame}
%
%
%% ---------------------------------135
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Adding $\displaystyle\int e^x \sin x\,dx$ to each side of the equation, we obtain
 \[2\int e^x\sin x\,dx=-e^x\cos x+e^x\sin x.\]
Dividing by $ 2 $ and adding the constant of integration, we get
 \[ \int e^x\sin x\,dx=\frac{1}{2}e^x(\sin x-\cos x)+C. \qedhere \]
\end{proof}
\end{frame}
%
%% ---------------------------------136
%
%
\begin{frame}
\begin{block}{Integration by Parts With the Evaluation Theorem}
	If we combine the formula for integration by parts with the Evaluation Theorem, we can evaluate definite integrals by parts. Evaluating both sides of Formula \ref{5.6.eqn1} between $a$ and $b$, assuming  $f'$ and $g'$ are continuous, and using the Evaluation Theorem, we obtain
	\begin{equation}\label{5.6.eqn6}
	\int _a^bf(x)g'(x)\,dx=f(x)g(x)\Big]_a^b-\int _a^bg(x)f'(x)\,dx.
	\end{equation}
\end{block}
\end{frame}
%
%% ---------------------------------137
%
%
\begin{frame}
\begin{example}
Calculate $\displaystyle \int_0^1\tan ^{-1}x\,dx$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
Let $u=\tan ^{-1}x,\,dv=dx$. Then $\displaystyle du=\frac{dx}{1+x^2},\,v=x$. So Formula \eqref{5.6.eqn6} gives
\begin{align*}
\int_0^1\tan ^{-1}x\,dx&=x\tan^{-1}x\Big]_0^1-\int_0^1\frac{x}{1+x^2}dx\\
&=1\cdot \tan ^{-1}1-0\cdot \tan ^{-1}0-\int_0^1\frac{x}{1+x^2}dx\\
&=\frac{\pi}{4}-\int_0^1\frac{x}{1+x^2}dx.
\end{align*}
\end{proof}
\end{frame}
%
%% ---------------------------------138
%
%
\begin{frame}
\begin{proof}[Solution]
To evaluate this integral we use the substitution $t=1+x^2$. Then $dt=2x\,dx,$ so $x\,dx=dt/2$. When $x=0$, $t=1;$ when $x=1$, $t=2$; so
\begin{align*}
\int_0^1\frac{x}{1+x^2}dx&=\frac{1}{2}\int_1^2\frac{dt}{t}=\left.\frac{1}{2}\ln |t|\right]_1^2\\
&=\frac{1}{2}(\ln 2-\ln 1)=\frac{1}{2}\ln 2.
\end{align*}
Therefore
\[ \displaystyle \int_0^1 \tan ^{-1}x\,dx=\frac{\pi}{4}-\frac{\ln 2}{2}. \qedhere \]
\end{proof}
\end{frame}

\subsection{Trigonometric Integrals}
\begin{frame}{\insertsubsectionhead}
\begin{example}
Evaluate $\displaystyle \int \cos^3xdx.$
\end{example}
\begin{proof}[Solution]\let\qed\relax
We would like to use the Substitution Rule, but simply substituting $u=\cos x$ isn't helpful, since then $du=-\sin xdx$. 

In order to integrate powers of cosine, we would need an extra $\sin x$ factor. Thus here we separate one cosine factor and convert the remaining $\cos ^2x$ factor to an expression involving sine using the identity $\sin^2x+\cos^2x=1$:\[ \cos^3x=\cos^2x\cdot \cos x=(1-\sin^2x)\cos x. \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{proof}[Solution (cont.)]
We can then evaluate the integral by substituting $u=\sin x$, so $du=\cos x dx$, then	
\begin{align*}
\int \cos^3xdx&= \int\cos ^2x\cdot \cos xdx\\
&= \int (1-\sin^2x)\cos x dx\\
&=\int (1-u^2)du\\
&= u-\dfrac{1}{3}u^3+C\\
&=\sin x -\dfrac{1}{3}\sin ^3x+C. \qedhere
\end{align*}
\end{proof}
\end{frame}

\begin{frame}
\begin{example}
Evaluate $\displaystyle \int_{0}^{\pi}\sin^2 xdx$
\end{example}
\begin{proof}[Solution]
If we write $\sin^2x=1-\cos^2x$, the integral no simpler to evaluate. Using the half-angle formula for $\sin^2x$, however, we have
\begin{align*}
\int_{0}^{\pi}\sin^2xdx&= \dfrac{1}{2}\int_{0}^{\pi}(1-cos2x)dx=\left[\dfrac{1}{2}(x-\dfrac{1}{2}\sin 2x)\right]_0^\pi\\
&= \dfrac{1}{2}\left(\pi-\dfrac{1}{2}\sin 2\pi\right)-\dfrac{1}{2}\left(0-\dfrac{1}{2}\sin 0\right)=\dfrac{1}{2}\pi. \qedhere
\end{align*}
\end{proof}
\end{frame}