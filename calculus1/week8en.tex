\section{Applications of Differentiation}
\subsection{Indeterminate Forms and L'Hospital's Rule}
\subsubsection{L'Hospital's Rule}

\begin{frame}{\insertsubsectionhead}{\insertsubsubsectionhead}
\begin{block}{}
Suppose $f$ and $g$ are differentiable and  $g'(x)\neq 0$ near $a$ (except possibly at $a$). Suppose that
\begin{equation*}
    \lim\limits_{x\to a}f(x)=0 \quad \quad \text{and} \quad \quad \lim\limits_{x\to a}g(x)=0
\end{equation*}
or that 
\begin{equation*}
    \lim\limits_{x\to a}f(x)=\pm\infty \quad \quad \text{and} \quad \quad \lim\limits_{x\to a}g(x)=\pm\infty
\end{equation*}
(In other words, we have an indeterminate form of type $\frac{0}{0}$ or $\frac{\infty}{\infty}.$ )

Then 
\begin{equation*}
    \lim\limits_{x\to a}\frac{f(x)}{g(x)}=\lim\limits_{x\to a}\frac{f'(x)}{g'(x)}
\end{equation*}
if the limit on the right side exists (or is $\infty$ or $-\infty$).
\end{block}
\end{frame}
%
%
%
%
\begin{frame}
\begin{alertblock}{NOTE}
L’Hospital’s Rule is also valid for one-sided limits and for limits at infinity
or negative infinity. 
\\\vspace{.5cm}
That is, $ ``x\to a" $ can be replaced by any of the following symbols: $x\to a^{+}$, $x\to a^{-}$, $x\to\infty,$  $x\to -\infty$.{}

\end{alertblock}

\end{frame}
%
%
%
%
\begin{frame}
\begin{example}
Find $\displaystyle\lim\limits_{x\to 1}\frac{\ln x}{x-1}.$
\end{example}
\begin{proof}[Solution]
 Since
\[ \lim\limits_{x\to 1}\ln x=\ln 1=0\quad\text{and}\quad\lim\limits_{x\to 1}(x-1)=0 \]
we can apply l'Hospital's Rule:
\begin{align*}
\lim\limits_{x\to 1}\frac{\ln x}{x-1}
&=\lim\limits_{x\to 1}\frac{\displaystyle\frac{d}{dx}(\ln x)}{\displaystyle\frac{d}{dx}(x-1)}
=\lim\limits_{x\to 1}\frac{1/x}{1} \\
&= \lim\limits_{x\to 1}\frac{1}{x}=1 \qedhere
\end{align*}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{example}
Calculate $\displaystyle\lim_{x\to \infty}\frac{e^{x}}{x^{2}}.$
\end{example}
\begin{proof}[Solution]
 We have
\[ \lim\limits_{x\to \infty}e^{x}=\infty\quad\mbox{and}\quad\lim\limits_{x\to \infty}x^{2}=\infty \]
so l’Hospital’s Rule gives
\begin{equation*}
    \lim\limits_{x\to \infty}\frac{e^{x}}{x^{2}}=\lim\limits_{x\to \infty}\frac{e^{x}}{2x}.
\end{equation*}
Since $e^{x}\to \infty$ and $2x\to \infty$ as $x\to \infty,$ the limit on the right side is also indeterminate, but a second application of L'Hospital's Rule gives:
\begin{equation*}
    \lim\limits_{x\to \infty}\frac{e^{x}}{x^{2}}
    =\lim\limits_{x\to \infty}\frac{e^{x}}{2x}
    =\lim\limits_{x\to \infty}\frac{e^{x}}{2}=\infty \qedhere
\end{equation*}
\end{proof}
\end{frame}
%
%
\begin{frame}
\begin{example}
Calculate $\displaystyle\lim_{x\to \infty}\frac{\ln x}{\sqrt[3]{x}}.$
\end{example}
\begin{proof}[Solution]
Since $\ln x \to \infty$ and $\sqrt[3]{x}\to \infty$ as $x\to \infty$, L'Hospital's Rule applies:
\[ \lim\limits_{x\to \infty}\dfrac{\ln x}{\sqrt[3]{x}}=\lim\limits_{x\to \infty} \dfrac{\frac{1}{x}}{\frac{1}{3}x^{-2/3}} \]
Notice that the limit on the right side is now indeterminate of type $\frac{0}{0}$. But instead of applying L'Hospital's Rule a second time as we did in previous example, we simplify the expression and see that a second application is unnecessary:
\[ \lim\limits_{x\to \infty}\dfrac{\ln x}{\sqrt[3]{x}}=\lim\limits_{x\to \infty} \dfrac{\frac{1}{x}}{\frac{1}{3}x^{-2/3}}=\lim\limits_{x\to \infty}\dfrac{3}{\sqrt[3]{x}}=0 \qedhere \]
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
Find $\displaystyle \lim\limits_{x \to 0} \dfrac{\tan x-x}{x^3}.$
\end{example}
\begin{proof}[Solution]\let\qed\relax
Noting that both $\tan x-x\to 0$ and $x^3 \to 0$ as $x\to 0$, we use L'Hospital's Rule:\[ \lim\limits_{x \to 0} \dfrac{\tan x-x}{x^3}=\lim\limits_{x\to 0}\dfrac{\sec^2 x-1}{3x^2} \]
Since the limit on the right side is still indeterminate of type $\frac{0}{0}$, we apply L'Hospital's Rule again:
\[\lim\limits_{x\to 0}\dfrac{\sec^2 x-1}{3x^2}=\lim\limits_{x\to 0}\dfrac{2\sec^2 x\tan x}{6x}  \]
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution (cont.)]
Again both numerator and denominator approach, so a third application of L'Hospital's Rule is necessary. Putting together all three steps, we get
\begin{align*}
\lim\limits_{x \to 0} \dfrac{\tan x-x}{x^3}&= \lim\limits_{x\to 0}\dfrac{\sec^2 x-1}{3x^2}= \lim\limits_{x\to 0}\dfrac{2\sec^2 x\tan x}{6x}\\
&= \lim\limits_{x\to 0}\dfrac{4\sec ^2x\tan^2x+2\sec^4x }{6}=\dfrac{2}{6}=\dfrac{1}{3} \qedhere
\end{align*}
\end{proof}
\end{frame}


%
\begin{frame}
\begin{example}
Find 
$\displaystyle\lim\limits_{x\to \pi^{-}}\frac{\sin x}{1-\cos x}.$
\end{example}


\begin{proof}[Solution]\let\qed\relax
If we blindly attempted to use l’Hospital’s Rule, we would get
\begin{equation*}
    \lim\limits_{x\to \pi^{-}}\frac{\sin x}{1-\cos x}
    =\lim\limits_{x\to \pi^{-}}\frac{\cos x}{\sin x}=-\infty
\end{equation*}
This is \textbf{wrong!}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]
\[ \lim\limits_{x\to \pi^{-}}\frac{\sin x}{1-\cos x} \]
Although the numerator $\sin x\to 0$ as $x\to \pi^{-}$, notice that the denominator $1-\cos x$ does not approach $0,$ L'Hospital's Rule can't be applied here.

The required limit is, in fact, easy to find because the function is continuous and the denominator is nonzero at $\pi$:

\begin{equation*}
    \lim\limits_{x\to \pi^{-}}\frac{\sin x}{1-\cos x}
    =\frac{\sin \pi}{1-\cos \pi}
    =\frac{0}{1-(-1)}=0 \qedhere
\end{equation*}
\end{proof}
\end{frame}

%
%
%
\subsubsection{Indeterminate Products}
%
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{}
	If, $ \lim\limits_{x\to  a}f(x)=0 \quad \mbox{and} \quad \lim\limits_{x\to a}g(x)=\infty (\mbox{or} -\infty) $ then it isn't clear what the value of $\lim\limits_{x\to  a}f(x)g(x)$, if any, will be.
	
	This kind of limit is called an \textbf{indeterminate form of type} $\mathbf{0\cdot\infty}.$
	
	We can deal with it by writing the product $f\cdot g$ as a quotient:
	\begin{equation*}
	f\cdot g=\frac{f}{1/g} \quad \text{or} \quad f\cdot g=\frac{g}{1/f}
	\end{equation*}
	This converts the given limit into an indeterminate form of type $\frac{0}{0}$ or $\frac{\infty}{\infty}$ so that we can use L'Hospital's Rule.
\end{block}
\end{frame}
%
%
%
%
%
%
%
%
\begin{frame}
\begin{example}
Evaluate 
$\lim\limits_{x\to 0^{+}}x\ln x.$ 
\end{example}
\begin{proof}[Solution]
The given limit is indeterminate because, as $x\to  0^{+}$ the first factor $(x)$ approaches $0$ while the second factor $(\ln x)$ approaches $-\infty$.

Writing $x=1/(1/x),$ we have $1/x\to \infty$ as $x\to 0^{+}.$

So L'Hospital's Rule gives
\begin{align*}
\lim\limits_{x\to 0^{+}}x\ln x
&=\lim\limits_{x\to 0^{+}}\frac{\ln x}{1/x}
=\lim\limits_{x\to 0^{+}}\dfrac{\displaystyle\frac{1}{x}}{\displaystyle\frac{-1}{x^{2}}} \\
&=\lim\limits_{x\to 0^{+}}(-x)=0. \qedhere
\end{align*}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{alertblock}{Note}
In solving this example another possible option would have been to write
\begin{equation*}
    \lim\limits_{x\to 0^{+}}x\ln x=\lim\limits_{x\to 0^{+}}\frac{x}{1/\ln x}.
\end{equation*}
This gives an indeterminate form of the type $\frac{0}{0},$ but if we apply l’Hospital’s Rule we get a more complicated expression than the one we started with. In general, when we rewrite an indeterminate product, we try to choose the option that leads to the simpler limit.
\end{alertblock}
\end{frame}
%
%
%
\subsubsection{Indeterminate Differences}
%
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{}
	If $ \lim\limits_{x\to  a}f(x)=\infty\quad\mbox{and}\quad\lim\limits_{x\to  a}g(x)=\infty, $
	then the limit
	\begin{equation*}
	\lim\limits_{x\to  a}[f(x)-g(x)]
	\end{equation*}
	is called an \textbf{indeterminate form of type} $\mathbf{\infty-\infty}.$
	
	To find out, we try to convert the difference into a quotient (for instance, by using a common denominator or rationalization, or factoring out a common factor) so that we have an indeterminate form of type $\frac{0}{0}$ or $\frac{\infty}{\infty}$.
\end{block}
\end{frame}
%
%
%
%
\begin{frame}
\begin{example}
Compute$\lim\limits_{x\to (\pi/2)^{-}}(\sec x-\tan x).$
\end{example}
\begin{proof}[Solution]
First notice that $\sec x\to \infty$ and $\tan x\to \infty$ as $x\to (\pi/2)^{-},$ so the limit is indeterminate. Here we use a common denominator:
\begin{align*}
\lim\limits_{x\to (\pi/2)^{-}}(\sec x-\tan x)
&=\lim\limits_{x\to (\pi/2)^{-}}\left(\frac{1}{\cos x}-\frac{\sin x}{\cos x}\right) \\
&=\lim\limits_{x\to (\pi/2)^{-}}\frac{1-\sin x}{\cos x}\\
&=\lim\limits_{x\to (\pi/2)^{-}} \frac{-\cos x}{-\sin x}=0
\end{align*}
Note that the use of l’Hospital’s Rule is justified because $1-\sin x\to 0$ and $\cos x\to 0$ as $x\to (\pi/2)^{-}.$
\end{proof}
\end{frame}

%
%
\subsubsection{Indeterminate Powers}
%
%
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{}
Several indeterminate forms arise from the limit
\begin{equation*}
    \lim_{x\to  a}[f(x)]^{g(x)}.
\end{equation*}
\begin{description}
	\item[type $0^{0}$:] $\lim\limits_{x\to  a}f(x)=0$ and $\lim\limits_{x\to  a}g(x)=0 $\medskip
	
	\item[type $\infty^{0}$:] $\lim\limits_{x\to  a}f(x)=\infty$ and $\lim\limits_{x\to  a}g(x)=0$\medskip
	
	\item[type $1^{\infty}$:] $\lim\limits_{x\to  a}f(x)=1$ and $\lim\limits_{x\to  a}g(x)=\pm\infty $ 
\end{description}
\end{block}
\end{frame}


% % % % % % % % % % % % % % % % % % % % % % % % % %  ????????????????????????

\begin{frame}
Each of these three cases can be treated either by taking the natural logarithm:
\[\text{let } y=\left[f(x)\right]^{g(x)}\quad\text{then}\quad\ln y=g(x)\ln f(x) \]
or by writing the function as an exponential:
\[ \left[f(x)\right]^{g(x)}=e^{g(x)\ln f(x)} \]
(Recall that both of these methods were used in differentiating such functions.)
 
In either method we are led to the indeterminate product $g(x)\ln f(x)$, which is of type $0\cdot \infty$.
 \end{frame}
%
%
%
%
\begin{frame}
\begin{example}
Calculate $\displaystyle \lim_{x\rightarrow 0^+}(1+\sin 4x)^{\cot x}$
\end{example}

\begin{proof}[Solution]\let\qed\relax
First notice that as $x\rightarrow 0^+$, we have \[ 1+\sin 4x\rightarrow 1 \qquad\mbox{and}\qquad\cot x\rightarrow \infty \]
so the given limit is indeterminate. Let
\[ y=(1+\sin 4x)^{\cot x} \]
Then
\begin{align*}
\ln y=\ln [(1+\sin 4x)^{\cot x}]&=\cot x\cdot\ln (1+\sin 4x)\\
&= \frac{\ln (1+\sin 4x)}{\tan x}.
\end{align*}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]
\[ \lim_{x\rightarrow 0^+}\ln y =\lim_{x\rightarrow 0^+}\frac{\ln(1+\sin 4x)}{\tan x}\qquad \left(\frac{0}{0}\text{ indeterminate} \right) \]
so L'Hospital's Rule gives
\[ =\lim_{x\rightarrow 0^+}\frac{\frac{4\cos 4x}{1+\sin 4x}}{\sec ^2x}=4. \]
So far we have computed the limit of $\ln y$, but what we want is the limit of $y$. To find this we use the fact that $y=e^{\ln y}$:
\[ \lim_{x\rightarrow 0^+}(1+\sin 4x)^{\cot x}=\lim_{x\rightarrow 0^+}y=\lim_{x\rightarrow 0^+}e^{\ln y}=e^4 \qedhere \]
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{example}
Find $\displaystyle\lim_{x\rightarrow 0^+}x^x$.
\end{example}
\begin{proof}[Solution]
Notice that this limit is indeterminate since $0^x=0$ for any $x>0$ but  $x^0=1$ for any $x\neq 0$.

We could proceed by writing the function as an exponential:
\[ x^x=(e^{\ln x})^x=e^{x\ln x}. \]
Before we used L’Hospital’s Rule to show that
$\lim_{x\rightarrow 0^+ }x\ln x=0$
Therefore
\[ \lim_{x\rightarrow 0^+}x^x=\lim_{x\rightarrow 0^+}e^{x\ln x}=e^0=1. \qedhere \]
\end{proof}
\end{frame}
%
%
%
\subsubsection{Optimization Problems}
%
\begin{frame}{\insertsubsubsectionhead}
\begin{block}{Steps in Optimization Problems}
\begin{enumerate}
\item {\usebeamercolor[fg]{structure}Understand the Problem}. The first step is to read the problem carefully until it is clearly understood. Ask yourself: What is the unknown? What are the given quantities? What are the given conditions?
\item {\usebeamercolor[fg]{structure}Draw a Diagram}. In most problems it is useful to draw a diagram and identify the given and required quantities on the diagram.
\item {\usebeamercolor[fg]{structure}Introduce Notation}. Assign a symbol to the quantity that is to be maximized or minimized (let's call it $Q$ for now). Also select symbols for other unknown quantities and label the diagram with these symbols $(a,b,c,d,\ldots,x,y)$. It may help to use initials as suggestive symbols-for example,  $A$ for area, for $h$ height, $t$ for time.
\end{enumerate}
\end{block}
\end{frame}

\begin{frame}
\begin{block}{}
\begin{enumerate}\setcounter{enumi}{3}
\item Express $Q$ in terms of some of the other symbols from Step {\usebeamercolor[fg]{structure}3}.
\item If $Q$  has been expressed as a function of more than one variable in Step {\usebeamercolor[fg]{structure}4}, use the given information to find relationships (in the form of equations) among these variables. Then use these equations to eliminate all but one of the variables in the expression for $Q$. Thus, $Q$ will be expressed as a function of one variable, say, $Q=f(x)$. Write the domain of this function.
\item Find the absolute maximum or minimum value of $f$. In particular, if the domain of $f$ is a closed interval, then the Closed Interval Method  can be used.
\end{enumerate}
\end{block}
\end{frame}




\begin{frame}
\begin{example}
A farmer has $ 2400 \, \textrm{m} $  of fencing and wants to fence off a rectangular field that borders a straight river. He needs no fence along the river. What are the dimensions of the field that has the largest area?
\end{example}
\begin{proof}[Solution]\let\qed\relax
In order to get a feeling for what is happening in this problem, let's experiment with some special cases. Figure shows three possible ways of laying out the $ 2400 \, \textrm{m} $ of fencing.
\begin{figure}[h]
\includegraphics[page=308,clip,trim=21mm 113mm 21mm 134mm,scale=.66]{stewart-ccac.pdf}
{\tiny$\textrm{Area}=220\,000\,\mathrm{m}^{2}$\hspace{2cm}$\textrm{Area}=700\,000\,\mathrm{m}^{2}$\hspace{2cm}$\textrm{Area}=400\,000\,\mathrm{m}^{2}$}
\end{figure}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}

\begin{proof}[Solution (cont.)]\let\qed\relax
We see that when we try shallow, wide fields or deep, narrow fields, we get relatively small areas. It seems plausible that there is some intermediate configuration that produces the largest area. Figure illustrates the general case. We wish to maximize the area $A$ of the rectangle.
Let $x$ and $y$ be the depth and width of the rectangle (in meters).
\begin{figure}[h]
\includegraphics[page=308,scale=1,clip,trim=22mm 60mm 145mm 194mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
Then we express $A$ in terms of  $x$ and $y$ 
\[ A=xy \]
We want to express $A$ as a function of just one variable, so we eliminate $y$ by expressing it in terms of $x$.

To do this we use the given information that the total length of the fencing is $ 2400 \, \textrm{m} $. Thus
\[ 2x+y=2400. \]
From this equation we have $y=2400-2x$ which gives
\[ A=x(2400-2x)=2400x-2x^2 .\]
Note that $x\ge  0$ and $x\le  1200$ (otherwise  $A<0$).
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]
So the function that we wish to maximize is
\[ A(x)=2400 x-2x^2\qquad 0\le  x\le  1200 \]
The derivative is $A'(x)=2400-4x$, so to find the critical numbers we solve the equation
\[ 2400-4x=0 \]
which gives $x=600$. 

The maximum value of $A$ must occur either at this critical number or at an endpoint of the interval.

Since $A(0)=0$, $A(600)=720000$ and $A(1200)=0,$ the Closed Interval Method gives the maximum value as $A(600)=720000$.

[Alternatively, we could have observed that $A''(x)=-4<0$ for all $x$, so $A$ is always concave downward and the local maximum at $x=600$ must be an absolute maximum.]

Thus, the rectangular field should be $ 600 \, \textrm{m} $ deep and $ 1200 \, \textrm{m} $ wide.
\end{proof}
\end{frame}
%
\begin{frame}
\begin{example}
A cylindrical can is to be made to hold $1\, \textrm{L} $ of oil. Find the dimensions that will minimize the cost of the metal to manufacture the can.
\end{example}
\begin{proof}[Solution]\let\qed\relax
Draw the diagram as in Figure with height $ h $ and radius $ r $.
\begin{figure}[h]
\includegraphics[page=309,scale=0.7,clip,trim=21mm 167mm 168mm 79mm,scale=1.4]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
In order to minimize the cost of the metal, we minimize the
total surface area of the cylinder (top, bottom, and sides). From Figure we see that the sides are made from a rectangular sheet with dimensions $2\pi r$ and $h$.
   \begin{figure}[h]
   \includegraphics[page=309,scale=0.7,clip,trim=21mm 114mm 174mm 124mm,scale=1.6]{stewart-ccac.pdf}
   \includegraphics[page=309,scale=0.7,clip,trim=46mm 114mm 146mm 124mm,scale=1.6]{stewart-ccac.pdf}
   \end{figure}
\end{proof}
\end{frame}

%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
So the surface area is
\[ A=2\pi r^2+2\pi rh \]
To eliminate $h$ we use the fact that the volume is given as $1\,\textrm{L}$ which we take to be $1000\, \textrm{cm}^3$. Thus
\[ \pi r^2h=1000 \]
which gives
\[ h=\frac{1000}{(\pi r^2)}. \]
Substitution of this into the expression for $A$gives
\[ A=2\pi r^2+2\pi r\left(\frac{1000}{\pi r^2}\right)=2\pi r^2+\frac{2000}{r}. \]
\end{proof}
\end{frame}

%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
Therefore, the function that we want to minimize is
\[ A(r)=2\pi r^2+\frac{2000}{r}\qquad r>0. \]
To find the critical numbers, we differentiate
\[A'(r)=4\pi r-\frac{2000}{r^2}=\frac{4(\pi r^3-500)}{r^2}\]
Then  $A'(r)=0$ when $\pi r^3=500$.

So the only critical number is $r=\sqrt[3]{\frac{500}{\pi}}$.
\end{proof}
\end{frame}
%
%
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Since the domain of $A$ is $(0,\infty)$ we can't use the argument of previous example concerning endpoints.

But we can observe that $A'(r)<0$ for $r<\sqrt[3]{\frac{500}{\pi}}$ and $A'(r)>0$ for $r>\sqrt[3]{\frac{500}{\pi}}$, so $A$ is decreasing for all $ r $ to the left of the critical number and increasing for all $r$ to the right.

Thus, $r=\sqrt[3]{\frac{500}{\pi}}$ must give rise to an \textbf{absolute minimum}.
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
Alternatively, we could argue that $A(r)\rightarrow \infty$ as $r\rightarrow 0^+$ and  $A(r)\rightarrow \infty$ as $r\rightarrow \infty$, so there must be a minimum value of $A(r)$ which must occur at the critical number.
    \begin{figure}[h]
    \includegraphics[page=309,clip,trim=21mm 54mm 156mm 186mm,scale=1.2]{stewart-ccac.pdf}
    \end{figure}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]
The value of $h$ corresponding to $r=\sqrt[3]{\frac{500}{\pi}}$ 
\[ h=\frac{1000}{\pi r^2}=\frac{1000}{\pi (500/\pi)^{2/3}}=2\sqrt[3]{\frac{500}{\pi}}=2r. \]
Thus, to minimize the cost of the can, the radius should be $\sqrt[3]{\frac{500}{\pi}}\,\textrm{cm}$ and the height should be equal to twice the radius, namely, the diameter.
\end{proof}
\end{frame}

%
%
%
%
\begin{frame}
\begin{block}{First Derivative Test for Absolute Extreme Values}
Suppose that $c$ is a critical number of a continuous function $f$ defined on an interval.
\begin{itemize}
\item[(a)] If $f'(x)>0$ for all $x<c$ and $f'(x)<0$ for all $x>c$, then $f(c)$ is the absolute maximum value of $f$.
\item[(b)] If $f'(x)<0$ for all $x<c$ and $f'(x)>0$ for all $x>c$, then $f(c)$ is the absolute minimum value of $f$.
\end{itemize}
\end{block}
\end{frame}
%
\begin{frame}
\begin{example}
 Find the point on the parabola $y^2=2x$ that is closest to the point $(1,4)$.
\end{example}
\begin{proof}[Solution]\let\qed\relax
\begin{minipage}{.45\textwidth}
	The distance between the point $(x,y)$ and the point $(1,4)$ is
	\[ d=\sqrt{(x-1)^2+(y-4)^2}. \]
\end{minipage}
\begin{minipage}{.5\textwidth}
	\begin{figure}[h]
		\includegraphics[page=310,clip,scale=0.7,trim=21mm 72mm 151mm 162mm,scale=1.4]{stewart-ccac.pdf}
	\end{figure}
\end{minipage}
\end{proof}
\end{frame}
%
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
But if, $(x,y)$ lies on the parabola, then $x=y^2/2$ so the expression for $d$becomes
\[ d=\sqrt{\left(\frac{1}{2}y^2-1\right)^2+(y-4)^2}. \]
(Alternatively, we could have substituted $y=\sqrt{2x}$ to get $d$ in terms of $x$ alone.) 

Instead of minimizing $d$, we minimize its square:
\[ d^2=f(y)=\left(\frac{1}{2}y^2-1\right)^2+(y-4)^2. \]
(You should convince yourself that the minimum of $d$ occurs at the same point as the minimum of $d^2$, but $d^2$ is easier to work with.) 
\end{proof}
\end{frame}
%
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]
Differentiating, we obtain
\[ f'(y)=2\left(\frac{1}{2}y^2-1\right)y+2(y-4)=y^3-8 \]
so $f'(y)=0$ when $y=2$.

Observe that $f'(y)<0$ when $y<2$ and $f'(y)>0$ when $y>2$.

So by the First Derivative Test for Absolute Extreme Values, the absolute minimum occurs when $y=2$.

(Or we could simply say that because of the geometric nature of the problem, it’s obvious that there is a closest point but not a farthest point.)
The corresponding value of $x$ is $x=y^2/2=2$.

Thus, the point on $y^2=2x$ closest to $(1,4)$ is $(2,2)$. 
\end{proof}
\end{frame}
%
%
%
%

\begin{frame}
\begin{example}
Find the area of the largest rectangle that can be inscribed in a semicircle of radius $r$.
\end{example}
\vspace{-.2cm}
\begin{proof}[Solution]\let\qed\relax
Let's take the semicircle to be the upper half of the circle $x^2+y^2=r^2 $ with center the origin. Then the word inscribed means that the rectangle has two vertices on the semicircle and two vertices on the $x$-axis as shown in Figure.
\vspace{-.5cm}
\begin{figure}
\centering
\includegraphics[page=312,clip,scale=0.9,trim=21mm 191mm 146mm 55mm,scale=1.3]{stewart-ccac.pdf}
\end{figure}
\end{proof}
\end{frame}

\begin{frame}
\begin{proof}[Solution (cont.)]\let\qed\relax
Let $(x,y)$ be the vertex that lies in the first quadrant. Then th rectangle has sides of lengths $2x$ and $y$, so its area is
\[ A=2xy \]
To eliminate $y$ we use the fact that $(x,y)$ lies on the circle $x^2+y^2=r^2$ and so $y=\sqrt{r^2-x^2}$. Thus
\[ A=2x\sqrt{r^2-x^2} \]
The domain of this function is $0\le  x\le  r$.
\end{proof}
\end{frame}
%
%
\begin{frame}
\begin{proof}[Solution (cont.)]
 Its derivative is
\[ A'=2\sqrt{r^2-x^2}-\dfrac{2x^2}{\sqrt{r^2-x^2}}=\dfrac{2(r^2-2x^2)}{\sqrt{r^2-x^2}} \]
which is $0$ when $2x^2=r^2$, that is, $x=r/\sqrt{2}$ (since $x\ge  0$). this value of $x$ gives a maximum value of $A$ since $A(0)=0$ and $A(r)=0$. Therefore, the area of the largest inscribed rectangle is
\[ A\left(\dfrac{r}{\sqrt{2}}\right)=2\dfrac{r}{\sqrt{2}}\sqrt{r^2-\dfrac{r^2}{2}}=r^2. \qedhere \] 
\end{proof}
\end{frame}